//=../libs/jquery/dist/jquery.js
//=../libs/uikit.js
//=../libs/uikit-icons.js
//=../libs/jquery.elevatezoom.js

$(function () {
    var screenWidth = $(window).width();

    $(".my-foto").elevateZoom();

    //initiate the plugin and pass the id of the div containing gallery images
    // $("#img_01").elevateZoom({
    //     gallery: 'gal1',
    //     cursor: 'pointer',
    //     galleryActiveClass: 'active',
    //     imageCrossfade: true,
    //     loadingIcon: 'https://i.stack.imgur.com/h6viz.gif'
    // });

//pass the images to Fancybox
//     $("#img_01").bind("click", function (e) {
//         var ez = $('#img_01').data('elevateZoom');
//         $.fancybox(ez.getGalleryList());
//         return false;
//     });

    $("div.sub-categories")
        .mouseenter(function () {
            $(this).siblings('a').addClass('active-category');
        })
        .mouseleave(function () {
            $(this).siblings('a').removeClass('active-category');
        });

    $('.favourite').on('click', function () {
        event.stopPropagation();
        $(this).toggleClass('fav-active');
    });

    $('.basket-page, .favourite-page, .order-page, .room-page, .product-page, .stocks-page, .help-page, .search-result-page, .discounts-page').siblings('header').addClass('toggleCategory');

    if (screenWidth < 1200) {
        $('.main-page, .category-page').siblings('header').addClass('toggleCategory');
    }

    $('.toggleCategory .show-category').on('click', function () {
        $(this).siblings('.categories-list').toggle();
    });

    $(document).on('click', function () {
        $('.toggleCategory .categories-list').fadeOut(200);
    });

    $('.toggleCategory .categories-list, .toggleCategory .show-category').on('click', function (event) {
        event.stopPropagation();
    });

    $('.category-page').siblings('header').addClass('category-header');
    var categoryListHeight = $('.category-header .categories .uk-list').outerHeight();

    if (screenWidth < 1200) {
        $('.filters').css({
            'margin-top': 0
        });

        $('.categories > ul').attr({'class': 'categories-list accordion-categories', 'uk-accordion': ''});

        $('.categories > ul > li > div').attr({'class': 'uk-accordion-content'}).removeAttr('uk-dropdown');

        $('.categories > ul > li > a').attr({'class': 'uk-accordion-title'});

    } else {
        $('.filters').css({
            'margin-top': categoryListHeight + 30 + 'px'
        });
    }

    // $('.basket').on('click', function() {
    // 	event.stopPropagation();
    // 	$(this).toggleClass('basket-active');
    // });

    set_($('#p-amount'), 100);

    function set_(_this, max) {
        var block = _this.parent();

        block.find(".increase").click(function () {
            var currentVal = parseInt(_this.val());
            if (currentVal != NaN && (currentVal + 1) <= max) {
                _this.val(currentVal + 1);
            }
        });

        block.find(".decrease").click(function () {
            var currentVal = parseInt(_this.val());
            if (currentVal != NaN && currentVal != 0) {
                _this.val(currentVal - 1);
            }
        });
    }

    $('.delete-product').on('click', function () {
        $(this).closest('tr').fadeOut();
    });

    $('.sale-section a[href^="#"]').on('click', function (event) {
        var target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 500);
        }
    });

});







