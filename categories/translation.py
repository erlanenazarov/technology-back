from modeltranslation.translator import translator, TranslationOptions
from .models import Category


class CategoryTranslator(TranslationOptions):
    fields = ('title', 'features', 'description', 'notes')


translator.register(Category, CategoryTranslator)
