# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404

from client_app.utils import generate_view_params
from django.shortcuts import render

from product.models import Product
from promotions.models import Promotion
from django.db.models import Q
import operator

# Create your views here.


def promotions_list(request):
    products = Product.objects.filter(is_stock=True, is_active=True).all()
    params = {
        'promotions': Promotion.objects.filter(is_active=True),
        'products': products
    }
    params.update(generate_view_params(request))
    return render(request, 'app/promotions-list.html', params)


def promotions_single(request, slug):
    try:
        promotion = Promotion.objects.get(slug=slug)
    except:
        raise Http404

    suggestions = Promotion.objects.filter(reduce(operator.and_, (Q(title__icontains=q) for q in promotion.title.split()))).exclude(id=promotion.id)

    protocol = 'https://' if request.is_secure() else 'http://'
    site_url = protocol + request.get_host() + request.get_full_path()

    params = {
        'promotion': promotion,
        'suggestions': [x.product for x in suggestions],
        'link': site_url
    }
    params.update(generate_view_params(request))
    return render(request, 'app/promotion-single.html', params)
