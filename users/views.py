# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import random
import string
import threading

import datetime
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.template import loader
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from MyBrands import settings
from order.models import Order
from users.forms import LoginForm, RegisterForm, UserEditForm, ChangePasswordForm
from client_app.utils import generate_view_params, send_email_notification
from .utils import get_first_and_last_name
# Create your views here.
from users.models import User


def sign_out(request):
    logout(request)
    next = request.GET.get('next', None)
    response = redirect(next if next else '/')
    return response


def sign_in(request):
    _form = LoginForm()
    messages = None
    if request.POST:
        _form = LoginForm(request.POST)
        if _form.is_valid():
            email = _form.cleaned_data['email']
            password = _form.cleaned_data['password']

            user = authenticate(email=email, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    return JsonResponse(dict(
                        success=True,
                        messages=['Добро пожаловать, %s' % user.first_name],
                        operation='pageReload'
                    ))
                else:
                    return JsonResponse(dict(success=False, messages=['Пользователь не активен'], operation=None))
            else:
                return JsonResponse(dict(success=False, messages=['Логин или пароль введен неверно!'], operation=None))

    params = {
        'form': _form,
        'messages': messages
    }
    params.update(generate_view_params(request))
    return render(request, 'account/login.html', params)


def get_site_url(request):
    return request.get_host()


def sign_up(request):
    _form = RegisterForm()
    messages = None

    if request.POST:
        _form = RegisterForm(request.POST)
        is_success = False
        messages = list()
        if _form.is_valid():
            print("Form is valid! There is cleaned data:")
            print (_form.cleaned_data)

            try:
                user = User.objects.get(email=_form.cleaned_data['email'])
                messages.append('Эта почта уже используется!')
            except ObjectDoesNotExist:
                first_name, last_name = get_first_and_last_name(_form.cleaned_data['name'])

                user = User(
                    email=_form.cleaned_data['email'],
                    password=make_password(_form.cleaned_data['password2']),
                    is_active=False,
                    first_name=first_name,
                    last_name=last_name,
                    username='%s_%s_%s' % (first_name, last_name, str(datetime.datetime.microsecond))
                )
                user.save()
                token = default_token_generator.make_token(user)
                uid = urlsafe_base64_encode(force_bytes(user.pk))
                template = loader.get_template('account/partial/confirm_email.html')
                protocol = 'https://' if request.is_secure() else 'http://'
                contexts = {
                    'link': protocol + request.get_host() + reverse('users:users_confirm',
                                                               kwargs={'uidb64': uid, 'token': token})
                }
                response = template.render(contexts, request)
                print contexts
                thread = threading.Thread(target=send_email_notification, args=(
                    'Активация профиля | Mega Market',
                    response,
                    [user.email],

                ))
                is_success = True
                thread.start()
                messages.append('Мы отправили вам письмо с подтверждением на вашу почту!')

        else:
            messages.append('Упс что-то пошло не так! Пожалуйста проверьте правильность введенных данных')
            messages.append(_form.error_messages)

        return JsonResponse(dict(success=is_success, messages=messages, operation=None))

    params = {
        'form': _form,
        'messages': messages
    }
    params.update(generate_view_params(request))
    return render(request, 'account/register.html', params)


def confirm_email(request, uidb64, token):
    messages = list()
    if uidb64 is not None and token is not None:
        uid = urlsafe_base64_decode(uidb64)
        try:
            user = User.objects.get(pk=uid)
            if not user.is_active:
                if default_token_generator.check_token(user, token):
                    user.is_active = True
                    user.save()
                    login(request, user)
                    messages.append('Успех! Добро пожаловать, %s' % user.first_name)
                else:
                    messages.append('Неверная ссылка')
            else:
                messages.append('Ссылка устарела')
        except ObjectDoesNotExist:
            messages.append('Пользователь не найден...')
    else:
        messages.append('Нет данных чтобы проверить правильность ссылки!')

    params = {
        'messages': messages
    }
    params.update(generate_view_params(request))
    return render(request, 'account/activate.html', params)


@csrf_exempt
@login_required
def profile(request):
    user = User.objects.get(id=request.user.id)
    params = {
        'user': user,
        'form': UserEditForm(request.POST),
        'messages': None,
        'change_password_form': ChangePasswordForm(request.POST)
    }

    if request.POST:
        if params['form'].is_valid():
            params['messages'] = list()
            if params['form'].cleaned_data['phone'] != "" and params['form'].cleaned_data['phone'] != user.phone:
                user.phone = params['form'].cleaned_data['phone']
                params['messages'].append('Телефон был изменен')

            if params['form'].cleaned_data['address'] != "" and params['form'].cleaned_data['address'] != user.address:
                user.address = params['form'].cleaned_data['address']
                params['messages'].append('Адрес был изменен')

            if params['form'].cleaned_data['name'] != "":
                first_name, last_name = get_first_and_last_name(params['form'].cleaned_data['name'])
                if first_name != user.first_name or last_name != user.last_name:
                    user.first_name = first_name
                    user.last_name = last_name
                    user.username = '%s_%s_%s' % (first_name, last_name, str(datetime.datetime.microsecond))
                    params['messages'].append('Имя было изменено')

            if params['form'].cleaned_data['email'] != user.email:
                user.changing_email = params['form'].cleaned_data['email']

                code = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
                user.changing_email_code = code
                link_param = urlsafe_base64_encode(code)

                protocol = 'https://' if request.is_secure() else 'http://'

                template = loader.get_template('account/partial/confirm_email.html')
                link = protocol + request.get_host() + reverse('users:change_email', kwargs={'code': link_param})

                response = template.render(dict(link=link), request)

                thread = threading.Thread(
                    target=send_email_notification,
                    args=(
                        'Подтвердите свою новую почту | %s' % settings.SITE_TITLE,
                        response,
                        list(params['form'].cleaned_data['email'])
                    )
                )
                thread.start()

                params['messages'].append('Мы отправили на %s письмо с подтверждением, пожалуйста проверьте свою почту!' % params['form'].cleaned_data['email'])

            user.save()

    params.update(generate_view_params(request))
    return render(request, 'account/profile.html', params)


@login_required
def my_orders(request):
    user = request.user
    orders = Order.objects.filter(user=user)

    params = {
        'orders': orders.all()
    }
    params.update(generate_view_params(request))
    return render(request, 'account/orders.html', params)


@require_POST
def change_password(request):
    user = User.objects.get(id=request.user.id)
    form = ChangePasswordForm(request.POST)

    if form.is_valid():
        auth = authenticate(email=user.email, password=form.cleaned_data['old_password'])
        if auth:
            if form.cleaned_data['new_password'] == form.cleaned_data['re_new_password']:
                user.password = make_password(form.cleaned_data['re_new_password'])
                user.save()
                login(request, user)
                return JsonResponse(dict(success=True, message='Пароль изменен!'))

            return JsonResponse(dict(success=False, message='Новые пароли не совпадают!'))

        return JsonResponse(dict(success=False, message='Старый пароль введен не верно!'))

    return JsonResponse(dict(success=False, message='Проверьте правильность набора полей!'))


@login_required
def change_email(request, code):
    _code = urlsafe_base64_decode(code)
    user = User.objects.get(id=request.user.id)

    params = {

    }

    if _code == user.changing_email_code:
        user.email = user.changing_email
        params['message'] = dict(success=True, message='Ваш E-Mail сменен!')
        user.changing_email = ""
        user.changing_email_code = ""
        user.save()
        login(request, user)
    else:
        params['message'] = dict(success=False, message='Код не совпадает или ссылка уже не действительна')

    params.update(generate_view_params(request))
    return render(request, 'account/change_email_result.html', params)