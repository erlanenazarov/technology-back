

def get_first_and_last_name(full_name):
    first_name = ''
    last_name = ''
    name = full_name

    if ' ' in name:
        name = name.split(' ')
        if len(name) > 2:
            first_name = name[1] + ' ' + name[2]
            last_name = name[0]
        elif len(name) == 2:
            first_name = name[0]
            last_name = name[1]
    else:
        first_name = name

    return (first_name, last_name)