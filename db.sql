--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: categories_brand; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories_brand (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    thumbnail character varying(100),
    is_active boolean NOT NULL,
    link character varying(255)
);


ALTER TABLE public.categories_brand OWNER TO postgres;

--
-- Name: categories_brand_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categories_brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_brand_id_seq OWNER TO postgres;

--
-- Name: categories_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categories_brand_id_seq OWNED BY public.categories_brand.id;


--
-- Name: categories_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories_category (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    description text,
    thumbnail character varying(100),
    is_active boolean NOT NULL,
    parent_category_id integer,
    seo_thumbnail character varying(100),
    features text,
    notes text,
    description_kg text,
    features_kg text,
    notes_kg text,
    title_kg character varying(255),
    description_ru text,
    features_ru text,
    notes_ru text,
    title_ru character varying(255)
);


ALTER TABLE public.categories_category OWNER TO postgres;

--
-- Name: categories_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categories_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_category_id_seq OWNER TO postgres;

--
-- Name: categories_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categories_category_id_seq OWNED BY public.categories_category.id;


--
-- Name: categories_subcategory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories_subcategory (
    id integer NOT NULL,
    title character varying(255) NOT NULL
);


ALTER TABLE public.categories_subcategory OWNER TO postgres;

--
-- Name: categories_subcategory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categories_subcategory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_subcategory_id_seq OWNER TO postgres;

--
-- Name: categories_subcategory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categories_subcategory_id_seq OWNED BY public.categories_subcategory.id;


--
-- Name: client_app_about; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_about (
    id integer NOT NULL,
    body text NOT NULL
);


ALTER TABLE public.client_app_about OWNER TO postgres;

--
-- Name: client_app_about_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_about_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_about_id_seq OWNER TO postgres;

--
-- Name: client_app_about_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_about_id_seq OWNED BY public.client_app_about.id;


--
-- Name: client_app_aboutinnumbers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_aboutinnumbers (
    id integer NOT NULL,
    key character varying(255) NOT NULL,
    number character varying(255) NOT NULL
);


ALTER TABLE public.client_app_aboutinnumbers OWNER TO postgres;

--
-- Name: client_app_aboutinnumbers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_aboutinnumbers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_aboutinnumbers_id_seq OWNER TO postgres;

--
-- Name: client_app_aboutinnumbers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_aboutinnumbers_id_seq OWNED BY public.client_app_aboutinnumbers.id;


--
-- Name: client_app_adminemails; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_adminemails (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE public.client_app_adminemails OWNER TO postgres;

--
-- Name: client_app_adminemails_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_adminemails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_adminemails_id_seq OWNER TO postgres;

--
-- Name: client_app_adminemails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_adminemails_id_seq OWNED BY public.client_app_adminemails.id;


--
-- Name: client_app_affiliatefeedback; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_affiliatefeedback (
    id integer NOT NULL,
    is_read boolean NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    phone_number character varying(255) NOT NULL,
    affiliate_id integer NOT NULL,
    created_at date
);


ALTER TABLE public.client_app_affiliatefeedback OWNER TO postgres;

--
-- Name: client_app_affiliatefeedback_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_affiliatefeedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_affiliatefeedback_id_seq OWNER TO postgres;

--
-- Name: client_app_affiliatefeedback_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_affiliatefeedback_id_seq OWNED BY public.client_app_affiliatefeedback.id;


--
-- Name: client_app_contact; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_contact (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    address character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    phone character varying(255),
    location character varying(42) NOT NULL
);


ALTER TABLE public.client_app_contact OWNER TO postgres;

--
-- Name: client_app_contact_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_contact_id_seq OWNER TO postgres;

--
-- Name: client_app_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_contact_id_seq OWNED BY public.client_app_contact.id;


--
-- Name: client_app_custompages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_custompages (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    content text NOT NULL,
    slug character varying(255)
);


ALTER TABLE public.client_app_custompages OWNER TO postgres;

--
-- Name: client_app_custompages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_custompages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_custompages_id_seq OWNER TO postgres;

--
-- Name: client_app_custompages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_custompages_id_seq OWNED BY public.client_app_custompages.id;


--
-- Name: client_app_datenschutz; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_datenschutz (
    id integer NOT NULL,
    body text NOT NULL
);


ALTER TABLE public.client_app_datenschutz OWNER TO postgres;

--
-- Name: client_app_datenschutz_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_datenschutz_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_datenschutz_id_seq OWNER TO postgres;

--
-- Name: client_app_datenschutz_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_datenschutz_id_seq OWNED BY public.client_app_datenschutz.id;


--
-- Name: client_app_delivery; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_delivery (
    id integer NOT NULL,
    key character varying(255) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.client_app_delivery OWNER TO postgres;

--
-- Name: client_app_delivery_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_delivery_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_delivery_id_seq OWNER TO postgres;

--
-- Name: client_app_delivery_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_delivery_id_seq OWNED BY public.client_app_delivery.id;


--
-- Name: client_app_feedback; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_feedback (
    id integer NOT NULL,
    is_read boolean NOT NULL,
    first_name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    phone_number character varying(255) NOT NULL,
    last_name character varying(255),
    created_at date,
    message text
);


ALTER TABLE public.client_app_feedback OWNER TO postgres;

--
-- Name: client_app_feedback_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_feedback_id_seq OWNER TO postgres;

--
-- Name: client_app_feedback_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_feedback_id_seq OWNED BY public.client_app_feedback.id;


--
-- Name: client_app_manufacturers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_manufacturers (
    id integer NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.client_app_manufacturers OWNER TO postgres;

--
-- Name: client_app_manufacturers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_manufacturers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_manufacturers_id_seq OWNER TO postgres;

--
-- Name: client_app_manufacturers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_manufacturers_id_seq OWNED BY public.client_app_manufacturers.id;


--
-- Name: client_app_ourteam; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_ourteam (
    id integer NOT NULL,
    image character varying(100) NOT NULL,
    profession character varying(255) NOT NULL,
    full_name character varying(255) NOT NULL
);


ALTER TABLE public.client_app_ourteam OWNER TO postgres;

--
-- Name: client_app_ourteam_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_ourteam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_ourteam_id_seq OWNER TO postgres;

--
-- Name: client_app_ourteam_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_ourteam_id_seq OWNED BY public.client_app_ourteam.id;


--
-- Name: client_app_repairtips; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_repairtips (
    id integer NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.client_app_repairtips OWNER TO postgres;

--
-- Name: client_app_repairtips_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_repairtips_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_repairtips_id_seq OWNER TO postgres;

--
-- Name: client_app_repairtips_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_repairtips_id_seq OWNED BY public.client_app_repairtips.id;


--
-- Name: client_app_returnpolicy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_returnpolicy (
    id integer NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.client_app_returnpolicy OWNER TO postgres;

--
-- Name: client_app_returnpolicy_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_returnpolicy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_returnpolicy_id_seq OWNER TO postgres;

--
-- Name: client_app_returnpolicy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_returnpolicy_id_seq OWNED BY public.client_app_returnpolicy.id;


--
-- Name: client_app_reviews; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_reviews (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    message text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE public.client_app_reviews OWNER TO postgres;

--
-- Name: client_app_reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_reviews_id_seq OWNER TO postgres;

--
-- Name: client_app_reviews_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_reviews_id_seq OWNED BY public.client_app_reviews.id;


--
-- Name: client_app_seotext; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_seotext (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    text text NOT NULL
);


ALTER TABLE public.client_app_seotext OWNER TO postgres;

--
-- Name: client_app_seotext_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_seotext_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_seotext_id_seq OWNER TO postgres;

--
-- Name: client_app_seotext_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_seotext_id_seq OWNED BY public.client_app_seotext.id;


--
-- Name: client_app_sitesettings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_sitesettings (
    id integer NOT NULL,
    phone character varying(255) NOT NULL,
    address character varying(255),
    facebook character varying(200),
    instagram character varying(200),
    telegram character varying(200),
    meta_description text,
    meta_key_words text
);


ALTER TABLE public.client_app_sitesettings OWNER TO postgres;

--
-- Name: client_app_sitesettings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_sitesettings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_sitesettings_id_seq OWNER TO postgres;

--
-- Name: client_app_sitesettings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_sitesettings_id_seq OWNED BY public.client_app_sitesettings.id;


--
-- Name: client_app_sitestrings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_sitestrings (
    id integer NOT NULL,
    nav_services character varying(255) NOT NULL,
    nav_works character varying(255) NOT NULL,
    nav_products character varying(255) NOT NULL,
    nav_about character varying(255) NOT NULL,
    nav_contacts character varying(255) NOT NULL,
    our_services character varying(255) NOT NULL,
    our_works character varying(255) NOT NULL,
    our_production character varying(255) NOT NULL,
    posts character varying(255) NOT NULL
);


ALTER TABLE public.client_app_sitestrings OWNER TO postgres;

--
-- Name: client_app_sitestrings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_sitestrings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_sitestrings_id_seq OWNER TO postgres;

--
-- Name: client_app_sitestrings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_sitestrings_id_seq OWNED BY public.client_app_sitestrings.id;


--
-- Name: client_app_slider; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_slider (
    id integer NOT NULL,
    image character varying(100) NOT NULL,
    link character varying(255),
    is_active boolean NOT NULL,
    short_description character varying(255),
    title character varying(255),
    is_sale boolean NOT NULL,
    font_color character varying(18)
);


ALTER TABLE public.client_app_slider OWNER TO postgres;

--
-- Name: client_app_slider_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_slider_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_slider_id_seq OWNER TO postgres;

--
-- Name: client_app_slider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_slider_id_seq OWNED BY public.client_app_slider.id;


--
-- Name: client_app_subscribers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_subscribers (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE public.client_app_subscribers OWNER TO postgres;

--
-- Name: client_app_subscribers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_subscribers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_subscribers_id_seq OWNER TO postgres;

--
-- Name: client_app_subscribers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_subscribers_id_seq OWNED BY public.client_app_subscribers.id;


--
-- Name: client_app_termsofsaleofgoods; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_app_termsofsaleofgoods (
    id integer NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.client_app_termsofsaleofgoods OWNER TO postgres;

--
-- Name: client_app_termsofsaleofgoods_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_app_termsofsaleofgoods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_app_termsofsaleofgoods_id_seq OWNER TO postgres;

--
-- Name: client_app_termsofsaleofgoods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_app_termsofsaleofgoods_id_seq OWNED BY public.client_app_termsofsaleofgoods.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: jet_bookmark; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jet_bookmark (
    id integer NOT NULL,
    url character varying(200) NOT NULL,
    title character varying(255) NOT NULL,
    "user" integer NOT NULL,
    date_add timestamp with time zone NOT NULL,
    CONSTRAINT jet_bookmark_user_check CHECK (("user" >= 0))
);


ALTER TABLE public.jet_bookmark OWNER TO postgres;

--
-- Name: jet_bookmark_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jet_bookmark_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jet_bookmark_id_seq OWNER TO postgres;

--
-- Name: jet_bookmark_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jet_bookmark_id_seq OWNED BY public.jet_bookmark.id;


--
-- Name: jet_pinnedapplication; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jet_pinnedapplication (
    id integer NOT NULL,
    app_label character varying(255) NOT NULL,
    "user" integer NOT NULL,
    date_add timestamp with time zone NOT NULL,
    CONSTRAINT jet_pinnedapplication_user_check CHECK (("user" >= 0))
);


ALTER TABLE public.jet_pinnedapplication OWNER TO postgres;

--
-- Name: jet_pinnedapplication_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jet_pinnedapplication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jet_pinnedapplication_id_seq OWNER TO postgres;

--
-- Name: jet_pinnedapplication_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jet_pinnedapplication_id_seq OWNED BY public.jet_pinnedapplication.id;


--
-- Name: news_post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.news_post (
    id integer NOT NULL,
    is_active boolean NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    preview character varying(100),
    content text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    short_description text,
    content_kg text,
    short_description_kg text,
    title_kg character varying(255),
    content_ru text,
    short_description_ru text,
    title_ru character varying(255)
);


ALTER TABLE public.news_post OWNER TO postgres;

--
-- Name: news_post_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_post_id_seq OWNER TO postgres;

--
-- Name: news_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.news_post_id_seq OWNED BY public.news_post.id;


--
-- Name: news_postgalleryimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.news_postgalleryimage (
    id integer NOT NULL,
    image character varying(100) NOT NULL,
    post_id integer NOT NULL
);


ALTER TABLE public.news_postgalleryimage OWNER TO postgres;

--
-- Name: news_postgalleryimage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_postgalleryimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_postgalleryimage_id_seq OWNER TO postgres;

--
-- Name: news_postgalleryimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.news_postgalleryimage_id_seq OWNED BY public.news_postgalleryimage.id;


--
-- Name: order_order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_order (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    is_ordered boolean NOT NULL,
    email character varying(255),
    name character varying(255),
    phone character varying(255),
    address character varying(1000),
    comment text,
    user_id integer,
    is_delivered boolean NOT NULL
);


ALTER TABLE public.order_order OWNER TO postgres;

--
-- Name: order_order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.order_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_order_id_seq OWNER TO postgres;

--
-- Name: order_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.order_order_id_seq OWNED BY public.order_order.id;


--
-- Name: order_order_order_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_order_order_items (
    id integer NOT NULL,
    order_id integer NOT NULL,
    orderrow_id integer NOT NULL
);


ALTER TABLE public.order_order_order_items OWNER TO postgres;

--
-- Name: order_order_order_items_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.order_order_order_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_order_order_items_id_seq OWNER TO postgres;

--
-- Name: order_order_order_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.order_order_order_items_id_seq OWNED BY public.order_order_order_items.id;


--
-- Name: order_orderrow; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_orderrow (
    id integer NOT NULL,
    count integer NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE public.order_orderrow OWNER TO postgres;

--
-- Name: order_orderrow_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.order_orderrow_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_orderrow_id_seq OWNER TO postgres;

--
-- Name: order_orderrow_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.order_orderrow_id_seq OWNED BY public.order_orderrow.id;


--
-- Name: product_allproductproperties; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_allproductproperties (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    title_kg character varying(255),
    title_ru character varying(255)
);


ALTER TABLE public.product_allproductproperties OWNER TO postgres;

--
-- Name: product_allproductproperties_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_allproductproperties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_allproductproperties_id_seq OWNER TO postgres;

--
-- Name: product_allproductproperties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_allproductproperties_id_seq OWNED BY public.product_allproductproperties.id;


--
-- Name: product_downloaddata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_downloaddata (
    id integer NOT NULL,
    mode character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.product_downloaddata OWNER TO postgres;

--
-- Name: product_datadownloaddata_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_datadownloaddata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_datadownloaddata_id_seq OWNER TO postgres;

--
-- Name: product_datadownloaddata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_datadownloaddata_id_seq OWNED BY public.product_downloaddata.id;


--
-- Name: product_product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_product (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    description text,
    is_active boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    brand_id integer,
    category_id integer,
    main_image character varying(100),
    article character varying(255),
    description_kg text,
    title_kg character varying(255),
    description_ru text,
    title_ru character varying(255)
);


ALTER TABLE public.product_product OWNER TO postgres;

--
-- Name: product_product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_product_id_seq OWNER TO postgres;

--
-- Name: product_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_product_id_seq OWNED BY public.product_product.id;


--
-- Name: product_productcolor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_productcolor (
    id integer NOT NULL,
    color character varying(255)
);


ALTER TABLE public.product_productcolor OWNER TO postgres;

--
-- Name: product_productcolor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_productcolor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productcolor_id_seq OWNER TO postgres;

--
-- Name: product_productcolor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_productcolor_id_seq OWNED BY public.product_productcolor.id;


--
-- Name: product_productcolor_size; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_productcolor_size (
    id integer NOT NULL,
    productcolor_id integer NOT NULL,
    productsizesettings_id integer NOT NULL
);


ALTER TABLE public.product_productcolor_size OWNER TO postgres;

--
-- Name: product_productcolor_size_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_productcolor_size_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productcolor_size_id_seq OWNER TO postgres;

--
-- Name: product_productcolor_size_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_productcolor_size_id_seq OWNED BY public.product_productcolor_size.id;


--
-- Name: product_productimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_productimage (
    id integer NOT NULL,
    image character varying(100) NOT NULL,
    product_id integer
);


ALTER TABLE public.product_productimage OWNER TO postgres;

--
-- Name: product_productimage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_productimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productimage_id_seq OWNER TO postgres;

--
-- Name: product_productimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_productimage_id_seq OWNED BY public.product_productimage.id;


--
-- Name: product_productproperty; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_productproperty (
    id integer NOT NULL,
    key_id integer,
    value character varying(10000) NOT NULL,
    product_id integer,
    value_type character varying(255),
    prop_tip text,
    value_kg character varying(10000),
    value_ru character varying(10000)
);


ALTER TABLE public.product_productproperty OWNER TO postgres;

--
-- Name: product_productproperty_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_productproperty_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productproperty_id_seq OWNER TO postgres;

--
-- Name: product_productproperty_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_productproperty_id_seq OWNED BY public.product_productproperty.id;


--
-- Name: product_productsize; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_productsize (
    id integer NOT NULL,
    sort_order integer NOT NULL,
    title character varying(255) NOT NULL,
    category_id integer NOT NULL,
    sub_category_id integer,
    CONSTRAINT product_productsize_sort_order_check CHECK ((sort_order >= 0))
);


ALTER TABLE public.product_productsize OWNER TO postgres;

--
-- Name: product_productsize_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_productsize_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productsize_id_seq OWNER TO postgres;

--
-- Name: product_productsize_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_productsize_id_seq OWNED BY public.product_productsize.id;


--
-- Name: product_productsizesettings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_productsizesettings (
    id integer NOT NULL,
    count integer NOT NULL,
    size_id integer,
    CONSTRAINT product_productsizesettings_count_check CHECK ((count >= 0))
);


ALTER TABLE public.product_productsizesettings OWNER TO postgres;

--
-- Name: product_productsizesettings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_productsizesettings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productsizesettings_id_seq OWNER TO postgres;

--
-- Name: product_productsizesettings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_productsizesettings_id_seq OWNED BY public.product_productsizesettings.id;


--
-- Name: promotions_promotion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.promotions_promotion (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text NOT NULL,
    expires_date date,
    product_id integer NOT NULL,
    slug character varying(255),
    is_active boolean NOT NULL
);


ALTER TABLE public.promotions_promotion OWNER TO postgres;

--
-- Name: promotions_promotion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.promotions_promotion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.promotions_promotion_id_seq OWNER TO postgres;

--
-- Name: promotions_promotion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.promotions_promotion_id_seq OWNED BY public.promotions_promotion.id;


--
-- Name: users_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(255),
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL,
    address character varying(1000),
    phone character varying(255),
    changing_email character varying(1000),
    changing_email_code character varying(1000)
);


ALTER TABLE public.users_user OWNER TO postgres;

--
-- Name: users_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.users_user_groups OWNER TO postgres;

--
-- Name: users_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_groups_id_seq OWNER TO postgres;

--
-- Name: users_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_user_groups_id_seq OWNED BY public.users_user_groups.id;


--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_id_seq OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users_user.id;


--
-- Name: users_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.users_user_user_permissions OWNER TO postgres;

--
-- Name: users_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: users_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_user_user_permissions_id_seq OWNED BY public.users_user_user_permissions.id;


--
-- Name: users_userimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_userimage (
    id integer NOT NULL,
    avatar character varying(100) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.users_userimage OWNER TO postgres;

--
-- Name: users_userimage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_userimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_userimage_id_seq OWNER TO postgres;

--
-- Name: users_userimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_userimage_id_seq OWNED BY public.users_userimage.id;


--
-- Name: works_donework; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.works_donework (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    short_description character varying(255) NOT NULL,
    customer character varying(255) NOT NULL,
    is_active boolean NOT NULL,
    slug character varying(255) NOT NULL,
    created_at date,
    address character varying(255),
    description text,
    description_kg text,
    short_description_kg character varying(255),
    title_kg character varying(255),
    description_ru text,
    short_description_ru character varying(255),
    title_ru character varying(255),
    customer_link character varying(255),
    year_of_exec character varying(255)
);


ALTER TABLE public.works_donework OWNER TO postgres;

--
-- Name: works_donework_assigned_products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.works_donework_assigned_products (
    id integer NOT NULL,
    donework_id integer NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE public.works_donework_assigned_products OWNER TO postgres;

--
-- Name: works_donework_assigned_products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.works_donework_assigned_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.works_donework_assigned_products_id_seq OWNER TO postgres;

--
-- Name: works_donework_assigned_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.works_donework_assigned_products_id_seq OWNED BY public.works_donework_assigned_products.id;


--
-- Name: works_donework_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.works_donework_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.works_donework_id_seq OWNER TO postgres;

--
-- Name: works_donework_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.works_donework_id_seq OWNED BY public.works_donework.id;


--
-- Name: works_donework_service; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.works_donework_service (
    id integer NOT NULL,
    donework_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.works_donework_service OWNER TO postgres;

--
-- Name: works_donework_service_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.works_donework_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.works_donework_service_id_seq OWNER TO postgres;

--
-- Name: works_donework_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.works_donework_service_id_seq OWNED BY public.works_donework_service.id;


--
-- Name: works_workgallery; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.works_workgallery (
    id integer NOT NULL,
    main_image character varying(100) NOT NULL,
    done_project_id integer NOT NULL
);


ALTER TABLE public.works_workgallery OWNER TO postgres;

--
-- Name: works_workgallery_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.works_workgallery_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.works_workgallery_id_seq OWNER TO postgres;

--
-- Name: works_workgallery_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.works_workgallery_id_seq OWNED BY public.works_workgallery.id;


--
-- Name: works_workgallery_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.works_workgallery_images (
    id integer NOT NULL,
    workgallery_id integer NOT NULL,
    workimage_id integer NOT NULL
);


ALTER TABLE public.works_workgallery_images OWNER TO postgres;

--
-- Name: works_workgallery_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.works_workgallery_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.works_workgallery_images_id_seq OWNER TO postgres;

--
-- Name: works_workgallery_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.works_workgallery_images_id_seq OWNED BY public.works_workgallery_images.id;


--
-- Name: works_workimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.works_workimage (
    id integer NOT NULL,
    image character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.works_workimage OWNER TO postgres;

--
-- Name: works_workimage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.works_workimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.works_workimage_id_seq OWNER TO postgres;

--
-- Name: works_workimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.works_workimage_id_seq OWNED BY public.works_workimage.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_brand ALTER COLUMN id SET DEFAULT nextval('public.categories_brand_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_category ALTER COLUMN id SET DEFAULT nextval('public.categories_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_subcategory ALTER COLUMN id SET DEFAULT nextval('public.categories_subcategory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_about ALTER COLUMN id SET DEFAULT nextval('public.client_app_about_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_aboutinnumbers ALTER COLUMN id SET DEFAULT nextval('public.client_app_aboutinnumbers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_adminemails ALTER COLUMN id SET DEFAULT nextval('public.client_app_adminemails_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_affiliatefeedback ALTER COLUMN id SET DEFAULT nextval('public.client_app_affiliatefeedback_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_contact ALTER COLUMN id SET DEFAULT nextval('public.client_app_contact_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_custompages ALTER COLUMN id SET DEFAULT nextval('public.client_app_custompages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_datenschutz ALTER COLUMN id SET DEFAULT nextval('public.client_app_datenschutz_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_delivery ALTER COLUMN id SET DEFAULT nextval('public.client_app_delivery_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_feedback ALTER COLUMN id SET DEFAULT nextval('public.client_app_feedback_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_manufacturers ALTER COLUMN id SET DEFAULT nextval('public.client_app_manufacturers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_ourteam ALTER COLUMN id SET DEFAULT nextval('public.client_app_ourteam_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_repairtips ALTER COLUMN id SET DEFAULT nextval('public.client_app_repairtips_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_returnpolicy ALTER COLUMN id SET DEFAULT nextval('public.client_app_returnpolicy_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_reviews ALTER COLUMN id SET DEFAULT nextval('public.client_app_reviews_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_seotext ALTER COLUMN id SET DEFAULT nextval('public.client_app_seotext_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_sitesettings ALTER COLUMN id SET DEFAULT nextval('public.client_app_sitesettings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_sitestrings ALTER COLUMN id SET DEFAULT nextval('public.client_app_sitestrings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_slider ALTER COLUMN id SET DEFAULT nextval('public.client_app_slider_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_subscribers ALTER COLUMN id SET DEFAULT nextval('public.client_app_subscribers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_termsofsaleofgoods ALTER COLUMN id SET DEFAULT nextval('public.client_app_termsofsaleofgoods_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jet_bookmark ALTER COLUMN id SET DEFAULT nextval('public.jet_bookmark_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jet_pinnedapplication ALTER COLUMN id SET DEFAULT nextval('public.jet_pinnedapplication_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_post ALTER COLUMN id SET DEFAULT nextval('public.news_post_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_postgalleryimage ALTER COLUMN id SET DEFAULT nextval('public.news_postgalleryimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_order ALTER COLUMN id SET DEFAULT nextval('public.order_order_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_order_order_items ALTER COLUMN id SET DEFAULT nextval('public.order_order_order_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_orderrow ALTER COLUMN id SET DEFAULT nextval('public.order_orderrow_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_allproductproperties ALTER COLUMN id SET DEFAULT nextval('public.product_allproductproperties_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_downloaddata ALTER COLUMN id SET DEFAULT nextval('public.product_datadownloaddata_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_product ALTER COLUMN id SET DEFAULT nextval('public.product_product_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productcolor ALTER COLUMN id SET DEFAULT nextval('public.product_productcolor_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productcolor_size ALTER COLUMN id SET DEFAULT nextval('public.product_productcolor_size_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productimage ALTER COLUMN id SET DEFAULT nextval('public.product_productimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productproperty ALTER COLUMN id SET DEFAULT nextval('public.product_productproperty_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productsize ALTER COLUMN id SET DEFAULT nextval('public.product_productsize_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productsizesettings ALTER COLUMN id SET DEFAULT nextval('public.product_productsizesettings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promotions_promotion ALTER COLUMN id SET DEFAULT nextval('public.promotions_promotion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user ALTER COLUMN id SET DEFAULT nextval('public.users_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_groups ALTER COLUMN id SET DEFAULT nextval('public.users_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.users_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_userimage ALTER COLUMN id SET DEFAULT nextval('public.users_userimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework ALTER COLUMN id SET DEFAULT nextval('public.works_donework_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework_assigned_products ALTER COLUMN id SET DEFAULT nextval('public.works_donework_assigned_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework_service ALTER COLUMN id SET DEFAULT nextval('public.works_donework_service_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_workgallery ALTER COLUMN id SET DEFAULT nextval('public.works_workgallery_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_workgallery_images ALTER COLUMN id SET DEFAULT nextval('public.works_workgallery_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_workimage ALTER COLUMN id SET DEFAULT nextval('public.works_workimage_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add bookmark	1	add_bookmark
2	Can change bookmark	1	change_bookmark
3	Can delete bookmark	1	delete_bookmark
4	Can add pinned application	2	add_pinnedapplication
5	Can change pinned application	2	change_pinnedapplication
6	Can delete pinned application	2	delete_pinnedapplication
7	Can add log entry	3	add_logentry
8	Can change log entry	3	change_logentry
9	Can delete log entry	3	delete_logentry
10	Can add group	4	add_group
11	Can change group	4	change_group
12	Can delete group	4	delete_group
13	Can add permission	5	add_permission
14	Can change permission	5	change_permission
15	Can delete permission	5	delete_permission
16	Can add content type	6	add_contenttype
17	Can change content type	6	change_contenttype
18	Can delete content type	6	delete_contenttype
19	Can add session	7	add_session
20	Can change session	7	change_session
21	Can delete session	7	delete_session
22	Can add avatar	8	add_userimage
23	Can change avatar	8	change_userimage
24	Can delete avatar	8	delete_userimage
25	Can add user	9	add_user
26	Can change user	9	change_user
27	Can delete user	9	delete_user
28	Can add почту	10	add_adminemails
29	Can change почту	10	change_adminemails
30	Can delete почту	10	delete_adminemails
31	Can add datenschutz	11	add_datenschutz
32	Can change datenschutz	11	change_datenschutz
33	Can delete datenschutz	11	delete_datenschutz
34	Can add Текст на главной	12	add_seotext
35	Can change Текст на главной	12	change_seotext
36	Can delete Текст на главной	12	delete_seotext
37	Can add Подписка	13	add_subscribers
38	Can change Подписка	13	change_subscribers
39	Can delete Подписка	13	delete_subscribers
40	Can add Доставка	14	add_delivery
41	Can change Доставка	14	change_delivery
42	Can delete Доставка	14	delete_delivery
43	Can add отзыв	15	add_reviews
44	Can change отзыв	15	change_reviews
45	Can delete отзыв	15	delete_reviews
46	Can add Контакты	16	add_contact
47	Can change Контакты	16	change_contact
48	Can delete Контакты	16	delete_contact
49	Can add affiliate feedback	17	add_affiliatefeedback
50	Can change affiliate feedback	17	change_affiliatefeedback
51	Can delete affiliate feedback	17	delete_affiliatefeedback
52	Can add слайд	18	add_slider
53	Can change слайд	18	change_slider
54	Can delete слайд	18	delete_slider
55	Can add Производители	19	add_manufacturers
56	Can change Производители	19	change_manufacturers
57	Can delete Производители	19	delete_manufacturers
58	Can add Советы по ремонту	20	add_repairtips
59	Can change Советы по ремонту	20	change_repairtips
60	Can delete Советы по ремонту	20	delete_repairtips
61	Can add Условия возврата товаров	21	add_returnpolicy
62	Can change Условия возврата товаров	21	change_returnpolicy
63	Can delete Условия возврата товаров	21	delete_returnpolicy
64	Can add Условия продажи товаров	22	add_termsofsaleofgoods
65	Can change Условия продажи товаров	22	change_termsofsaleofgoods
66	Can delete Условия продажи товаров	22	delete_termsofsaleofgoods
67	Can add страницу	23	add_custompages
68	Can change страницу	23	change_custompages
69	Can delete страницу	23	delete_custompages
70	Can add писмо	24	add_feedback
71	Can change писмо	24	change_feedback
72	Can delete писмо	24	delete_feedback
73	Can add настройку	25	add_sitesettings
74	Can change настройку	25	change_sitesettings
75	Can delete настройку	25	delete_sitesettings
76	Can add цифры	26	add_aboutinnumbers
77	Can change цифры	26	change_aboutinnumbers
78	Can delete цифры	26	delete_aboutinnumbers
79	Can add our team	27	add_ourteam
80	Can change our team	27	change_ourteam
81	Can delete our team	27	delete_ourteam
82	Can add О нас	28	add_about
83	Can change О нас	28	change_about
84	Can delete О нас	28	delete_about
85	Can add цвет	29	add_productcolor
86	Can change цвет	29	change_productcolor
87	Can delete цвет	29	delete_productcolor
88	Can add размер	30	add_productsize
89	Can change размер	30	change_productsize
90	Can delete размер	30	delete_productsize
91	Can add свойство	31	add_productproperty
92	Can change свойство	31	change_productproperty
93	Can delete свойство	31	delete_productproperty
94	Can add свйство	32	add_allproductproperties
95	Can change свйство	32	change_allproductproperties
96	Can delete свйство	32	delete_allproductproperties
97	Can add product size settings	33	add_productsizesettings
98	Can change product size settings	33	change_productsizesettings
99	Can delete product size settings	33	delete_productsizesettings
100	Can add download data	34	add_downloaddata
101	Can change download data	34	change_downloaddata
102	Can delete download data	34	delete_downloaddata
103	Can add Картинка для товаров	35	add_productimage
104	Can change Картинка для товаров	35	change_productimage
105	Can delete Картинка для товаров	35	delete_productimage
106	Can add Продукт	36	add_product
107	Can change Продукт	36	change_product
108	Can delete Продукт	36	delete_product
109	Can add Элемент заказа	37	add_orderrow
110	Can change Элемент заказа	37	change_orderrow
111	Can delete Элемент заказа	37	delete_orderrow
112	Can add Заказ	38	add_order
113	Can change Заказ	38	change_order
114	Can delete Заказ	38	delete_order
115	Can add Категория	39	add_category
116	Can change Категория	39	change_category
117	Can delete Категория	39	delete_category
118	Can add Бренд	40	add_brand
119	Can change Бренд	40	change_brand
120	Can delete Бренд	40	delete_brand
121	Can add подкатегорию	41	add_subcategory
122	Can change подкатегорию	41	change_subcategory
123	Can delete подкатегорию	41	delete_subcategory
124	Can add акцию	42	add_promotion
125	Can change акцию	42	change_promotion
126	Can delete акцию	42	delete_promotion
127	Can add новость	43	add_post
128	Can change новость	43	change_post
129	Can delete новость	43	delete_post
130	Can add Картинки	44	add_postgalleryimage
131	Can change Картинки	44	change_postgalleryimage
132	Can delete Картинки	44	delete_postgalleryimage
133	Can add Готовый проект	45	add_donework
134	Can change Готовый проект	45	change_donework
135	Can delete Готовый проект	45	delete_donework
136	Can add Картинка работы	46	add_workimage
137	Can change Картинка работы	46	change_workimage
138	Can delete Картинка работы	46	delete_workimage
139	Can add Галлерея выполненных работ	47	add_workgallery
140	Can change Галлерея выполненных работ	47	change_workgallery
141	Can delete Галлерея выполненных работ	47	delete_workgallery
142	Can add Текста на сайте	48	add_sitestrings
143	Can change Текста на сайте	48	change_sitestrings
144	Can delete Текста на сайте	48	delete_sitestrings
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 144, true);


--
-- Data for Name: categories_brand; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories_brand (id, title, thumbnail, is_active, link) FROM stdin;
4	Meria Balykchy	brands/008_Meria_Balykchy.png	t	https://www.facebook.com/balykchymeria/
3	Meria Karakol	brands/008_Meria_Karakol.png	t	http://www.karakol.com.kg/
2	Meria Tokmok	brands/009_Meria_Tokmok.png	t	http://meria.tokmokcity.kg/
12	Jannat Regency	brands/010_Jannat_Regency.png	t	https://jannat.kg/
11	CAPS	brands/001_CAPS.png	t	http://caps.kg/
10	OOBA	brands/002_OOBA_I0gb0QW.jpg	t	http://ooba.kg/
9	AVANGARD STYLE	brands/003_AVANGARD_STYLE_VCdfYMW.jpg	t	https://avangardstyle.kg/
8	Glavstroy	brands/004_Glavstroy_junXaIK.jpg	t	http://www.glavstroy.kg/
7	Imaratstroy	brands/005_Imaratstroy_MCFVLCg.png	t	http://www.imaratstroy.kg/
6	Emark Group	brands/006_Emark_Group_igVCrwR.jpeg	t	http://emark.kg/
5	Meria Bishkek	brands/007_Meria_Bishkek_ezWK74g.jpg	t	http://www.meria.kg/ru
13	KICB	brands/011_KICB.png	t	https://kicb.net/welcome/
14	Nurzaman	brands/012_Nurzaman.png	t	https://nurzaman.kg/
\.


--
-- Name: categories_brand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_brand_id_seq', 14, true);


--
-- Data for Name: categories_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories_category (id, title, slug, description, thumbnail, is_active, parent_category_id, seo_thumbnail, features, notes, description_kg, features_kg, notes_kg, title_kg, description_ru, features_ru, notes_ru, title_ru) FROM stdin;
3	Праздничное освещение	prazdnichnoe-osveshenie	<p>&nbsp; &nbsp; &nbsp;Праздничная иллюминация - это особый тип оформления зданий, улиц, площадей, деревьев и других объектов с помощью различных гирлянд, световых фигур и&nbsp;ландшафтных светильников.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Часто подобное оформление делается к какому- либо празднику и имеет конкретную тематику.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В Компании &quot;Технология&quot; накоплен богатый опыт реализации подобных проектов.</p>	categories/prazdnichnoe-osveshenie_thumbnail_2018-10-22_190348.624220.png	t	\N		<p>&nbsp; &nbsp; &nbsp;Световые приборы и конструкции обычно изготавливаются по индивидуальным проектам, поэтому желательно иметь достаточно времени для осуществления всех этапов реализации проекта:</p>\r\n\r\n<ul>\r\n\t<li>Создание дизайна, образа</li>\r\n\t<li>Производство осветительной продукции</li>\r\n\t<li>Монтаж оборудования на месте в установленные сроки</li>\r\n</ul>	<p>&nbsp; &nbsp; &nbsp;Праздничное украшение зданий и ландшафта световыми композициями во время праздников обладает не только эстетической функцией. Подобная иллюминация способна привлечь внимание к конкретной рекламе или вывеске, а также&nbsp;подчеркнуть историческое значение города или определенного района.</p>	\N	\N	\N		<p>&nbsp; &nbsp; &nbsp;Праздничная иллюминация - это особый тип оформления зданий, улиц, площадей, деревьев и других объектов с помощью различных гирлянд, световых фигур и&nbsp;ландшафтных светильников.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Часто подобное оформление делается к какому- либо празднику и имеет конкретную тематику.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В Компании &quot;Технология&quot; накоплен богатый опыт реализации подобных проектов.</p>	<p>&nbsp; &nbsp; &nbsp;Световые приборы и конструкции обычно изготавливаются по индивидуальным проектам, поэтому желательно иметь достаточно времени для осуществления всех этапов реализации проекта:</p>\r\n\r\n<ul>\r\n\t<li>Создание дизайна, образа</li>\r\n\t<li>Производство осветительной продукции</li>\r\n\t<li>Монтаж оборудования на месте в установленные сроки</li>\r\n</ul>	<p>&nbsp; &nbsp; &nbsp;Праздничное украшение зданий и ландшафта световыми композициями во время праздников обладает не только эстетической функцией. Подобная иллюминация способна привлечь внимание к конкретной рекламе или вывеске, а также&nbsp;подчеркнуть историческое значение города или определенного района.</p>	Праздничное освещение
2	Декоративное освещение	dekorativnoe-osveshenie	<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; &nbsp;Ночное освещение можно разделить на уличное и декоративное.</span></p>\r\n\r\n<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; &nbsp;Для&nbsp;<a href="http://37.46.128.92/categories/service/ulichnoe-osveshenie/">уличного освещения</a>, как правило, используются магистральные светильники, которые освещают автодороги, площади&nbsp; т.п.</span></p>\r\n\r\n<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; Декоративные светильники, в отличие от магистральных, устанавливаются на фасады зданий, а так же ближлежащие территории для того, чтоб выгодно подчеркнуть архитектурные особенности зданий.</span></p>	categories/dekorativnoe-osveshenie_thumbnail_2018-08-25_152149.433894.png	t	\N	categories/seo/002_Decorativnoe_osveschenie_usluga-1.jpg	<p><span style="font-family:Georgia,serif">При осуществлении декоративного освещения нужно:</span></p>\r\n\r\n<ul>\r\n\t<li><span style="font-family:Georgia,serif">составить образ проекта, самостоятельно или с привлечением дизайнеров, архитекторов</span></li>\r\n\t<li><span style="font-family:Georgia,serif">подобрать типы и виды светильников, подходящие для реализации проекта</span></li>\r\n\t<li><span style="font-family:Georgia,serif">произвести расчет&nbsp;по матералам, работам, стоимости оборудования</span></li>\r\n\t<li><span style="font-family:Georgia,serif">выдерживать&nbsp;четкий график поставок и работ для получения результата в намеченный срок</span></li>\r\n</ul>	<p>&nbsp; &nbsp; &nbsp;В проектах декоративного освещения в основном используются следующие светильники:</p>	\N	\N	\N		<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; &nbsp;Ночное освещение можно разделить на уличное и декоративное.</span></p>\r\n\r\n<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; &nbsp;Для&nbsp;<a href="http://37.46.128.92/categories/service/ulichnoe-osveshenie/">уличного освещения</a>, как правило, используются магистральные светильники, которые освещают автодороги, площади&nbsp; т.п.</span></p>\r\n\r\n<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; Декоративные светильники, в отличие от магистральных, устанавливаются на фасады зданий, а так же ближлежащие территории для того, чтоб выгодно подчеркнуть архитектурные особенности зданий.</span></p>	<p><span style="font-family:Georgia,serif">При осуществлении декоративного освещения нужно:</span></p>\r\n\r\n<ul>\r\n\t<li><span style="font-family:Georgia,serif">составить образ проекта, самостоятельно или с привлечением дизайнеров, архитекторов</span></li>\r\n\t<li><span style="font-family:Georgia,serif">подобрать типы и виды светильников, подходящие для реализации проекта</span></li>\r\n\t<li><span style="font-family:Georgia,serif">произвести расчет&nbsp;по матералам, работам, стоимости оборудования</span></li>\r\n\t<li><span style="font-family:Georgia,serif">выдерживать&nbsp;четкий график поставок и работ для получения результата в намеченный срок</span></li>\r\n</ul>	<p>&nbsp; &nbsp; &nbsp;В проектах декоративного освещения в основном используются следующие светильники:</p>	Декоративное освещение
5	Уличное освещение	ulichnoe-osveshenie	<p>&nbsp; &nbsp; &nbsp;Уровень освещенности автомобильных дорог&nbsp;имеет&nbsp;важное значение для обеспечения безопасности передвижения всех участников уличного движения.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Компанией &quot;Технология&quot;&nbsp;накоплен богатый опыт в реализации проектов по освещению городских автодорог: магистралей, улиц, парковых зон и площадей. Реализация&nbsp;данных проектов проводится с особой&nbsp;ответственностью, также многие из проектов приняты на техническое обслуживание.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Существуют различные типы дорог, для каждого из них есть определенные требования по организации освещения, а так же уровню освещенности. А при выборе типа светильников для освещения дорог и улиц целесообразно обратить внимание на&nbsp;светодиодные светильники, которые обладают рядом преимуществ перед старыми типами осветительных приборов (ртутные лампы, газоразрядные и др.). Использование светодиодных светильников позволит:</p>\r\n\r\n<ul>\r\n\t<li>задать оптимальный угол&nbsp;рассеивания света</li>\r\n\t<li>выбрать необходимую цветовую&nbsp;температуру света (теплый, холодный, нейтральный)</li>\r\n\t<li>обеспечить независимость освещения от&nbsp;перепадов&nbsp;напряжения в сети</li>\r\n\t<li>сократить затраты на обслуживание светильников и на электроэнергию</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp;&nbsp;Специалисты компании &quot;Технология&quot; в кратчайшие сроки подберут светильники для реализации любой из Ваших идей.</p>	categories/ulichnoe-osveshenie_thumbnail_2018-08-25_153110.575094.png	t	\N		<p>При проектировании уличного освещения следует принимать во внимание:</p>\r\n\r\n<ul>\r\n\t<li>Тип автодороги</li>\r\n\t<li>Количество полос движения</li>\r\n\t<li>Расстояние между опорами освещения и их высоту</li>\r\n</ul>	<p>&nbsp;По результатам исследований, есть решающие факторы, влияющие на&nbsp;количество ДТП на автодорогах:</p>\r\n\r\n<ul>\r\n\t<li>качество дорожного покрытия</li>\r\n\t<li>наличие и качество дорожной разметки</li>\r\n\t<li>уровень и качество освещенности</li>\r\n</ul>	\N	\N	\N		<p>&nbsp; &nbsp; &nbsp;Уровень освещенности автомобильных дорог&nbsp;имеет&nbsp;важное значение для обеспечения безопасности передвижения всех участников уличного движения.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Компанией &quot;Технология&quot;&nbsp;накоплен богатый опыт в реализации проектов по освещению городских автодорог: магистралей, улиц, парковых зон и площадей. Реализация&nbsp;данных проектов проводится с особой&nbsp;ответственностью, также многие из проектов приняты на техническое обслуживание.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Существуют различные типы дорог, для каждого из них есть определенные требования по организации освещения, а так же уровню освещенности. А при выборе типа светильников для освещения дорог и улиц целесообразно обратить внимание на&nbsp;светодиодные светильники, которые обладают рядом преимуществ перед старыми типами осветительных приборов (ртутные лампы, газоразрядные и др.). Использование светодиодных светильников позволит:</p>\r\n\r\n<ul>\r\n\t<li>задать оптимальный угол&nbsp;рассеивания света</li>\r\n\t<li>выбрать необходимую цветовую&nbsp;температуру света (теплый, холодный, нейтральный)</li>\r\n\t<li>обеспечить независимость освещения от&nbsp;перепадов&nbsp;напряжения в сети</li>\r\n\t<li>сократить затраты на обслуживание светильников и на электроэнергию</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp;&nbsp;Специалисты компании &quot;Технология&quot; в кратчайшие сроки подберут светильники для реализации любой из Ваших идей.</p>	<p>При проектировании уличного освещения следует принимать во внимание:</p>\r\n\r\n<ul>\r\n\t<li>Тип автодороги</li>\r\n\t<li>Количество полос движения</li>\r\n\t<li>Расстояние между опорами освещения и их высоту</li>\r\n</ul>	<p>&nbsp;По результатам исследований, есть решающие факторы, влияющие на&nbsp;количество ДТП на автодорогах:</p>\r\n\r\n<ul>\r\n\t<li>качество дорожного покрытия</li>\r\n\t<li>наличие и качество дорожной разметки</li>\r\n\t<li>уровень и качество освещенности</li>\r\n</ul>	Уличное освещение
6	Производство опор освещения	proizvodstvo-opor-osvesheniya	<p>&nbsp; &nbsp; &nbsp;В компании &quot;Технология&quot; налажен полный цикл производства и установки опор освещения: от поставок сырья до финальной покраски, транспортировки и монтажа на месте установки. Отсутствие посредников гарантирует выгодную цену на продукцию с учетом доставки до любого региона КР и государств&nbsp;зоны ЕАЭС.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Каждая партия до начала производства&nbsp;индивидуально утверждается с учетом пожеланий заказчика. Конструкторский отдел производит расчеты опор&nbsp;по индивидуальным размерам, при этом обеспечивается надежность конструкции и оптимальная цена на опору. Покраска опоры возможна обычной влагоустойчивой или&nbsp;порошковой окраске по коду RAL.</p>	categories/proizvodstvo-opor-osvesheniya_thumbnail_2018-08-31_123605.404508.png	t	\N		<p><strong>Способ установки металлических опор:</strong></p>\r\n\r\n<ul>\r\n\t<li>Фланцевые опоры монтируются на железобетонную фундаментную основу с помощью фланцевого крепления болтами или шпильками к закладной детали фундамента .</li>\r\n\t<li>Заранее подготавливается земляной котлован для последующей установки фундаментной основы.</li>\r\n\t<li>При протяжке силовых&nbsp;и коммуникационных кабелей подземным методом, все провода заводятся внутрь фундамента и выводятся внутрь опоры на уровень технического отверстия для последующего расключения</li>\r\n</ul>	<p><strong>Опоры освещения используются для:</strong></p>\r\n\r\n<ul>\r\n\t<li>организации освещения&nbsp;на городских и загородных автодорогах и населенных пунктах</li>\r\n\t<li>установки инфраструктуры силовых линий городского общественного транспорта</li>\r\n\t<li>монтажа объектов регулирования дорожного движения</li>\r\n\t<li>для прокладки кабелей электрической сети наружного освещения воздушным или подземным способом</li>\r\n\t<li>установки информационных и рекламных щитов</li>\r\n</ul>	<p>&nbsp; &nbsp;В компании &quot;Технология&quot; налажен полный цикл производства и установки опор освещения: от поставок сырья до финальной покраски, транспортировки и монтажа на месте установки. Отсутствие посредников гарантирует выгодную цену на продукцию с учетом доставки до любого региона КР и государства зоны ЕАЭС.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Каждая партия до начала производства&nbsp;индивидуально утверждается с учетом пожеланий заказчика. Конструкторский отдел производит расчеты опор&nbsp;по индивидуальным размерам, при этом обеспечивается надежность конструкции и оптимальная цена на опору. Покраска опоры возможна обычной влагоустойчивой или&nbsp;порошковой окраске по коду RAL.</p>	\N	\N		<p>&nbsp; &nbsp; &nbsp;В компании &quot;Технология&quot; налажен полный цикл производства и установки опор освещения: от поставок сырья до финальной покраски, транспортировки и монтажа на месте установки. Отсутствие посредников гарантирует выгодную цену на продукцию с учетом доставки до любого региона КР и государств&nbsp;зоны ЕАЭС.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Каждая партия до начала производства&nbsp;индивидуально утверждается с учетом пожеланий заказчика. Конструкторский отдел производит расчеты опор&nbsp;по индивидуальным размерам, при этом обеспечивается надежность конструкции и оптимальная цена на опору. Покраска опоры возможна обычной влагоустойчивой или&nbsp;порошковой окраске по коду RAL.</p>	<p><strong>Способ установки металлических опор:</strong></p>\r\n\r\n<ul>\r\n\t<li>Фланцевые опоры монтируются на железобетонную фундаментную основу с помощью фланцевого крепления болтами или шпильками к закладной детали фундамента .</li>\r\n\t<li>Заранее подготавливается земляной котлован для последующей установки фундаментной основы.</li>\r\n\t<li>При протяжке силовых&nbsp;и коммуникационных кабелей подземным методом, все провода заводятся внутрь фундамента и выводятся внутрь опоры на уровень технического отверстия для последующего расключения</li>\r\n</ul>	<p><strong>Опоры освещения используются для:</strong></p>\r\n\r\n<ul>\r\n\t<li>организации освещения&nbsp;на городских и загородных автодорогах и населенных пунктах</li>\r\n\t<li>установки инфраструктуры силовых линий городского общественного транспорта</li>\r\n\t<li>монтажа объектов регулирования дорожного движения</li>\r\n\t<li>для прокладки кабелей электрической сети наружного освещения воздушным или подземным способом</li>\r\n\t<li>установки информационных и рекламных щитов</li>\r\n</ul>	Производство опор освещения
4	Внутреннее освещение	vnutrennee-osveshenie	<p>&nbsp; &nbsp; &nbsp; &nbsp;При проектировке освещения внутри различных типов помещений (от промышленных складов и торговых центров до офисов или квартир) немаловажно продумать обеспечение необходимой освещенности. Типы и виды светильников подбираются в зависимости от потребностей заказчика. Например, в торговом или складском помещении важно обеспечить уровень освещенности, достаточной для чтения мелких шрифтов на упаковках товаров. А в комнатах заведений, предназначенных для отдыха и проведения досуга, важнее создать теплую непринужденную обстановку.</p>	categories/vnutrennee-osveshenie_thumbnail_2018-08-31_162838.190404.png	t	\N		<p>Освещение любого помещения складывается, как правило, из трех типов:</p>\r\n\r\n<ul>\r\n\t<li>Общее (равномерный рассеянный свет)</li>\r\n\t<li>Рабочее (направленное)</li>\r\n\t<li>Декоративное</li>\r\n</ul>	<p>&nbsp; &nbsp; &nbsp;Важно продумать полный цикл светотехнического сценария помещения, поэтому инвестиции в проектирование и создание дизайна полностью оправданы и помогут сократить ненужные расходы на последующие переделки и устранения недочетов.</p>	\N	\N	\N		<p>&nbsp; &nbsp; &nbsp; &nbsp;При проектировке освещения внутри различных типов помещений (от промышленных складов и торговых центров до офисов или квартир) немаловажно продумать обеспечение необходимой освещенности. Типы и виды светильников подбираются в зависимости от потребностей заказчика. Например, в торговом или складском помещении важно обеспечить уровень освещенности, достаточной для чтения мелких шрифтов на упаковках товаров. А в комнатах заведений, предназначенных для отдыха и проведения досуга, важнее создать теплую непринужденную обстановку.</p>	<p>Освещение любого помещения складывается, как правило, из трех типов:</p>\r\n\r\n<ul>\r\n\t<li>Общее (равномерный рассеянный свет)</li>\r\n\t<li>Рабочее (направленное)</li>\r\n\t<li>Декоративное</li>\r\n</ul>	<p>&nbsp; &nbsp; &nbsp;Важно продумать полный цикл светотехнического сценария помещения, поэтому инвестиции в проектирование и создание дизайна полностью оправданы и помогут сократить ненужные расходы на последующие переделки и устранения недочетов.</p>	Внутреннее освещение
8	Камеры видеонаблюдения	kamery-videonablyudeniya	\N		t	7		\N	\N	\N	\N	\N		\N	\N	\N	Камеры видеонаблюдения
7	Системы видеонаблюдения	sistemy-promyshlennogo-videonablyudeniya	<p>&nbsp; &nbsp; &nbsp;Видеонаблюдение &mdash; ключевая часть системы безопасности в современном мире. Видеоконтроль за происходящим используется на промышленных объектах, городских улицах и частных участках.<br />\r\n&nbsp; &nbsp; &nbsp;Любой владелец бизнеса видит преимущества грамотно реализованной&nbsp;видеосистемы:</p>\r\n\r\n<ul>\r\n\t<li>контроль доступа к объекту</li>\r\n\t<li>наблюдение за рабочими процессами</li>\r\n\t<li>архивация данных</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; Возможности системы видеонаблюдения позволяют оптимизировать рабочие процессы, деятельность техники и людей на местах, свести к минимуму или избежать возникновение потерь или ЧП на объекте.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Традиционно системой видеонаблюдения называют&nbsp;комплекс оборудования и коммуникационных линий, предназначенный для кодирования, передачи, концентрации, декодирования визуальной информации для ее зрительного восприятия и обработки, регистрации и хранения.</p>	categories/sistemy-promyshlennogo-videonablyudeniya_thumbnail_2018-10-11_181240.725412.png	t	\N		<p>Система видеонаблюдения представляет собой&nbsp;разветвленную структуру:</p>\r\n\r\n<ul>\r\n\t<li>периферийное оборудование: видеокамеры, поворотные платформы, устройства подсветки (расположеные непосредственно в зонах видеонаблюдения)</li>\r\n\t<li>стационарное&nbsp;оборудование: монитороы, устройства управления, видеорегистраторы, специализированное&nbsp;ПО (устанавливаются внутри административных помещений &mdash; пунктах операторов или охраны)</li>\r\n\t<li>система электропитания и каналов передачи данных</li>\r\n</ul>	<p>Системы видеоконтроля используются для:</p>\r\n\r\n<ul>\r\n\t<li>контроля доступа на объекте</li>\r\n\t<li>контроля за выполнением инструкций</li>\r\n\t<li>контроля технологических процессов</li>\r\n\t<li>наблюдения за процессами в особых условиях, исключающих или ограничивающих присутствие человека</li>\r\n\t<li>снятия показаний контрольно-измерительной аппаратуры</li>\r\n\t<li>и пр.</li>\r\n</ul>	\N	\N	\N		<p>&nbsp; &nbsp; &nbsp;Видеонаблюдение &mdash; ключевая часть системы безопасности в современном мире. Видеоконтроль за происходящим используется на промышленных объектах, городских улицах и частных участках.<br />\r\n&nbsp; &nbsp; &nbsp;Любой владелец бизнеса видит преимущества грамотно реализованной&nbsp;видеосистемы:</p>\r\n\r\n<ul>\r\n\t<li>контроль доступа к объекту</li>\r\n\t<li>наблюдение за рабочими процессами</li>\r\n\t<li>архивация данных</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; Возможности системы видеонаблюдения позволяют оптимизировать рабочие процессы, деятельность техники и людей на местах, свести к минимуму или избежать возникновение потерь или ЧП на объекте.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Традиционно системой видеонаблюдения называют&nbsp;комплекс оборудования и коммуникационных линий, предназначенный для кодирования, передачи, концентрации, декодирования визуальной информации для ее зрительного восприятия и обработки, регистрации и хранения.</p>	<p>Система видеонаблюдения представляет собой&nbsp;разветвленную структуру:</p>\r\n\r\n<ul>\r\n\t<li>периферийное оборудование: видеокамеры, поворотные платформы, устройства подсветки (расположеные непосредственно в зонах видеонаблюдения)</li>\r\n\t<li>стационарное&nbsp;оборудование: монитороы, устройства управления, видеорегистраторы, специализированное&nbsp;ПО (устанавливаются внутри административных помещений &mdash; пунктах операторов или охраны)</li>\r\n\t<li>система электропитания и каналов передачи данных</li>\r\n</ul>	<p>Системы видеоконтроля используются для:</p>\r\n\r\n<ul>\r\n\t<li>контроля доступа на объекте</li>\r\n\t<li>контроля за выполнением инструкций</li>\r\n\t<li>контроля технологических процессов</li>\r\n\t<li>наблюдения за процессами в особых условиях, исключающих или ограничивающих присутствие человека</li>\r\n\t<li>снятия показаний контрольно-измерительной аппаратуры</li>\r\n\t<li>и пр.</li>\r\n</ul>	Системы видеонаблюдения
\.


--
-- Name: categories_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_category_id_seq', 8, true);


--
-- Data for Name: categories_subcategory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories_subcategory (id, title) FROM stdin;
\.


--
-- Name: categories_subcategory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_subcategory_id_seq', 1, false);


--
-- Data for Name: client_app_about; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_about (id, body) FROM stdin;
1	<p>&nbsp; &nbsp; &nbsp;Компания &laquo;Технология&raquo; была основана в 2010 году и на данный момент является одной из лидирующих компаний в Кыргызстане по реализации товаров и проектов в сфере LED - освещения. С начала своей деятельности, мы успели реализовать множество проектов различной сложности и масштабов.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Сегодня &laquo;Технология&raquo; - это компания, ориентированная на комплексный подход к решению задач любой сложности в области светодиодного освещения и различных телекоммуникационных систем.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Наша компания делает особый акцент на высоком уровне сервиса для наших клиентов. Наши консультанты постоянно повышают квалификацию, а потому всегда готовы проконсультировать Вас и подобрать оборудование для решения любого рода задач в освоенных нами сферах деятельности.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В наших предложениях вы найдете надежные, проверенные временем и доказавшие свою эффективность товары. Широкий ассортимент позволит решить задачи высокого уровня сложности, а наши цены весьма демократичны. Доверьте реализацию своего проекта нашим специалистам и убедитесь в этом сами!</p>
\.


--
-- Name: client_app_about_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_about_id_seq', 1, true);


--
-- Data for Name: client_app_aboutinnumbers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_aboutinnumbers (id, key, number) FROM stdin;
\.


--
-- Name: client_app_aboutinnumbers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_aboutinnumbers_id_seq', 1, false);


--
-- Data for Name: client_app_adminemails; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_adminemails (id, email, is_active) FROM stdin;
2	sales@technology.kg	t
1	okay11007@gmail.com	f
\.


--
-- Name: client_app_adminemails_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_adminemails_id_seq', 2, true);


--
-- Data for Name: client_app_affiliatefeedback; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_affiliatefeedback (id, is_read, first_name, last_name, phone_number, affiliate_id, created_at) FROM stdin;
\.


--
-- Name: client_app_affiliatefeedback_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_affiliatefeedback_id_seq', 1, false);


--
-- Data for Name: client_app_contact; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_contact (id, title, address, email, phone, location) FROM stdin;
1	Главный офис	Шопокова 101/1	sales@technology.kg	[{"id":2,"text":"+996 312 97 90 97"},{"id":3,"text":"+996 555 111 333"}]	42.87237874576274,74.61549282073975
\.


--
-- Name: client_app_contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_contact_id_seq', 1, true);


--
-- Data for Name: client_app_custompages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_custompages (id, title, content, slug) FROM stdin;
\.


--
-- Name: client_app_custompages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_custompages_id_seq', 1, false);


--
-- Data for Name: client_app_datenschutz; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_datenschutz (id, body) FROM stdin;
\.


--
-- Name: client_app_datenschutz_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_datenschutz_id_seq', 1, false);


--
-- Data for Name: client_app_delivery; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_delivery (id, key, value) FROM stdin;
\.


--
-- Name: client_app_delivery_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_delivery_id_seq', 1, false);


--
-- Data for Name: client_app_feedback; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_feedback (id, is_read, first_name, email, phone_number, last_name, created_at, message) FROM stdin;
1	f	Erlan	okay11007@gmail.com	707330726		2018-08-20	fdgdfgdfg
2	t	Erlan	okay11007@gmail.com	707330726		2018-08-21	алопролыварпл оварплыовра
\.


--
-- Name: client_app_feedback_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_feedback_id_seq', 2, true);


--
-- Data for Name: client_app_manufacturers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_manufacturers (id, content) FROM stdin;
\.


--
-- Name: client_app_manufacturers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_manufacturers_id_seq', 1, false);


--
-- Data for Name: client_app_ourteam; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_ourteam (id, image, profession, full_name) FROM stdin;
\.


--
-- Name: client_app_ourteam_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_ourteam_id_seq', 1, false);


--
-- Data for Name: client_app_repairtips; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_repairtips (id, content) FROM stdin;
\.


--
-- Name: client_app_repairtips_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_repairtips_id_seq', 1, false);


--
-- Data for Name: client_app_returnpolicy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_returnpolicy (id, content) FROM stdin;
\.


--
-- Name: client_app_returnpolicy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_returnpolicy_id_seq', 1, false);


--
-- Data for Name: client_app_reviews; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_reviews (id, name, email, message, created_at, is_active) FROM stdin;
\.


--
-- Name: client_app_reviews_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_reviews_id_seq', 1, false);


--
-- Data for Name: client_app_seotext; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_seotext (id, title, text) FROM stdin;
\.


--
-- Name: client_app_seotext_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_seotext_id_seq', 1, true);


--
-- Data for Name: client_app_sitesettings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_sitesettings (id, phone, address, facebook, instagram, telegram, meta_description, meta_key_words) FROM stdin;
1	[{"id":3,"text":"+996 555 111 333"},{"id":4,"text":"+996 312 97 90 97"}]	Шопокова 101/1	https://www.facebook.com/technology.kg/	https://www.instagram.com/technology_kg/			
\.


--
-- Name: client_app_sitesettings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_sitesettings_id_seq', 1, true);


--
-- Data for Name: client_app_sitestrings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_sitestrings (id, nav_services, nav_works, nav_products, nav_about, nav_contacts, our_services, our_works, our_production, posts) FROM stdin;
1	УСЛУГИ	РАБОТЫ	ПРОДУКЦИЯ	О КОМПАНИИ	КОНТАКТЫ	Наши услуги	Наши работы	Продукция	Статьи
\.


--
-- Name: client_app_sitestrings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_sitestrings_id_seq', 1, true);


--
-- Data for Name: client_app_slider; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_slider (id, image, link, is_active, short_description, title, is_sale, font_color) FROM stdin;
20	slider/Slider_06_-_raschet_proektov_THoK07o.jpg		t	Работы, материалы, сроки, суммы	Расчет проектов под ключ	f	#929DAD
19	slider/Slider_05_-_proizvodstvo_PH4zurk.jpg	http://37.46.128.92/categories/service/proizvodstvo-opor-osvesheniya/	t	Опоры освещения, металлоконструкции любой сложности	Собственное производство	f	#FF5212
16	slider/Slider_03_-_ulichnoe_osveschenie.jpg	http://37.46.128.92/categories/service/ulichnoe-osveshenie/	t	Скверы, парки, площади, автодороги	Уличное освещение	f	#E5E4E1
21	slider/Slider_07_-_vnutrennee_osveschenie.jpg	http://37.46.128.92/categories/service/vnutrennee-osveshenie/	t	Торговые и складские территории, офисы, места отдыха	Внутреннее освещение	f	#74CBB1
22	slider/Slider_02_-_prazdnichnoe_osveschenie_V2s0ycg.jpg	http://37.46.128.92/categories/service/prazdnichnoe-osveshenie/	t	Оформление мероприятий республиканского значения	Праздничное освещение	f	#99FF66
18	slider/Slider_04_-_decorativnoe_osveschenie.jpg	http://37.46.128.92/categories/service/dekorativnoe-osveshenie/	t	Подсветка фасадов зданий	Декоративное освещение	f	#FFFFFF
\.


--
-- Name: client_app_slider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_slider_id_seq', 22, true);


--
-- Data for Name: client_app_subscribers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_subscribers (id, email, is_active) FROM stdin;
\.


--
-- Name: client_app_subscribers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_subscribers_id_seq', 1, false);


--
-- Data for Name: client_app_termsofsaleofgoods; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_app_termsofsaleofgoods (id, content) FROM stdin;
\.


--
-- Name: client_app_termsofsaleofgoods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_app_termsofsaleofgoods_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-08-20 05:18:18.087992-04	1	okay11007@gmail.com	1	[{"added": {}}]	10	1
2	2018-08-21 03:41:51.052095-04	3		1	[{"added": {}}]	9	1
3	2018-08-21 03:42:27.30589-04	3		2	[{"changed": {"fields": ["is_staff", "is_superuser", "date_joined"]}}]	9	1
4	2018-08-21 03:53:27.313272-04	1	/application/assets/uploads/slider/slide_slide1.wt.png	1	[{"added": {}}]	18	3
5	2018-08-21 03:58:38.157646-04	1	Уличное освещение	1	[{"added": {}}]	39	3
6	2018-08-21 03:58:56.405744-04	1	Южная магистраль	1	[{"added": {}}]	45	3
7	2018-08-21 03:59:15.900861-04	1	done_works/slide1.jpg	1	[{"added": {}}]	46	3
8	2018-08-21 03:59:24.225156-04	2	done_works/filip-mroz-538852-unsplash.jpg	1	[{"added": {}}]	46	3
9	2018-08-21 03:59:39.08759-04	1	Южная магистраль	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[1]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
10	2018-08-21 04:07:08.283567-04	1	Радиус освещения	1	[{"added": {}}]	32	3
11	2018-08-21 04:09:18.833724-04	2	Радиус освещения	1	[{"added": {}}]	32	3
12	2018-08-21 04:09:29.851448-04	1	Светодиодные прожекторы	1	[{"added": {}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
13	2018-08-21 04:09:40.337796-04	1	Радиус освещения	3		32	3
14	2018-08-21 04:09:58.532624-04	1	Светодиодные прожекторы	2	[{"added": {"object": "\\u0420\\u0430\\u0434\\u0438\\u0443\\u0441 \\u043e\\u0441\\u0432\\u0435\\u0449\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
15	2018-08-21 04:19:32.792847-04	1	Главный офис	1	[{"added": {}}]	16	3
16	2018-08-21 04:20:16.689579-04	2	Erlan - okay11007@gmail.com	2	[{"changed": {"fields": ["is_read"]}}]	24	3
17	2018-08-21 04:21:12.567619-04	1	okay11007@gmail.com	2	[{"changed": {"fields": ["is_active"]}}]	10	3
18	2018-08-21 04:21:19.141508-04	1	okay11007@gmail.com	2	[{"changed": {"fields": ["is_active"]}}]	10	3
19	2018-08-21 04:25:28.520462-04	1	Страница О Нас	1	[{"added": {}}]	28	3
20	2018-08-21 04:26:36.218242-04	1	Настройка сайтов	1	[{"added": {}}]	25	3
21	2018-08-22 02:27:22.618347-04	2	/application/assets/uploads/slider/slide_T2-3.wt.png	1	[{"added": {}}]	18	3
22	2018-08-22 02:29:11.921013-04	2	/application/assets/uploads/slider/slide_slide_T2-3.wt.png	2	[{"changed": {"fields": ["title", "short_description"]}}]	18	3
23	2018-08-22 02:29:29.651715-04	2	/application/assets/uploads/slider/slide_slide_slide_T2-3.wt.png	2	[{"changed": {"fields": ["short_description"]}}]	18	3
24	2018-08-22 02:30:52.910829-04	2	/application/assets/uploads/slider/slide_slide_slide_slide_T2-3.wt.png	2	[{"changed": {"fields": ["link", "title", "short_description"]}}]	18	3
25	2018-08-22 02:31:13.672255-04	2	/application/assets/uploads/slider/slide_slide_slide_slide_slide_T2-3.wt.png	2	[{"changed": {"fields": ["title", "short_description"]}}]	18	3
26	2018-08-22 02:31:32.703576-04	2	/application/assets/uploads/slider/slide_slide_slide_slide_slide_slide_T2-3.wt.png	2	[{"changed": {"fields": ["title"]}}]	18	3
27	2018-08-22 03:05:00.365049-04	2	/application/assets/uploads/slider/slide_slide_slide_slide_slide_slide_slide_T2-3.wt.png	2	[]	18	3
28	2018-08-22 03:10:21.303177-04	2	/application/assets/uploads/slider/slide_slide_slide_slide_slide_slide_slide_slide_T2-3.wt.png	2	[{"changed": {"fields": ["is_active"]}}]	18	3
29	2018-08-22 03:10:42.880491-04	2	/application/assets/uploads/slider/slide_slide_slide_slide_slide_slide_slide_slide_T2-3.wt.png	3		18	3
30	2018-08-22 03:16:16.188938-04	3	/application/assets/uploads/slider/slide_IMG_2013-1.wt.png	1	[{"added": {}}]	18	3
31	2018-08-22 03:18:26.018707-04	3	/application/assets/uploads/slider/slide_IMG_2013-1.wt.png	3		18	3
32	2018-08-22 03:18:34.066272-04	4	/application/assets/uploads/slider/slide_IMG_2013-2.wt.png	1	[{"added": {}}]	18	3
33	2018-08-22 03:19:51.839141-04	4	/application/assets/uploads/slider/slide_IMG_2013-2.wt.png	3		18	3
34	2018-08-22 03:19:59.215793-04	5	/application/assets/uploads/slider/slide_IMG_2013-2_edNUdKH.wt.png	1	[{"added": {}}]	18	3
35	2018-08-22 03:21:04.065451-04	5	/application/assets/uploads/slider/slide_IMG_2013-2_edNUdKH.wt.png	3		18	3
36	2018-08-22 03:21:13.729692-04	6	/application/assets/uploads/slider/slide_IMG_2013-2_BMchYWC.wt.png	1	[{"added": {}}]	18	3
37	2018-08-22 03:22:08.48182-04	6	/application/assets/uploads/slider/slide_IMG_2013-2_BMchYWC.wt.png	3		18	3
38	2018-08-22 03:22:14.46116-04	7	/application/assets/uploads/slider/slide_IMG_2013-2_z4w5AsJ.wt.png	1	[{"added": {}}]	18	3
39	2018-08-22 03:22:48.412836-04	7	/application/assets/uploads/slider/slide_IMG_2013-2_z4w5AsJ.wt.png	3		18	3
40	2018-08-22 03:22:55.186599-04	8	/application/assets/uploads/slider/slide_IMG_2013-2_kUyfTWk.wt.png	1	[{"added": {}}]	18	3
41	2018-08-22 03:31:44.924958-04	8	/application/assets/uploads/slider/slide_IMG_2013-2_kUyfTWk.wt.png	3		18	3
42	2018-08-22 03:32:16.020353-04	9	/application/assets/uploads/slider/slide_IMG_2013-1_a1lpIcR.wt.png	1	[{"added": {}}]	18	3
43	2018-08-22 03:33:40.696015-04	9	/application/assets/uploads/slider/slide_slide_IMG_2013-1_a1lpIcR.wt.png	2	[{"changed": {"fields": ["title", "short_description"]}}]	18	3
44	2018-08-22 03:34:28.145066-04	9	/application/assets/uploads/slider/slide_slide_slide_IMG_2013-1_a1lpIcR.wt.png	2	[{"changed": {"fields": ["link"]}}]	18	3
45	2018-08-22 03:54:14.21986-04	9	/application/assets/uploads/slider/slide_slide_slide_IMG_2013-1_a1lpIcR.wt.png	3		18	3
46	2018-08-22 03:54:22.595891-04	10	/application/assets/uploads/slider/slide_IMG_2013-2_vDg8uvO.wt.png	1	[{"added": {}}]	18	3
47	2018-08-22 03:56:27.637867-04	10	/application/assets/uploads/slider/slide_IMG_2013-2_vDg8uvO.wt.png	3		18	3
48	2018-08-22 03:56:41.784558-04	11	/application/assets/uploads/slider/slide_IMG_2013-2_AJ769T1.wt.png	1	[{"added": {}}]	18	3
49	2018-08-22 04:00:17.290298-04	11	/application/assets/uploads/slider/slide_IMG_2013-2_AJ769T1.wt.png	3		18	3
50	2018-08-22 04:00:33.46728-04	12	/application/assets/uploads/slider/slide_IMG_2013-2_LkjDCN0.wt.png	1	[{"added": {}}]	18	3
51	2018-08-22 04:03:37.199125-04	13	/application/assets/uploads/slider/slide_LD4C_2017.wt.png	1	[{"added": {}}]	18	3
52	2018-08-22 04:06:53.565306-04	14	/application/assets/uploads/slider/slide_LD4C_1.wt.png	1	[{"added": {}}]	18	3
53	2018-08-22 04:26:59.182241-04	1	/application/assets/uploads/slider/slide_slide_slide1.wt.png	2	[{"changed": {"fields": ["is_active"]}}]	18	3
54	2018-08-22 05:38:57.005343-04	12	/application/assets/uploads/slider/slide_IMG_2013-2_LkjDCN0.wt.png	3		18	3
55	2018-08-22 05:42:23.247645-04	14	/application/assets/uploads/slider/slide_LD4C_1.wt.png	3		18	3
56	2018-08-22 05:42:23.257414-04	13	/application/assets/uploads/slider/slide_LD4C_2017.wt.png	3		18	3
57	2018-08-22 05:45:47.40795-04	15	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie.wt.png	1	[{"added": {}}]	18	3
58	2018-08-22 05:46:11.72044-04	15	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie.wt.png	3		18	3
59	2018-08-22 05:46:20.289762-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_1.wt.png	1	[{"added": {}}]	18	3
60	2018-08-22 05:48:25.720515-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_2_wide.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
61	2018-08-22 05:49:40.155638-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_2_small.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
62	2018-08-22 05:50:51.548341-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_2_wide_small.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
63	2018-08-22 05:51:24.791881-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_2_wide_byNsfmD.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
64	2018-08-22 05:52:27.338682-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_ns97fWt.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
65	2018-08-22 05:53:25.764962-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_2_wide_drjfuzB.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
66	2018-08-22 05:53:57.237503-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_2_wide_IbiW1qp.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
67	2018-08-22 05:54:57.699745-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_2_wide_TlbpMwF.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
68	2018-08-22 05:55:43.267621-04	16	/application/assets/uploads/slider/slide_slide_001_-_Ulichnoe_osveschenie_2_wide_TlbpMwF.wt.png	2	[{"changed": {"fields": ["link"]}}]	18	3
69	2018-08-22 05:56:07.464521-04	1	/application/assets/uploads/slider/slide_slide_slide_slide1.wt.png	2	[]	18	3
70	2018-08-22 05:58:30.164438-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_1_84S6vuP.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
71	2018-08-22 06:00:44.718868-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_1_wide.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
72	2018-08-22 06:01:58.386249-04	16	/application/assets/uploads/slider/slide_001_-_Ulichnoe_osveschenie_1_wide_MqSnlIp.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
73	2018-08-22 06:23:47.897737-04	17	/application/assets/uploads/slider/slide_002_-_decorativnoye_osveschenie_1-1.wt.png	1	[{"added": {}}]	18	3
74	2018-08-22 06:26:02.429316-04	17	/application/assets/uploads/slider/slide_002_-_decorativnoye_osveschenie_1-2.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
75	2018-08-22 06:31:26.820574-04	17	/application/assets/uploads/slider/slide_002_-_decorativnoye_osveschenie_1.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
76	2018-08-22 06:31:54.651052-04	17	/application/assets/uploads/slider/slide_002_-_decorativnoye_osveschenie_1-1_3XGEBo5.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
77	2018-08-22 06:34:50.966864-04	17	/application/assets/uploads/slider/slide_002_-_decorativnoye_osveschenie_2-wide.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
78	2018-08-22 06:38:37.774494-04	17	/application/assets/uploads/slider/slide_002_-_prazdnichnoe_osveschenie_2-1.wt.png	2	[{"changed": {"fields": ["image", "title"]}}]	18	3
79	2018-08-22 06:40:45.095202-04	17	/application/assets/uploads/slider/slide_002_-_prazdnichnoe_osveschenie_2-2.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
80	2018-08-22 06:42:09.688273-04	17	/application/assets/uploads/slider/slide_002_-_prazdnichnoe_osveschenie_2-2_kUPTZvc.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
81	2018-08-22 06:46:02.34186-04	17	/application/assets/uploads/slider/slide_002_-_prazdnichnoe_osveschenie_2-3.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
82	2018-08-22 06:48:11.221024-04	17	/application/assets/uploads/slider/slide_002_-_prazdnichnoe_osveschenie_2-3_RRJlj7d.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
83	2018-08-22 06:58:24.522715-04	16	/application/assets/uploads/slider/slide_003_-_ulichnoe_osveschenie_0-1.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
84	2018-08-22 06:59:31.816226-04	16	/application/assets/uploads/slider/slide_002_-_prazdnichnoe_osveschenie_2-2_ZAOzdIe.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
85	2018-08-22 07:00:10.852595-04	16	/application/assets/uploads/slider/slide_003_-_ulichnoe_osveschenie_0-1_8ZOUB7L.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
86	2018-08-22 07:22:08.060792-04	18	/application/assets/uploads/slider/slide_004_-_decorativnoe_osveschenie_1-1.wt.png	1	[{"added": {}}]	18	3
87	2018-08-22 07:27:13.631739-04	19	/application/assets/uploads/slider/slide_005_Proizvodstvo_1-1.wt.png	1	[{"added": {}}]	18	3
88	2018-08-22 07:36:53.996358-04	20	/application/assets/uploads/slider/slide_006_-_raschet_proektov_1-1.wt.png	1	[{"added": {}}]	18	3
89	2018-08-22 07:55:11.942505-04	21	/application/assets/uploads/slider/slide_007_-_vnutrennee_osveschenie_1-2.wt.png	1	[{"added": {}}]	18	3
90	2018-08-22 08:00:35.383123-04	1	/application/assets/uploads/slider/slide_slide_slide_slide_slide1.wt.png	2	[{"changed": {"fields": ["link"]}}]	18	3
91	2018-08-23 05:29:09.436514-04	1	Уличное освещение	2	[{"changed": {"fields": ["description"]}}]	39	3
92	2018-08-23 05:30:23.951517-04	1	Уличное освещение	2	[{"changed": {"fields": ["description_ru"]}}]	39	3
93	2018-08-23 05:31:48.252649-04	1	Уличное освещение	2	[{"changed": {"fields": ["description"]}}]	39	3
94	2018-08-23 05:32:35.078607-04	1	Уличное освещение	2	[{"changed": {"fields": ["description"]}}]	39	3
95	2018-08-23 05:35:00.709525-04	1	Уличное освещение	2	[{"changed": {"fields": ["description", "description_ru"]}}]	39	3
96	2018-08-23 05:36:59.049428-04	1	Уличное освещение	2	[{"changed": {"fields": ["description", "features", "features_kg", "notes", "notes_ru", "notes_kg"]}}]	39	3
97	2018-08-23 05:41:35.388793-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
98	2018-08-23 05:43:28.626085-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
99	2018-08-23 05:45:46.548054-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
100	2018-08-23 05:46:49.784816-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
101	2018-08-23 05:52:44.423402-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
102	2018-08-23 05:57:03.11956-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
103	2018-08-23 06:32:25.17381-04	1	Уличное освещение	2	[{"changed": {"fields": ["description", "description_kg"]}}]	39	3
104	2018-08-23 06:33:06.680495-04	1	Уличное освещение	2	[{"changed": {"fields": ["description_ru"]}}]	39	3
105	2018-08-23 07:00:05.089761-04	2	Декоративное освещение	1	[{"added": {}}]	39	3
106	2018-08-23 07:17:56.993463-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
107	2018-08-23 07:20:48.825972-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
108	2018-08-23 07:21:56.546953-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
109	2018-08-23 07:23:11.769392-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
110	2018-08-23 07:25:12.725452-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
111	2018-08-23 07:26:52.323514-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
112	2018-08-23 07:59:47.386062-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
113	2018-08-23 08:26:50.935192-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
114	2018-08-23 08:28:34.951651-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
115	2018-08-23 08:29:53.243205-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
116	2018-08-23 08:30:39.873943-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
117	2018-08-23 08:32:59.592998-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
118	2018-08-23 08:33:52.976842-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail", "seo_thumbnail"]}}]	39	3
119	2018-08-23 08:37:16.207653-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
120	2018-08-23 08:43:49.239176-04	2	Декоративное освещение	2	[{"changed": {"fields": ["description"]}}]	39	3
121	2018-08-23 08:46:51.353805-04	2	Декоративное освещение	2	[{"changed": {"fields": ["description", "description_ru"]}}]	39	3
122	2018-08-23 09:19:24.55676-04	2	Декоративное освещение	2	[{"changed": {"fields": ["features"]}}]	39	3
123	2018-08-23 09:24:44.164633-04	2	Декоративное освещение	2	[{"changed": {"fields": ["features"]}}]	39	3
124	2018-08-23 09:28:51.567393-04	2	Декоративное освещение	2	[{"changed": {"fields": ["features"]}}]	39	3
125	2018-08-23 09:29:18.203283-04	2	Декоративное освещение	2	[{"changed": {"fields": ["features", "features_ru"]}}]	39	3
126	2018-08-23 09:29:45.433931-04	2	Декоративное освещение	2	[{"changed": {"fields": ["features", "features_ru"]}}]	39	3
127	2018-08-23 09:30:54.165569-04	2	Декоративное освещение	2	[{"changed": {"fields": ["features", "features_ru"]}}]	39	3
128	2018-08-23 09:31:23.129623-04	2	Декоративное освещение	2	[{"changed": {"fields": ["features", "features_ru"]}}]	39	3
129	2018-08-23 09:32:04.615804-04	2	Декоративное освещение	2	[{"changed": {"fields": ["features", "features_ru"]}}]	39	3
130	2018-08-23 09:32:43.034295-04	2	Декоративное освещение	2	[{"changed": {"fields": ["notes"]}}]	39	3
131	2018-08-23 09:33:06.522806-04	2	Декоративное освещение	2	[{"changed": {"fields": ["notes"]}}]	39	3
132	2018-08-23 09:33:29.950411-04	2	Декоративное освещение	2	[{"changed": {"fields": ["notes", "notes_ru"]}}]	39	3
133	2018-08-23 09:36:15.401296-04	3	Праздничное освещение	1	[{"added": {}}]	39	3
134	2018-08-23 09:38:30.600631-04	2	Декоративное освещение	2	[{"changed": {"fields": ["notes_ru"]}}]	39	3
135	2018-08-23 09:49:41.226438-04	1	Светодиодные линейные светильники	2	[{"changed": {"fields": ["title", "title_ru", "title_kg", "description", "description_ru", "description_kg", "main_image"]}}]	36	3
136	2018-08-23 09:50:42.098775-04	1	Светодиодные линейные светильники	2	[]	36	3
137	2018-08-23 09:52:16.825698-04	1	Светодиодные линейные светильники	3		36	3
138	2018-08-23 09:53:51.158639-04	2	Светодиодные линейные светильники	1	[{"added": {}}]	36	3
139	2018-08-23 09:54:47.648607-04	2	Светодиодные линейные светильники	2	[]	36	3
140	2018-08-23 10:03:35.557257-04	2	Светодиодные линейные светильники	2	[{"changed": {"fields": ["main_image"]}}]	36	3
141	2018-08-23 10:04:45.557105-04	2	Светодиодные линейные светильники	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
142	2018-08-23 10:06:19.861773-04	2	Светодиодные линейные светильники	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
143	2018-08-23 10:11:38.255467-04	2	Светодиодные линейные светильники	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
144	2018-08-23 10:12:21.175533-04	2	Светодиодные линейные светильники	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
145	2018-08-23 13:23:52.24127-04	2	Светодиодные линейные светильники	2	[{"deleted": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
146	2018-08-23 13:24:33.085481-04	2	Светодиодные линейные светильники	2	[]	36	3
147	2018-08-24 05:04:00.914778-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
148	2018-08-24 05:05:56.06929-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
149	2018-08-24 05:09:58.055373-04	17	/application/assets/uploads/slider/slide_002_-_prazdnichnoe_osveschenie_2-3_RRJlj7d.wt.png	3		18	3
150	2018-08-24 05:10:43.7289-04	22	/application/assets/uploads/slider/slide_002_-_prazdnichnoe_osveschenie_2.wt.png	1	[{"added": {}}]	18	3
151	2018-08-24 06:19:38.349668-04	22	/application/assets/uploads/slider/slide_002_-_prazdnichnoe_osveschenie_2_oNHBIPj.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
152	2018-08-24 06:21:55.782014-04	21	/application/assets/uploads/slider/slide_007_-_vnutrennee_osveschenie_original.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
153	2018-08-24 06:23:42.654557-04	21	/application/assets/uploads/slider/slide_007_-_vnutrennee_osveschenie_1.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
154	2018-08-24 06:24:39.452748-04	21	/application/assets/uploads/slider/slide_007_-_vnutrennee_osveschenie_original_j0iFzIt.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
155	2018-08-24 06:30:08.731899-04	20	/application/assets/uploads/slider/slide_006_-_raschet_proektov_1.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
156	2018-08-24 06:45:55.300317-04	19	/application/assets/uploads/slider/slide_005_Proizvodstvo_1.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
157	2018-08-24 06:48:52.337347-04	18	/application/assets/uploads/slider/slide_004_-_decorativnoe_osveschenie.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
158	2018-08-24 06:52:01.010893-04	16	/application/assets/uploads/slider/slide_003_-_ulichnoe_osveschenie.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
159	2018-08-24 06:58:24.301707-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
160	2018-08-24 07:00:31.593548-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
161	2018-08-24 07:02:03.204513-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
162	2018-08-24 07:04:10.335101-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
163	2018-08-24 07:37:24.09666-04	1	Уличное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
164	2018-08-24 07:58:05.980995-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
165	2018-08-24 07:58:50.803197-04	2	Декоративное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
166	2018-08-24 08:16:38.323662-04	3	Праздничное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
167	2018-08-24 08:17:42.657653-04	3	Праздничное освещение	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
168	2018-08-25 00:24:00.319071-04	2	Светодиодные линейные светильники	2	[{"deleted": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
169	2018-08-25 00:24:11.020456-04	2	Светодиодные линейные светильники	2	[]	36	3
170	2018-08-25 03:37:45.16092-04	2	Температура свечения	2	[{"changed": {"fields": ["title", "title_ru", "title_kg"]}}]	32	3
171	2018-08-25 03:38:30.318242-04	2	Светодиодные линейные светильники	2	[{"added": {"object": "\\u0422\\u0435\\u043c\\u043f\\u0435\\u0440\\u0430\\u0442\\u0443\\u0440\\u0430 \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
172	2018-08-25 03:39:10.598526-04	3	Входное напряжение	1	[{"added": {}}]	32	3
173	2018-08-25 03:39:34.087803-04	4	Степень пылевлагозащиты	1	[{"added": {}}]	32	3
174	2018-08-25 03:40:32.795929-04	5	Световой поток	1	[{"added": {}}]	32	3
175	2018-08-25 03:40:42.533533-04	6	Размеры	1	[{"added": {}}]	32	3
176	2018-08-25 03:40:51.956411-04	6	Размеры	3		32	3
177	2018-08-25 03:44:44.006111-04	7	Угол свечения	1	[{"added": {}}]	32	3
178	2018-08-25 03:46:14.875084-04	8	Гарантия	1	[{"added": {}}]	32	3
179	2018-08-25 03:50:34.863501-04	2	Светодиодные линейные светильники	2	[{"added": {"object": "\\u0412\\u0445\\u043e\\u0434\\u043d\\u043e\\u0435 \\u043d\\u0430\\u043f\\u0440\\u044f\\u0436\\u0435\\u043d\\u0438\\u0435", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0421\\u0442\\u0435\\u043f\\u0435\\u043d\\u044c \\u043f\\u044b\\u043b\\u0435\\u0432\\u043b\\u0430\\u0433\\u043e\\u0437\\u0430\\u0449\\u0438\\u0442\\u044b", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0421\\u0432\\u0435\\u0442\\u043e\\u0432\\u043e\\u0439 \\u043f\\u043e\\u0442\\u043e\\u043a", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
180	2018-08-25 03:52:16.422774-04	2	Светодиодные линейные светильники	2	[{"added": {"object": "\\u0423\\u0433\\u043e\\u043b \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
181	2018-08-25 03:53:10.463497-04	9	Размер	1	[{"added": {}}]	32	3
182	2018-08-25 03:53:49.222872-04	2	Светодиодные линейные светильники	2	[{"added": {"object": "\\u0420\\u0430\\u0437\\u043c\\u0435\\u0440", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
183	2018-08-25 03:54:40.41299-04	2	Светодиодные линейные светильники	2	[{"changed": {"fields": ["value", "value_ru"], "object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"changed": {"fields": ["value", "value_ru"], "object": "\\u0420\\u0430\\u0437\\u043c\\u0435\\u0440", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
184	2018-08-25 03:55:04.553843-04	2	Светодиодные линейные светильники	2	[{"changed": {"fields": ["key"], "object": "\\u0420\\u0430\\u0437\\u043c\\u0435\\u0440", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"changed": {"fields": ["key"], "object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
185	2018-08-25 03:57:29.357836-04	2	Светодиодные линейные светильники	2	[]	36	3
186	2018-08-25 04:13:40.413645-04	4	Внутреннее освещение	1	[{"added": {}}]	39	3
187	2018-08-25 04:31:15.841752-04	4	Внутреннее освещение	2	[{"changed": {"fields": ["description", "description_ru"]}}]	39	3
188	2018-08-25 04:33:46.708585-04	1	Уличное освещение	2	[{"changed": {"fields": ["description", "description_ru"]}}]	39	3
189	2018-08-25 04:40:38.933898-04	4	Внутреннее освещение	2	[{"changed": {"fields": ["features", "features_ru"]}}]	39	3
190	2018-08-25 04:46:24.761442-04	4	Внутреннее освещение	2	[{"changed": {"fields": ["description_ru", "features", "features_ru"]}}]	39	3
191	2018-08-25 04:49:11.245143-04	4	Внутреннее освещение	2	[{"changed": {"fields": ["description", "description_ru"]}}]	39	3
192	2018-08-25 04:50:27.09448-04	4	Внутреннее освещение	2	[{"changed": {"fields": ["description", "description_ru", "notes", "notes_ru"]}}]	39	3
193	2018-08-25 05:01:54.086832-04	3	Праздничное освещение	2	[{"changed": {"fields": ["description_ru", "features_ru"]}}]	39	3
194	2018-08-25 05:07:36.61153-04	3	Праздничное освещение	2	[{"changed": {"fields": ["description", "description_ru", "notes", "notes_ru"]}}]	39	3
195	2018-08-25 05:12:10.909458-04	1	Уличное освещение	2	[{"changed": {"fields": ["description", "description_ru", "features", "features_ru"]}}]	39	3
196	2018-08-25 05:13:02.641677-04	1	Уличное освещение	2	[{"changed": {"fields": ["notes", "notes_ru"]}}]	39	3
197	2018-08-25 05:14:08.409598-04	1	Уличное освещение	2	[{"changed": {"fields": ["description", "description_ru"]}}]	39	3
198	2018-08-25 05:21:49.860957-04	2	Декоративное освещение	2	[{"changed": {"fields": ["description", "description_ru"]}}]	39	3
199	2018-08-25 05:23:11.500031-04	1	Уличное освещение	2	[{"changed": {"fields": ["description", "description_ru"]}}]	39	3
200	2018-08-25 05:29:32.484012-04	1	Уличное освещение	3		39	3
201	2018-08-25 05:31:10.950174-04	5	Уличное освещение	1	[{"added": {}}]	39	3
202	2018-08-25 06:27:25.265709-04	2	Уличное освещение, г. Бишкек	1	[{"added": {}}]	45	3
203	2018-08-25 06:36:10.873978-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["description", "description_ru"]}}]	45	3
204	2018-08-25 06:47:46.998695-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["description", "description_ru"]}}]	45	3
205	2018-08-25 06:50:06.488859-04	2	Уличное освещение, г. Бишкек	2	[]	45	3
206	2018-08-25 06:50:54.543397-04	2	Уличное освещение, г. Бишкек	2	[]	45	3
207	2018-08-25 06:52:00.363833-04	3	done_works/_01.jpg	1	[{"added": {}}]	46	3
208	2018-08-25 06:52:41.898797-04	2	Уличное освещение, г. Бишкек	2	[{"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
209	2018-08-25 06:54:21.21382-04	4	done_works/_02.jpg	1	[{"added": {}}]	46	3
210	2018-08-25 06:55:02.320513-04	5	done_works/_03.jpg	1	[{"added": {}}]	46	3
211	2018-08-25 06:55:30.320809-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
212	2018-08-25 07:36:15.118791-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["main_image"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
213	2018-08-25 07:39:33.084359-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
214	2018-08-25 07:46:10.36012-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
215	2018-08-25 07:47:17.237307-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
216	2018-08-25 07:47:44.301348-04	6	done_works/DSC_0678-2.jpg	1	[{"added": {}}]	46	3
217	2018-08-25 07:47:48.838696-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
218	2018-08-25 08:09:51.547282-04	7	done_works/DSC_0678-3.jpg	1	[{"added": {}}]	46	3
219	2018-08-25 08:09:54.040446-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
220	2018-08-25 08:14:28.143342-04	8	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_oblozhka.jpg	1	[{"added": {}}]	46	3
221	2018-08-25 08:14:33.406484-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
222	2018-08-25 08:15:09.221701-04	2	Галлерея #[2]	2	[{"changed": {"fields": ["main_image"]}}]	47	3
223	2018-08-25 08:15:41.757333-04	7	done_works/DSC_0678-3.jpg	3		46	3
224	2018-08-25 08:15:41.763356-04	6	done_works/DSC_0678-2.jpg	3		46	3
225	2018-08-25 08:15:41.764443-04	5	done_works/_03.jpg	3		46	3
226	2018-08-25 08:15:41.765456-04	4	done_works/_02.jpg	3		46	3
227	2018-08-25 08:15:41.766434-04	3	done_works/_01.jpg	3		46	3
228	2018-08-25 08:15:41.767466-04	2	done_works/filip-mroz-538852-unsplash.jpg	3		46	3
229	2018-08-25 08:15:41.768588-04	1	done_works/slide1.jpg	3		46	3
230	2018-08-25 08:27:48.03731-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
231	2018-08-25 08:39:29.420857-04	9	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_01.jpg	1	[{"added": {}}]	46	3
232	2018-08-25 08:39:34.657618-04	10	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_01-1.jpg	1	[{"added": {}}]	46	3
233	2018-08-25 08:39:37.331638-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
234	2018-08-25 08:50:58.812339-04	10	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_01-1.jpg	3		46	3
235	2018-08-25 08:50:58.817269-04	9	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_01.jpg	3		46	3
236	2018-08-25 08:50:58.818285-04	8	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_oblozhka.jpg	3		46	3
237	2018-08-25 08:51:45.364503-04	11	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_01_mTT6m0l.jpg	1	[{"added": {}}]	46	3
238	2018-08-25 08:51:53.60065-04	12	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_02.jpg	1	[{"added": {}}]	46	3
239	2018-08-25 08:52:04.188559-04	13	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_03.jpg	1	[{"added": {}}]	46	3
240	2018-08-25 08:52:09.821064-04	14	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_04.jpg	1	[{"added": {}}]	46	3
241	2018-08-25 08:52:15.58986-04	15	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_05.jpg	1	[{"added": {}}]	46	3
242	2018-08-25 08:52:22.52126-04	16	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_06.jpg	1	[{"added": {}}]	46	3
243	2018-08-25 08:52:28.759305-04	17	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_07.jpg	1	[{"added": {}}]	46	3
244	2018-08-25 08:52:34.6879-04	18	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_08.jpg	1	[{"added": {}}]	46	3
245	2018-08-25 08:52:40.659406-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
246	2018-08-25 08:54:27.057172-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
247	2018-08-25 08:55:06.418883-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
248	2018-08-25 08:55:41.854124-04	15	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_05.jpg	3		46	3
249	2018-08-25 08:55:41.856065-04	14	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_04.jpg	3		46	3
250	2018-08-25 08:55:41.857045-04	12	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_02.jpg	3		46	3
251	2018-08-25 09:02:44.698687-04	19	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_02_iFgfYxm.jpg	1	[{"added": {}}]	46	3
252	2018-08-25 09:03:40.443002-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
253	2018-08-25 09:08:28.371604-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
254	2018-08-25 09:08:38.389921-04	13	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_03.jpg	3		46	3
255	2018-08-25 09:08:51.724225-04	20	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_03_nFiMf7k.jpg	1	[{"added": {}}]	46	3
256	2018-08-25 09:08:53.389276-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
257	2018-08-25 09:11:38.561701-04	21	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_04_ed53H5k.jpg	1	[{"added": {}}]	46	3
258	2018-08-25 09:11:40.087216-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
259	2018-08-25 09:15:38.039291-04	22	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_05_Wk2AS9m.jpg	1	[{"added": {}}]	46	3
260	2018-08-25 09:15:39.313056-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
261	2018-08-25 09:26:04.971336-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
262	2018-08-25 09:26:15.294486-04	22	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_05_Wk2AS9m.jpg	3		46	3
263	2018-08-25 09:26:29.983722-04	23	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_05_LSaLqsu.jpg	1	[{"added": {}}]	46	3
264	2018-08-25 09:26:31.170758-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
265	2018-08-25 09:30:16.874754-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
266	2018-08-25 09:30:26.955656-04	16	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_06.jpg	3		46	3
267	2018-08-25 09:30:39.320797-04	24	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_06_4Rleei7.jpg	1	[{"added": {}}]	46	3
268	2018-08-25 09:30:42.401427-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
328	2018-08-28 02:47:46.7064-04	4	Светодиодные настенные светильники	2	[{"deleted": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
269	2018-08-25 09:33:06.931075-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
270	2018-08-25 09:33:15.528937-04	17	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_07.jpg	3		46	3
271	2018-08-25 09:33:25.774853-04	25	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_07_QAB7fNh.jpg	1	[{"added": {}}]	46	3
272	2018-08-25 09:33:26.996283-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
273	2018-08-25 09:34:39.28828-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
274	2018-08-25 09:34:48.938167-04	19	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_02_iFgfYxm.jpg	3		46	3
275	2018-08-25 09:36:12.662325-04	26	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_oblozhka_strk9rG.jpg	1	[{"added": {}}]	46	3
276	2018-08-25 09:36:14.172592-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
277	2018-08-25 09:38:41.613586-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
278	2018-08-25 09:38:49.286221-04	21	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_04_ed53H5k.jpg	3		46	3
279	2018-08-25 09:39:01.771238-04	27	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_04_H5iRLMo.jpg	1	[{"added": {}}]	46	3
280	2018-08-25 09:39:04.285958-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
281	2018-08-25 09:44:42.428268-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
282	2018-08-25 09:44:50.705093-04	27	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_04_H5iRLMo.jpg	3		46	3
283	2018-08-25 09:45:02.980948-04	28	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_04_T8OFpy7.jpg	1	[{"added": {}}]	46	3
284	2018-08-25 09:45:04.294614-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
285	2018-08-25 09:47:40.738672-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
286	2018-08-25 09:47:48.863791-04	18	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_08.jpg	3		46	3
287	2018-08-25 09:47:59.450239-04	29	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_08_7eSYqUN.jpg	1	[{"added": {}}]	46	3
288	2018-08-25 09:48:00.722396-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
289	2018-08-25 09:53:18.457677-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
290	2018-08-25 09:53:27.583522-04	23	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_05_LSaLqsu.jpg	3		46	3
291	2018-08-25 09:53:38.511594-04	30	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_05_zWZS0eT.jpg	1	[{"added": {}}]	46	3
292	2018-08-25 09:53:40.034769-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
293	2018-08-25 10:09:40.891411-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
294	2018-08-25 10:09:49.109182-04	20	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_03_nFiMf7k.jpg	3		46	3
295	2018-08-25 10:10:00.822549-04	31	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_03_1P8Jeaz.jpg	1	[{"added": {}}]	46	3
296	2018-08-25 10:10:02.323785-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
297	2018-08-25 10:14:24.998785-04	32	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_02_wkjSDya.jpg	1	[{"added": {}}]	46	3
298	2018-08-25 10:14:26.371186-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
299	2018-08-25 10:31:14.499409-04	3	Светодиодные магистральные светильники	1	[{"added": {}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "\\u0422\\u0435\\u043c\\u043f\\u0435\\u0440\\u0430\\u0442\\u0443\\u0440\\u0430 \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
300	2018-08-25 10:31:42.477259-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
301	2018-08-25 10:32:33.220979-04	3	Светодиодные магистральные светильники	2	[{"changed": {"fields": ["main_image"]}}]	36	3
302	2018-08-27 07:48:37.736045-04	16	/application/assets/uploads/slider/slide_slide_003_-_ulichnoe_osveschenie.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
303	2018-08-27 07:48:44.034099-04	18	/application/assets/uploads/slider/slide_slide_004_-_decorativnoe_osveschenie.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
304	2018-08-27 07:48:53.581501-04	19	/application/assets/uploads/slider/slide_slide_005_Proizvodstvo_1.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
305	2018-08-27 07:49:07.710944-04	20	/application/assets/uploads/slider/slide_slide_006_-_raschet_proektov_1.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
306	2018-08-27 07:49:16.5657-04	21	/application/assets/uploads/slider/slide_slide_007_-_vnutrennee_osveschenie_original_j0iFzIt.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
307	2018-08-27 07:49:20.533708-04	22	/application/assets/uploads/slider/slide_slide_002_-_prazdnichnoe_osveschenie_2_oNHBIPj.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
308	2018-08-27 08:01:22.846518-04	22	/application/assets/uploads/slider/slide_slide_slide_002_-_prazdnichnoe_osveschenie_2_oNHBIPj.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
309	2018-08-27 08:28:26.827382-04	1	Главный офис	2	[{"changed": {"fields": ["email", "phone", "location"]}}]	16	3
310	2018-08-27 08:29:17.770327-04	1	Главный офис	2	[]	16	3
311	2018-08-27 08:43:05.303492-04	9	Гарантия	2	[{"changed": {"fields": ["title", "title_ru"]}}]	32	3
312	2018-08-27 08:43:26.833747-04	8	Размер	2	[{"changed": {"fields": ["title", "title_ru"]}}]	32	3
313	2018-08-27 08:44:05.351734-04	2	Светодиодные линейные светильники	2	[{"changed": {"fields": ["value", "value_ru"], "object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"changed": {"fields": ["value", "value_ru"], "object": "\\u0420\\u0430\\u0437\\u043c\\u0435\\u0440", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
314	2018-08-27 08:45:30.197597-04	2	Светодиодные линейные светильники	2	[{"changed": {"fields": ["key", "value", "value_ru"], "object": "\\u0420\\u0430\\u0437\\u043c\\u0435\\u0440", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"changed": {"fields": ["key", "value", "value_ru"], "object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
315	2018-08-27 10:06:50.914451-04	3	Освещение Бизнес- Центра, г. Бишкек	1	[{"added": {}}]	45	3
316	2018-08-27 10:14:05.976495-04	33	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_00.jpg	1	[{"added": {}}]	46	3
317	2018-08-27 10:14:13.540686-04	34	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_01.jpg	1	[{"added": {}}]	46	3
318	2018-08-27 10:14:19.784984-04	35	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_02.jpg	1	[{"added": {}}]	46	3
319	2018-08-27 10:14:21.528615-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[3]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
320	2018-08-28 01:48:15.539043-04	4	Светодиодные настенные светильники	1	[{"added": {}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
321	2018-08-28 01:49:07.266536-04	4	Светодиодные настенные светильники	2	[{"added": {"object": "\\u0422\\u0435\\u043c\\u043f\\u0435\\u0440\\u0430\\u0442\\u0443\\u0440\\u0430 \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
322	2018-08-28 01:49:23.182215-04	4	Светодиодные настенные светильники	2	[{"changed": {"fields": ["main_image"]}}]	36	3
323	2018-08-28 01:51:58.190361-04	10	Мощность	1	[{"added": {}}]	32	3
324	2018-08-28 01:52:13.481029-04	9	Мощность	2	[{"changed": {"fields": ["title", "title_ru"]}}]	32	3
325	2018-08-28 01:52:24.87776-04	10	Гарантия	2	[{"changed": {"fields": ["title", "title_ru"]}}]	32	3
326	2018-08-28 01:56:51.559179-04	4	Светодиодные настенные светильники	2	[{"added": {"object": "\\u0412\\u0445\\u043e\\u0434\\u043d\\u043e\\u0435 \\u043d\\u0430\\u043f\\u0440\\u044f\\u0436\\u0435\\u043d\\u0438\\u0435", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0421\\u0442\\u0435\\u043f\\u0435\\u043d\\u044c \\u043f\\u044b\\u043b\\u0435\\u0432\\u043b\\u0430\\u0433\\u043e\\u0437\\u0430\\u0449\\u0438\\u0442\\u044b", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0421\\u0432\\u0435\\u0442\\u043e\\u0432\\u043e\\u0439 \\u043f\\u043e\\u0442\\u043e\\u043a", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0423\\u0433\\u043e\\u043b \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0420\\u0430\\u0437\\u043c\\u0435\\u0440", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u041c\\u043e\\u0449\\u043d\\u043e\\u0441\\u0442\\u044c", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
327	2018-08-28 02:47:31.702801-04	4	Светодиодные настенные светильники	2	[{"changed": {"fields": ["main_image"]}}]	36	3
329	2018-08-28 02:48:32.015721-04	4	Светодиодные настенные светильники	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
330	2018-08-28 03:00:20.340285-04	4	Фасадное освещение гостиничного комплекса	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
331	2018-08-28 03:20:05.217871-04	36	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_01.jpg	1	[{"added": {}}]	46	3
332	2018-08-28 03:20:15.679524-04	4	Фасадное освещение гостиничного комплекса	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
333	2018-08-28 03:31:51.499035-04	37	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_02.jpg	1	[{"added": {}}]	46	3
334	2018-08-28 03:31:58.131107-04	38	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_03.jpg	1	[{"added": {}}]	46	3
335	2018-08-28 03:32:01.136427-04	4	Фасадное освещение гостиничного комплекса	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
336	2018-08-28 05:40:36.02198-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["title", "title_ru", "created_at"]}}]	45	3
337	2018-08-28 08:14:27.811629-04	39	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_01.jpg	1	[{"added": {}}]	46	3
338	2018-08-28 08:14:49.116908-04	5	Фасадное освещение ЖК "Монреаль"	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[5]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
339	2018-08-28 08:17:01.463309-04	40	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_01-1.jpg	1	[{"added": {}}]	46	3
340	2018-08-28 08:17:03.110272-04	5	Фасадное освещение ЖК "Монреаль"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[5]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
341	2018-08-28 08:18:53.362895-04	5	Фасадное освещение ЖК "Монреаль"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[5]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
342	2018-08-28 08:25:59.556314-04	41	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_02.jpg	1	[{"added": {}}]	46	3
343	2018-08-28 08:26:06.577971-04	42	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_03.jpg	1	[{"added": {}}]	46	3
344	2018-08-28 08:26:07.597146-04	5	Фасадное освещение ЖК "Монреаль"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[5]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
345	2018-08-28 09:13:16.545137-04	43	done_works/003-1.jpg	1	[{"added": {}}]	46	3
346	2018-08-28 09:13:49.492212-04	6	Наружнее освещение супермаркетов "Глобус"	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[6]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
347	2018-08-28 09:27:48.764606-04	44	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_01.jpg	1	[{"added": {}}]	46	3
348	2018-08-28 09:27:49.890754-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[6]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
349	2018-08-28 09:37:04.101163-04	45	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_02.jpg	1	[{"added": {}}]	46	3
350	2018-08-28 09:37:10.396527-04	46	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_03.jpg	1	[{"added": {}}]	46	3
351	2018-08-28 09:37:16.239394-04	47	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_04.jpg	1	[{"added": {}}]	46	3
352	2018-08-28 09:37:22.911637-04	48	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_05.jpg	1	[{"added": {}}]	46	3
353	2018-08-28 09:37:23.948967-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[6]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
354	2018-08-28 09:57:46.332842-04	49	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_01.jpg	1	[{"added": {}}]	46	3
355	2018-08-28 09:57:49.606868-04	7	Освещение супермаркета "Боорсок", г. Бишкек	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[7]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
356	2018-08-28 09:59:20.160298-04	50	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_01-1.jpg	1	[{"added": {}}]	46	3
415	2018-08-29 07:27:26.687298-04	71	done_works/Proekt_Ala-Too_Novyi_god_2017_-_04.jpg	1	[{"added": {}}]	46	3
510	2018-08-30 08:41:13.163689-04	118	done_works/Proekt_U.Salieva_-_04_QIst3y3.jpg	1	[{"added": {}}]	46	3
357	2018-08-28 09:59:22.199808-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[7]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
358	2018-08-28 10:00:04.904445-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[7]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
359	2018-08-28 10:04:48.707189-04	51	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_02.jpg	1	[{"added": {}}]	46	3
360	2018-08-28 10:04:50.245008-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[7]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
361	2018-08-28 10:06:31.782865-04	52	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_02-1.jpg	1	[{"added": {}}]	46	3
362	2018-08-28 10:06:33.274052-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[7]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
363	2018-08-28 10:08:12.150478-04	53	done_works/1-1.jpg	1	[{"added": {}}]	46	3
364	2018-08-28 10:08:19.348698-04	54	done_works/2-1.jpg	1	[{"added": {}}]	46	3
365	2018-08-28 10:08:21.326739-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[7]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
366	2018-08-28 10:09:20.6581-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[7]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
367	2018-08-28 10:19:18.659115-04	55	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_01_9i20QRy.jpg	1	[{"added": {}}]	46	3
368	2018-08-28 10:19:24.821625-04	56	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_02_yBdXfop.jpg	1	[{"added": {}}]	46	3
369	2018-08-28 10:19:30.209353-04	57	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_03.jpg	1	[{"added": {}}]	46	3
370	2018-08-28 10:19:36.056974-04	58	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_04.jpg	1	[{"added": {}}]	46	3
371	2018-08-28 10:19:41.102734-04	59	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_05.jpg	1	[{"added": {}}]	46	3
372	2018-08-28 10:19:42.304868-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[7]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
373	2018-08-28 10:22:37.899246-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
374	2018-08-28 10:40:50.850964-04	5	Светодиодные лампы	1	[{"added": {}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "\\u0422\\u0435\\u043c\\u043f\\u0435\\u0440\\u0430\\u0442\\u0443\\u0440\\u0430 \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
375	2018-08-28 10:42:10.843008-04	5	Светодиодные лампы	2	[{"deleted": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
376	2018-08-28 10:42:21.016582-04	5	Светодиодные лампы	2	[{"changed": {"fields": ["main_image"]}}]	36	3
377	2018-08-28 10:43:12.29314-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
378	2018-08-28 10:43:23.194469-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}]	45	3
379	2018-08-28 10:48:25.047848-04	2	sales@technology.kg	1	[{"added": {}}]	10	3
380	2018-08-28 10:48:29.535703-04	1	okay11007@gmail.com	2	[{"changed": {"fields": ["is_active"]}}]	10	3
381	2018-08-28 10:49:14.850614-04	1	Настройка сайтов	2	[{"changed": {"fields": ["phone"]}}]	25	3
382	2018-08-29 03:53:47.996202-04	6	Светодиодные декорации	1	[{"added": {}}]	36	3
412	2018-08-29 07:21:42.801408-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
414	2018-08-29 07:23:36.918639-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
545	2018-08-30 10:36:15.527715-04	137	done_works/Proekt_Park_T.Moldo_-_03.jpg	1	[{"added": {}}]	46	3
383	2018-08-29 03:56:38.549179-04	6	Светодиодные декорации	2	[{"added": {"object": "\\u0422\\u0435\\u043c\\u043f\\u0435\\u0440\\u0430\\u0442\\u0443\\u0440\\u0430 \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0412\\u0445\\u043e\\u0434\\u043d\\u043e\\u0435 \\u043d\\u0430\\u043f\\u0440\\u044f\\u0436\\u0435\\u043d\\u0438\\u0435", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0421\\u0442\\u0435\\u043f\\u0435\\u043d\\u044c \\u043f\\u044b\\u043b\\u0435\\u0432\\u043b\\u0430\\u0433\\u043e\\u0437\\u0430\\u0449\\u0438\\u0442\\u044b", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0421\\u0432\\u0435\\u0442\\u043e\\u0432\\u043e\\u0439 \\u043f\\u043e\\u0442\\u043e\\u043a", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0423\\u0433\\u043e\\u043b \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0420\\u0430\\u0437\\u043c\\u0435\\u0440", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u041c\\u043e\\u0449\\u043d\\u043e\\u0441\\u0442\\u044c", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
384	2018-08-29 03:56:58.16084-04	6	Светодиодные декорации	2	[{"changed": {"fields": ["main_image"]}}]	36	3
385	2018-08-29 03:57:31.963558-04	6	Светодиодные декорации	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
386	2018-08-29 03:58:55.008962-04	6	Светодиодные декорации	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
387	2018-08-29 04:24:17.733722-04	6	Светодиодные декорации	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
388	2018-08-29 04:24:28.234854-04	6	Светодиодные декорации	2	[{"changed": {"fields": ["main_image"]}}]	36	3
389	2018-08-29 04:30:26.176553-04	6	Светодиодные декорации	2	[{"changed": {"fields": ["image"], "object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
390	2018-08-29 04:30:34.129961-04	6	Светодиодные декорации	2	[{"changed": {"fields": ["main_image"]}}]	36	3
391	2018-08-29 04:38:13.142142-04	6	Светодиодные декорации	2	[{"changed": {"fields": ["main_image"]}}]	36	3
392	2018-08-29 04:38:25.776008-04	6	Светодиодные декорации	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
393	2018-08-29 05:33:12.002908-04	6	Светодиодные декорации	2	[{"deleted": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"deleted": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
394	2018-08-29 05:33:41.34662-04	6	Светодиодные декорации	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
395	2018-08-29 05:33:50.480569-04	6	Светодиодные декорации	2	[{"changed": {"fields": ["main_image"]}}]	36	3
396	2018-08-29 05:43:29.895549-04	60	done_works/001.jpg	1	[{"added": {}}]	46	3
397	2018-08-29 05:43:49.739087-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[8]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
398	2018-08-29 06:19:57.321098-04	61	done_works/Proekt_Ala-Too_Novyi_god_2018_-_01.jpg	1	[{"added": {}}]	46	3
399	2018-08-29 06:20:06.338322-04	62	done_works/Proekt_Ala-Too_Novyi_god_2018_-_02.jpg	1	[{"added": {}}]	46	3
400	2018-08-29 06:20:11.922329-04	63	done_works/Proekt_Ala-Too_Novyi_god_2018_-_03.jpg	1	[{"added": {}}]	46	3
401	2018-08-29 06:20:17.635117-04	64	done_works/Proekt_Ala-Too_Novyi_god_2018_-_04.jpg	1	[{"added": {}}]	46	3
402	2018-08-29 06:20:23.500007-04	65	done_works/Proekt_Ala-Too_Novyi_god_2018_-_05.jpg	1	[{"added": {}}]	46	3
403	2018-08-29 06:20:29.026185-04	66	done_works/Proekt_Ala-Too_Novyi_god_2018_-_06.jpg	1	[{"added": {}}]	46	3
404	2018-08-29 06:20:30.177801-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[8]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
405	2018-08-29 06:23:46.720418-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
406	2018-08-29 06:48:32.040149-04	67	done_works/Proekt_Ala-Too_Noryz_2018_-_01.jpg	1	[{"added": {}}]	46	3
407	2018-08-29 06:48:35.475713-04	9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г,	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[9]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
408	2018-08-29 06:49:40.982495-04	9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
409	2018-08-29 07:03:34.428916-04	68	done_works/Proekt_Ala-Too_Novyi_god_2017_-_01.jpg	1	[{"added": {}}]	46	3
410	2018-08-29 07:03:35.928118-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
411	2018-08-29 07:21:34.646945-04	69	done_works/Proekt_Ala-Too_Novyi_god_2017_-_02.jpg	1	[{"added": {}}]	46	3
413	2018-08-29 07:23:35.531539-04	70	done_works/Proekt_Ala-Too_Novyi_god_2017_-_03.jpg	1	[{"added": {}}]	46	3
416	2018-08-29 07:27:27.834855-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
417	2018-08-29 07:28:38.975726-04	72	done_works/Proekt_Ala-Too_Novyi_god_2017_-_05.jpg	1	[{"added": {}}]	46	3
418	2018-08-29 07:28:40.774137-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
419	2018-08-29 07:34:02.95179-04	73	done_works/Proekt_Ala-Too_Novyi_god_2017_-_06.jpg	1	[{"added": {}}]	46	3
420	2018-08-29 07:34:04.134271-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
421	2018-08-29 07:35:23.816916-04	74	done_works/Proekt_Ala-Too_Novyi_god_2017_-_07.jpg	1	[{"added": {}}]	46	3
422	2018-08-29 07:35:25.021378-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
423	2018-08-29 07:37:01.898348-04	75	done_works/Proekt_Ala-Too_Novyi_god_2017_-_08.jpg	1	[{"added": {}}]	46	3
424	2018-08-29 07:37:06.983638-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
425	2018-08-29 08:18:33.89229-04	76	done_works/Proekt_Ala-Too_Noryz_2018_-_02.jpg	1	[{"added": {}}]	46	3
426	2018-08-29 08:18:44.226852-04	77	done_works/Proekt_Ala-Too_Noryz_2018_-_03.jpg	1	[{"added": {}}]	46	3
427	2018-08-29 08:18:50.564978-04	78	done_works/Proekt_Ala-Too_Noryz_2018_-_04.jpg	1	[{"added": {}}]	46	3
428	2018-08-29 08:18:55.743942-04	79	done_works/Proekt_Ala-Too_Noryz_2018_-_05.jpg	1	[{"added": {}}]	46	3
429	2018-08-29 08:19:02.035818-04	80	done_works/Proekt_Ala-Too_Noryz_2018_-_06.jpg	1	[{"added": {}}]	46	3
430	2018-08-29 08:19:07.347789-04	81	done_works/Proekt_Ala-Too_Noryz_2018_-_07.jpg	1	[{"added": {}}]	46	3
431	2018-08-29 08:19:12.989256-04	82	done_works/Proekt_Ala-Too_Noryz_2018_-_08.jpg	1	[{"added": {}}]	46	3
432	2018-08-29 08:19:27.933791-04	83	done_works/Proekt_Ala-Too_Noryz_2018_-_01_IU94XDq.jpg	1	[{"added": {}}]	46	3
433	2018-08-29 08:19:29.060895-04	9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[9]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
434	2018-08-29 08:39:42.913219-04	84	done_works/Proekt_Ala-Too_Novyi_god_2016_-_01.JPG	1	[{"added": {}}]	46	3
435	2018-08-29 08:39:48.082228-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[11]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
436	2018-08-29 08:43:54.19975-04	85	done_works/Proekt_Ala-Too_Novyi_god_2017_-_04_xMp29qb.jpg	1	[{"added": {}}]	46	3
437	2018-08-29 08:43:56.012161-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
438	2018-08-29 08:48:12.665151-04	86	done_works/Proekt_Ala-Too_Novyi_god_2017_-_04-1.jpg	1	[{"added": {}}]	46	3
439	2018-08-29 08:48:46.131624-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
440	2018-08-29 08:51:28.724687-04	87	done_works/Proekt_Ala-Too_Novyi_god_2016_-_02.jpg	1	[{"added": {}}]	46	3
441	2018-08-29 08:51:30.264624-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[11]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
442	2018-08-29 08:51:41.147424-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
443	2018-08-29 09:10:57.559466-04	88	done_works/Proekt_Ala-Too_Novyi_god_2016_-_02_osl8qrP.jpg	1	[{"added": {}}]	46	3
444	2018-08-29 09:11:02.981604-04	89	done_works/Proekt_Ala-Too_Novyi_god_2016_-_03.jpg	1	[{"added": {}}]	46	3
445	2018-08-29 09:11:08.938089-04	90	done_works/Proekt_Ala-Too_Novyi_god_2016_-_04.jpg	1	[{"added": {}}]	46	3
446	2018-08-29 09:11:22.965293-04	91	done_works/Proekt_Ala-Too_Novyi_god_2016_-_05.jpg	1	[{"added": {}}]	46	3
447	2018-08-29 09:11:28.684242-04	92	done_works/Proekt_Ala-Too_Novyi_god_2016_-_06.jpg	1	[{"added": {}}]	46	3
448	2018-08-29 09:11:33.620834-04	93	done_works/Proekt_Ala-Too_Novyi_god_2016_-_01_xzhUYE4.JPG	1	[{"added": {}}]	46	3
449	2018-08-29 09:11:35.390983-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[11]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
450	2018-08-29 09:26:49.898734-04	94	done_works/Proekt_Aska-_Tash_01.jpg	1	[{"added": {}}]	46	3
451	2018-08-29 09:26:51.344279-04	12	декоративная подсветка стелы Аска- Таш, г. Бишкек	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[12]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
452	2018-08-29 09:34:53.637642-04	95	done_works/Proekt_Aska-_Tash_02.jpg	1	[{"added": {}}]	46	3
453	2018-08-29 09:34:58.844958-04	96	done_works/Proekt_Aska-_Tash_03.jpg	1	[{"added": {}}]	46	3
454	2018-08-29 09:35:05.065534-04	97	done_works/Proekt_Aska-_Tash_04.jpg	1	[{"added": {}}]	46	3
455	2018-08-29 09:35:10.074214-04	98	done_works/Proekt_Aska-_Tash_05.jpg	1	[{"added": {}}]	46	3
456	2018-08-29 09:35:16.324259-04	99	done_works/Proekt_Aska-_Tash_01_drLcZqC.jpg	1	[{"added": {}}]	46	3
457	2018-08-29 09:35:17.617925-04	12	декоративная подсветка стелы Аска- Таш, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[12]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
458	2018-08-30 00:12:38.023693-04	20	/application/assets/uploads/slider/slide_slide_slide_006_-_raschet_proektov_1.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
459	2018-08-30 00:13:20.757794-04	20	/application/assets/uploads/slider/slide_slide_slide_slide_006_-_raschet_proektov_1.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
460	2018-08-30 00:13:47.363939-04	20	/application/assets/uploads/slider/slide_slide_slide_slide_slide_006_-_raschet_proektov_1.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
461	2018-08-30 04:18:03.801835-04	7	Светодиодные прожекторы	1	[{"added": {}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "\\u0422\\u0435\\u043c\\u043f\\u0435\\u0440\\u0430\\u0442\\u0443\\u0440\\u0430 \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
462	2018-08-30 04:18:50.103098-04	7	Светодиодные прожекторы	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
463	2018-08-30 04:20:09.326867-04	7	Светодиодные прожекторы	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
464	2018-08-30 04:20:55.463234-04	7	Светодиодные прожекторы	2	[]	36	3
465	2018-08-30 04:23:41.077689-04	7	Светодиодные прожекторы	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
466	2018-08-30 04:24:46.171283-04	7	Светодиодные прожекторы	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
467	2018-08-30 04:25:16.824872-04	7	Светодиодные прожекторы	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
468	2018-08-30 04:26:51.028474-04	7	Светодиодные прожекторы	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
469	2018-08-30 04:27:18.425896-04	7	Светодиодные прожекторы	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
470	2018-08-30 04:28:44.8198-04	7	Светодиодные прожекторы	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
471	2018-08-30 04:29:55.496603-04	7	Светодиодные прожекторы	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
472	2018-08-30 04:32:54.557746-04	7	Светодиодные прожекторы	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "\\u0412\\u0445\\u043e\\u0434\\u043d\\u043e\\u0435 \\u043d\\u0430\\u043f\\u0440\\u044f\\u0436\\u0435\\u043d\\u0438\\u0435", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0421\\u0442\\u0435\\u043f\\u0435\\u043d\\u044c \\u043f\\u044b\\u043b\\u0435\\u0432\\u043b\\u0430\\u0433\\u043e\\u0437\\u0430\\u0449\\u0438\\u0442\\u044b", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0421\\u0432\\u0435\\u0442\\u043e\\u0432\\u043e\\u0439 \\u043f\\u043e\\u0442\\u043e\\u043a", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0423\\u0433\\u043e\\u043b \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u041c\\u043e\\u0449\\u043d\\u043e\\u0441\\u0442\\u044c", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
473	2018-08-30 04:36:15.988454-04	12	декоративная подсветка стелы Аска- Таш, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
474	2018-08-30 04:36:34.076478-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
475	2018-08-30 04:36:52.840107-04	5	Фасадное освещение ЖК "Монреаль"	2	[{"changed": {"fields": ["created_at"]}}]	45	3
476	2018-08-30 06:10:26.447966-04	8	Светодиодные трековые светильники	1	[{"added": {}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
506	2018-08-30 08:33:22.00266-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
508	2018-08-30 08:37:20.779778-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
546	2018-08-30 10:36:20.328397-04	138	done_works/Proekt_Park_T.Moldo_-_04.jpg	1	[{"added": {}}]	46	3
477	2018-08-30 06:12:40.81733-04	8	Светодиодные трековые светильники	2	[{"added": {"object": "\\u0422\\u0435\\u043c\\u043f\\u0435\\u0440\\u0430\\u0442\\u0443\\u0440\\u0430 \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0412\\u0445\\u043e\\u0434\\u043d\\u043e\\u0435 \\u043d\\u0430\\u043f\\u0440\\u044f\\u0436\\u0435\\u043d\\u0438\\u0435", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0421\\u0442\\u0435\\u043f\\u0435\\u043d\\u044c \\u043f\\u044b\\u043b\\u0435\\u0432\\u043b\\u0430\\u0433\\u043e\\u0437\\u0430\\u0449\\u0438\\u0442\\u044b", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0421\\u0432\\u0435\\u0442\\u043e\\u0432\\u043e\\u0439 \\u043f\\u043e\\u0442\\u043e\\u043a", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0423\\u0433\\u043e\\u043b \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u041c\\u043e\\u0449\\u043d\\u043e\\u0441\\u0442\\u044c", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"added": {"object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
478	2018-08-30 06:13:09.991454-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
479	2018-08-30 06:54:25.896377-04	100	done_works/Proekt_Flaf_KG_-_04.jpg	1	[{"added": {}}]	46	3
480	2018-08-30 06:55:08.123664-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[13]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
481	2018-08-30 06:55:41.236536-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	2	[{"changed": {"fields": ["service", "created_at"]}}]	45	3
482	2018-08-30 06:58:38.733634-04	101	done_works/Proekt_Flaf_KG_-_00.jpg	1	[{"added": {}}]	46	3
483	2018-08-30 07:00:38.160095-04	102	done_works/Proekt_Flaf_KG_-_01.jpg	1	[{"added": {}}]	46	3
484	2018-08-30 07:00:39.674891-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[13]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
485	2018-08-30 07:08:25.168046-04	103	done_works/Proekt_Flaf_KG_-_01_6tfgnjM.jpg	1	[{"added": {}}]	46	3
486	2018-08-30 07:08:26.290894-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[13]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
487	2018-08-30 07:09:37.605296-04	104	done_works/Proekt_Flaf_KG_-_01_2GTXSTN.jpg	1	[{"added": {}}]	46	3
488	2018-08-30 07:09:38.65606-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[13]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
489	2018-08-30 07:52:47.083528-04	105	done_works/Proekt_Flaf_KG_-_02.jpg	1	[{"added": {}}]	46	3
490	2018-08-30 07:52:53.48536-04	106	done_works/Proekt_Flaf_KG_-_04_0U9uiKb.jpg	1	[{"added": {}}]	46	3
491	2018-08-30 07:53:01.402294-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[13]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
492	2018-08-30 08:07:12.650354-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
493	2018-08-30 08:29:09.054568-04	107	done_works/Proekt_U.Salieva_-_01.jpg	1	[{"added": {}}]	46	3
494	2018-08-30 08:29:10.554589-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
495	2018-08-30 08:29:45.271385-04	108	done_works/Proekt_U.Salieva_-_02.jpg	1	[{"added": {}}]	46	3
496	2018-08-30 08:29:51.122786-04	109	done_works/Proekt_U.Salieva_-_03.jpg	1	[{"added": {}}]	46	3
497	2018-08-30 08:29:56.619444-04	110	done_works/Proekt_U.Salieva_-_04.jpg	1	[{"added": {}}]	46	3
498	2018-08-30 08:30:01.419728-04	111	done_works/Proekt_U.Salieva_-_05.jpg	1	[{"added": {}}]	46	3
499	2018-08-30 08:30:03.627158-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
500	2018-08-30 08:31:04.375346-04	112	done_works/Proekt_U.Salieva_-_02_jlWC93u.jpg	1	[{"added": {}}]	46	3
501	2018-08-30 08:31:06.049796-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
502	2018-08-30 08:31:56.734615-04	113	done_works/Proekt_U.Salieva_-_03_pNQSDLN.jpg	1	[{"added": {}}]	46	3
503	2018-08-30 08:32:02.407924-04	114	done_works/Proekt_U.Salieva_-_03-1.jpg	1	[{"added": {}}]	46	3
504	2018-08-30 08:32:03.613301-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
505	2018-08-30 08:33:21.089606-04	115	done_works/Proekt_U.Salieva_-_03-2.jpg	1	[{"added": {}}]	46	3
507	2018-08-30 08:37:19.064576-04	116	done_works/Proekt_U.Salieva_-_03-3.jpg	1	[{"added": {}}]	46	3
509	2018-08-30 08:41:07.537721-04	117	done_works/Proekt_U.Salieva_-_03_wBfeHNV.jpg	1	[{"added": {}}]	46	3
511	2018-08-30 08:41:16.351282-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
512	2018-08-30 08:43:14.699517-04	119	done_works/Proekt_U.Salieva_-_04-2.jpg	1	[{"added": {}}]	46	3
513	2018-08-30 08:43:15.871066-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
514	2018-08-30 08:45:12.731472-04	120	done_works/Proekt_U.Salieva_-_01_yS7wIk1.jpg	1	[{"added": {}}]	46	3
515	2018-08-30 08:45:18.19708-04	121	done_works/Proekt_U.Salieva_-_01-1.jpg	1	[{"added": {}}]	46	3
516	2018-08-30 08:45:19.385465-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
517	2018-08-30 08:47:06.305183-04	122	done_works/Proekt_U.Salieva_-_02_yp9m8uh.jpg	1	[{"added": {}}]	46	3
518	2018-08-30 08:47:11.659013-04	123	done_works/Proekt_U.Salieva_-_02-1.jpg	1	[{"added": {}}]	46	3
519	2018-08-30 08:47:12.77784-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
520	2018-08-30 08:50:40.075369-04	124	done_works/Proekt_U.Salieva_-_01_muMEbEU.jpg	1	[{"added": {}}]	46	3
521	2018-08-30 08:50:47.222634-04	125	done_works/Proekt_U.Salieva_-_02_008pyP5.jpg	1	[{"added": {}}]	46	3
522	2018-08-30 08:50:54.876758-04	126	done_works/Proekt_U.Salieva_-_03_fXT3qDN.jpg	1	[{"added": {}}]	46	3
523	2018-08-30 08:51:00.581525-04	127	done_works/Proekt_U.Salieva_-_04_1qurXsI.jpg	1	[{"added": {}}]	46	3
524	2018-08-30 08:51:06.377508-04	128	done_works/Proekt_U.Salieva_-_05_LmHFypb.jpg	1	[{"added": {}}]	46	3
525	2018-08-30 08:51:07.890561-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
526	2018-08-30 09:13:05.107498-04	9	Опоры освещения	1	[{"added": {}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "\\u0420\\u0430\\u0437\\u043c\\u0435\\u0440", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
527	2018-08-30 09:13:49.211997-04	9	Опоры освещения	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
528	2018-08-30 09:34:40.432879-04	9	Опоры освещения	2	[{"changed": {"fields": ["image"], "object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
529	2018-08-30 09:35:32.546313-04	9	Опоры освещения	2	[{"changed": {"fields": ["image"], "object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
530	2018-08-30 09:44:08.006612-04	9	Опоры освещения	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
531	2018-08-30 09:44:54.186188-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
532	2018-08-30 09:45:12.05763-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
533	2018-08-30 10:17:09.078364-04	129	done_works/01.jpg	1	[{"added": {}}]	46	3
534	2018-08-30 10:17:14.870299-04	130	done_works/01-1.jpg	1	[{"added": {}}]	46	3
535	2018-08-30 10:17:16.073405-04	15	Реконструкция освещения в парке им. Т. Молдо, г. Бишкек	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[15]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
536	2018-08-30 10:19:50.737616-04	131	done_works/01-1_FdrZ5LF.jpg	1	[{"added": {}}]	46	3
537	2018-08-30 10:19:52.193775-04	15	Реконструкция освещения в парке им. Т. Молдо, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[15]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
538	2018-08-30 10:21:50.5548-04	132	done_works/01_V35N3aQ.jpg	1	[{"added": {}}]	46	3
539	2018-08-30 10:21:56.672898-04	133	done_works/01-1_NiBXZZq.jpg	1	[{"added": {}}]	46	3
540	2018-08-30 10:22:16.11387-04	134	done_works/01-2.jpg	1	[{"added": {}}]	46	3
541	2018-08-30 10:22:17.218258-04	15	Реконструкция освещения в парке им. Т. Молдо, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[15]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
542	2018-08-30 10:26:17.725491-04	135	done_works/01-2_S9XKQ3D.jpg	1	[{"added": {}}]	46	3
543	2018-08-30 10:26:19.70352-04	15	Реконструкция освещения в парке им. Т. Молдо, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[15]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
544	2018-08-30 10:36:07.800273-04	136	done_works/Proekt_Park_T.Moldo_-_02.jpg	1	[{"added": {}}]	46	3
547	2018-08-30 10:36:26.045717-04	139	done_works/Proekt_Park_T.Moldo_-_01.jpg	1	[{"added": {}}]	46	3
548	2018-08-30 10:36:27.197702-04	15	Реконструкция освещения в парке им. Т. Молдо, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[15]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
549	2018-08-30 10:45:18.015217-04	140	done_works/01-1_ou7WPtv.jpg	1	[{"added": {}}]	46	3
550	2018-08-30 10:46:30.371588-04	141	done_works/01-1_lzeUq6T.jpg	1	[{"added": {}}]	46	3
551	2018-08-30 10:50:24.476376-04	16	Реконструкция освещения  аллея	1	[{"added": {}}]	45	3
552	2018-08-30 10:52:00.783348-04	142	done_works/Proekt_Alleya_molodezhi_01-1.jpg	1	[{"added": {}}]	46	3
553	2018-08-30 10:53:25.762778-04	16	Реконструкция освещения  аллея	2	[{"changed": {"fields": ["description_kg", "created_at"]}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[16]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
554	2018-08-30 10:54:24.256717-04	16	Реконструкция освещения  на Аллее Молодежи, г. Бишкек	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
555	2018-08-30 10:54:53.804446-04	16	Реконструкция освещения  на Аллее Молодежи, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
556	2018-08-30 10:55:12.030584-04	143	done_works/Proekt_Alleya_molodezhi_02.jpg	1	[{"added": {}}]	46	3
557	2018-08-30 10:55:13.759602-04	16	Реконструкция освещения  на Аллее Молодежи, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[16]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
558	2018-08-30 10:58:47.026998-04	143	done_works/Proekt_Alleya_molodezhi_02.jpg	3		46	3
559	2018-08-30 10:58:47.032463-04	142	done_works/Proekt_Alleya_molodezhi_01-1.jpg	3		46	3
560	2018-08-30 10:59:45.07843-04	141	done_works/01-1_lzeUq6T.jpg	3		46	3
561	2018-08-30 10:59:45.084329-04	140	done_works/01-1_ou7WPtv.jpg	3		46	3
562	2018-08-30 11:01:07.778204-04	135	done_works/01-2_S9XKQ3D.jpg	3		46	3
563	2018-08-30 11:01:07.779978-04	134	done_works/01-2.jpg	3		46	3
564	2018-08-30 11:01:07.780958-04	133	done_works/01-1_NiBXZZq.jpg	3		46	3
565	2018-08-30 11:01:07.781919-04	132	done_works/01_V35N3aQ.jpg	3		46	3
566	2018-08-30 11:01:07.790532-04	131	done_works/01-1_FdrZ5LF.jpg	3		46	3
567	2018-08-30 11:01:07.792506-04	130	done_works/01-1.jpg	3		46	3
568	2018-08-30 11:01:07.793859-04	129	done_works/01.jpg	3		46	3
569	2018-08-30 11:01:51.805776-04	60	done_works/001.jpg	3		46	3
570	2018-08-30 11:01:51.807551-04	54	done_works/2-1.jpg	3		46	3
571	2018-08-30 11:01:51.808561-04	53	done_works/1-1.jpg	3		46	3
572	2018-08-30 11:02:10.906693-04	43	done_works/003-1.jpg	3		46	3
573	2018-08-30 11:04:46.591788-04	16	Освещение  на Аллее Молодежи, г. Бишкек	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}, {"deleted": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[None]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
574	2018-08-30 11:09:08.39519-04	16	Освещение  на Аллее Молодежи, г. Бишкек	2	[{"changed": {"fields": ["slug", "created_at"]}}]	45	3
575	2018-08-30 11:10:29.600473-04	16	Освещение  на Аллее Молодежи, г. Бишкек	3		45	3
576	2018-08-30 11:16:18.376566-04	17	Реконструкция Аллеи Молодежи, г. Бишкек	1	[{"added": {}}]	45	3
577	2018-08-30 11:19:03.397186-04	123	done_works/Proekt_U.Salieva_-_02-1.jpg	3		46	3
578	2018-08-30 11:19:03.400872-04	122	done_works/Proekt_U.Salieva_-_02_yp9m8uh.jpg	3		46	3
579	2018-08-30 11:19:03.402871-04	121	done_works/Proekt_U.Salieva_-_01-1.jpg	3		46	3
580	2018-08-30 11:19:03.404316-04	120	done_works/Proekt_U.Salieva_-_01_yS7wIk1.jpg	3		46	3
581	2018-08-30 11:19:03.40554-04	119	done_works/Proekt_U.Salieva_-_04-2.jpg	3		46	3
582	2018-08-30 11:19:03.406691-04	118	done_works/Proekt_U.Salieva_-_04_QIst3y3.jpg	3		46	3
583	2018-08-30 11:19:03.40798-04	117	done_works/Proekt_U.Salieva_-_03_wBfeHNV.jpg	3		46	3
584	2018-08-30 11:19:03.409201-04	116	done_works/Proekt_U.Salieva_-_03-3.jpg	3		46	3
585	2018-08-30 11:19:03.412244-04	115	done_works/Proekt_U.Salieva_-_03-2.jpg	3		46	3
586	2018-08-30 11:19:03.413102-04	114	done_works/Proekt_U.Salieva_-_03-1.jpg	3		46	3
587	2018-08-30 11:19:03.413951-04	113	done_works/Proekt_U.Salieva_-_03_pNQSDLN.jpg	3		46	3
588	2018-08-30 11:19:03.415191-04	112	done_works/Proekt_U.Salieva_-_02_jlWC93u.jpg	3		46	3
589	2018-08-30 11:19:03.417626-04	111	done_works/Proekt_U.Salieva_-_05.jpg	3		46	3
590	2018-08-30 11:19:03.418671-04	110	done_works/Proekt_U.Salieva_-_04.jpg	3		46	3
591	2018-08-30 11:19:03.419779-04	109	done_works/Proekt_U.Salieva_-_03.jpg	3		46	3
592	2018-08-30 11:19:03.420639-04	108	done_works/Proekt_U.Salieva_-_02.jpg	3		46	3
593	2018-08-30 11:19:03.421491-04	107	done_works/Proekt_U.Salieva_-_01.jpg	3		46	3
594	2018-08-30 11:20:18.903675-04	17	Реконструкция Аллеи Молодежи, г. Бишкек	3		45	3
595	2018-08-30 11:22:24.197947-04	144	done_works/Proekt_Alleya_molodezhi_02_lGNqtZr.jpg	1	[{"added": {}}]	46	3
596	2018-08-30 11:22:46.75294-04	18	Уличное освещение Аллеи Молодежи, г. Бишкек	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[17]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
597	2018-08-30 11:23:53.39535-04	18	Уличное освещение Аллеи Молодежи, г. Бишкек	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
598	2018-08-30 11:24:26.125247-04	145	done_works/Proekt_Alleya_molodezhi_03.jpg	1	[{"added": {}}]	46	3
599	2018-08-30 11:24:27.487732-04	18	Уличное освещение Аллеи Молодежи, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[17]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
600	2018-08-30 11:25:23.518168-04	145	done_works/Proekt_Alleya_molodezhi_03.jpg	3		46	3
601	2018-08-30 11:25:23.519865-04	144	done_works/Proekt_Alleya_molodezhi_02_lGNqtZr.jpg	3		46	3
602	2018-08-30 11:25:35.435501-04	146	done_works/Proekt_Alleya_molodezhi_01.jpg	1	[{"added": {}}]	46	3
603	2018-08-30 11:25:37.098323-04	18	Уличное освещение Аллеи Молодежи, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[17]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
604	2018-08-30 11:27:59.091755-04	147	done_works/Proekt_Alleya_molodezhi_02_gx9Kc5j.jpg	1	[{"added": {}}]	46	3
605	2018-08-30 11:28:00.618018-04	18	Уличное освещение Аллеи Молодежи, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[17]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
606	2018-08-30 11:32:03.075774-04	148	done_works/Proekt_Alleya_molodezhi_01_S8LIQAw.jpg	1	[{"added": {}}]	46	3
607	2018-08-30 11:32:04.836111-04	17	Галлерея #[17]	2	[{"changed": {"fields": ["main_image"]}}]	47	3
608	2018-08-30 11:32:26.745149-04	148	done_works/Proekt_Alleya_molodezhi_01_S8LIQAw.jpg	3		46	3
609	2018-08-30 11:32:26.747648-04	147	done_works/Proekt_Alleya_molodezhi_02_gx9Kc5j.jpg	3		46	3
610	2018-08-30 11:32:26.755538-04	146	done_works/Proekt_Alleya_molodezhi_01.jpg	3		46	3
611	2018-08-30 11:32:37.694654-04	149	done_works/Proekt_Alleya_molodezhi_01_Y7hdvhm.jpg	1	[{"added": {}}]	46	3
612	2018-08-30 11:32:43.741617-04	150	done_works/Proekt_Alleya_molodezhi_02_clUeA6F.jpg	1	[{"added": {}}]	46	3
613	2018-08-30 11:32:49.540195-04	151	done_works/Proekt_Alleya_molodezhi_03_tRGUmgy.jpg	1	[{"added": {}}]	46	3
614	2018-08-30 11:32:55.242719-04	152	done_works/Proekt_Alleya_molodezhi_04.JPG	1	[{"added": {}}]	46	3
615	2018-08-30 11:32:57.446469-04	18	Уличное освещение Аллеи Молодежи, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[17]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
616	2018-08-31 02:24:44.653624-04	6	Производство опор освещения	1	[{"added": {}}]	39	3
617	2018-08-31 02:25:32.839086-04	6	Производство опор освещения	2	[{"changed": {"fields": ["description", "description_ru"]}}]	39	3
618	2018-08-31 02:26:10.697428-04	6	Производство опор освещения	2	[{"changed": {"fields": ["description_ru", "description_kg"]}}]	39	3
619	2018-08-31 02:36:05.809408-04	6	Производство опор освещения	2	[{"changed": {"fields": ["features", "features_ru"]}}]	39	3
620	2018-08-31 02:43:46.709793-04	18	Уличное освещение Аллеи Молодежи, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
621	2018-08-31 04:19:50.88258-04	153	done_works/Proekt_Gazprom_01.jpg	1	[{"added": {}}]	46	3
622	2018-08-31 04:19:52.525573-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[18]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
623	2018-08-31 04:20:54.45728-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["created_at"]}}]	45	3
624	2018-08-31 04:22:35.797361-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["created_at"]}}]	45	3
625	2018-08-31 04:46:18.983506-04	154	done_works/Proekt_Gazprom_02.jpg	1	[{"added": {}}]	46	3
626	2018-08-31 04:46:21.395384-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[18]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
627	2018-08-31 04:47:49.879383-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["description", "created_at"]}}]	45	3
628	2018-08-31 04:48:45.362347-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
629	2018-08-31 04:49:25.925883-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
630	2018-08-31 04:49:47.576254-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
631	2018-08-31 04:50:55.781248-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
632	2018-08-31 04:53:38.934126-04	153	done_works/Proekt_Gazprom_01.jpg	3		46	3
633	2018-08-31 04:58:10.218371-04	155	done_works/Proekt_Gazprom_01_MrDnDXl.jpg	1	[{"added": {}}]	46	3
634	2018-08-31 04:58:15.570222-04	156	done_works/Proekt_Gazprom_03.jpg	1	[{"added": {}}]	46	3
635	2018-08-31 04:58:17.125523-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[18]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
636	2018-08-31 05:01:33.224296-04	9	Опоры освещения	2	[{"changed": {"fields": ["main_image"]}}]	36	3
637	2018-08-31 05:02:36.529603-04	9	Опоры освещения	2	[{"changed": {"fields": ["main_image"]}}]	36	3
638	2018-08-31 05:17:39.815403-04	20	Внутреннее освещение магазинов	1	[{"added": {}}]	45	3
639	2018-08-31 05:24:24.744109-04	157	done_works/Proekt_vnutrennee_osveschenie_01.jpg	1	[{"added": {}}]	46	3
640	2018-08-31 05:24:26.337055-04	20	Внутреннее освещение магазинов	2	[{"changed": {"fields": ["created_at"]}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[19]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
641	2018-08-31 06:28:38.618227-04	4	Внутреннее освещение	2	[]	39	3
642	2018-08-31 06:29:55.371977-04	158	done_works/Proekt_vnutrennee_osveschenie_02.jpg	1	[{"added": {}}]	46	3
643	2018-08-31 06:30:01.908475-04	159	done_works/Proekt_vnutrennee_osveschenie_03.jpg	1	[{"added": {}}]	46	3
644	2018-08-31 06:30:07.580253-04	160	done_works/Proekt_vnutrennee_osveschenie_04.jpg	1	[{"added": {}}]	46	3
645	2018-08-31 06:30:13.149616-04	161	done_works/Proekt_vnutrennee_osveschenie_05.jpg	1	[{"added": {}}]	46	3
646	2018-08-31 06:30:18.26165-04	162	done_works/Proekt_vnutrennee_osveschenie_06.jpg	1	[{"added": {}}]	46	3
647	2018-08-31 06:30:23.533048-04	163	done_works/Proekt_vnutrennee_osveschenie_07.jpg	1	[{"added": {}}]	46	3
648	2018-08-31 06:30:24.842454-04	20	Внутреннее освещение магазинов	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[19]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
649	2018-08-31 07:00:59.454675-04	10	Светодиодные ленты	1	[{"added": {}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
650	2018-08-31 07:01:35.414793-04	20	Внутреннее освещение магазинов	2	[{"changed": {"fields": ["created_at"]}}]	45	3
651	2018-08-31 07:09:36.878739-04	164	done_works/Proekt_Okt_administraziya_01.jpg	1	[{"added": {}}]	46	3
652	2018-08-31 07:09:38.421522-04	21	Освещение здания Октябрьской администрации, г. Бишкек	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[20]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
653	2018-08-31 07:17:00.843913-04	165	done_works/Proekt_Okt_administraziya_02.jpg	1	[{"added": {}}]	46	3
654	2018-08-31 07:17:06.133301-04	166	done_works/Proekt_Okt_administraziya_03.jpg	1	[{"added": {}}]	46	3
655	2018-08-31 07:17:11.822017-04	167	done_works/Proekt_Okt_administraziya_04.jpg	1	[{"added": {}}]	46	3
656	2018-08-31 07:17:12.781568-04	21	Освещение здания Октябрьской администрации, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[20]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
657	2018-08-31 07:34:53.160904-04	168	done_works/Proekt_igry_Kochevnikov_01.jpg	1	[{"added": {}}]	46	3
658	2018-08-31 07:35:07.378509-04	22	Освещение ипподрома для Игр кочевников, 2014г.	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[21]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
659	2018-08-31 07:35:22.500292-04	22	Освещение ипподрома для Игр кочевников, 2014г.	2	[{"changed": {"fields": ["address", "created_at"]}}]	45	3
660	2018-08-31 07:37:27.75344-04	169	done_works/Proekt_igry_Kochevnikov_02.jpg	1	[{"added": {}}]	46	3
661	2018-08-31 07:37:28.880673-04	22	Освещение ипподрома для Игр кочевников, 2014г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[21]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
662	2018-08-31 07:38:30.699255-04	22	Освещение ипподрома для Игр кочевников, 2014г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[21]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
663	2018-08-31 07:38:40.058613-04	169	done_works/Proekt_igry_Kochevnikov_02.jpg	3		46	3
664	2018-08-31 07:38:51.194275-04	170	done_works/Proekt_igry_Kochevnikov_02_0ktIDEA.jpg	1	[{"added": {}}]	46	3
665	2018-08-31 07:38:52.378993-04	22	Освещение ипподрома для Игр кочевников, 2014г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[21]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
666	2018-08-31 08:13:38.313694-04	7	Системы промышленного видеонаблюдения	1	[{"added": {}}]	39	3
667	2018-08-31 08:14:21.814909-04	7	Системы промышленного видеонаблюдения	2	[{"changed": {"fields": ["features", "features_ru", "notes", "notes_ru"]}}]	39	3
668	2018-08-31 08:15:23.733205-04	7	Системы промышленного видеонаблюдения	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
669	2018-08-31 08:16:41.563812-04	7	Системы промышленного видеонаблюдения	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
670	2018-08-31 08:17:55.625298-04	7	Системы промышленного видеонаблюдения	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
671	2018-08-31 08:19:11.803938-04	7	Системы промышленного видеонаблюдения	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
672	2018-08-31 08:19:58.651264-04	2	Светодиодные линейные светильники	2	[{"changed": {"fields": ["article"]}}]	36	3
673	2018-08-31 08:21:24.803851-04	7	Системы промышленного видеонаблюдения	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
674	2018-08-31 08:23:56.260175-04	7	Системы промышленного видеонаблюдения	2	[{"changed": {"fields": ["thumbnail"]}}]	39	3
675	2018-08-31 08:38:03.852035-04	7	Системы промышленного видеонаблюдения	2	[{"changed": {"fields": ["description", "description_ru", "features", "features_ru", "notes", "notes_ru"]}}]	39	3
676	2018-08-31 08:41:43.691157-04	7	Системы промышленного видеонаблюдения	2	[{"changed": {"fields": ["description", "description_ru"]}}]	39	3
708	2018-09-02 10:09:12.633681-04	182	done_works/Proekt_Balykchy_osveshenie_05.jpg	1	[{"added": {}}]	46	3
835	2018-09-06 11:04:20.853208-04	198	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_02_G6vJ4Ke.jpg	1	[{"added": {}}]	46	3
954	2018-09-10 10:37:40.236139-04	239	done_works/Proekt_Ala-Too_Novyi_god_2018_-_06_ylYyVBQ.jpg	1	[{"added": {}}]	46	3
677	2018-08-31 08:57:00.755897-04	11	Системы видеонаблюдения	1	[{"added": {}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
678	2018-08-31 09:07:41.980767-04	11	Системы видеонаблюдения	2	[{"changed": {"fields": ["description", "description_ru"]}}, {"changed": {"fields": ["value", "value_ru"], "object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
679	2018-08-31 09:08:10.993751-04	11	Системы видеонаблюдения	2	[{"changed": {"fields": ["main_image"]}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"changed": {"fields": ["image"], "object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
680	2018-08-31 09:20:48.224889-04	11	Системы видеонаблюдения	2	[{"deleted": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"deleted": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
681	2018-08-31 09:21:08.839601-04	11	Системы видеонаблюдения	2	[{"changed": {"fields": ["main_image"]}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
682	2018-09-02 08:17:04.738695-04	171	done_works/Proekt_Balykchy_perehody_01.jpg	1	[{"added": {}}]	46	3
683	2018-09-02 08:17:06.576515-04	23	Освещение пешеходных переходов, г. Балыкчы	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[22]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
684	2018-09-02 08:19:48.580627-04	23	Освещение пешеходных переходов, г. Балыкчы	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
685	2018-09-02 08:23:54.312983-04	172	done_works/Proekt_Balykchy_perehody_02.jpg	1	[{"added": {}}]	46	3
686	2018-09-02 08:23:55.657235-04	23	Освещение пешеходных переходов, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[22]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
687	2018-09-02 08:24:49.848678-04	23	Освещение пешеходных переходов, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[22]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
688	2018-09-02 08:24:57.693286-04	172	done_works/Proekt_Balykchy_perehody_02.jpg	3		46	3
689	2018-09-02 08:37:30.954135-04	173	done_works/Proekt_Balykchy_perehody_02-1.jpg	1	[{"added": {}}]	46	3
690	2018-09-02 08:37:32.294627-04	23	Освещение пешеходных переходов, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[22]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
691	2018-09-02 08:38:03.343293-04	23	Освещение пешеходных переходов, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[22]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
692	2018-09-02 08:38:10.132483-04	173	done_works/Proekt_Balykchy_perehody_02-1.jpg	3		46	3
693	2018-09-02 08:38:57.566865-04	174	done_works/64508-1.jpg	1	[{"added": {}}]	46	3
694	2018-09-02 08:39:05.746856-04	175	done_works/64509-1.jpg	1	[{"added": {}}]	46	3
695	2018-09-02 08:39:11.111711-04	176	done_works/64510-1.jpg	1	[{"added": {}}]	46	3
696	2018-09-02 08:39:12.393775-04	23	Освещение пешеходных переходов, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[22]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
697	2018-09-02 08:39:34.814374-04	23	Освещение пешеходных переходов, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[22]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
698	2018-09-02 08:39:42.94038-04	176	done_works/64510-1.jpg	3		46	3
699	2018-09-02 08:39:42.942143-04	175	done_works/64509-1.jpg	3		46	3
700	2018-09-02 08:39:42.943122-04	174	done_works/64508-1.jpg	3		46	3
701	2018-09-02 08:40:09.542729-04	177	done_works/Proekt_Balykchy_perehody_02_S1fCBKE.jpg	1	[{"added": {}}]	46	3
702	2018-09-02 08:40:10.720342-04	23	Освещение пешеходных переходов, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[22]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
703	2018-09-02 10:05:58.139946-04	178	done_works/Proekt_Balykchy_osveshenie_01.jpg	1	[{"added": {}}]	46	3
704	2018-09-02 10:05:59.41235-04	24	Уличное освещение г. Балыкчы	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[23]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
705	2018-09-02 10:08:54.072587-04	179	done_works/Proekt_Balykchy_osveshenie_02.jpg	1	[{"added": {}}]	46	3
706	2018-09-02 10:09:00.08076-04	180	done_works/Proekt_Balykchy_osveshenie_03.jpg	1	[{"added": {}}]	46	3
707	2018-09-02 10:09:05.772991-04	181	done_works/Proekt_Balykchy_osveshenie_04.jpg	1	[{"added": {}}]	46	3
709	2018-09-02 10:09:14.120538-04	24	Уличное освещение г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[23]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
710	2018-09-02 11:28:09.928714-04	183	done_works/Proekt_Balykchy_video_00.jpg	1	[{"added": {}}]	46	3
711	2018-09-02 11:28:11.48781-04	25	Установка сети видеонаблюдения, г. Балыкчы	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[24]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
712	2018-09-02 11:33:06.20993-04	184	done_works/Proekt_Balykchy_video_01.jpg	1	[{"added": {}}]	46	3
713	2018-09-02 11:33:11.803939-04	185	done_works/Proekt_Balykchy_video_02.jpg	1	[{"added": {}}]	46	3
714	2018-09-02 11:33:12.781802-04	25	Установка сети видеонаблюдения, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[24]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
715	2018-09-03 04:14:02.973309-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
716	2018-09-03 04:14:58.666476-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
717	2018-09-03 04:15:12.621303-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}]	45	3
718	2018-09-03 04:15:23.213112-04	5	Фасадное освещение ЖК "Монреаль"	2	[{"changed": {"fields": ["created_at"]}}]	45	3
719	2018-09-03 04:15:39.585449-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}]	45	3
720	2018-09-03 04:15:54.4962-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
721	2018-09-03 04:16:09.242056-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
722	2018-09-03 04:16:18.633139-04	9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
723	2018-09-03 04:16:28.105411-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
724	2018-09-03 04:16:37.829654-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
725	2018-09-03 04:16:45.941908-04	12	декоративная подсветка стелы Аска- Таш, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
726	2018-09-03 04:16:58.564187-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
727	2018-09-03 04:17:14.47-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
728	2018-09-03 04:17:35.304288-04	15	Реконструкция освещения в парке им. Т. Молдо, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
729	2018-09-03 04:17:52.011926-04	18	Уличное освещение Аллеи Молодежи, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
730	2018-09-03 04:18:07.77715-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["created_at"]}}]	45	3
731	2018-09-03 04:18:18.12036-04	20	Внутреннее освещение магазинов	2	[{"changed": {"fields": ["created_at"]}}]	45	3
732	2018-09-03 04:18:35.59658-04	21	Освещение здания Октябрьской администрации, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
733	2018-09-03 04:18:44.659838-04	22	Освещение ипподрома для Игр кочевников, 2014г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
734	2018-09-03 04:18:53.921037-04	22	Освещение ипподрома для Игр кочевников, 2014г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
735	2018-09-03 04:19:07.544337-04	23	Освещение пешеходных переходов, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}]	45	3
736	2018-09-03 04:19:15.833672-04	24	Уличное освещение г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}]	45	3
737	2018-09-03 04:19:31.589992-04	25	Установка сети видеонаблюдения, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}]	45	3
738	2018-09-03 04:49:59.480665-04	18	/application/assets/uploads/slider/slide_slide_slide_004_-_decorativnoe_osveschenie.wt.png	2	[{"changed": {"fields": ["link"]}}]	18	3
739	2018-09-03 04:51:10.581635-04	19	/application/assets/uploads/slider/slide_slide_slide_005_Proizvodstvo_1.wt.png	2	[{"changed": {"fields": ["link"]}}]	18	3
740	2018-09-03 04:54:22.490615-04	20	/application/assets/uploads/slider/slide_slide_slide_slide_slide_slide_006_-_raschet_proektov_1.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
741	2018-09-03 04:55:01.124821-04	20	/application/assets/uploads/slider/slide_006_-_raschet_proektov_3.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
742	2018-09-03 04:55:49.151596-04	21	/application/assets/uploads/slider/slide_slide_slide_007_-_vnutrennee_osveschenie_original_j0iFzIt.wt.png	2	[{"changed": {"fields": ["link", "font_color"]}}]	18	3
743	2018-09-03 04:56:41.306225-04	21	/application/assets/uploads/slider/slide_slide_slide_slide_007_-_vnutrennee_osveschenie_original_j0iFzIt.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
744	2018-09-03 04:57:13.994854-04	21	/application/assets/uploads/slider/slide_slide_slide_slide_slide_007_-_vnutrennee_osveschenie_original_j0iFzIt.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
745	2018-09-03 04:57:49.660766-04	21	/application/assets/uploads/slider/slide_slide_slide_slide_slide_slide_007_-_vnutrennee_osveschenie_original_j0iFzIt.wt.png	2	[{"changed": {"fields": ["short_description"]}}]	18	3
746	2018-09-03 04:58:42.309952-04	20	/application/assets/uploads/slider/slide_slide_006_-_raschet_proektov_3.wt.png	2	[{"changed": {"fields": ["font_color"]}}]	18	3
747	2018-09-03 05:52:11.225979-04	186	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_06.jpg	1	[{"added": {}}]	46	3
834	2018-09-06 11:04:07.310594-04	197	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_07_y4jiukd.jpg	1	[{"added": {}}]	46	3
836	2018-09-06 11:04:37.260417-04	199	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_oblozhka_ZaP4Jou.jpg	1	[{"added": {}}]	46	3
748	2018-09-03 05:52:13.16705-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[6]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
749	2018-09-03 07:20:58.205952-04	187	done_works/Proekt_Park_Fuchika_01.jpg	1	[{"added": {}}]	46	3
750	2018-09-03 07:20:59.670609-04	26	Реконструкция парка им. Ю. Фучика, г. Бишкек	1	[{"added": {}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[25]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
751	2018-09-03 07:24:40.960617-04	188	done_works/Proekt_Park_Fuchika_01-1.jpg	1	[{"added": {}}]	46	3
752	2018-09-03 07:24:42.323091-04	26	Реконструкция парка им. Ю. Фучика, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[25]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
753	2018-09-03 07:33:30.678373-04	189	done_works/Proekt_Park_Fuchika_00.jpg	1	[{"added": {}}]	46	3
754	2018-09-03 07:33:39.525524-04	26	Реконструкция парка им. Ю. Фучика, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[25]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
755	2018-09-03 07:33:48.734855-04	188	done_works/Proekt_Park_Fuchika_01-1.jpg	3		46	3
756	2018-09-03 07:33:48.737537-04	187	done_works/Proekt_Park_Fuchika_01.jpg	3		46	3
757	2018-09-03 07:39:22.217232-04	190	done_works/Proekt_Park_Fuchika_01_gxfbOCd.jpg	1	[{"added": {}}]	46	3
758	2018-09-03 07:39:23.222614-04	26	Реконструкция парка им. Ю. Фучика, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[25]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
759	2018-09-03 08:04:10.595582-04	191	done_works/Proekt_Park_Fuchika_02.jpg	1	[{"added": {}}]	46	3
760	2018-09-03 08:04:16.928667-04	192	done_works/Proekt_Park_Fuchika_03.jpg	1	[{"added": {}}]	46	3
761	2018-09-03 08:04:23.105674-04	193	done_works/Proekt_Park_Fuchika_04.jpg	1	[{"added": {}}]	46	3
762	2018-09-03 08:04:29.08722-04	194	done_works/Proekt_Park_Fuchika_05.jpg	1	[{"added": {}}]	46	3
763	2018-09-03 08:04:30.336836-04	26	Реконструкция парка им. Ю. Фучика, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[25]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
764	2018-09-03 08:21:23.928225-04	26	Реконструкция парка им. Ю. Фучика, г. Бишкек	2	[{"changed": {"fields": ["customer", "created_at"]}}]	45	3
765	2018-09-05 01:27:21.248327-04	7	Системы видеонаблюдения	2	[{"changed": {"fields": ["title", "title_ru"]}}]	39	3
766	2018-09-05 01:29:54.194544-04	18	Освещение Аллеи Молодежи, г. Бишкек	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
767	2018-09-05 01:34:59.72125-04	2	Светодиодные линейные светильники	2	[{"added": {"object": "\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}, {"changed": {"fields": ["value", "value_ru"], "object": "\\u041c\\u043e\\u0449\\u043d\\u043e\\u0441\\u0442\\u044c", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
768	2018-09-05 03:33:15.33319-04	21	/application/assets/uploads/slider/slide_007_-_vnutrennee_osveschenie_1-2_lL9153r.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
769	2018-09-05 03:42:25.849809-04	21	/application/assets/uploads/slider/slide_007_-_vnutrennee_osveschenie_1-2_YoC04gT.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
770	2018-09-05 04:33:39.357971-04	21	/application/assets/uploads/slider/slide_007_-_vnutrennee_osveschenie_1-4.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
771	2018-09-05 04:35:35.83552-04	21	/application/assets/uploads/slider/slide_007_-_vnutrennee_osveschenie_1-2_bWN9dYh.wt.png	2	[{"changed": {"fields": ["image"]}}]	18	3
772	2018-09-05 04:54:34.033298-04	52	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_02-1.jpg	3		46	3
773	2018-09-05 04:54:34.035485-04	51	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_02.jpg	3		46	3
774	2018-09-05 04:54:34.036959-04	50	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_01-1.jpg	3		46	3
775	2018-09-05 04:54:46.965005-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
776	2018-09-05 04:56:16.384083-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
777	2018-09-05 04:56:39.15446-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
778	2018-09-05 04:56:54.728542-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}]	45	3
779	2018-09-05 05:14:42.010609-04	5	Фасадное освещение ЖК "Монреаль"	2	[{"changed": {"fields": ["created_at"]}}]	45	3
780	2018-09-05 05:14:57.232233-04	40	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_01-1.jpg	3		46	3
781	2018-09-05 05:18:46.797808-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}]	45	3
782	2018-09-05 05:19:59.265544-04	49	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_01.jpg	3		46	3
783	2018-09-05 05:20:09.307541-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
784	2018-09-05 05:21:46.442478-04	67	done_works/Proekt_Ala-Too_Noryz_2018_-_01.jpg	3		46	3
785	2018-09-05 05:22:30.18181-04	9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
786	2018-09-05 05:25:54.651172-04	88	done_works/Proekt_Ala-Too_Novyi_god_2016_-_02_osl8qrP.jpg	3		46	3
787	2018-09-06 00:19:05.710498-04	183	done_works/Proekt_Balykchy_video_00.jpg	3		46	3
788	2018-09-06 00:19:10.439195-04	25	Установка сети видеонаблюдения, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}]	45	3
789	2018-09-06 00:19:37.270193-04	24	Уличное освещение г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[23]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
790	2018-09-06 00:20:51.629721-04	25	Установка сети видеонаблюдения, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[24]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
791	2018-09-06 04:29:42.788874-04	21	/application/assets/uploads/slider/007_-_vnutrennee_osveschenie_1-2.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
792	2018-09-06 04:36:20.121434-04	21	/application/assets/uploads/slider/Slaider_07_-_vnutrennee_osveschenie_01.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
793	2018-09-06 06:44:32.49418-04	22	/application/assets/uploads/slider/Slaider_02_-_prazdnichnoe_osveschenie_01.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
794	2018-09-06 07:01:23.326002-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
795	2018-09-06 07:01:31.776157-04	21	/application/assets/uploads/slider/Slider_07_-_vnutrennee_osveschenie.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
796	2018-09-06 07:02:12.382253-04	16	/application/assets/uploads/slider/Slider_03_-_ulichnoe_osveschenie.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
797	2018-09-06 07:29:15.52181-04	18	/application/assets/uploads/slider/Slider_04_-_decorativnoe_osveschenie.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
798	2018-09-06 07:56:10.444691-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
799	2018-09-06 07:56:44.499888-04	1	/application/assets/uploads/slider/slide_slide_slide_slide_slide1.wt.png	3		18	3
800	2018-09-06 08:02:17.475422-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo_KpLs71M.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
801	2018-09-06 08:07:45.657412-04	20	/application/assets/uploads/slider/Slider_06_-_raschet_proektov.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
802	2018-09-06 08:14:25.291405-04	20	/application/assets/uploads/slider/Slider_06_-_raschet_proektov_THoK07o.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
803	2018-09-06 08:18:06.902484-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie-1.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
804	2018-09-06 08:20:52.548058-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie-2.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
805	2018-09-06 08:34:57.784956-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie-3.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
806	2018-09-06 08:36:59.10158-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie-3_wLCdGlK.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
807	2018-09-06 08:40:25.825812-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie-4.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
808	2018-09-06 09:05:20.792481-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie-5.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
809	2018-09-06 09:30:24.433068-04	22	/application/assets/uploads/slider/002_-_prazdnichnoe_osveschenie_2-1-original-1.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
810	2018-09-06 09:32:20.000434-04	21	/application/assets/uploads/slider/Slider_07_-_vnutrennee_osveschenie.jpg	2	[{"changed": {"fields": ["font_color"]}}]	18	3
811	2018-09-06 09:33:21.514144-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie_V2s0ycg.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
812	2018-09-06 09:46:02.686543-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo_CZHo9ug.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
813	2018-09-06 09:48:01.715895-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo-1.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
814	2018-09-06 09:49:39.438935-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo-2.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
815	2018-09-06 09:50:20.102617-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo_8vJPGOw.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
816	2018-09-06 09:52:52.009186-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo_PH4zurk.jpg	2	[{"changed": {"fields": ["image"]}}]	18	3
817	2018-09-06 09:53:24.80896-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo_PH4zurk.jpg	2	[{"changed": {"fields": ["font_color"]}}]	18	3
818	2018-09-06 09:53:55.898267-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo_PH4zurk.jpg	2	[{"changed": {"fields": ["font_color"]}}]	18	3
819	2018-09-06 09:54:23.049777-04	20	/application/assets/uploads/slider/Slider_06_-_raschet_proektov_THoK07o.jpg	2	[{"changed": {"fields": ["font_color"]}}]	18	3
820	2018-09-06 09:54:57.199519-04	20	/application/assets/uploads/slider/Slider_06_-_raschet_proektov_THoK07o.jpg	2	[{"changed": {"fields": ["font_color"]}}]	18	3
821	2018-09-06 09:56:20.981058-04	20	/application/assets/uploads/slider/Slider_06_-_raschet_proektov_THoK07o.jpg	2	[{"changed": {"fields": ["font_color"]}}]	18	3
822	2018-09-06 11:01:51.312004-04	195	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_01_lbiqlvx.jpg	1	[{"added": {}}]	46	3
823	2018-09-06 11:01:52.846662-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
824	2018-09-06 11:02:24.677799-04	32	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_02_wkjSDya.jpg	3		46	3
825	2018-09-06 11:02:24.683296-04	31	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_03_1P8Jeaz.jpg	3		46	3
826	2018-09-06 11:02:24.684357-04	30	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_05_zWZS0eT.jpg	3		46	3
827	2018-09-06 11:02:24.685333-04	29	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_08_7eSYqUN.jpg	3		46	3
828	2018-09-06 11:02:24.686274-04	28	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_04_T8OFpy7.jpg	3		46	3
829	2018-09-06 11:02:24.687214-04	26	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_oblozhka_strk9rG.jpg	3		46	3
830	2018-09-06 11:02:24.688162-04	25	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_07_QAB7fNh.jpg	3		46	3
831	2018-09-06 11:02:24.689014-04	24	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_06_4Rleei7.jpg	3		46	3
832	2018-09-06 11:02:24.689893-04	11	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_01_mTT6m0l.jpg	3		46	3
833	2018-09-06 11:03:55.428732-04	196	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_08_gCtHlVE.jpg	1	[{"added": {}}]	46	3
837	2018-09-06 11:04:48.668817-04	200	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_06_wQoa2V1.jpg	1	[{"added": {}}]	46	3
838	2018-09-06 11:04:56.121762-04	201	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_05_TIrW7X8.jpg	1	[{"added": {}}]	46	3
839	2018-09-06 11:05:02.469965-04	202	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_04_EfS0c2j.jpg	1	[{"added": {}}]	46	3
840	2018-09-06 11:05:14.366998-04	203	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_03_jrJpshy.jpg	1	[{"added": {}}]	46	3
841	2018-09-06 11:05:15.718337-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
842	2018-09-06 11:05:29.218478-04	2	Галлерея #[2]	2	[]	47	3
843	2018-09-06 11:08:06.805825-04	2	Галлерея #[2]	2	[]	47	3
844	2018-09-06 11:08:37.464679-04	204	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_01_kstWWsp.jpg	1	[{"added": {}}]	46	3
845	2018-09-06 11:08:40.804137-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[2]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
846	2018-09-06 23:53:32.360313-04	202	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_04_EfS0c2j.jpg	3		46	3
847	2018-09-06 23:59:16.281982-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
848	2018-09-07 01:19:56.529536-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[3]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
849	2018-09-07 01:22:06.692737-04	205	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_00-1.jpg	1	[{"added": {}}]	46	3
850	2018-09-07 01:22:09.267454-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[3]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
851	2018-09-07 01:22:25.14587-04	35	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_02.jpg	3		46	3
852	2018-09-07 01:22:25.147739-04	34	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_01.jpg	3		46	3
853	2018-09-07 01:22:25.148723-04	33	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_00.jpg	3		46	3
854	2018-09-07 01:52:52.894651-04	206	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_01_d7zYsmL.jpg	1	[{"added": {}}]	46	3
855	2018-09-07 01:53:12.69024-04	207	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_02_4zlSgUX.jpg	1	[{"added": {}}]	46	3
856	2018-09-07 01:53:18.683333-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[3]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
857	2018-09-07 01:53:35.811792-04	205	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_00-1.jpg	3		46	3
858	2018-09-07 01:54:05.874662-04	208	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_00_BAXbQmJ.jpg	1	[{"added": {}}]	46	3
859	2018-09-07 01:54:13.453829-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[3]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
860	2018-09-07 01:54:23.465899-04	207	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_02_4zlSgUX.jpg	3		46	3
861	2018-09-07 01:54:23.469415-04	206	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_01_d7zYsmL.jpg	3		46	3
862	2018-09-07 01:54:36.939568-04	209	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_01_hrTVtXY.jpg	1	[{"added": {}}]	46	3
863	2018-09-07 01:54:42.597665-04	210	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_02_aW8YEta.jpg	1	[{"added": {}}]	46	3
864	2018-09-07 01:54:44.048703-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[3]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
865	2018-09-07 02:12:01.493982-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
866	2018-09-07 02:22:11.145343-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
867	2018-09-07 02:22:31.103064-04	38	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_03.jpg	3		46	3
868	2018-09-07 02:22:31.105348-04	37	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_02.jpg	3		46	3
869	2018-09-07 02:22:31.10938-04	36	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_01.jpg	3		46	3
870	2018-09-07 02:23:21.041187-04	211	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_01_JrQvHbd.jpg	1	[{"added": {}}]	46	3
871	2018-09-07 02:23:22.98021-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
872	2018-09-07 02:50:16.683067-04	212	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_02_YQztwYS.jpg	1	[{"added": {}}]	46	3
912	2018-09-07 08:59:55.995378-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo_PH4zurk.jpg	2	[{"changed": {"fields": ["short_description"]}}]	18	3
873	2018-09-07 02:50:18.486743-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
874	2018-09-07 03:38:25.089111-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}]	45	3
875	2018-09-07 03:39:54.960677-04	42	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_03.jpg	3		46	3
876	2018-09-07 03:39:54.965504-04	41	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_02.jpg	3		46	3
877	2018-09-07 03:39:54.966555-04	39	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_01.jpg	3		46	3
878	2018-09-07 03:59:28.175815-04	213	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_00.jpg	1	[{"added": {}}]	46	3
879	2018-09-07 03:59:30.373297-04	5	Фасадное освещение ЖК "Монреаль"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[5]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
880	2018-09-07 05:34:46.070259-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
881	2018-09-07 05:35:07.727948-04	212	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_02_YQztwYS.jpg	3		46	3
882	2018-09-07 05:35:43.76793-04	214	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_02_lPY2JzC.jpg	1	[{"added": {}}]	46	3
883	2018-09-07 05:35:52.745847-04	215	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_01_n5m2Mhf.jpg	1	[{"added": {}}]	46	3
884	2018-09-07 05:35:54.28769-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
885	2018-09-07 05:36:13.961205-04	211	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_01_JrQvHbd.jpg	3		46	3
886	2018-09-07 05:38:07.680283-04	215	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_01_n5m2Mhf.jpg	3		46	3
887	2018-09-07 05:38:07.682251-04	214	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_02_lPY2JzC.jpg	3		46	3
888	2018-09-07 05:38:19.673911-04	216	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_02_j5TDvD6.jpg	1	[{"added": {}}]	46	3
889	2018-09-07 05:38:25.581343-04	217	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_01_i7E7lit.jpg	1	[{"added": {}}]	46	3
890	2018-09-07 05:38:27.133957-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
891	2018-09-07 05:39:20.699801-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
892	2018-09-07 08:28:11.911031-04	213	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_00.jpg	3		46	3
893	2018-09-07 08:29:02.101752-04	218	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_00_eNXL5xc.jpg	1	[{"added": {}}]	46	3
894	2018-09-07 08:29:08.494781-04	219	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_02_4tUXiZB.jpg	1	[{"added": {}}]	46	3
895	2018-09-07 08:29:14.393199-04	220	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_03_ysPPiVE.jpg	1	[{"added": {}}]	46	3
896	2018-09-07 08:29:19.47759-04	221	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_04.jpg	1	[{"added": {}}]	46	3
897	2018-09-07 08:29:21.162296-04	5	Фасадное освещение ЖК "Монреаль"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[5]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
898	2018-09-07 08:38:35.111296-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[3]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
899	2018-09-07 08:40:58.357693-04	3	Галлерея #[3]	2	[]	47	3
900	2018-09-07 08:41:46.035057-04	3	Галлерея #[3]	2	[]	47	3
901	2018-09-07 08:42:50.421818-04	209	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_01_hrTVtXY.jpg	3		46	3
902	2018-09-07 08:43:11.995198-04	222	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_03.jpg	1	[{"added": {}}]	46	3
903	2018-09-07 08:43:13.601982-04	3	Галлерея #[3]	2	[]	47	3
904	2018-09-07 08:44:53.226517-04	217	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_01_i7E7lit.jpg	3		46	3
905	2018-09-07 08:45:16.267768-04	223	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_03_nFthXeu.jpg	1	[{"added": {}}]	46	3
906	2018-09-07 08:45:17.547791-04	4	Галлерея #[4]	2	[]	47	3
907	2018-09-07 08:45:58.79957-04	4	Галлерея #[4]	2	[]	47	3
908	2018-09-07 08:46:13.377851-04	223	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_03_nFthXeu.jpg	3		46	3
909	2018-09-07 08:46:54.198461-04	224	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_03_BmYyNnH.jpg	1	[{"added": {}}]	46	3
910	2018-09-07 08:46:56.90898-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[4]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
911	2018-09-07 08:47:07.421951-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["created_at"]}}]	45	3
913	2018-09-08 01:51:30.24963-04	1	Настройка сайтов	2	[{"changed": {"fields": ["phone"]}}]	25	3
914	2018-09-10 07:59:59.390911-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[6]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
915	2018-09-10 08:00:44.737544-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[6]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
916	2018-09-10 08:01:14.535613-04	48	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_05.jpg	3		46	3
917	2018-09-10 08:01:14.537484-04	47	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_04.jpg	3		46	3
918	2018-09-10 08:01:14.541616-04	46	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_03.jpg	3		46	3
919	2018-09-10 08:01:14.542863-04	45	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_02.jpg	3		46	3
920	2018-09-10 08:01:14.547572-04	44	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_01.jpg	3		46	3
921	2018-09-10 08:01:46.345988-04	225	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_01_fixGFFu.jpg	1	[{"added": {}}]	46	3
922	2018-09-10 08:01:47.829847-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[6]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
923	2018-09-10 08:02:53.542988-04	186	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_06.jpg	3		46	3
924	2018-09-10 08:56:54.80428-04	226	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_02_n59sjVE.jpg	1	[{"added": {}}]	46	3
925	2018-09-10 08:56:56.467074-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[6]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
926	2018-09-10 09:03:55.713341-04	227	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_03_vznDW9i.jpg	1	[{"added": {}}]	46	3
927	2018-09-10 09:03:56.696522-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[6]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
928	2018-09-10 09:13:50.061187-04	228	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_05_hWVuelh.jpg	1	[{"added": {}}]	46	3
929	2018-09-10 09:13:56.292908-04	229	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_06_yAzdcoo.jpg	1	[{"added": {}}]	46	3
930	2018-09-10 09:13:57.485723-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[6]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
931	2018-09-10 09:46:01.254854-04	59	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_05.jpg	3		46	3
932	2018-09-10 09:46:01.259826-04	58	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_04.jpg	3		46	3
933	2018-09-10 09:46:01.264046-04	57	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_03.jpg	3		46	3
934	2018-09-10 09:46:01.265529-04	56	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_02_yBdXfop.jpg	3		46	3
935	2018-09-10 09:46:01.266599-04	55	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_01_9i20QRy.jpg	3		46	3
936	2018-09-10 09:46:25.876239-04	230	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_01_O7T4Eqc.jpg	1	[{"added": {}}]	46	3
937	2018-09-10 09:46:35.219994-04	231	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_04_9cmzhcj.jpg	1	[{"added": {}}]	46	3
938	2018-09-10 09:46:41.019495-04	232	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_05_jjH0zcp.jpg	1	[{"added": {}}]	46	3
939	2018-09-10 09:46:45.569175-04	233	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_02_DIpwPJn.jpg	1	[{"added": {}}]	46	3
940	2018-09-10 09:46:46.559306-04	7	Освещение супермаркета "Боорсок", г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[7]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
941	2018-09-10 09:53:06.717332-04	66	done_works/Proekt_Ala-Too_Novyi_god_2018_-_06.jpg	3		46	3
942	2018-09-10 09:53:06.719955-04	65	done_works/Proekt_Ala-Too_Novyi_god_2018_-_05.jpg	3		46	3
943	2018-09-10 09:53:06.721084-04	64	done_works/Proekt_Ala-Too_Novyi_god_2018_-_04.jpg	3		46	3
944	2018-09-10 09:53:06.722127-04	63	done_works/Proekt_Ala-Too_Novyi_god_2018_-_03.jpg	3		46	3
945	2018-09-10 09:53:06.72312-04	62	done_works/Proekt_Ala-Too_Novyi_god_2018_-_02.jpg	3		46	3
946	2018-09-10 09:53:06.724103-04	61	done_works/Proekt_Ala-Too_Novyi_god_2018_-_01.jpg	3		46	3
947	2018-09-10 09:59:19.987373-04	234	done_works/Proekt_Ala-Too_Novyi_god_2018_-_01_LoZ66Mu.jpg	1	[{"added": {}}]	46	3
948	2018-09-10 09:59:21.380947-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[8]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
949	2018-09-10 10:25:23.356692-04	235	done_works/Proekt_Ala-Too_Novyi_god_2018_-_02_pVxnZog.jpg	1	[{"added": {}}]	46	3
950	2018-09-10 10:25:24.683321-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[8]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
951	2018-09-10 10:37:23.796334-04	236	done_works/Proekt_Ala-Too_Novyi_god_2018_-_03_P8ehgLj.jpg	1	[{"added": {}}]	46	3
952	2018-09-10 10:37:30.171762-04	237	done_works/Proekt_Ala-Too_Novyi_god_2018_-_04_JoxylfM.jpg	1	[{"added": {}}]	46	3
953	2018-09-10 10:37:35.561895-04	238	done_works/Proekt_Ala-Too_Novyi_god_2018_-_05_34xWRZ6.jpg	1	[{"added": {}}]	46	3
955	2018-09-10 10:37:41.810461-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[8]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
956	2018-09-11 01:58:30.239572-04	240	done_works/Proekt_Ala-Too_Noryz_2018_-_01_KDHyGGy.jpg	1	[{"added": {}}]	46	3
957	2018-09-11 01:59:04.077889-04	9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[9]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
958	2018-09-11 02:00:13.184267-04	83	done_works/Proekt_Ala-Too_Noryz_2018_-_01_IU94XDq.jpg	3		46	3
959	2018-09-11 02:00:13.216521-04	82	done_works/Proekt_Ala-Too_Noryz_2018_-_08.jpg	3		46	3
960	2018-09-11 02:00:13.222386-04	81	done_works/Proekt_Ala-Too_Noryz_2018_-_07.jpg	3		46	3
961	2018-09-11 02:00:13.224456-04	80	done_works/Proekt_Ala-Too_Noryz_2018_-_06.jpg	3		46	3
962	2018-09-11 02:00:13.225835-04	79	done_works/Proekt_Ala-Too_Noryz_2018_-_05.jpg	3		46	3
963	2018-09-11 02:00:13.22715-04	78	done_works/Proekt_Ala-Too_Noryz_2018_-_04.jpg	3		46	3
964	2018-09-11 02:00:13.228956-04	77	done_works/Proekt_Ala-Too_Noryz_2018_-_03.jpg	3		46	3
965	2018-09-11 02:00:13.230184-04	76	done_works/Proekt_Ala-Too_Noryz_2018_-_02.jpg	3		46	3
966	2018-09-11 05:43:17.269915-04	241	done_works/Proekt_Ala-Too_Noryz_2018_-_03_OScXLH9.jpg	1	[{"added": {}}]	46	3
967	2018-09-11 05:43:26.569335-04	242	done_works/Proekt_Ala-Too_Noryz_2018_-_02_ovo83Io.jpg	1	[{"added": {}}]	46	3
968	2018-09-11 05:43:38.194685-04	243	done_works/Proekt_Ala-Too_Noryz_2018_-_02-1.jpg	1	[{"added": {}}]	46	3
969	2018-09-11 05:43:52.724734-04	244	done_works/Proekt_Ala-Too_Noryz_2018_-_07_60aMNnl.jpg	1	[{"added": {}}]	46	3
970	2018-09-11 05:44:01.156025-04	245	done_works/Proekt_Ala-Too_Noryz_2018_-_08_vhSET7I.jpg	1	[{"added": {}}]	46	3
971	2018-09-11 05:44:08.452513-04	246	done_works/Proekt_Ala-Too_Noryz_2018_-_06_S2InamT.jpg	1	[{"added": {}}]	46	3
972	2018-09-11 05:44:18.589478-04	247	done_works/Proekt_Ala-Too_Noryz_2018_-_04_jKmrZ0w.jpg	1	[{"added": {}}]	46	3
973	2018-09-11 05:44:25.809356-04	248	done_works/Proekt_Ala-Too_Noryz_2018_-_05_cUDqD0x.jpg	1	[{"added": {}}]	46	3
974	2018-09-11 05:44:29.416083-04	9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[9]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
975	2018-09-11 08:11:36.034937-04	86	done_works/Proekt_Ala-Too_Novyi_god_2017_-_04-1.jpg	3		46	3
976	2018-09-11 08:11:36.037199-04	85	done_works/Proekt_Ala-Too_Novyi_god_2017_-_04_xMp29qb.jpg	3		46	3
977	2018-09-11 08:11:36.042543-04	84	done_works/Proekt_Ala-Too_Novyi_god_2016_-_01.JPG	3		46	3
978	2018-09-11 08:11:36.04374-04	75	done_works/Proekt_Ala-Too_Novyi_god_2017_-_08.jpg	3		46	3
979	2018-09-11 08:11:36.044709-04	74	done_works/Proekt_Ala-Too_Novyi_god_2017_-_07.jpg	3		46	3
980	2018-09-11 08:11:36.045644-04	73	done_works/Proekt_Ala-Too_Novyi_god_2017_-_06.jpg	3		46	3
981	2018-09-11 08:11:36.046615-04	72	done_works/Proekt_Ala-Too_Novyi_god_2017_-_05.jpg	3		46	3
982	2018-09-11 08:11:36.047536-04	71	done_works/Proekt_Ala-Too_Novyi_god_2017_-_04.jpg	3		46	3
983	2018-09-11 08:11:36.048715-04	70	done_works/Proekt_Ala-Too_Novyi_god_2017_-_03.jpg	3		46	3
984	2018-09-11 08:11:36.049804-04	69	done_works/Proekt_Ala-Too_Novyi_god_2017_-_02.jpg	3		46	3
985	2018-09-11 08:11:36.050688-04	68	done_works/Proekt_Ala-Too_Novyi_god_2017_-_01.jpg	3		46	3
986	2018-09-11 08:12:08.017244-04	249	done_works/Proekt_Ala-Too_Novyi_god_2017_-_00.jpg	1	[{"added": {}}]	46	3
987	2018-09-11 08:12:09.350708-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
988	2018-09-11 08:47:34.085839-04	250	done_works/Proekt_Ala-Too_Novyi_god_2017_-_02_RgB1DKB.jpg	1	[{"added": {}}]	46	3
989	2018-09-11 08:47:43.914588-04	251	done_works/Proekt_Ala-Too_Novyi_god_2017_-_03_m8V4wvr.jpg	1	[{"added": {}}]	46	3
990	2018-09-11 08:47:49.486977-04	252	done_works/Proekt_Ala-Too_Novyi_god_2017_-_01_UzUYjUt.jpg	1	[{"added": {}}]	46	3
991	2018-09-11 08:48:06.749585-04	253	done_works/Proekt_Ala-Too_Novyi_god_2017_-_04_NUvFRY9.jpg	1	[{"added": {}}]	46	3
992	2018-09-11 08:48:29.557507-04	254	done_works/Proekt_Ala-Too_Novyi_god_2017_-_07_4lae5FR.jpg	1	[{"added": {}}]	46	3
993	2018-09-11 08:48:34.88987-04	255	done_works/Proekt_Ala-Too_Novyi_god_2017_-_05_ngT3hFL.jpg	1	[{"added": {}}]	46	3
994	2018-09-11 08:48:41.517197-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[10]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
995	2018-09-12 05:29:55.214117-04	93	done_works/Proekt_Ala-Too_Novyi_god_2016_-_01_xzhUYE4.JPG	3		46	3
996	2018-09-12 05:29:55.216214-04	92	done_works/Proekt_Ala-Too_Novyi_god_2016_-_06.jpg	3		46	3
997	2018-09-12 05:29:55.217207-04	91	done_works/Proekt_Ala-Too_Novyi_god_2016_-_05.jpg	3		46	3
998	2018-09-12 05:29:55.218204-04	90	done_works/Proekt_Ala-Too_Novyi_god_2016_-_04.jpg	3		46	3
999	2018-09-12 05:29:55.219132-04	89	done_works/Proekt_Ala-Too_Novyi_god_2016_-_03.jpg	3		46	3
1000	2018-09-12 05:29:55.220054-04	87	done_works/Proekt_Ala-Too_Novyi_god_2016_-_02.jpg	3		46	3
1001	2018-09-12 05:30:23.809465-04	256	done_works/Proekt_Ala-Too_Novyi_god_2016_-_00.JPG	1	[{"added": {}}]	46	3
1002	2018-09-12 05:30:25.512818-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[11]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1003	2018-09-12 05:31:14.618374-04	257	done_works/Proekt_Ala-Too_Novyi_god_2016_-_03.JPG	1	[{"added": {}}]	46	3
1098	2018-09-17 01:58:40.192111-04	290	done_works/Proekt_Alleya_molodezhi_03_hCsoWA0.jpg	1	[{"added": {}}]	46	3
1004	2018-09-12 05:31:15.847578-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[11]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1005	2018-09-12 05:32:05.143735-04	258	done_works/Proekt_Ala-Too_Novyi_god_2016_-_06.JPG	1	[{"added": {}}]	46	3
1006	2018-09-12 05:32:13.933809-04	259	done_works/Proekt_Ala-Too_Novyi_god_2016_-_05.JPG	1	[{"added": {}}]	46	3
1007	2018-09-12 05:32:26.589932-04	260	done_works/Proekt_Ala-Too_Novyi_god_2016_-_04.JPG	1	[{"added": {}}]	46	3
1008	2018-09-12 05:32:37.542964-04	261	done_works/Proekt_Ala-Too_Novyi_god_2016_-_02.JPG	1	[{"added": {}}]	46	3
1009	2018-09-12 05:32:44.66867-04	262	done_works/Proekt_Ala-Too_Novyi_god_2016_-_01_pDxOZdZ.JPG	1	[{"added": {}}]	46	3
1010	2018-09-12 05:32:45.923891-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[11]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1011	2018-09-12 07:33:45.874235-04	12	декоративная подсветка стелы Аска- Таш, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[12]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1012	2018-09-12 07:34:09.177077-04	99	done_works/Proekt_Aska-_Tash_01_drLcZqC.jpg	3		46	3
1013	2018-09-12 07:34:09.179567-04	98	done_works/Proekt_Aska-_Tash_05.jpg	3		46	3
1014	2018-09-12 07:34:09.189871-04	97	done_works/Proekt_Aska-_Tash_04.jpg	3		46	3
1015	2018-09-12 07:34:09.191417-04	96	done_works/Proekt_Aska-_Tash_03.jpg	3		46	3
1016	2018-09-12 07:34:09.192515-04	95	done_works/Proekt_Aska-_Tash_02.jpg	3		46	3
1017	2018-09-12 07:34:09.193543-04	94	done_works/Proekt_Aska-_Tash_01.jpg	3		46	3
1018	2018-09-12 07:34:29.056396-04	263	done_works/Proekt_Aska-_Tash_01_B29afV6.jpg	1	[{"added": {}}]	46	3
1019	2018-09-12 07:34:30.314859-04	12	декоративная подсветка стелы Аска- Таш, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[12]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1020	2018-09-12 08:57:58.860807-04	264	done_works/Proekt_Aska-_Tash_02_Fm7Er7N.jpg	1	[{"added": {}}]	46	3
1021	2018-09-12 08:58:05.388406-04	265	done_works/Proekt_Aska-_Tash_03_TpPUTFd.jpg	1	[{"added": {}}]	46	3
1022	2018-09-12 08:58:11.311592-04	266	done_works/Proekt_Aska-_Tash_04_YpFdHjM.jpg	1	[{"added": {}}]	46	3
1023	2018-09-12 08:58:17.32629-04	267	done_works/Proekt_Aska-_Tash_05_N9zyc1s.jpg	1	[{"added": {}}]	46	3
1024	2018-09-12 08:58:18.758482-04	12	декоративная подсветка стелы Аска- Таш, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[12]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1025	2018-09-13 01:50:04.100555-04	106	done_works/Proekt_Flaf_KG_-_04_0U9uiKb.jpg	3		46	3
1026	2018-09-13 01:50:04.114573-04	105	done_works/Proekt_Flaf_KG_-_02.jpg	3		46	3
1027	2018-09-13 01:50:04.117333-04	104	done_works/Proekt_Flaf_KG_-_01_2GTXSTN.jpg	3		46	3
1028	2018-09-13 01:50:04.119074-04	103	done_works/Proekt_Flaf_KG_-_01_6tfgnjM.jpg	3		46	3
1029	2018-09-13 01:50:04.121266-04	102	done_works/Proekt_Flaf_KG_-_01.jpg	3		46	3
1030	2018-09-13 01:50:04.122788-04	101	done_works/Proekt_Flaf_KG_-_00.jpg	3		46	3
1031	2018-09-13 01:50:04.124354-04	100	done_works/Proekt_Flaf_KG_-_04.jpg	3		46	3
1032	2018-09-13 01:50:37.310825-04	268	done_works/Proekt_Flaf_KG_-_05.jpg	1	[{"added": {}}]	46	3
1033	2018-09-13 01:50:38.73855-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[13]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1034	2018-09-13 01:59:38.419748-04	269	done_works/Proekt_Flaf_KG_-_02_06PYoQY.jpg	1	[{"added": {}}]	46	3
1035	2018-09-13 01:59:48.355047-04	270	done_works/Proekt_Flaf_KG_-_04_M7k3GLF.jpg	1	[{"added": {}}]	46	3
1036	2018-09-13 02:03:38.896287-04	271	done_works/Proekt_Flaf_KG_-_01_R5jjjPH.jpg	1	[{"added": {}}]	46	3
1037	2018-09-13 02:03:44.71572-04	272	done_works/Proekt_Flaf_KG_-_03.jpg	1	[{"added": {}}]	46	3
1038	2018-09-13 02:03:46.100116-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[13]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1039	2018-09-13 02:04:27.126453-04	269	done_works/Proekt_Flaf_KG_-_02_06PYoQY.jpg	3		46	3
1040	2018-09-13 02:04:45.438978-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
1041	2018-09-15 00:36:35.013127-04	1	Настройка текстов на сайте	1	[{"added": {}}]	48	1
1042	2018-09-15 01:13:17.215024-04	1	Настройка текстов на сайте	2	[{"changed": {"fields": ["nav_services", "nav_works", "nav_products", "nav_about", "nav_contacts"]}}]	48	1
1043	2018-09-15 07:21:31.998697-04	25	Установка сети видеонаблюдения, г. Балыкчы	2	[{"changed": {"fields": ["customer", "created_at"]}}]	45	3
1044	2018-09-15 08:00:14.180469-04	8	Камеры видеонаблюдения	1	[{"added": {}}]	39	3
1045	2018-09-15 08:01:29.504311-04	8	Камеры видеонаблюдения	2	[{"changed": {"fields": ["is_active"]}}]	39	3
1046	2018-09-15 08:04:44.892015-04	12	видеокамера	1	[{"added": {}}]	36	3
1047	2018-09-15 08:05:13.912807-04	12	видеокамера	2	[{"added": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}, {"added": {"object": "\\u0422\\u0435\\u043c\\u043f\\u0435\\u0440\\u0430\\u0442\\u0443\\u0440\\u0430 \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
1048	2018-09-15 08:05:44.172117-04	12	видеокамера	2	[{"changed": {"fields": ["description", "description_ru"]}}]	36	3
1049	2018-09-15 08:06:37.046097-04	12	видеокамера	2	[{"changed": {"fields": ["value_kg", "value_type"], "object": "\\u0422\\u0435\\u043c\\u043f\\u0435\\u0440\\u0430\\u0442\\u0443\\u0440\\u0430 \\u0441\\u0432\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f", "name": "\\u0441\\u0432\\u043e\\u0439\\u0441\\u0442\\u0432\\u043e"}}]	36	3
1050	2018-09-15 08:07:09.185085-04	12	видеокамера	2	[{"changed": {"fields": ["main_image"]}}]	36	3
1051	2018-09-15 08:09:18.538861-04	1	Видеонаблюдение	1	[{"added": {}}]	40	3
1052	2018-09-15 08:09:21.752947-04	12	видеокамера	2	[{"changed": {"fields": ["brand"]}}]	36	3
1053	2018-09-15 08:09:49.372591-04	1	Видеонаблюдение	2	[{"changed": {"fields": ["thumbnail"]}}]	40	3
1054	2018-09-15 08:09:51.159753-04	12	видеокамера	2	[]	36	3
1055	2018-09-15 08:26:59.227195-04	1	Видеонаблюдение	3		40	3
1056	2018-09-15 08:51:52.106177-04	128	done_works/Proekt_U.Salieva_-_05_LmHFypb.jpg	3		46	3
1057	2018-09-15 08:51:52.113227-04	127	done_works/Proekt_U.Salieva_-_04_1qurXsI.jpg	3		46	3
1058	2018-09-15 08:51:52.114586-04	126	done_works/Proekt_U.Salieva_-_03_fXT3qDN.jpg	3		46	3
1059	2018-09-15 08:51:52.115582-04	125	done_works/Proekt_U.Salieva_-_02_008pyP5.jpg	3		46	3
1060	2018-09-15 08:51:52.116607-04	124	done_works/Proekt_U.Salieva_-_01_muMEbEU.jpg	3		46	3
1061	2018-09-15 08:52:13.885294-04	273	done_works/Proekt_U.Salieva_-_01_oWqEyOH.jpg	1	[{"added": {}}]	46	3
1062	2018-09-15 08:52:19.515449-04	274	done_works/Proekt_U.Salieva_-_02_jBMzshw.jpg	1	[{"added": {}}]	46	3
1063	2018-09-15 08:52:24.593945-04	275	done_works/Proekt_U.Salieva_-_03_k9EDClc.jpg	1	[{"added": {}}]	46	3
1064	2018-09-15 08:52:30.074522-04	276	done_works/Proekt_U.Salieva_-_04_MgGUKup.jpg	1	[{"added": {}}]	46	3
1065	2018-09-15 08:52:36.125637-04	277	done_works/Proekt_U.Salieva_-_05_UwxTaUr.jpg	1	[{"added": {}}]	46	3
1066	2018-09-15 08:52:37.483824-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1067	2018-09-15 09:00:10.446895-04	277	done_works/Proekt_U.Salieva_-_05_UwxTaUr.jpg	3		46	3
1068	2018-09-15 09:00:10.456028-04	276	done_works/Proekt_U.Salieva_-_04_MgGUKup.jpg	3		46	3
1069	2018-09-15 09:00:10.457327-04	275	done_works/Proekt_U.Salieva_-_03_k9EDClc.jpg	3		46	3
1070	2018-09-15 09:00:10.458581-04	274	done_works/Proekt_U.Salieva_-_02_jBMzshw.jpg	3		46	3
1071	2018-09-15 09:00:10.520675-04	273	done_works/Proekt_U.Salieva_-_01_oWqEyOH.jpg	3		46	3
1072	2018-09-15 09:00:49.744359-04	278	done_works/Proekt_U.Salieva_-_01_wB0gBqV.jpg	1	[{"added": {}}]	46	3
1073	2018-09-15 09:00:51.298567-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1074	2018-09-15 09:12:27.58173-04	278	done_works/Proekt_U.Salieva_-_01_wB0gBqV.jpg	3		46	3
1075	2018-09-15 09:12:41.054077-04	279	done_works/Proekt_U.Salieva_-_01_NZtmGvl.jpg	1	[{"added": {}}]	46	3
1076	2018-09-15 09:12:42.22028-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1077	2018-09-15 09:13:16.461562-04	280	done_works/Proekt_U.Salieva_-_02_3gffI7t.jpg	1	[{"added": {}}]	46	3
1078	2018-09-15 09:13:21.305648-04	281	done_works/Proekt_U.Salieva_-_03_siSjfRR.jpg	1	[{"added": {}}]	46	3
1079	2018-09-15 09:13:26.458599-04	282	done_works/Proekt_U.Salieva_-_04_6OfqETg.jpg	1	[{"added": {}}]	46	3
1080	2018-09-15 09:13:32.203533-04	283	done_works/Proekt_U.Salieva_-_05_b3Y7V7e.jpg	1	[{"added": {}}]	46	3
1081	2018-09-15 09:13:33.408994-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1082	2018-09-15 10:33:47.338418-04	139	done_works/Proekt_Park_T.Moldo_-_01.jpg	3		46	3
1083	2018-09-15 10:33:47.345531-04	138	done_works/Proekt_Park_T.Moldo_-_04.jpg	3		46	3
1084	2018-09-15 10:33:47.346532-04	137	done_works/Proekt_Park_T.Moldo_-_03.jpg	3		46	3
1085	2018-09-15 10:33:47.347496-04	136	done_works/Proekt_Park_T.Moldo_-_02.jpg	3		46	3
1086	2018-09-15 10:34:17.503315-04	284	done_works/Proekt_Park_T.Moldo_-_01_oIrH0jc.jpg	1	[{"added": {}}]	46	3
1087	2018-09-15 10:34:18.931055-04	15	Реконструкция освещения в парке им. Т. Молдо, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[15]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1088	2018-09-15 10:35:10.681431-04	285	done_works/Proekt_Park_T.Moldo_-_02_LE9g6J5.jpg	1	[{"added": {}}]	46	3
1089	2018-09-15 10:35:16.049685-04	286	done_works/Proekt_Park_T.Moldo_-_03_IqvgnZU.jpg	1	[{"added": {}}]	46	3
1090	2018-09-15 10:35:21.960151-04	287	done_works/Proekt_Park_T.Moldo_-_04_3HNSMg7.jpg	1	[{"added": {}}]	46	3
1091	2018-09-15 10:35:23.067705-04	15	Реконструкция освещения в парке им. Т. Молдо, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[15]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1092	2018-09-17 01:56:12.991814-04	152	done_works/Proekt_Alleya_molodezhi_04.JPG	3		46	3
1093	2018-09-17 01:56:12.994007-04	151	done_works/Proekt_Alleya_molodezhi_03_tRGUmgy.jpg	3		46	3
1094	2018-09-17 01:56:12.995305-04	150	done_works/Proekt_Alleya_molodezhi_02_clUeA6F.jpg	3		46	3
1095	2018-09-17 01:56:12.996721-04	149	done_works/Proekt_Alleya_molodezhi_01_Y7hdvhm.jpg	3		46	3
1096	2018-09-17 01:58:29.821777-04	288	done_works/Proekt_Alleya_molodezhi_01_QHSriZh.jpg	1	[{"added": {}}]	46	3
1097	2018-09-17 01:58:35.108251-04	289	done_works/Proekt_Alleya_molodezhi_02_EcGe0FB.jpg	1	[{"added": {}}]	46	3
1099	2018-09-17 01:58:46.874308-04	291	done_works/Proekt_Alleya_molodezhi_04.jpg	1	[{"added": {}}]	46	3
1100	2018-09-17 01:58:52.180321-04	292	done_works/Proekt_Alleya_molodezhi_05.JPG	1	[{"added": {}}]	46	3
1101	2018-09-17 01:58:53.644666-04	18	Освещение Аллеи Молодежи, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[17]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1102	2018-09-17 07:52:44.933202-04	156	done_works/Proekt_Gazprom_03.jpg	3		46	3
1103	2018-09-17 07:52:44.939627-04	155	done_works/Proekt_Gazprom_01_MrDnDXl.jpg	3		46	3
1104	2018-09-17 07:52:44.942265-04	154	done_works/Proekt_Gazprom_02.jpg	3		46	3
1105	2018-09-17 07:53:18.885898-04	293	done_works/Proekt_Gazprom_02_MHZ00Ms.jpg	1	[{"added": {}}]	46	3
1106	2018-09-17 07:53:23.931828-04	294	done_works/Proekt_Gazprom_01_bCA3kIt.jpg	1	[{"added": {}}]	46	3
1107	2018-09-17 07:53:28.390496-04	295	done_works/Proekt_Gazprom_03_Mw1Q13F.jpg	1	[{"added": {}}]	46	3
1108	2018-09-17 07:53:29.835893-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[18]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1109	2018-09-18 02:52:34.404263-04	163	done_works/Proekt_vnutrennee_osveschenie_07.jpg	3		46	3
1110	2018-09-18 02:52:34.407276-04	162	done_works/Proekt_vnutrennee_osveschenie_06.jpg	3		46	3
1111	2018-09-18 02:52:34.408457-04	161	done_works/Proekt_vnutrennee_osveschenie_05.jpg	3		46	3
1112	2018-09-18 02:52:34.412344-04	160	done_works/Proekt_vnutrennee_osveschenie_04.jpg	3		46	3
1113	2018-09-18 02:52:34.415531-04	159	done_works/Proekt_vnutrennee_osveschenie_03.jpg	3		46	3
1114	2018-09-18 02:52:34.417317-04	158	done_works/Proekt_vnutrennee_osveschenie_02.jpg	3		46	3
1115	2018-09-18 02:52:34.418911-04	157	done_works/Proekt_vnutrennee_osveschenie_01.jpg	3		46	3
1116	2018-09-18 02:53:16.258466-04	296	done_works/Proekt_vnutrennee_osveschenie_01_dfOTmQx.jpg	1	[{"added": {}}]	46	3
1117	2018-09-18 02:53:22.254791-04	297	done_works/Proekt_vnutrennee_osveschenie_02_CJ2jt06.jpg	1	[{"added": {}}]	46	3
1118	2018-09-18 02:53:27.347303-04	298	done_works/Proekt_vnutrennee_osveschenie_03_86xyW6Q.jpg	1	[{"added": {}}]	46	3
1119	2018-09-18 02:53:36.683854-04	299	done_works/Proekt_vnutrennee_osveschenie_04_PZH5Fxt.jpg	1	[{"added": {}}]	46	3
1120	2018-09-18 02:53:43.111206-04	300	done_works/Proekt_vnutrennee_osveschenie_05_4Rdb2YO.jpg	1	[{"added": {}}]	46	3
1121	2018-09-18 02:53:48.752494-04	301	done_works/Proekt_vnutrennee_osveschenie_06_tLOSw5G.jpg	1	[{"added": {}}]	46	3
1122	2018-09-18 02:53:53.937607-04	302	done_works/Proekt_vnutrennee_osveschenie_07_Zebqwog.jpg	1	[{"added": {}}]	46	3
1123	2018-09-18 02:53:55.575585-04	20	Внутреннее освещение магазинов	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[19]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1124	2018-09-18 03:52:01.400936-04	166	done_works/Proekt_Okt_administraziya_03.jpg	3		46	3
1125	2018-09-18 03:52:01.404813-04	165	done_works/Proekt_Okt_administraziya_02.jpg	3		46	3
1126	2018-09-18 03:52:01.408782-04	164	done_works/Proekt_Okt_administraziya_01.jpg	3		46	3
1127	2018-09-18 03:52:41.599127-04	167	done_works/Proekt_Okt_administraziya_04.jpg	3		46	3
1128	2018-09-18 03:53:06.234052-04	303	done_works/Proekt_Okt_administraziya_01_V7vSgyT.jpg	1	[{"added": {}}]	46	3
1129	2018-09-18 03:53:11.750436-04	304	done_works/Proekt_Okt_administraziya_02_LEFFHk4.jpg	1	[{"added": {}}]	46	3
1130	2018-09-18 03:53:18.978218-04	305	done_works/Proekt_Okt_administraziya_03_fUvJ7Lw.jpg	1	[{"added": {}}]	46	3
1131	2018-09-18 03:53:24.222935-04	306	done_works/Proekt_Okt_administraziya_04_s8qCPwX.jpg	1	[{"added": {}}]	46	3
1132	2018-09-18 03:53:25.449961-04	21	Освещение здания Октябрьской администрации, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[20]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1133	2018-09-18 07:03:58.553835-04	170	done_works/Proekt_igry_Kochevnikov_02_0ktIDEA.jpg	3		46	3
1134	2018-09-18 07:03:58.558439-04	168	done_works/Proekt_igry_Kochevnikov_01.jpg	3		46	3
1135	2018-09-18 07:04:25.594785-04	307	done_works/Proekt_igry_Kochevnikov_02_o81H38G.jpg	1	[{"added": {}}]	46	3
1136	2018-09-18 07:04:32.246777-04	308	done_works/Proekt_igry_Kochevnikov_03.jpg	1	[{"added": {}}]	46	3
1137	2018-09-18 07:04:37.135777-04	309	done_works/Proekt_igry_Kochevnikov_00.jpg	1	[{"added": {}}]	46	3
1138	2018-09-18 07:04:43.455126-04	310	done_works/Proekt_igry_Kochevnikov_01_3pRvt8q.jpg	1	[{"added": {}}]	46	3
1139	2018-09-18 07:04:44.731353-04	22	Освещение ипподрома для Игр кочевников, 2014г.	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[21]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1140	2018-09-18 07:47:00.6987-04	177	done_works/Proekt_Balykchy_perehody_02_S1fCBKE.jpg	3		46	3
1141	2018-09-18 07:47:00.700962-04	171	done_works/Proekt_Balykchy_perehody_01.jpg	3		46	3
1142	2018-09-18 07:47:25.019223-04	311	done_works/Proekt_Balykchy_perehody_00.jpg	1	[{"added": {}}]	46	3
1143	2018-09-18 07:47:30.172844-04	312	done_works/Proekt_Balykchy_perehody_01_BD29eE5.jpg	1	[{"added": {}}]	46	3
1144	2018-09-18 07:47:35.381341-04	313	done_works/Proekt_Balykchy_perehody_02_PxO1a2L.jpg	1	[{"added": {}}]	46	3
1145	2018-09-18 07:47:36.458194-04	23	Освещение пешеходных переходов, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[22]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1146	2018-09-18 08:00:19.69927-04	182	done_works/Proekt_Balykchy_osveshenie_05.jpg	3		46	3
1147	2018-09-18 08:00:19.704377-04	181	done_works/Proekt_Balykchy_osveshenie_04.jpg	3		46	3
1148	2018-09-18 08:00:19.705398-04	180	done_works/Proekt_Balykchy_osveshenie_03.jpg	3		46	3
1149	2018-09-18 08:00:19.70636-04	179	done_works/Proekt_Balykchy_osveshenie_02.jpg	3		46	3
1150	2018-09-18 08:00:19.707392-04	178	done_works/Proekt_Balykchy_osveshenie_01.jpg	3		46	3
1151	2018-09-18 08:09:06.919122-04	314	done_works/Proekt_Balykchy_osveshenie_02_X0ffpF2.jpg	1	[{"added": {}}]	46	3
1152	2018-09-18 08:09:13.173829-04	315	done_works/Proekt_Balykchy_osveshenie_03_4v3flwh.jpg	1	[{"added": {}}]	46	3
1153	2018-09-18 08:09:18.368585-04	316	done_works/Proekt_Balykchy_osveshenie_01_Bs3iXY7.jpg	1	[{"added": {}}]	46	3
1154	2018-09-18 08:09:24.353955-04	317	done_works/Proekt_Balykchy_osveshenie_05_yrdaeo5.jpg	1	[{"added": {}}]	46	3
1155	2018-09-18 08:09:25.532559-04	24	Уличное освещение г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[23]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1156	2018-09-18 09:20:40.444518-04	185	done_works/Proekt_Balykchy_video_02.jpg	3		46	3
1157	2018-09-18 09:20:40.446498-04	184	done_works/Proekt_Balykchy_video_01.jpg	3		46	3
1158	2018-09-18 09:21:04.404083-04	318	done_works/Proekt_Balykchy_video_01_sFgjmeQ.jpg	1	[{"added": {}}]	46	3
1159	2018-09-18 09:21:15.661391-04	319	done_works/Proekt_Balykchy_video_02_g7Sr3a7.jpg	1	[{"added": {}}]	46	3
1160	2018-09-18 09:21:20.750307-04	320	done_works/Proekt_Balykchy_video_03.jpg	1	[{"added": {}}]	46	3
1161	2018-09-18 09:21:25.81592-04	321	done_works/Proekt_Balykchy_video_04.jpg	1	[{"added": {}}]	46	3
1162	2018-09-18 09:21:31.827801-04	322	done_works/Proekt_Balykchy_video_05.jpg	1	[{"added": {}}]	46	3
1163	2018-09-18 09:21:33.323177-04	25	Установка сети видеонаблюдения, г. Балыкчы	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[24]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1164	2018-09-18 10:02:43.568159-04	194	done_works/Proekt_Park_Fuchika_05.jpg	3		46	3
1165	2018-09-18 10:02:43.570777-04	193	done_works/Proekt_Park_Fuchika_04.jpg	3		46	3
1166	2018-09-18 10:02:43.57179-04	192	done_works/Proekt_Park_Fuchika_03.jpg	3		46	3
1167	2018-09-18 10:02:43.572752-04	191	done_works/Proekt_Park_Fuchika_02.jpg	3		46	3
1168	2018-09-18 10:02:43.573704-04	190	done_works/Proekt_Park_Fuchika_01_gxfbOCd.jpg	3		46	3
1169	2018-09-18 10:02:43.574713-04	189	done_works/Proekt_Park_Fuchika_00.jpg	3		46	3
1170	2018-09-18 10:03:07.603556-04	323	done_works/Proekt_Park_Fuchika_00_eUrm4gc.jpg	1	[{"added": {}}]	46	3
1171	2018-09-18 10:03:12.440664-04	324	done_works/Proekt_Park_Fuchika_01_u20f4sY.jpg	1	[{"added": {}}]	46	3
1172	2018-09-18 10:03:17.365914-04	325	done_works/Proekt_Park_Fuchika_02_lvUOL0R.jpg	1	[{"added": {}}]	46	3
1173	2018-09-18 10:03:23.96616-04	326	done_works/Proekt_Park_Fuchika_03_4To59Dc.jpg	1	[{"added": {}}]	46	3
1174	2018-09-18 10:03:39.83585-04	327	done_works/Proekt_Park_Fuchika_05_H2n0JaC.jpg	1	[{"added": {}}]	46	3
1175	2018-09-18 10:07:49.614179-04	328	done_works/Proekt_Park_Fuchika_04_n63uHtW.jpg	1	[{"added": {}}]	46	3
1176	2018-09-18 10:07:51.095461-04	26	Реконструкция парка им. Ю. Фучика, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}, {"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[25]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1177	2018-09-18 11:06:11.570162-04	27	Реконструкция парка Победы им. Д. Асанова	1	[{"added": {}}]	45	3
1178	2018-09-18 11:07:25.022213-04	27	Реконструкция парка Победы им. Д. Асанова	2	[{"changed": {"fields": ["created_at"]}}]	45	3
1179	2018-09-18 11:08:21.276599-04	329	done_works/Proekt_Park_pobedy_01.jpg	1	[{"added": {}}]	46	3
1180	2018-09-18 11:08:27.055969-04	330	done_works/Proekt_Park_pobedy_02.jpg	1	[{"added": {}}]	46	3
1181	2018-09-18 11:08:32.157568-04	331	done_works/Proekt_Park_pobedy_05.jpg	1	[{"added": {}}]	46	3
1182	2018-09-18 11:08:33.557065-04	27	Реконструкция парка Победы им. Д. Асанова	2	[{"changed": {"fields": ["created_at"]}}, {"added": {"object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[26]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1183	2018-09-19 00:18:38.169903-04	5	Фасадное освещение ЖК "Монреаль"	2	[{"changed": {"fields": ["address", "created_at"]}}]	45	3
1184	2018-09-19 04:32:19.117813-04	1	Страница О Нас	2	[{"changed": {"fields": ["body"]}}]	28	3
1185	2018-09-19 04:35:44.387924-04	1	тт	1	[{"added": {}}]	12	3
1186	2018-09-19 04:36:08.122604-04	1	тт	3		12	3
1187	2018-09-19 04:39:44.190215-04	1	22	1	[{"added": {}}]	43	3
1188	2018-09-19 04:40:39.623268-04	1	22	2	[{"changed": {"fields": ["preview"]}}, {"added": {"object": "news/gallery/Proekt_Park_pobedy_02.jpg", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0438"}}, {"added": {"object": "news/gallery/Proekt_Park_pobedy_05.jpg", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0438"}}]	43	3
1189	2018-09-19 04:48:48.790287-04	1	22	2	[{"deleted": {"object": "news/gallery/Proekt_Park_pobedy_02.jpg", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0438"}}, {"deleted": {"object": "news/gallery/Proekt_Park_pobedy_05.jpg", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0438"}}]	43	3
1190	2018-09-19 04:49:03.379431-04	1	22	2	[{"changed": {"fields": ["preview"]}}, {"added": {"object": "news/gallery/Novost_01_-_1.jpg", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0438"}}]	43	3
1191	2018-09-19 04:51:41.321742-04	1	22	3		43	3
1192	2018-09-19 04:54:38.26274-04	2	Юбилейная международная специализированная выставка «Энергетика и освещение»	1	[{"added": {}}]	43	3
1193	2018-09-19 04:55:11.27304-04	2	Юбилейная международная специализированная выставка «Энергетика и освещение»	2	[{"added": {"object": "news/gallery/Novost_01_-_1_TZ5JEoZ.jpg", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0438"}}]	43	3
1194	2018-09-19 04:59:11.860068-04	2	Юбилейная международная специализированная выставка «Энергетика и освещение»	2	[{"deleted": {"object": "news/gallery/Novost_01_-_1_TZ5JEoZ.jpg", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0438"}}]	43	3
1195	2018-09-19 04:59:49.817299-04	2	Юбилейная международная специализированная выставка «Энергетика и освещение»	2	[{"added": {"object": "news/gallery/Novost_01-1.jpg", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0438"}}]	43	3
1252	2018-09-24 08:35:34.62146-04	12	Декоративная подсветка стелы Аска- Таш	2	[{"changed": {"fields": ["title", "title_ru", "created_at"]}}]	45	3
1196	2018-09-20 00:31:36.481138-04	2	Юбилейная международная специализированная выставка «Энергетика и освещение»	2	[]	43	3
1197	2018-09-21 01:59:53.458403-04	2	Юбилейная международная специализированная выставка «Энергетика и освещение»	2	[]	43	3
1198	2018-09-22 06:59:19.931234-04	1	Настройка сайтов	2	[{"changed": {"fields": ["facebook"]}}]	25	3
1199	2018-09-22 07:01:34.991286-04	8	Камеры видеонаблюдения	2	[{"changed": {"fields": ["is_active"]}}]	39	3
1200	2018-09-22 07:02:52.04481-04	1	Главный офис	2	[{"changed": {"fields": ["phone"]}}]	16	3
1201	2018-09-22 08:18:15.692643-04	26	Реконструкция парка им. Ю. Фучика, г. Бишкек	2	[{"changed": {"fields": ["created_at"]}}]	45	3
1202	2018-09-22 09:18:02.22171-04	1	Настройка сайтов	2	[{"changed": {"fields": ["facebook", "instagram"]}}]	25	3
1203	2018-09-22 09:24:07.134093-04	19	/application/assets/uploads/slider/Slider_05_-_proizvodstvo_PH4zurk.jpg	2	[{"changed": {"fields": ["short_description"]}}]	18	3
1204	2018-09-22 09:25:29.105744-04	18	/application/assets/uploads/slider/Slider_04_-_decorativnoe_osveschenie.jpg	2	[{"changed": {"fields": ["short_description"]}}]	18	3
1205	2018-09-22 09:25:47.462294-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie_V2s0ycg.jpg	2	[]	18	3
1206	2018-09-22 09:26:26.881606-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie_V2s0ycg.jpg	2	[{"changed": {"fields": ["link", "short_description"]}}]	18	3
1207	2018-09-22 09:27:52.86692-04	21	/application/assets/uploads/slider/Slider_07_-_vnutrennee_osveschenie.jpg	2	[{"changed": {"fields": ["short_description"]}}]	18	3
1208	2018-09-22 09:28:49.835122-04	22	/application/assets/uploads/slider/Slider_02_-_prazdnichnoe_osveschenie_V2s0ycg.jpg	2	[{"changed": {"fields": ["short_description"]}}]	18	3
1209	2018-09-23 10:21:33.597986-04	2	Forbes	1	[{"added": {}}]	40	1
1210	2018-09-23 10:22:38.513298-04	3	Coca-Cola	1	[{"added": {}}]	40	1
1211	2018-09-23 10:23:06.264923-04	4	MegaCom	1	[{"added": {}}]	40	1
1212	2018-09-23 10:23:28.589131-04	5	Beeline	1	[{"added": {}}]	40	1
1213	2018-09-23 10:24:20.350901-04	6	Netflix	1	[{"added": {}}]	40	1
1214	2018-09-23 10:24:46.572961-04	7	Forbes	1	[{"added": {}}]	40	1
1215	2018-09-23 10:24:55.304758-04	8	Forbes	1	[{"added": {}}]	40	1
1216	2018-09-23 10:25:02.219967-04	9	Forbes	1	[{"added": {}}]	40	1
1217	2018-09-23 10:25:07.35665-04	10	Forbes	1	[{"added": {}}]	40	1
1218	2018-09-23 10:25:12.812608-04	11	Forbes	1	[{"added": {}}]	40	1
1219	2018-09-23 10:26:02.820627-04	2	Юбилейная международная специализированная выставка «Энергетика и освещение»	2	[{"changed": {"fields": ["preview"]}}]	43	1
1220	2018-09-23 10:26:23.549994-04	2	Юбилейная международная специализированная выставка «Энергетика и освещение»	2	[{"changed": {"fields": ["preview"]}}]	43	1
1221	2018-09-24 02:48:18.862257-04	11	Forbes	2	[{"changed": {"fields": ["thumbnail", "link"]}}]	40	3
1222	2018-09-24 02:48:32.699153-04	11	CAPS	2	[{"changed": {"fields": ["title"]}}]	40	3
1223	2018-09-24 03:45:19.424542-04	10	OOBA	2	[{"changed": {"fields": ["title", "thumbnail", "link"]}}]	40	3
1224	2018-09-24 03:53:14.260696-04	9	AVANGARD STYLE	2	[{"changed": {"fields": ["title", "thumbnail", "link"]}}]	40	3
1225	2018-09-24 03:57:03.927628-04	8	Glavstroy	2	[{"changed": {"fields": ["title", "thumbnail", "link"]}}]	40	3
1226	2018-09-24 03:58:43.297287-04	7	Imaratstroy	2	[{"changed": {"fields": ["title", "thumbnail", "link"]}}]	40	3
1227	2018-09-24 04:02:38.008721-04	6	Emark Group	2	[{"changed": {"fields": ["title", "thumbnail", "link"]}}]	40	3
1228	2018-09-24 04:04:46.058189-04	5	Meria Bishkek	2	[{"changed": {"fields": ["title", "thumbnail", "link"]}}]	40	3
1229	2018-09-24 04:06:20.447766-04	10	OOBA	2	[{"changed": {"fields": ["thumbnail"]}}]	40	3
1230	2018-09-24 04:10:30.064271-04	4	Meria Balykchy	2	[{"changed": {"fields": ["title", "thumbnail", "link"]}}]	40	3
1231	2018-09-24 04:13:45.350457-04	3	Meria Karakol	2	[{"changed": {"fields": ["title", "thumbnail", "link"]}}]	40	3
1232	2018-09-24 04:20:37.273212-04	2	Meria Tokmok	2	[{"changed": {"fields": ["title", "thumbnail", "link"]}}]	40	3
1233	2018-09-24 04:23:01.452933-04	12	Jannat Regency	1	[{"added": {}}]	40	3
1234	2018-09-24 04:26:56.330875-04	11	CAPS	2	[{"changed": {"fields": ["thumbnail"]}}]	40	3
1235	2018-09-24 04:27:06.544552-04	10	OOBA	2	[{"changed": {"fields": ["thumbnail"]}}]	40	3
1236	2018-09-24 04:27:11.548181-04	9	AVANGARD STYLE	2	[{"changed": {"fields": ["thumbnail"]}}]	40	3
1237	2018-09-24 04:27:17.332543-04	8	Glavstroy	2	[{"changed": {"fields": ["thumbnail"]}}]	40	3
1238	2018-09-24 04:27:23.258777-04	7	Imaratstroy	2	[{"changed": {"fields": ["thumbnail"]}}]	40	3
1239	2018-09-24 04:27:31.601353-04	6	Emark Group	2	[{"changed": {"fields": ["thumbnail"]}}]	40	3
1240	2018-09-24 04:27:37.681196-04	5	Meria Bishkek	2	[{"changed": {"fields": ["thumbnail"]}}]	40	3
1241	2018-09-24 04:31:50.018796-04	13	KICB	1	[{"added": {}}]	40	3
1242	2018-09-24 04:31:56.45526-04	13	KICB	2	[]	40	3
1243	2018-09-24 08:12:16.975774-04	14	Nurzaman	1	[{"added": {}}]	40	3
1244	2018-09-24 08:28:36.198765-04	9	Опоры освещения	2	[{"changed": {"fields": ["main_image"]}}]	36	3
1245	2018-09-24 08:29:09.408469-04	9	Опоры освещения	2	[{"changed": {"fields": ["image"], "object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
1246	2018-09-24 08:29:56.049891-04	9	Опоры освещения	2	[{"deleted": {"object": "ProductImage object", "name": "\\u041a\\u0430\\u0440\\u0442\\u0438\\u043d\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u0432"}}]	36	3
1247	2018-09-24 08:30:54.982489-04	26	Реконструкция парка им. Ю. Фучика, г. Бишкек	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
1248	2018-09-24 08:31:47.000706-04	25	Установка сети видеонаблюдения, г. Балыкчы	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
1249	2018-09-24 08:33:21.154181-04	18	Освещение Аллеи Молодежи, г. Бишкек	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
1250	2018-09-24 08:34:10.682984-04	14	Освещение сквера им. У. Салиевой, г. Бишкек	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
1251	2018-09-24 08:34:39.7739-04	13	Праздничное оформление Площади Ала- Тоо, Бишкек, 2017г.	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
1293	2018-09-25 04:28:12.642783-04	18	Реконструкция Аллеи Молодежи	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1253	2018-09-24 08:36:24.649573-04	27	Реконструкция парка Победы им. Д. Асанова	2	[{"changed": {"fields": ["description", "description_ru", "created_at"]}}]	45	3
1254	2018-09-24 08:36:47.312613-04	26	Реконструкция парка им. Ю. Фучика	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
1255	2018-09-24 08:37:09.698178-04	25	Установка сети видеонаблюдения	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
1256	2018-09-24 08:37:33.493722-04	24	Уличное освещение	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
1257	2018-09-24 08:38:20.05429-04	23	Освещение пешеходных переходов	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
1258	2018-09-24 08:38:29.418737-04	22	Освещение ипподрома для Игр кочевников, 2014г.	2	[{"changed": {"fields": ["created_at"]}}]	45	3
1259	2018-09-24 08:38:47.896825-04	21	Освещение здания Октябрьской администрации	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
1260	2018-09-24 08:39:24.018359-04	18	Реконструкция Аллеи Молодежи	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
1261	2018-09-24 08:39:33.703579-04	18	Реконструкция Аллеи Молодежи	2	[{"changed": {"fields": ["created_at"]}}]	45	3
1262	2018-09-24 08:39:55.518693-04	15	Реконструкция освещения в парке им. Т. Молдо	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
1263	2018-09-24 08:40:12.276883-04	14	Освещение сквера им. У. Салиевой	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
1264	2018-09-24 08:40:34.936532-04	13	Праздничное оформление Площади Ала- Тоо, 2017г.	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
1265	2018-09-24 08:41:40.90762-04	7	Освещение супермаркета "Боорсок"	2	[{"changed": {"fields": ["title", "title_ru", "short_description", "short_description_ru", "created_at"]}}]	45	3
1266	2018-09-24 08:42:35.408997-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1267	2018-09-24 08:42:49.597758-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1268	2018-09-24 08:43:25.440377-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1269	2018-09-24 08:44:26.071183-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["created_at"]}}]	45	3
1270	2018-09-24 08:45:22.364867-04	27	Реконструкция парка Победы им. Д. Асанова	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1271	2018-09-24 08:47:16.766104-04	24	Уличное освещение	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1272	2018-09-24 08:48:40.343415-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1273	2018-09-24 08:48:59.509246-04	18	Реконструкция Аллеи Молодежи	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1274	2018-09-24 08:49:27.684312-04	15	Реконструкция освещения в парке им. Т. Молдо	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1275	2018-09-24 08:49:46.14976-04	14	Освещение сквера им. У. Салиевой	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1276	2018-09-24 08:49:58.128639-04	13	Праздничное оформление Площади Ала- Тоо, 2017г.	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1277	2018-09-24 08:50:07.660998-04	12	Декоративная подсветка стелы Аска- Таш	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1278	2018-09-24 08:50:17.451498-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1279	2018-09-24 08:50:29.059653-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1280	2018-09-24 08:50:37.611501-04	9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1281	2018-09-24 08:50:47.333858-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	2	[{"changed": {"fields": ["customer_link", "created_at"]}}]	45	3
1282	2018-09-24 09:38:49.727902-04	2	Юбилейная международная специализированная выставка «Энергетика и освещение»	2	[{"changed": {"fields": ["preview", "short_description", "short_description_ru"]}}]	43	3
1283	2018-09-25 04:21:39.204542-04	27	Реконструкция парка Победы им. Д. Асанова	2	[{"changed": {"fields": ["description", "description_ru", "year_of_exec"]}}]	45	3
1284	2018-09-25 04:26:29.708807-04	26	Реконструкция парка им. Ю. Фучика	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1285	2018-09-25 04:26:38.140511-04	25	Установка сети видеонаблюдения	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1286	2018-09-25 04:26:57.030446-04	24	Уличное освещение	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1287	2018-09-25 04:27:06.57613-04	24	Уличное освещение	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1288	2018-09-25 04:27:16.610183-04	23	Освещение пешеходных переходов	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1289	2018-09-25 04:27:26.770967-04	22	Освещение ипподрома для Игр кочевников, 2014г.	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1290	2018-09-25 04:27:38.35696-04	21	Освещение здания Октябрьской администрации	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1291	2018-09-25 04:27:52.804293-04	20	Внутреннее освещение магазинов	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1292	2018-09-25 04:28:02.656201-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1345	2018-10-07 23:14:56.720009-04	1	Настройка сайтов	2	[]	25	3
1294	2018-09-25 04:28:30.063448-04	15	Реконструкция освещения в парке им. Т. Молдо	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1295	2018-09-25 04:28:38.863153-04	14	Освещение сквера им. У. Салиевой	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1296	2018-09-25 04:28:52.103423-04	13	Праздничное оформление Площади Ала- Тоо, 2017г.	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1297	2018-09-25 04:29:06.165609-04	12	Декоративная подсветка стелы Аска- Таш	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1298	2018-09-25 04:29:17.902385-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1299	2018-09-25 04:29:25.464536-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1300	2018-09-25 04:29:38.925379-04	9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1301	2018-09-25 04:29:50.83019-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1302	2018-09-25 04:30:53.825948-04	7	Освещение супермаркета "Боорсок"	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1303	2018-09-25 04:31:25.558752-04	6	Наружнее освещение супермаркетов "Глобус"	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1304	2018-09-25 04:31:33.224502-04	5	Фасадное освещение ЖК "Монреаль"	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1305	2018-09-25 04:31:47.223837-04	4	Фасадное освещение гостиничного комплекса Jannat	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1306	2018-09-25 04:33:34.205288-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1307	2018-09-25 04:33:52.849709-04	2	Уличное освещение, г. Бишкек	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1308	2018-09-25 04:34:14.107307-04	27	Реконструкция парка Победы им. Д. Асанова	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1309	2018-09-25 04:34:19.368519-04	26	Реконструкция парка им. Ю. Фучика	2	[]	45	3
1310	2018-09-25 04:34:23.472082-04	25	Установка сети видеонаблюдения	2	[]	45	3
1311	2018-09-25 04:34:29.724489-04	24	Уличное освещение	2	[]	45	3
1312	2018-09-25 04:34:35.219286-04	23	Освещение пешеходных переходов	2	[]	45	3
1313	2018-09-25 04:34:41.205481-04	21	Освещение здания Октябрьской администрации	2	[]	45	3
1314	2018-09-25 04:34:47.541278-04	19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	2	[]	45	3
1315	2018-09-25 04:34:54.541956-04	18	Реконструкция Аллеи Молодежи	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1316	2018-09-25 04:35:02.900216-04	15	Реконструкция освещения в парке им. Т. Молдо	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1317	2018-09-25 04:35:11.459239-04	13	Праздничное оформление Площади Ала- Тоо, 2017г.	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1318	2018-09-25 04:35:17.629597-04	12	Декоративная подсветка стелы Аска- Таш	2	[]	45	3
1319	2018-09-25 04:35:22.877536-04	11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	2	[]	45	3
1320	2018-09-25 04:35:27.800399-04	10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	2	[]	45	3
1321	2018-09-25 04:35:35.132049-04	9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1322	2018-09-25 04:35:43.682983-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	2	[{"changed": {"fields": ["year_of_exec"]}}]	45	3
1323	2018-09-25 04:35:49.657514-04	7	Освещение супермаркета "Боорсок"	2	[]	45	3
1324	2018-09-25 04:35:56.300135-04	6	Наружнее освещение супермаркетов "Глобус"	2	[]	45	3
1325	2018-09-25 04:36:05.024877-04	3	Освещение Бизнес- Центра, г. Бишкек	2	[]	45	3
1326	2018-09-26 08:57:48.789316-04	25	Установка сети видеонаблюдения	2	[{"changed": {"fields": ["description", "description_ru"]}}]	45	3
1327	2018-09-26 23:26:16.543919-04	3	Как выбрать температуру освещения?	1	[{"added": {}}]	43	3
1328	2018-09-26 23:36:57.245177-04	3	Как выбрать температуру освещения?	2	[{"changed": {"fields": ["preview", "short_description", "short_description_ru"]}}]	43	3
1329	2018-09-26 23:39:33.406917-04	3	Как выбрать температуру освещения?	2	[{"changed": {"fields": ["preview"]}}]	43	3
1330	2018-09-26 23:41:52.104872-04	3	Как выбрать температуру освещения?	2	[{"changed": {"fields": ["content", "content_ru"]}}]	43	3
1331	2018-09-27 00:44:35.978234-04	4	Почему светодиоды?	1	[{"added": {}}]	43	3
1332	2018-09-27 00:56:43.904365-04	4	Почему светодиоды?	2	[{"changed": {"fields": ["preview"]}}]	43	3
1333	2018-09-27 02:07:34.372739-04	5	Чего вы не знали о светодиодах	1	[{"added": {}}]	43	3
1334	2018-09-27 02:31:21.381932-04	2	Юбилейная международная специализированная выставка «Энергетика и освещение»	2	[]	43	3
1335	2018-09-27 05:01:48.851261-04	2	Международная выставка «Энергетика и освещение»	2	[{"changed": {"fields": ["title", "title_ru"]}}]	43	3
1336	2018-09-28 04:40:12.696967-04	8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	2	[]	45	3
1337	2018-10-02 05:53:18.766648-04	14	Освещение сквера им. У. Салиевой	2	[{"changed": {"fields": ["description", "description_ru"]}}]	45	3
1338	2018-10-02 23:57:07.553101-04	18	Реконструкция Аллеи Молодежи	2	[{"changed": {"fields": ["description", "description_ru"]}}]	45	3
1339	2018-10-04 06:00:14.131013-04	22	Освещение ипподрома для Игр кочевников, 2014г.	2	[{"changed": {"fields": ["description", "description_ru"]}}]	45	3
1340	2018-10-05 01:07:29.363694-04	318	done_works/Proekt_Balykchy_video_01_sFgjmeQ.jpg	3		46	3
1341	2018-10-05 01:31:32.327005-04	26	Реконструкция парка им. Ю. Фучика	2	[]	45	3
1342	2018-10-07 23:06:10.23263-04	1	Главный офис	2	[{"changed": {"fields": ["phone"]}}]	16	3
1343	2018-10-07 23:07:07.585555-04	1	Главный офис	2	[]	16	3
1344	2018-10-07 23:13:11.59643-04	1	Настройка сайтов	2	[{"changed": {"fields": ["phone"]}}]	25	3
1346	2018-10-07 23:15:45.895751-04	1	Настройка сайтов	2	[{"changed": {"fields": ["phone"]}}]	25	3
1347	2018-10-07 23:17:03.198051-04	1	Настройка сайтов	2	[{"changed": {"fields": ["phone"]}}]	25	3
1348	2018-10-08 01:48:06.458091-04	25	Установка сети видеонаблюдения	2	[{"changed": {"fields": ["description", "description_ru"]}}]	45	3
1349	2018-10-10 01:52:30.327835-04	332	done_works/Proekt_Balykchy_video_06.jpg	1	[{"added": {}}]	46	3
1350	2018-10-10 01:52:32.807881-04	25	Установка сети видеонаблюдения	2	[{"changed": {"fields": ["main_image", "images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[24]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1351	2018-10-11 02:14:34.968666-04	18	/application/assets/uploads/slider/Slider_04_-_decorativnoe_osveschenie.jpg	2	[{"changed": {"fields": ["short_description"]}}]	18	3
1352	2018-10-11 07:21:34.803911-04	1	Страница О Нас	2	[{"changed": {"fields": ["body"]}}]	28	1
1353	2018-10-11 08:12:41.44253-04	7	Системы видеонаблюдения	2	[{"changed": {"fields": ["features", "features_ru"]}}]	39	3
1354	2018-10-22 08:16:50.649726-04	3	Праздничное освещение	2	[{"changed": {"fields": ["notes", "notes_ru"]}}]	39	3
1355	2018-10-22 09:03:49.000232-04	3	Праздничное освещение	2	[]	39	3
1356	2018-11-05 03:40:41.165493-05	14	Освещение сквера им. У. Салиевой	2	[{"changed": {"fields": ["main_image"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1357	2018-11-05 03:41:14.989625-05	333	done_works/Proekt_U.Salieva_-_11.jpg	1	[{"added": {}}]	46	3
1358	2018-11-05 03:41:18.244324-05	14	Освещение сквера им. У. Салиевой	2	[{"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1359	2018-11-05 04:16:03.431001-05	334	done_works/Proekt_U.Salieva_-_12.jpg	1	[{"added": {}}]	46	3
1360	2018-11-05 04:16:10.472304-05	335	done_works/Proekt_U.Salieva_-_13.jpg	1	[{"added": {}}]	46	3
1361	2018-11-05 04:16:16.311255-05	336	done_works/Proekt_U.Salieva_-_14.jpg	1	[{"added": {}}]	46	3
1362	2018-11-05 04:16:22.125117-05	337	done_works/Proekt_U.Salieva_-_15.jpg	1	[{"added": {}}]	46	3
1363	2018-11-05 04:16:24.006993-05	14	Освещение сквера им. У. Салиевой	2	[{"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
1364	2018-11-05 04:16:52.984945-05	283	done_works/Proekt_U.Salieva_-_05_b3Y7V7e.jpg	3		46	3
1365	2018-11-05 04:16:52.987186-05	282	done_works/Proekt_U.Salieva_-_04_6OfqETg.jpg	3		46	3
1366	2018-11-05 04:16:52.988141-05	281	done_works/Proekt_U.Salieva_-_03_siSjfRR.jpg	3		46	3
1367	2018-11-05 04:16:52.989128-05	280	done_works/Proekt_U.Salieva_-_02_3gffI7t.jpg	3		46	3
1368	2018-11-05 04:16:52.990114-05	279	done_works/Proekt_U.Salieva_-_01_NZtmGvl.jpg	3		46	3
1369	2018-11-05 04:22:43.855741-05	338	done_works/Proekt_U.Salieva_-_16.jpg	1	[{"added": {}}]	46	3
1370	2018-11-05 04:22:49.782571-05	339	done_works/Proekt_U.Salieva_-_17.jpg	1	[{"added": {}}]	46	3
1371	2018-11-05 04:22:51.183634-05	14	Освещение сквера им. У. Салиевой	2	[{"changed": {"fields": ["images"], "object": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f #[14]", "name": "\\u0413\\u0430\\u043b\\u043b\\u0435\\u0440\\u0435\\u044f \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442"}}]	45	3
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1371, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	jet	bookmark
2	jet	pinnedapplication
3	admin	logentry
4	auth	group
5	auth	permission
6	contenttypes	contenttype
7	sessions	session
8	users	userimage
9	users	user
10	client_app	adminemails
11	client_app	datenschutz
12	client_app	seotext
13	client_app	subscribers
14	client_app	delivery
15	client_app	reviews
16	client_app	contact
17	client_app	affiliatefeedback
18	client_app	slider
19	client_app	manufacturers
20	client_app	repairtips
21	client_app	returnpolicy
22	client_app	termsofsaleofgoods
23	client_app	custompages
24	client_app	feedback
25	client_app	sitesettings
26	client_app	aboutinnumbers
27	client_app	ourteam
28	client_app	about
29	product	productcolor
30	product	productsize
31	product	productproperty
32	product	allproductproperties
33	product	productsizesettings
34	product	downloaddata
35	product	productimage
36	product	product
37	order	orderrow
38	order	order
39	categories	category
40	categories	brand
41	categories	subcategory
42	promotions	promotion
43	news	post
44	news	postgalleryimage
45	works	donework
46	works	workimage
47	works	workgallery
48	client_app	sitestrings
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 48, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-08-20 04:48:02.18028-04
2	contenttypes	0002_remove_content_type_name	2018-08-20 04:48:02.197755-04
3	auth	0001_initial	2018-08-20 04:48:02.336414-04
4	auth	0002_alter_permission_name_max_length	2018-08-20 04:48:02.346723-04
5	auth	0003_alter_user_email_max_length	2018-08-20 04:48:02.355686-04
6	auth	0004_alter_user_username_opts	2018-08-20 04:48:02.364619-04
7	auth	0005_alter_user_last_login_null	2018-08-20 04:48:02.374056-04
8	auth	0006_require_contenttypes_0002	2018-08-20 04:48:02.375701-04
9	auth	0007_alter_validators_add_error_messages	2018-08-20 04:48:02.384868-04
10	auth	0008_alter_user_username_max_length	2018-08-20 04:48:02.394371-04
11	users	0001_initial	2018-08-20 04:48:02.553603-04
12	admin	0001_initial	2018-08-20 04:48:02.612768-04
13	admin	0002_logentry_remove_auto_add	2018-08-20 04:48:02.629284-04
14	categories	0001_initial	2018-08-20 04:48:02.686496-04
15	categories	0002_subcategory	2018-08-20 04:48:02.704129-04
16	categories	0003_category_parent_category	2018-08-20 04:48:02.726348-04
17	categories	0004_auto_20171222_2115	2018-08-20 04:48:02.770448-04
18	categories	0005_auto_20180302_1609	2018-08-20 04:48:02.785274-04
19	categories	0006_auto_20180818_1227	2018-08-20 04:48:02.794392-04
20	categories	0007_auto_20180818_1336	2018-08-20 04:48:02.818856-04
21	categories	0008_auto_20180818_1442	2018-08-20 04:48:02.860125-04
22	categories	0009_auto_20180818_1446	2018-08-20 04:48:02.882649-04
23	product	0001_initial	2018-08-20 04:48:03.1757-04
24	product	0002_auto_20170901_2112	2018-08-20 04:48:03.262138-04
25	product	0003_auto_20170901_2114	2018-08-20 04:48:03.365466-04
26	product	0004_auto_20170902_0934	2018-08-20 04:48:03.407381-04
27	product	0005_productsearchresults	2018-08-20 04:48:03.468659-04
28	product	0006_auto_20170903_0857	2018-08-20 04:48:03.514898-04
29	product	0007_auto_20170903_1038	2018-08-20 04:48:03.606887-04
30	product	0008_auto_20170903_1043	2018-08-20 04:48:03.677234-04
31	product	0009_auto_20170903_1044	2018-08-20 04:48:03.71008-04
32	product	0010_auto_20170903_1708	2018-08-20 04:48:03.865947-04
33	product	0011_auto_20170903_1717	2018-08-20 04:48:03.965745-04
34	product	0012_auto_20170904_0745	2018-08-20 04:48:04.043886-04
35	product	0013_auto_20170906_1405	2018-08-20 04:48:04.232852-04
36	product	0014_auto_20171222_1204	2018-08-20 04:48:04.375113-04
37	product	0015_auto_20171222_1240	2018-08-20 04:48:04.391726-04
38	product	0016_product_documentation	2018-08-20 04:48:04.410819-04
39	product	0017_auto_20171224_1536	2018-08-20 04:48:04.483915-04
40	product	0018_productproperty_value_type	2018-08-20 04:48:04.527597-04
41	product	0019_auto_20171224_2039	2018-08-20 04:48:04.545569-04
42	product	0020_product_article	2018-08-20 04:48:04.563052-04
43	client_app	0001_initial	2018-08-20 04:48:04.84042-04
44	client_app	0002_seotext	2018-08-20 04:48:04.859167-04
45	client_app	0003_contact	2018-08-20 04:48:04.883144-04
46	client_app	0004_contact_phone	2018-08-20 04:48:04.909293-04
47	client_app	0005_subscribers	2018-08-20 04:48:04.932159-04
48	client_app	0006_auto_20171222_1555	2018-08-20 04:48:04.973773-04
49	client_app	0007_auto_20171222_1645	2018-08-20 04:48:05.003712-04
50	client_app	0008_auto_20171222_1909	2018-08-20 04:48:05.039657-04
51	client_app	0009_auto_20171222_1926	2018-08-20 04:48:05.183541-04
52	client_app	0010_reviews	2018-08-20 04:48:05.21353-04
53	client_app	0011_auto_20171222_2045	2018-08-20 04:48:05.235379-04
54	client_app	0012_auto_20171224_0016	2018-08-20 04:48:05.252639-04
55	client_app	0013_contact_location	2018-08-20 04:48:05.286692-04
56	client_app	0014_auto_20171224_0040	2018-08-20 04:48:05.297951-04
57	client_app	0015_affiliatefeedback	2018-08-20 04:48:05.336589-04
58	client_app	0016_auto_20171225_0841	2018-08-20 04:48:05.447911-04
59	client_app	0017_auto_20171225_1859	2018-08-20 04:48:05.481773-04
60	client_app	0018_slider_is_sale	2018-08-20 04:48:05.508731-04
61	client_app	0019_sitesettings_weight	2018-08-20 04:48:05.541315-04
62	client_app	0020_auto_20171225_2120	2018-08-20 04:48:05.561536-04
63	client_app	0021_auto_20180112_2140	2018-08-20 04:48:05.636764-04
64	client_app	0022_sitesettings_delivery	2018-08-20 04:48:05.656167-04
65	client_app	0023_manufacturers_repairtips_returnpolicy_termsofsaleofgoods	2018-08-20 04:48:05.744532-04
66	client_app	0024_custompages	2018-08-20 04:48:05.784213-04
67	client_app	0025_auto_20180317_1745	2018-08-20 04:48:05.921146-04
68	client_app	0026_auto_20180317_1749	2018-08-20 04:48:05.937974-04
69	client_app	0027_auto_20180420_1418	2018-08-20 04:48:05.968165-04
70	client_app	0028_feedback_message	2018-08-20 04:48:05.98266-04
71	client_app	0029_auto_20180818_1227	2018-08-20 04:48:06.008767-04
72	client_app	0030_auto_20180818_1238	2018-08-20 04:48:06.026174-04
73	jet	0001_initial	2018-08-20 04:48:06.086914-04
74	jet	0002_delete_userdashboardmodule	2018-08-20 04:48:06.095186-04
75	news	0001_initial	2018-08-20 04:48:06.12661-04
76	news	0002_auto_20171224_2135	2018-08-20 04:48:06.174526-04
77	news	0003_auto_20180302_1609	2018-08-20 04:48:06.196889-04
78	news	0004_auto_20180818_1251	2018-08-20 04:48:06.246505-04
79	news	0005_auto_20180818_1442	2018-08-20 04:48:06.280242-04
80	news	0006_auto_20180818_1446	2018-08-20 04:48:06.309119-04
81	order	0001_initial	2018-08-20 04:48:06.492713-04
82	order	0002_remove_order_user	2018-08-20 04:48:06.542752-04
83	order	0003_auto_20171225_2120	2018-08-20 04:48:06.725539-04
84	order	0004_order_address	2018-08-20 04:48:06.750406-04
85	order	0005_order_comment	2018-08-20 04:48:06.778423-04
86	order	0006_remove_order_total_weight	2018-08-20 04:48:06.802915-04
87	order	0007_order_user	2018-08-20 04:48:06.88282-04
88	order	0008_order_is_delivered	2018-08-20 04:48:06.964388-04
89	order	0009_auto_20180324_1307	2018-08-20 04:48:07.06185-04
90	product	0021_productproperty_prop_tip	2018-08-20 04:48:07.115684-04
91	product	0022_auto_20180112_2128	2018-08-20 04:48:07.275981-04
92	product	0023_product_consumption	2018-08-20 04:48:07.3559-04
93	product	0024_auto_20180131_1952	2018-08-20 04:48:07.398673-04
94	product	0025_auto_20180131_1953	2018-08-20 04:48:07.431201-04
95	product	0026_remove_product_consumption	2018-08-20 04:48:07.472139-04
96	product	0027_auto_20180302_1607	2018-08-20 04:48:07.523169-04
97	product	0028_product_best_seller	2018-08-20 04:48:07.619379-04
98	product	0029_auto_20180317_1718	2018-08-20 04:48:07.76902-04
99	product	0030_datadownloaddata	2018-08-20 04:48:07.829915-04
100	product	0031_auto_20180317_1841	2018-08-20 04:48:07.877976-04
101	product	0032_remove_product_documentation	2018-08-20 04:48:07.915096-04
102	product	0033_auto_20180324_1307	2018-08-20 04:48:07.961692-04
103	product	0034_auto_20180324_1416	2018-08-20 04:48:08.005218-04
104	product	0035_auto_20180420_1418	2018-08-20 04:48:08.037629-04
105	product	0036_auto_20180818_1442	2018-08-20 04:48:08.378429-04
106	product	0037_auto_20180818_1446	2018-08-20 04:48:08.593029-04
107	promotions	0001_initial	2018-08-20 04:48:08.664307-04
108	promotions	0002_auto_20171224_2045	2018-08-20 04:48:08.759969-04
109	promotions	0003_promotion_is_active	2018-08-20 04:48:08.827235-04
110	promotions	0004_auto_20180302_1609	2018-08-20 04:48:08.886809-04
111	sessions	0001_initial	2018-08-20 04:48:08.931133-04
112	users	0002_auto_20170814_1146	2018-08-20 04:48:09.093626-04
113	users	0003_auto_20170823_1213	2018-08-20 04:48:09.281735-04
114	users	0004_auto_20170823_1324	2018-08-20 04:48:09.415814-04
115	users	0005_auto_20180322_1330	2018-08-20 04:48:09.494539-04
116	users	0006_user_changing_email	2018-08-20 04:48:09.533853-04
117	users	0007_user_changing_email_code	2018-08-20 04:48:09.645123-04
118	works	0001_initial	2018-08-20 04:48:09.880503-04
119	works	0002_donework_is_active	2018-08-20 04:48:09.974204-04
120	works	0003_donework_slug	2018-08-20 04:48:10.056517-04
121	works	0004_auto_20180817_1233	2018-08-20 04:48:10.182466-04
122	works	0005_donework_address	2018-08-20 04:48:10.22624-04
123	works	0006_donework_description	2018-08-20 04:48:10.278438-04
124	works	0007_auto_20180818_1442	2018-08-20 04:48:10.402199-04
125	works	0008_auto_20180818_1446	2018-08-20 04:48:10.540173-04
126	client_app	0031_auto_20180820_1532	2018-08-20 05:41:03.00148-04
127	users	0008_auto_20180820_1532	2018-08-20 05:41:05.236558-04
128	users	0009_auto_20180821_1338	2018-08-21 03:41:27.174175-04
129	client_app	0032_slider_font_color	2018-08-27 07:33:46.291094-04
130	categories	0010_auto_20180903_1045	2018-09-03 01:13:14.752467-04
131	categories	0011_auto_20180903_1110	2018-09-03 01:13:14.780447-04
132	product	0038_auto_20180903_1045	2018-09-03 01:13:16.979331-04
133	works	0009_auto_20180903_1045	2018-09-03 01:13:19.359428-04
134	client_app	0033_sitestrings	2018-09-15 00:35:28.474589-04
135	categories	0012_auto_20180923_2005	2018-09-23 10:14:01.608242-04
136	client_app	0034_auto_20180923_1912	2018-09-23 10:14:02.531584-04
137	client_app	0035_auto_20180923_1918	2018-09-23 10:14:02.547159-04
138	news	0007_auto_20180923_1912	2018-09-23 10:14:02.595724-04
139	users	0010_auto_20180923_1912	2018-09-23 10:14:04.500694-04
140	works	0010_auto_20180923_1953	2018-09-23 10:14:05.220447-04
141	works	0011_donework_customer_link	2018-09-23 10:14:05.270912-04
142	works	0012_auto_20180925_1204	2018-09-25 02:06:13.495516-04
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 142, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
ybzeizm13fqdky8tsmnej133cigf1mb7	ZjQ3MWQ5YjMxMDNmZGE0NDdhMDkyYjEwOGY1NTM3YjBhZTUzNzc2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6IjJlNWJlNDZjNTY3M2M1MDdjODFiYzgwZTE1NmE0NTYxMmNhN2NkZmEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-09-03 04:57:03.248431-04
6z6xca6607whrohtgnwqubpaxuyxcnj2	OTgzZjk1NzZiNDI5OTk0NDEwZjBmNTE5ZjI4OThkYzBkODk1N2IxNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhNDQ3Mjg0MWM5M2M0OTcxNjc0ZmRmYzg3YjZkMGZmYWQ4MDY3MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2018-09-04 03:43:03.302146-04
ku9knu4h6z1g0r45zdiruuv27l2ykwxp	OTgzZjk1NzZiNDI5OTk0NDEwZjBmNTE5ZjI4OThkYzBkODk1N2IxNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhNDQ3Mjg0MWM5M2M0OTcxNjc0ZmRmYzg3YjZkMGZmYWQ4MDY3MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2018-09-05 02:18:07.750939-04
kwowbop6wa8ywkfcis5eeybltp1rrh0r	OTgzZjk1NzZiNDI5OTk0NDEwZjBmNTE5ZjI4OThkYzBkODk1N2IxNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhNDQ3Mjg0MWM5M2M0OTcxNjc0ZmRmYzg3YjZkMGZmYWQ4MDY3MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2018-09-19 03:23:45.434584-04
pwe3bbtpanw57henlvd6gfyub1l71oon	ZjQ3MWQ5YjMxMDNmZGE0NDdhMDkyYjEwOGY1NTM3YjBhZTUzNzc2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6IjJlNWJlNDZjNTY3M2M1MDdjODFiYzgwZTE1NmE0NTYxMmNhN2NkZmEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-09-20 01:29:04.887094-04
9qlxdln17hlmliq1g8hzqhcr88f6px0i	OTgzZjk1NzZiNDI5OTk0NDEwZjBmNTE5ZjI4OThkYzBkODk1N2IxNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhNDQ3Mjg0MWM5M2M0OTcxNjc0ZmRmYzg3YjZkMGZmYWQ4MDY3MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2018-10-03 04:32:01.44381-04
l1hlto5h91ti9u89zi92czegl11f2agi	ZjQ3MWQ5YjMxMDNmZGE0NDdhMDkyYjEwOGY1NTM3YjBhZTUzNzc2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6IjJlNWJlNDZjNTY3M2M1MDdjODFiYzgwZTE1NmE0NTYxMmNhN2NkZmEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-10-06 07:29:46.872722-04
o4qduvptjgaysgiwil7r548vwxb4re2o	ZjQ3MWQ5YjMxMDNmZGE0NDdhMDkyYjEwOGY1NTM3YjBhZTUzNzc2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6IjJlNWJlNDZjNTY3M2M1MDdjODFiYzgwZTE1NmE0NTYxMmNhN2NkZmEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-10-07 07:30:56.395308-04
ey2o4ii9887o2gkhbtbnqk4mdtl7xgdv	ZjQ3MWQ5YjMxMDNmZGE0NDdhMDkyYjEwOGY1NTM3YjBhZTUzNzc2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6IjJlNWJlNDZjNTY3M2M1MDdjODFiYzgwZTE1NmE0NTYxMmNhN2NkZmEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-10-07 10:20:57.397939-04
lxjdseiurk53pimtd3v26h9ilth53zdp	OTgzZjk1NzZiNDI5OTk0NDEwZjBmNTE5ZjI4OThkYzBkODk1N2IxNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhNDQ3Mjg0MWM5M2M0OTcxNjc0ZmRmYzg3YjZkMGZmYWQ4MDY3MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2018-10-17 07:26:22.431799-04
126665fkwvuh2xkppgkld4a7rtufttre	ZjQ3MWQ5YjMxMDNmZGE0NDdhMDkyYjEwOGY1NTM3YjBhZTUzNzc2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6IjJlNWJlNDZjNTY3M2M1MDdjODFiYzgwZTE1NmE0NTYxMmNhN2NkZmEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-10-25 07:21:03.745696-04
umdouwtgq5pab0ljck3m73w8pt20qynp	OTgzZjk1NzZiNDI5OTk0NDEwZjBmNTE5ZjI4OThkYzBkODk1N2IxNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhNDQ3Mjg0MWM5M2M0OTcxNjc0ZmRmYzg3YjZkMGZmYWQ4MDY3MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2018-11-01 01:18:46.161099-04
l1qcxvd1nsi1lz71wwa76nw2uvuubrb0	OTgzZjk1NzZiNDI5OTk0NDEwZjBmNTE5ZjI4OThkYzBkODk1N2IxNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhNDQ3Mjg0MWM5M2M0OTcxNjc0ZmRmYzg3YjZkMGZmYWQ4MDY3MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2018-11-01 23:06:39.850701-04
tkp30rje2svcp4yvrpltfx9u0e829mws	OTgzZjk1NzZiNDI5OTk0NDEwZjBmNTE5ZjI4OThkYzBkODk1N2IxNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhNDQ3Mjg0MWM5M2M0OTcxNjc0ZmRmYzg3YjZkMGZmYWQ4MDY3MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2018-11-13 03:33:26.594016-05
a5hxkffh10vh9pw7nnxzlv9ijlit7z9k	OTgzZjk1NzZiNDI5OTk0NDEwZjBmNTE5ZjI4OThkYzBkODk1N2IxNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhNDQ3Mjg0MWM5M2M0OTcxNjc0ZmRmYzg3YjZkMGZmYWQ4MDY3MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2018-11-15 23:01:06.55124-05
z26x035kxr8w9d60x8dnaaj85h609to7	OTgzZjk1NzZiNDI5OTk0NDEwZjBmNTE5ZjI4OThkYzBkODk1N2IxNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhNDQ3Mjg0MWM5M2M0OTcxNjc0ZmRmYzg3YjZkMGZmYWQ4MDY3MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2018-11-19 03:26:15.751333-05
\.


--
-- Data for Name: jet_bookmark; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jet_bookmark (id, url, title, "user", date_add) FROM stdin;
\.


--
-- Name: jet_bookmark_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jet_bookmark_id_seq', 1, false);


--
-- Data for Name: jet_pinnedapplication; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jet_pinnedapplication (id, app_label, "user", date_add) FROM stdin;
\.


--
-- Name: jet_pinnedapplication_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jet_pinnedapplication_id_seq', 1, false);


--
-- Data for Name: news_post; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.news_post (id, is_active, title, slug, preview, content, created_at, updated_at, short_description, content_kg, short_description_kg, title_kg, content_ru, short_description_ru, title_ru) FROM stdin;
2	t	Международная выставка «Энергетика и освещение»	yubilejnaya-mezhdunarodnaya-specializirovannaya-vystavka-energetika-i-osveshenie	news/Novost_01-2.jpg	<p>&nbsp; &nbsp; &nbsp;EnergyExpo является единственным, специализированным событием в энергетической индустрии Кыргызской Респубкики.</p>\r\n\r\n<h3>Разделы выставки:</h3>\r\n\r\n<p><strong>Электротехническое оборудование:</strong></p>\r\n\r\n<ul>\r\n\t<li>трансформаторные подстанции</li>\r\n\t<li>низковольтное оборудование</li>\r\n\t<li>высоковольтное оборудование</li>\r\n\t<li>электродвигатели, электрогенераторы, электроприводы</li>\r\n\t<li>кабельная продукция, провода, арматура</li>\r\n</ul>\r\n\r\n<p><strong>Энергосбережение, инновации и энергосберегающие технологии и оборудование:</strong></p>\r\n\r\n<ul>\r\n\t<li>оборудование для энергосбережения</li>\r\n\t<li>энергосберегающие технологии</li>\r\n\t<li>светотехнические изделия: энергосберегающие лампы, световые приборы, пускорегулирующая и управляющая аппаратура</li>\r\n</ul>\r\n\r\n<p><strong>Альтернативные источники электроэнергии:</strong></p>\r\n\r\n<ul>\r\n\t<li>биоэнергетика</li>\r\n\t<li>ветровая энергетика</li>\r\n\t<li>водородная энергетика</li>\r\n\t<li>геотермальная энергетика</li>\r\n\t<li>малая гидроэнергетика</li>\r\n\t<li>солнечная энергетика</li>\r\n</ul>\r\n\r\n<p><strong>Энергетическое машиностроение:</strong></p>\r\n\r\n<ul>\r\n\t<li>турбины и турбовспомогательное оборудование</li>\r\n\t<li>котлы и котло-вспомогательное оборудование</li>\r\n\t<li>дизели и дизель-генераторы</li>\r\n\t<li>оборудования для ЛЭП</li>\r\n</ul>	2018-09-19 04:54:38.243616-04	2018-09-27 05:01:48.84726-04	Выставка «Энергетика и освещение» пройдет с 24 по 26 апреля 2019 года в городе Бишкек		\N		<p>&nbsp; &nbsp; &nbsp;EnergyExpo является единственным, специализированным событием в энергетической индустрии Кыргызской Респубкики.</p>\r\n\r\n<h3>Разделы выставки:</h3>\r\n\r\n<p><strong>Электротехническое оборудование:</strong></p>\r\n\r\n<ul>\r\n\t<li>трансформаторные подстанции</li>\r\n\t<li>низковольтное оборудование</li>\r\n\t<li>высоковольтное оборудование</li>\r\n\t<li>электродвигатели, электрогенераторы, электроприводы</li>\r\n\t<li>кабельная продукция, провода, арматура</li>\r\n</ul>\r\n\r\n<p><strong>Энергосбережение, инновации и энергосберегающие технологии и оборудование:</strong></p>\r\n\r\n<ul>\r\n\t<li>оборудование для энергосбережения</li>\r\n\t<li>энергосберегающие технологии</li>\r\n\t<li>светотехнические изделия: энергосберегающие лампы, световые приборы, пускорегулирующая и управляющая аппаратура</li>\r\n</ul>\r\n\r\n<p><strong>Альтернативные источники электроэнергии:</strong></p>\r\n\r\n<ul>\r\n\t<li>биоэнергетика</li>\r\n\t<li>ветровая энергетика</li>\r\n\t<li>водородная энергетика</li>\r\n\t<li>геотермальная энергетика</li>\r\n\t<li>малая гидроэнергетика</li>\r\n\t<li>солнечная энергетика</li>\r\n</ul>\r\n\r\n<p><strong>Энергетическое машиностроение:</strong></p>\r\n\r\n<ul>\r\n\t<li>турбины и турбовспомогательное оборудование</li>\r\n\t<li>котлы и котло-вспомогательное оборудование</li>\r\n\t<li>дизели и дизель-генераторы</li>\r\n\t<li>оборудования для ЛЭП</li>\r\n</ul>	Выставка «Энергетика и освещение» пройдет с 24 по 26 апреля 2019 года в городе Бишкек	Международная выставка «Энергетика и освещение»
3	t	Как выбрать температуру освещения?	kak-vybrat-temperaturu-osvesheniya	news/01_Temperatura_osvesheniya-1.jpg	<p>&nbsp; &nbsp; &nbsp;Человеческий глаз устроен таким образом, что способен улавливать малейшие отклонения цветовой температуры. Изменения данного показателя влияют на наше эмоциональное и психологическое состояние, работоспособность. Именно поэтому при создании гармоничного и комфортного освещения нельзя этим пренебрегать.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Важно подобрать подходящую цветовую температуру свечения светильников для помещений различного назначения.&nbsp;</p>\r\n\r\n<ul>\r\n\t<li>Спальная комната &mdash; это место, где должна царить спокойная непринужденная атмосфера. Поэтому освещение в этой комнате должно быть мягким, равномерным и приглушенным. Для этого лучше всего подойдут лампы теплого света.</li>\r\n\t<li>Детская - вы наверняка знаете, что для ребенка требуется много света. Температура освещения зависит от возраста вашего ребенка. Например, для детей дошкольного возраста рекомендуется перед сном включать лампу теплого света, что позволит перестроить организм ребенка на сон. В том случае, если ваш ребенок &mdash; школьник, ему необходима настольная лампа холодного оттенка, которая быстрее поможет настроиться на рабочий лад.</li>\r\n\t<li>Прихожая создает первое впечатление о квартире, поэтому очень важно подобрать правильное освещение. Лучше всего выбрать нейтральный белый оттенок света.</li>\r\n\t<li>Кухня - приготовление еды требует яркого акцентного освещения. Для этого лучше всего подойдет холодный оттенок. А вот обеденный стол не требует яркого света &mdash; тут все решает ваш вкус и предпочтения. Если вы любите засиживаться вечерами с друзьями или семьей, то теплый приглушенный свет создаст атмосферу уюта за вашим столом.</li>\r\n</ul>	2018-09-26 23:26:16.541855-04	2018-09-26 23:41:52.101915-04	Холодный? Тёплый? Натуральный?		\N	Как выбрать температуру освещения?	<p>&nbsp; &nbsp; &nbsp;Человеческий глаз устроен таким образом, что способен улавливать малейшие отклонения цветовой температуры. Изменения данного показателя влияют на наше эмоциональное и психологическое состояние, работоспособность. Именно поэтому при создании гармоничного и комфортного освещения нельзя этим пренебрегать.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Важно подобрать подходящую цветовую температуру свечения светильников для помещений различного назначения.&nbsp;</p>\r\n\r\n<ul>\r\n\t<li>Спальная комната &mdash; это место, где должна царить спокойная непринужденная атмосфера. Поэтому освещение в этой комнате должно быть мягким, равномерным и приглушенным. Для этого лучше всего подойдут лампы теплого света.</li>\r\n\t<li>Детская - вы наверняка знаете, что для ребенка требуется много света. Температура освещения зависит от возраста вашего ребенка. Например, для детей дошкольного возраста рекомендуется перед сном включать лампу теплого света, что позволит перестроить организм ребенка на сон. В том случае, если ваш ребенок &mdash; школьник, ему необходима настольная лампа холодного оттенка, которая быстрее поможет настроиться на рабочий лад.</li>\r\n\t<li>Прихожая создает первое впечатление о квартире, поэтому очень важно подобрать правильное освещение. Лучше всего выбрать нейтральный белый оттенок света.</li>\r\n\t<li>Кухня - приготовление еды требует яркого акцентного освещения. Для этого лучше всего подойдет холодный оттенок. А вот обеденный стол не требует яркого света &mdash; тут все решает ваш вкус и предпочтения. Если вы любите засиживаться вечерами с друзьями или семьей, то теплый приглушенный свет создаст атмосферу уюта за вашим столом.</li>\r\n</ul>	Холодный? Тёплый? Натуральный?	Как выбрать температуру освещения?
4	t	Почему светодиоды?	pochemu-svetodiody	news/Preimuschestva_LED-1.jpg	<p>&nbsp; &nbsp; &nbsp;В настоящее время светодиоды обрели широкую популярность. Применение их очень масштабно и выгодно. Светодиоды уже давно нашли свое применение в различных сферах нашей жизни: от подсветки жидкокристаллических экранов до освещения крупных промышленных объектов.<br />\r\n&nbsp; &nbsp; &nbsp;Области применения различных светодиодов:</p>\r\n\r\n<ul>\r\n\t<li>архитектурная и ландшафтная подсветка</li>\r\n\t<li>дизайн помещений *дизайн мебели</li>\r\n\t<li>освещение производственных зданий и сооружений</li>\r\n\t<li>освещение&nbsp;помещений различной площади и назначения</li>\r\n\t<li>подсветка витрин и вывесок торговых центров и магазинов</li>\r\n\t<li>освещение фасадов зданий&nbsp;&nbsp; &nbsp;</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Преимуществами светодиодов являются:</p>\r\n\r\n<ul>\r\n\t<li>большая экономия потребляемой электроэнергии и высокий КПД</li>\r\n\t<li>отсутствие вредных излучений и вредных составляющих компонентов в светодиодах</li>\r\n\t<li>экологическая безопасность</li>\r\n\t<li>время непрерывной работы светодиода - до 50 тысяч часов, что в десятки раз превышает срок службы традиционных источников света</li>\r\n\t<li>высокая механическая прочность и виброустойчивость</li>\r\n\t<li>диапазон рабочих температур от -60 до +40 &deg;С</li>\r\n\t<li>мгновенное включение/выключение при любой температуре</li>\r\n\t<li>полное отсутствие мерцания</li>\r\n\t<li>минимальные затраты на эксплуатацию и обслуживание</li>\r\n</ul>	2018-09-27 00:44:35.976256-04	2018-09-27 00:56:43.900706-04	Преимущества LED		\N		<p>&nbsp; &nbsp; &nbsp;В настоящее время светодиоды обрели широкую популярность. Применение их очень масштабно и выгодно. Светодиоды уже давно нашли свое применение в различных сферах нашей жизни: от подсветки жидкокристаллических экранов до освещения крупных промышленных объектов.<br />\r\n&nbsp; &nbsp; &nbsp;Области применения различных светодиодов:</p>\r\n\r\n<ul>\r\n\t<li>архитектурная и ландшафтная подсветка</li>\r\n\t<li>дизайн помещений *дизайн мебели</li>\r\n\t<li>освещение производственных зданий и сооружений</li>\r\n\t<li>освещение&nbsp;помещений различной площади и назначения</li>\r\n\t<li>подсветка витрин и вывесок торговых центров и магазинов</li>\r\n\t<li>освещение фасадов зданий&nbsp;&nbsp; &nbsp;</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Преимуществами светодиодов являются:</p>\r\n\r\n<ul>\r\n\t<li>большая экономия потребляемой электроэнергии и высокий КПД</li>\r\n\t<li>отсутствие вредных излучений и вредных составляющих компонентов в светодиодах</li>\r\n\t<li>экологическая безопасность</li>\r\n\t<li>время непрерывной работы светодиода - до 50 тысяч часов, что в десятки раз превышает срок службы традиционных источников света</li>\r\n\t<li>высокая механическая прочность и виброустойчивость</li>\r\n\t<li>диапазон рабочих температур от -60 до +40 &deg;С</li>\r\n\t<li>мгновенное включение/выключение при любой температуре</li>\r\n\t<li>полное отсутствие мерцания</li>\r\n\t<li>минимальные затраты на эксплуатацию и обслуживание</li>\r\n</ul>	Преимущества LED	Почему светодиоды?
5	t	Чего вы не знали о светодиодах	chego-vy-ne-znali-o-svetodiodah	news/O_svetodiodah.jpg	<p>&nbsp; &nbsp; &nbsp;Светодиод&nbsp;&ndash; это полупроводниковый прибор, преобразующий энергию электрического тока в световую, основой которого является излучающий кристалл. По мере повышения эффективности светодиодов растет число их возможных применений.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Светодиоды создают из слоев полупроводниковых материалов.&nbsp;Конструкция светодиода состоит из полупроводникового кристалла на подложке, корпуса с контактными выводами и оптической системы. Корпуса для мощных светодиодов также содержат теплоотвод для рассеяния избытков тепла.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Первые светодиоды появились в 70 годах прошлого столетия, но широкое распространение получили спустя несколько десятилетий.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Современные светодиоды отличаются миниатюрностью, прочностью, длительным сроком службы, хорошими оптическими характеристиками и высоким квантовым выходом излучения. В отличие от многих других источников света, светодиоды могут преобразовывать электрическую энергию в световую с к.п.д. близким к единице.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Что же касается воздействия на человека, то монохроматизм светодиодов означает также полное отсутствие у них губительного ультрафиолетового излучения.</p>\r\n\r\n<p>&nbsp;</p>	2018-09-27 02:07:34.370703-04	2018-09-27 02:07:34.370726-04	Коротко о светодиодах		\N		<p>&nbsp; &nbsp; &nbsp;Светодиод&nbsp;&ndash; это полупроводниковый прибор, преобразующий энергию электрического тока в световую, основой которого является излучающий кристалл. По мере повышения эффективности светодиодов растет число их возможных применений.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Светодиоды создают из слоев полупроводниковых материалов.&nbsp;Конструкция светодиода состоит из полупроводникового кристалла на подложке, корпуса с контактными выводами и оптической системы. Корпуса для мощных светодиодов также содержат теплоотвод для рассеяния избытков тепла.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Первые светодиоды появились в 70 годах прошлого столетия, но широкое распространение получили спустя несколько десятилетий.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Современные светодиоды отличаются миниатюрностью, прочностью, длительным сроком службы, хорошими оптическими характеристиками и высоким квантовым выходом излучения. В отличие от многих других источников света, светодиоды могут преобразовывать электрическую энергию в световую с к.п.д. близким к единице.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Что же касается воздействия на человека, то монохроматизм светодиодов означает также полное отсутствие у них губительного ультрафиолетового излучения.</p>\r\n\r\n<p>&nbsp;</p>	Коротко о светодиодах	Чего вы не знали о светодиодах
\.


--
-- Name: news_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_post_id_seq', 5, true);


--
-- Data for Name: news_postgalleryimage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.news_postgalleryimage (id, image, post_id) FROM stdin;
5	news/gallery/Novost_01-1.jpg	2
\.


--
-- Name: news_postgalleryimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_postgalleryimage_id_seq', 5, true);


--
-- Data for Name: order_order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_order (id, created_at, is_ordered, email, name, phone, address, comment, user_id, is_delivered) FROM stdin;
\.


--
-- Name: order_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_order_id_seq', 1, false);


--
-- Data for Name: order_order_order_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_order_order_items (id, order_id, orderrow_id) FROM stdin;
\.


--
-- Name: order_order_order_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_order_order_items_id_seq', 1, false);


--
-- Data for Name: order_orderrow; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_orderrow (id, count, product_id) FROM stdin;
\.


--
-- Name: order_orderrow_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_orderrow_id_seq', 1, false);


--
-- Data for Name: product_allproductproperties; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_allproductproperties (id, title, title_kg, title_ru) FROM stdin;
2	Температура свечения		Температура свечения
3	Входное напряжение		Входное напряжение
4	Степень пылевлагозащиты		Степень пылевлагозащиты
5	Световой поток		Световой поток
7	Угол свечения		Угол свечения
8	Размер		Размер
9	Мощность		Мощность
10	Гарантия		Гарантия
\.


--
-- Name: product_allproductproperties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_allproductproperties_id_seq', 10, true);


--
-- Name: product_datadownloaddata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_datadownloaddata_id_seq', 1, false);


--
-- Data for Name: product_downloaddata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_downloaddata (id, mode, created_at, user_id) FROM stdin;
\.


--
-- Data for Name: product_product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_product (id, title, slug, description, is_active, created_at, brand_id, category_id, main_image, article, description_kg, title_kg, description_ru, title_ru) FROM stdin;
3	Светодиодные магистральные светильники	svetodiodnye-magistralnye-svetilniki	<p>&nbsp; &nbsp; &nbsp;Для освещения автодорог, улиц и магистралей наиболее часто используются светильники консольного типа крепления. Как правило, местами установки таких светильников служат опоры освещения различной высоты.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Правильно подобранные осветительные приборы позволяют добиться равномерной освещенности на поверхности дорожного покрытия.&nbsp;</p>	t	2018-08-25 10:31:13.008365-04	\N	5	products/Product_magistralnyi_svetilnik_00.jpg	Светодиодные магистральные светильники	\N		<p>&nbsp; &nbsp; &nbsp;Для освещения автодорог, улиц и магистралей наиболее часто используются светильники консольного типа крепления. Как правило, местами установки таких светильников служат опоры освещения различной высоты.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Правильно подобранные осветительные приборы позволяют добиться равномерной освещенности на поверхности дорожного покрытия.&nbsp;</p>	Светодиодные магистральные светильники
2	Светодиодные линейные светильники	svetodiodnye-linejnye-svetilniki	<p>Светодиодные линейные светильники уличного типа применяются для подчеркивания контуров зданий, а так же для создания различных форм на фасадах.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;При установке светильников в ряд создается эффект единой сплошной линии.</p>	t	2018-08-23 09:53:51.156416-04	\N	2	products/lineynyi_svetilnik_WL-HLG-10W_fhNp8SM.png	Светодиодные линейные светильники	\N		<p>Светодиодные линейные светильники уличного типа применяются для подчеркивания контуров зданий, а так же для создания различных форм на фасадах.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;При установке светильников в ряд создается эффект единой сплошной линии.</p>	Светодиодные линейные светильники
4	Светодиодные настенные светильники	svetodiodnye-nastennye-svetilniki	<p>&nbsp; &nbsp; &nbsp;Световой дизайн зданий позволяет подчеркнуть архитектурные особенности и сделать внешний вид уникальным.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Настенные светильники с различными углами и направлениями свечения дают возможность выделить детали здания (колонны, окна или лепнину). При профессиональном подходе к реализации художественного освещения, фасад Вашего здания будет прекрасно смотреться в ночное время и выгодно выделяться на фоне близлежащих строений.</p>	t	2018-08-28 01:48:15.357209-04	\N	2	products/Product_nastennyi_svetilnik_02.jpg	Светодиодные настенные светильники	\N		<p>&nbsp; &nbsp; &nbsp;Световой дизайн зданий позволяет подчеркнуть архитектурные особенности и сделать внешний вид уникальным.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Настенные светильники с различными углами и направлениями свечения дают возможность выделить детали здания (колонны, окна или лепнину). При профессиональном подходе к реализации художественного освещения, фасад Вашего здания будет прекрасно смотреться в ночное время и выгодно выделяться на фоне близлежащих строений.</p>	Светодиодные настенные светильники
5	Светодиодные лампы	svetodiodnye-lampy	<p>&nbsp; &nbsp; &nbsp;Светодиодные лампы стремительно вытесняют все остальные виды ламп в бытовом применении.Всем известны преимущества использования осветительных приборов с диодным источником света:</p>\r\n\r\n<ul>\r\n\t<li>долговечность</li>\r\n\t<li>экономичность</li>\r\n\t<li>остутствие пульсации</li>\r\n\t<li>высокий КПД</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В настоящее время любой светильник можно оборудовать лампами на основе светодиодов.</p>	t	2018-08-28 10:40:50.046556-04	\N	4	products/Product_lampa_00_YRyFLMo.jpg	Светодиодные лампы	\N		<p>&nbsp; &nbsp; &nbsp;Светодиодные лампы стремительно вытесняют все остальные виды ламп в бытовом применении.Всем известны преимущества использования осветительных приборов с диодным источником света:</p>\r\n\r\n<ul>\r\n\t<li>долговечность</li>\r\n\t<li>экономичность</li>\r\n\t<li>остутствие пульсации</li>\r\n\t<li>высокий КПД</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В настоящее время любой светильник можно оборудовать лампами на основе светодиодов.</p>	Светодиодные лампы
6	Светодиодные декорации	svetodiodnye-dekoracii	<p>&nbsp; &nbsp; Праздничное оформления домов, улиц, парков и площадей в большинстве случаев имеет определенную тематику и декорации изготавливаются индивидуально под каждый проект.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Фигуры всем знакомых персонажей или животных способны создать непередаваемую атмофсеру праздника, которая принесет много положительных эмоций и детям,&nbsp;и взрослым.</p>\r\n\r\n<p>.</p>	t	2018-08-29 03:53:47.993839-04	\N	3	products/Product_prozdnichnoe_osveschenie_-_02_qIWFqM4.jpg	Светодиодные декорации	\N		<p>&nbsp; &nbsp; Праздничное оформления домов, улиц, парков и площадей в большинстве случаев имеет определенную тематику и декорации изготавливаются индивидуально под каждый проект.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Фигуры всем знакомых персонажей или животных способны создать непередаваемую атмофсеру праздника, которая принесет много положительных эмоций и детям,&nbsp;и взрослым.</p>\r\n\r\n<p>.</p>	Светодиодные декорации
7	Светодиодные прожекторы	svetodiodnye-prozhektory	<p>&nbsp; &nbsp; &nbsp;В настоящее время светодиодные прожекторы почти полностью вытеснили устаревшие приборы освещения с галогенными и металлогалогенными лампами. Область применения прожекторов достаточно широка: от складских территорий и больших промышленных объектов до обычного бытового использования. Установка возможна как внутри помещений, так и на открытом воздухе. Преимуществами светодиодных осветительных приборов являются:</p>\r\n\r\n<ul>\r\n\t<li>Небольшие габариты и вес</li>\r\n\t<li>Экономичность</li>\r\n\t<li>Срок службы до 50 000 часов</li>\r\n\t<li>Ударостойкость и виброустойчивость</li>\r\n\t<li>Работа температурном диапазоне от -50&deg; до +60&deg;</li>\r\n\t<li>Устойчивость к перепадам напряжения</li>\r\n\t<li>Широкий ассортимент</li>\r\n\t<li>Хорошая цветопередача</li>\r\n\t<li>Пожаробезопасность</li>\r\n</ul>	t	2018-08-30 04:18:03.6137-04	\N	5	products/Product_prozhektor_01.jpg	Светодиодные прожекторы	\N		<p>&nbsp; &nbsp; &nbsp;В настоящее время светодиодные прожекторы почти полностью вытеснили устаревшие приборы освещения с галогенными и металлогалогенными лампами. Область применения прожекторов достаточно широка: от складских территорий и больших промышленных объектов до обычного бытового использования. Установка возможна как внутри помещений, так и на открытом воздухе. Преимуществами светодиодных осветительных приборов являются:</p>\r\n\r\n<ul>\r\n\t<li>Небольшие габариты и вес</li>\r\n\t<li>Экономичность</li>\r\n\t<li>Срок службы до 50 000 часов</li>\r\n\t<li>Ударостойкость и виброустойчивость</li>\r\n\t<li>Работа температурном диапазоне от -50&deg; до +60&deg;</li>\r\n\t<li>Устойчивость к перепадам напряжения</li>\r\n\t<li>Широкий ассортимент</li>\r\n\t<li>Хорошая цветопередача</li>\r\n\t<li>Пожаробезопасность</li>\r\n</ul>	Светодиодные прожекторы
8	Светодиодные трековые светильники	svetodiodnye-trekovye-svetilniki	<p>&nbsp; &nbsp; &nbsp;Светодиодные трековые светильники на шинопроводе являются мобильными&nbsp;прожекторами, которые используются внутри&nbsp;помещений и создают узконаправленный или рассеяннй свет. Областью применения таких&nbsp;светильников являются витрины торговых центров, интерьеры офисов, а также рабочие места или&nbsp;выставочные экспонаты, товары или другие предметы.&nbsp;В результате использования&nbsp;акцентированной подсветки создается интересная игра света и тени в пространстве, что позволяет&nbsp; создать комфорную остановку для посетителей или работников.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Шинопровод светильника позволяет без труда установить прибор и регулировать напрвление падения света в любое время.</p>	t	2018-08-30 06:10:25.751637-04	\N	4	products/Product_trekovyi_svetilnik_01.jpg	Светодиодные трековые светильники	\N		<p>&nbsp; &nbsp; &nbsp;Светодиодные трековые светильники на шинопроводе являются мобильными&nbsp;прожекторами, которые используются внутри&nbsp;помещений и создают узконаправленный или рассеяннй свет. Областью применения таких&nbsp;светильников являются витрины торговых центров, интерьеры офисов, а также рабочие места или&nbsp;выставочные экспонаты, товары или другие предметы.&nbsp;В результате использования&nbsp;акцентированной подсветки создается интересная игра света и тени в пространстве, что позволяет&nbsp; создать комфорную остановку для посетителей или работников.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Шинопровод светильника позволяет без труда установить прибор и регулировать напрвление падения света в любое время.</p>	Светодиодные трековые светильники
10	Светодиодные ленты	svetodiodnye-lenty	<p>&nbsp; &nbsp; &nbsp;Светодиодные ленты в декорировании используются&nbsp;в целях&nbsp;дополнительного подсвечивания деталей интерьера и экстерьера. Такой&nbsp;подсветкой можно разделить помещение на отдельные функциональные зоны или расставить&nbsp;акценты на различных элементах интерьера.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Конструктивные особенности светодиодных лент&nbsp;позволяют обеспечить различное&nbsp;применение для светотехнической или декоративной подсветки. Гибкие светодиодные ленты обеспечивают мягкое фоновое свечение, которое&nbsp;определяется как часто используемый&nbsp;декоративный эффект.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В то же время светодиодные ленты в применении не ограничиваются лишь квартирами или витринами в магазинах. Часто&nbsp;её можно увидеть в&nbsp;красочном оформлении ночных клубов, кафе и&nbsp;ресторанов, а так же при декорировании внешних фасадов зданий. Светодиодная подсветка с применением лент предоставляет большие возможности для дизайнеров и проектировщиков световых сценариев.</p>	t	2018-08-31 07:00:58.783163-04	\N	4	products/Product_lenta_02.jpg	Светодиодные ленты	\N		<p>&nbsp; &nbsp; &nbsp;Светодиодные ленты в декорировании используются&nbsp;в целях&nbsp;дополнительного подсвечивания деталей интерьера и экстерьера. Такой&nbsp;подсветкой можно разделить помещение на отдельные функциональные зоны или расставить&nbsp;акценты на различных элементах интерьера.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Конструктивные особенности светодиодных лент&nbsp;позволяют обеспечить различное&nbsp;применение для светотехнической или декоративной подсветки. Гибкие светодиодные ленты обеспечивают мягкое фоновое свечение, которое&nbsp;определяется как часто используемый&nbsp;декоративный эффект.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В то же время светодиодные ленты в применении не ограничиваются лишь квартирами или витринами в магазинах. Часто&nbsp;её можно увидеть в&nbsp;красочном оформлении ночных клубов, кафе и&nbsp;ресторанов, а так же при декорировании внешних фасадов зданий. Светодиодная подсветка с применением лент предоставляет большие возможности для дизайнеров и проектировщиков световых сценариев.</p>	Светодиодные ленты
11	Системы видеонаблюдения	sistemy-videonablyudeniya	<p>&nbsp; &nbsp; &nbsp;Современные системы промышленного видеонаблюдения - это многофункциональные решения, которые&nbsp;дополняют и усиливают эффективность работы других элементов системы&nbsp;безопасности объектов. Например,&nbsp;такие&nbsp;части как система контроля доступа или охранно-пожарная сигнализация в совокупности с грамотно построенной видеосистемой обеспечат максимальную защиту и своевременное принятие решений в случаях ЧП.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;При расчетах системы видеонаблюдения следует учитывать&nbsp;не только технологические новинки и тенденции, но и реальные условия эксплуатации и функционирования.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В настоящее время компания &quot;Технология&quot;&nbsp;предлагает решения на основе&nbsp; IP-видеокамер, как наиболее удобный и функциональный способ решения задачи обеспечения непрерывного видеоконтроля и архивации данных.</p>	t	2018-08-31 08:57:00.571857-04	\N	7	products/Product_videonabludenie_01_glavnaya_KXE7i9l.jpg	Системы видеонаблюдения	\N		<p>&nbsp; &nbsp; &nbsp;Современные системы промышленного видеонаблюдения - это многофункциональные решения, которые&nbsp;дополняют и усиливают эффективность работы других элементов системы&nbsp;безопасности объектов. Например,&nbsp;такие&nbsp;части как система контроля доступа или охранно-пожарная сигнализация в совокупности с грамотно построенной видеосистемой обеспечат максимальную защиту и своевременное принятие решений в случаях ЧП.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;При расчетах системы видеонаблюдения следует учитывать&nbsp;не только технологические новинки и тенденции, но и реальные условия эксплуатации и функционирования.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В настоящее время компания &quot;Технология&quot;&nbsp;предлагает решения на основе&nbsp; IP-видеокамер, как наиболее удобный и функциональный способ решения задачи обеспечения непрерывного видеоконтроля и архивации данных.</p>	Системы видеонаблюдения
9	Опоры освещения	opory-osvesheniya	<p>&nbsp; &nbsp; &nbsp;Опоры наружного освещения предназначены для фиксации осветительного прибора на большой высоте. Их дизайн должен вписываться в окружающее пространство. В компании &quot;Технология&quot; при производстве опор этому уделяется значительное внимание.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Металлические столбы&nbsp;используются при благоустройстве автодорог и мест отдыха, парковок и скверов. Некоторые из наших проектов реализованы с помощью декоративных опор с привлекательный дизайном, в остальных случаях используется стандарное многофункциональное решение - опоры высотой от 5 до 12 метров.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Для установки столбов используется бетонное основание с фланцевым креплением.</p>	t	2018-08-30 09:13:04.543558-04	\N	5	products/Product_opora_000.jpg	Опоры освещения	\N		<p>&nbsp; &nbsp; &nbsp;Опоры наружного освещения предназначены для фиксации осветительного прибора на большой высоте. Их дизайн должен вписываться в окружающее пространство. В компании &quot;Технология&quot; при производстве опор этому уделяется значительное внимание.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Металлические столбы&nbsp;используются при благоустройстве автодорог и мест отдыха, парковок и скверов. Некоторые из наших проектов реализованы с помощью декоративных опор с привлекательный дизайном, в остальных случаях используется стандарное многофункциональное решение - опоры высотой от 5 до 12 метров.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Для установки столбов используется бетонное основание с фланцевым креплением.</p>	Опоры освещения
\.


--
-- Name: product_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_product_id_seq', 12, true);


--
-- Data for Name: product_productcolor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_productcolor (id, color) FROM stdin;
\.


--
-- Name: product_productcolor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_productcolor_id_seq', 1, false);


--
-- Data for Name: product_productcolor_size; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_productcolor_size (id, productcolor_id, productsizesettings_id) FROM stdin;
\.


--
-- Name: product_productcolor_size_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_productcolor_size_id_seq', 1, false);


--
-- Data for Name: product_productimage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_productimage (id, image, product_id) FROM stdin;
3	products/uploaded_at_2018-08-23_200445.405436.png	2
4	products/uploaded_at_2018-08-23_200619.734218.png	2
5	products/uploaded_at_2018-08-23_201138.137417.png	2
6	products/uploaded_at_2018-08-23_201220.495745.png	2
8	products/uploaded_at_2018-08-23_201220.829142.png	2
10	products/uploaded_at_2018-08-25_203113.010377.png	3
11	products/uploaded_at_2018-08-25_203113.226591.png	3
12	products/uploaded_at_2018-08-25_203113.499587.png	3
13	products/uploaded_at_2018-08-25_203113.780590.png	3
14	products/uploaded_at_2018-08-25_203113.932013.png	3
15	products/uploaded_at_2018-08-25_203114.095597.png	3
16	products/uploaded_at_2018-08-25_203114.230659.png	3
18	products/uploaded_at_2018-08-28_124831.110935.png	4
19	products/uploaded_at_2018-08-28_124831.257060.png	4
20	products/uploaded_at_2018-08-28_124831.535762.png	4
21	products/uploaded_at_2018-08-28_124831.720945.png	4
22	products/uploaded_at_2018-08-28_124831.978093.png	4
24	products/uploaded_at_2018-08-28_204050.217492.png	5
25	products/uploaded_at_2018-08-28_204050.312356.png	5
26	products/uploaded_at_2018-08-28_204050.484499.png	5
27	products/uploaded_at_2018-08-28_204050.567837.png	5
28	products/uploaded_at_2018-08-28_204050.755146.png	5
31	products/uploaded_at_2018-08-29_153339.190607.png	6
32	products/uploaded_at_2018-08-29_153339.484404.png	6
33	products/uploaded_at_2018-08-29_153340.231784.png	6
34	products/uploaded_at_2018-08-29_153341.047986.png	6
35	products/uploaded_at_2018-08-30_141803.616613.png	7
36	products/uploaded_at_2018-08-30_143254.003637.png	7
37	products/uploaded_at_2018-08-30_143254.218280.png	7
38	products/uploaded_at_2018-08-30_143254.397357.png	7
39	products/uploaded_at_2018-08-30_161025.753554.png	8
40	products/uploaded_at_2018-08-30_161025.983716.png	8
41	products/uploaded_at_2018-08-30_161026.160944.png	8
42	products/uploaded_at_2018-08-30_161026.301932.png	8
45	products/uploaded_at_2018-08-30_194407.767309.png	9
46	products/uploaded_at_2018-08-30_194407.870616.png	9
47	products/uploaded_at_2018-08-31_170058.785662.png	10
48	products/uploaded_at_2018-08-31_170059.132185.png	10
49	products/uploaded_at_2018-08-31_170059.289588.png	10
52	products/uploaded_at_2018-08-31_192108.317278.png	11
53	products/uploaded_at_2018-08-31_192108.663477.png	11
43	products/uploaded_at_2018-09-24_182909.227354.png	9
\.


--
-- Name: product_productimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_productimage_id_seq', 54, true);


--
-- Data for Name: product_productproperty; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_productproperty (id, key_id, value, product_id, value_type, prop_tip, value_kg, value_ru) FROM stdin;
2	2	2700К-6500К (опционально)	2				2700К-6500К (опционально)
3	3	АС220V, DC12V, DC24V	2				АС220V, DC12V, DC24V
4	4	IP65	2				IP65
5	5	400 - 1500 Лм (в зависимости от размера)	2				400 - 1500 Лм (в зависимости от размера)
6	7	10°, 20°, 30°, 45°, 90°, 120°	2				10°, 20°, 30°, 45°, 90°, 120°
9	2	2700К-6500К (опционально)	3				2700К-6500К (опционально)
7	8	500мм - 1200мм	2				500мм - 1200мм
10	2	2700К-6500К (опционально)	4				2700К-6500К (опционально)
11	3	АС220V, DC12V, DC24V	4				АС220V, DC12V, DC24V
12	4	IP65, IP54	4				IP65, IP54
13	5	400 - 1500 Лм (в зависимости от мощности)	4				400 - 1500 Лм (в зависимости от мощности)
14	7	1°, 5°, 10°, 30°, 45°, 90°, 120°	4				1°, 5°, 10°, 30°, 45°, 90°, 120°
15	8	100мм-300мм	4				100мм-300мм
16	9	3Вт - 24Вт	4				3Вт - 24Вт
17	10	от 12 мес.	4				от 12 мес.
18	2	2700К-6500К (опционально)	5				2700К-6500К (опционально)
19	2	2700К-6500К, RGB	6				2700К-6500К, RGB
20	3	АС220V	6				АС220V
21	4	IP65, IP67, IP68	6				IP65, IP67, IP68
22	5	400 - 1500 Лм (в зависимости от размера)	6				400 - 1500 Лм (в зависимости от размера)
23	7	120°, 180°, 360°	6				120°, 180°, 360°
24	8	0.5м - 25м	6				0.5м - 25м
25	9	2Вт - 2000Вт	6				2Вт - 2000Вт
26	10	от 12 мес.	6				от 12 мес.
27	2	2700К-6500К (опционально)	7				2700К-6500К (опционально)
28	3	АС220V	7				АС220V
29	4	IP65	7				IP65
30	5	2 000 - 40 000 Лм (в зависимости от мощности)	7				2 000 - 40 000 Лм (в зависимости от мощности)
31	7	45°, 90°, 120°	7				45°, 90°, 120°
32	9	10Вт - 1000Вт	7				10Вт - 1000Вт
33	10	от 12 мес.	7				от 12 мес.
34	2	2700К-6500К (опционально)	8				2700К-6500К (опционально)
35	3	АС220V	8				АС220V
36	4	IP20	8				IP20
37	5	700 - 4 000 Лм (в зависимости от мощности)	8				700 - 4 000 Лм (в зависимости от мощности)
38	7	30°, 45°, 90°, 120°	8				30°, 45°, 90°, 120°
39	9	7Вт - 40Вт	8				7Вт - 40Вт
40	10	от 12 мес.	8				от 12 мес.
41	8	5м - 12м	9				5м - 12м
42	10	от 12 мес.	11				от 12 мес.
8	9	3Вт - 12Вт (в зависимости от размера)	2				3Вт - 12Вт (в зависимости от размера)
43	10	от 12 мес.	2				от 12 мес.
\.


--
-- Name: product_productproperty_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_productproperty_id_seq', 44, true);


--
-- Data for Name: product_productsize; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_productsize (id, sort_order, title, category_id, sub_category_id) FROM stdin;
\.


--
-- Name: product_productsize_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_productsize_id_seq', 1, false);


--
-- Data for Name: product_productsizesettings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_productsizesettings (id, count, size_id) FROM stdin;
\.


--
-- Name: product_productsizesettings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_productsizesettings_id_seq', 1, false);


--
-- Data for Name: promotions_promotion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.promotions_promotion (id, title, description, expires_date, product_id, slug, is_active) FROM stdin;
\.


--
-- Name: promotions_promotion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.promotions_promotion_id_seq', 1, false);


--
-- Data for Name: users_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_user (id, password, last_login, is_superuser, username, first_name, last_name, is_staff, is_active, date_joined, email, address, phone, changing_email, changing_email_code) FROM stdin;
1	pbkdf2_sha256$30000$r4bbcW2nqmDL$rUATeIK0Xzf8v+H1lH0n52Ma84MEOLu3vVoRdXVRV8Y=	2018-10-11 07:21:03.743451-04	t	admin	Admin	Admin	t	t	2018-08-20 04:56:42.692735-04	admin@localhost	\N	\N	\N	\N
3	pbkdf2_sha256$30000$PCtPXovepdkN$kyGLdCAODdshX/YeSRxHNjuWoKoACTG0AYyHpg3Ecc0=	2018-11-05 03:26:15.74944-05	t				t	t	2018-08-21 03:41:51-04	l.bokhan@technology.kg			\N	\N
\.


--
-- Data for Name: users_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: users_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_user_groups_id_seq', 1, false);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_user_id_seq', 3, true);


--
-- Data for Name: users_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: users_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: users_userimage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_userimage (id, avatar, user_id) FROM stdin;
\.


--
-- Name: users_userimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_userimage_id_seq', 1, false);


--
-- Data for Name: works_donework; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.works_donework (id, title, short_description, customer, is_active, slug, created_at, address, description, description_kg, short_description_kg, title_kg, description_ru, short_description_ru, title_ru, customer_link, year_of_exec) FROM stdin;
27	Реконструкция парка Победы им. Д. Асанова	Реконструкция парка Победы им. Д. Асанова	МП "Бишкексвет"	t	rekonstrukciya-parka-pobedy-im-d-asanova	2017-10-07	г. Бишкек	<p>&nbsp; &nbsp; &nbsp;Компанией&nbsp;&quot;Технология&quot; была проведена часть работ из общего плана реконструкции парка Победы им. Д. Асанова в г. Бишкек. Были произведены и установлены новые опоры освещения со светодиодными прожекторами, а так же заменены старые линии электропитания на территории парка.</p>	\N			<p>&nbsp; &nbsp; &nbsp;Компанией&nbsp;&quot;Технология&quot; была проведена часть работ из общего плана реконструкции парка Победы им. Д. Асанова в г. Бишкек. Были произведены и установлены новые опоры освещения со светодиодными прожекторами, а так же заменены старые линии электропитания на территории парка.</p>	Реконструкция парка Победы им. Д. Асанова	Реконструкция парка Победы им. Д. Асанова	http://meria.kg/ru/structures/object/8	2017г.
25	Установка сети видеонаблюдения	Установка сети видеонаблюдения	Фонд развития Иссык-Кульской области	t	ustanovka-seti-videonablyudeniya-g-balykchy	2018-08-06	г. Балыкчы	<p>&nbsp; &nbsp; &nbsp;В рамках пошаговой реализации проекта &quot;Умный город&quot; в г. Балыкчы (Иссык-Кульская область) компанией &quot;Технология&quot;, при содействии с&nbsp;служб при мэрии города, была установлена и запущена сеть уличных видеокамер на дорогах и перекрестках города.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Были установлены цифровые камеры видеонаблюдения в количестве 84 шт.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Серверное оборудование&nbsp;установлено в&nbsp;здании ГОВД г. Балыкчи, оборудован наблюдательный пост для&nbsp;&nbsp;инспекторов, которые будут следить за&nbsp;происходящим в&nbsp;городе в онлайн режиме.</p>	\N			<p>&nbsp; &nbsp; &nbsp;В рамках пошаговой реализации проекта &quot;Умный город&quot; в г. Балыкчы (Иссык-Кульская область) компанией &quot;Технология&quot;, при содействии с&nbsp;служб при мэрии города, была установлена и запущена сеть уличных видеокамер на дорогах и перекрестках города.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Были установлены цифровые камеры видеонаблюдения в количестве 84 шт.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Серверное оборудование&nbsp;установлено в&nbsp;здании ГОВД г. Балыкчи, оборудован наблюдательный пост для&nbsp;&nbsp;инспекторов, которые будут следить за&nbsp;происходящим в&nbsp;городе в онлайн режиме.</p>	Установка сети видеонаблюдения	Установка сети видеонаблюдения		2018г.
14	Освещение сквера им. У. Салиевой	Освещение сквера им. У. Салиевой	МП "Бишкексвет"	t	osveshenie-skvera-im-u-salievoj-g-bishkek	2018-07-10	г. Бишкек	<p>&nbsp; &nbsp;Компания &nbsp;&laquo;Технология&raquo; &nbsp;приняла &nbsp;участие &nbsp;в &nbsp;проекте &nbsp;мэрии г. Бишкек по капитальной реконструкции инфраструктуры сквера им. Уркии Салиевой.</p>\r\n\r\n<p>&nbsp; &nbsp;В процессе работ были заменены устаревшие линии электропередач, которые были проложены подземным методом в трубе ПВХ. Также установлены новые опоры освещения собственного производства.</p>\r\n\r\n<p>&nbsp; &nbsp;На территории &nbsp;сквера для основного &nbsp;освещения задействованы современные светодиодные светильники с функцией автоматического включения и выключения. А для декоративной подсветки аллеи установлены цветные прожекторы.</p>	\N			<p>&nbsp; &nbsp;Компания &nbsp;&laquo;Технология&raquo; &nbsp;приняла &nbsp;участие &nbsp;в &nbsp;проекте &nbsp;мэрии г. Бишкек по капитальной реконструкции инфраструктуры сквера им. Уркии Салиевой.</p>\r\n\r\n<p>&nbsp; &nbsp;В процессе работ были заменены устаревшие линии электропередач, которые были проложены подземным методом в трубе ПВХ. Также установлены новые опоры освещения собственного производства.</p>\r\n\r\n<p>&nbsp; &nbsp;На территории &nbsp;сквера для основного &nbsp;освещения задействованы современные светодиодные светильники с функцией автоматического включения и выключения. А для декоративной подсветки аллеи установлены цветные прожекторы.</p>	Освещение сквера им. У. Салиевой	Освещение сквера им. У. Салиевой	http://meria.kg/ru/structures/object/8	2018г.
5	Фасадное освещение ЖК "Монреаль"	Фасадное освещение ЖК "Монреаль"	СК "АВАНГАРД- СТИЛЬ"	t	fasadnoe-osveshenie-zhk-monreal	2018-06-14	г. Бишкек, ул. Панфилова 164	<p>&nbsp; &nbsp; &nbsp;Основной задачей архитектурной подсветки фасадов зданий является создание требуемого визуального эффекта. Сочетание контурной и акцентированной подсветки позволяет создать незабываемый уникальный эффект здания.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В проекте подсветки ЖК &laquo;Монреаль&raquo; были спользованы светильники с различными углами рассеивания светового потока, что позволило выгодно подчеркнуть рельеф здания.</p>	\N			<p>&nbsp; &nbsp; &nbsp;Основной задачей архитектурной подсветки фасадов зданий является создание требуемого визуального эффекта. Сочетание контурной и акцентированной подсветки позволяет создать незабываемый уникальный эффект здания.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В проекте подсветки ЖК &laquo;Монреаль&raquo; были спользованы светильники с различными углами рассеивания светового потока, что позволило выгодно подчеркнуть рельеф здания.</p>	Фасадное освещение ЖК "Монреаль"	Фасадное освещение ЖК "Монреаль"		2018г.
4	Фасадное освещение гостиничного комплекса Jannat	Фасадное освещение JANNAT REGENCY	JANNAT REGENCY	t	fasadnoe-osveshenie-gostinichnogo-kompleksa	2017-09-20	г. Бишкек, ул. Аалы Токомбаева, 21/2	<p>&nbsp;При разработке проекта фасадного освещения Jannat Hotel одним из основных условий было создание привлекательного современного облика существующего здания, гармонич-ного сочетания цветовых оттенков. В то же время ночная подсветка не должна создавать неудобств для посетителей заведения Премиум- класса.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Такого эффекта помогает достичь грамотное использование акцентированной подсветки фасада, подчеркивающей уникальную форму и рельеф здания.&nbsp;</p>	\N			<p>&nbsp;При разработке проекта фасадного освещения Jannat Hotel одним из основных условий было создание привлекательного современного облика существующего здания, гармонич-ного сочетания цветовых оттенков. В то же время ночная подсветка не должна создавать неудобств для посетителей заведения Премиум- класса.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Такого эффекта помогает достичь грамотное использование акцентированной подсветки фасада, подчеркивающей уникальную форму и рельеф здания.&nbsp;</p>	Фасадное освещение JANNAT REGENCY	Фасадное освещение гостиничного комплекса Jannat	https://jannat.kg/	2017г.
8	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	МП "Бишкексвет"	t	novogodnee-ukrashenie-ploshadi-ala-too-2017-2018g	2017-12-20	г. Бишкек, площадь Ала- Тоо	<p>&nbsp; &nbsp; &nbsp;Проект новогоднего оформления площади Ала-Тоо в г. Бишкек состоял из установки праздничных декораций, светодиодных фигур, а так же монтаж светодиодного 3D- полотна. Система светильников управляется через центральный сервер со специализированным программным обеспечением.</p>	\N			<p>&nbsp; &nbsp; &nbsp;Проект новогоднего оформления площади Ала-Тоо в г. Бишкек состоял из установки праздничных декораций, светодиодных фигур, а так же монтаж светодиодного 3D- полотна. Система светильников управляется через центральный сервер со специализированным программным обеспечением.</p>	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	Новогоднее украшение площади Ала- Тоо, 2017-2018г.	http://meria.kg/ru/structures/object/8	2017г.
19	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	ОсОО «Газпром Кыргызстан»	t	fasadnoe-osveshenie-zdaniya-osoo-gazprom-kyrgyzstan	2016-12-12	г. Бишкек, ул. Горького, 22	<p>&nbsp; &nbsp;Фасадное освещение здания офиса компании &laquo;Газпром Кыргызстан&raquo; было осуществлено компанией &laquo;Технология&raquo; в 2016 году. Акцентированная подсветка существующего фасада позволила выделить рельеф фасада и добавить зданию красивый облик в ночное время.</p>	\N			<p>&nbsp; &nbsp;Фасадное освещение здания офиса компании &laquo;Газпром Кыргызстан&raquo; было осуществлено компанией &laquo;Технология&raquo; в 2016 году. Акцентированная подсветка существующего фасада позволила выделить рельеф фасада и добавить зданию красивый облик в ночное время.</p>	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	Фасадное освещение здания ОсОО «Газпром Кыргызстан»	http://kyrgyzstan.gazprom.ru/	2016г.
11	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	МП "Бишкексвет"	t	novogodnee-ukrashenie-ploshadi-ala-too-2015-2016g	2015-12-20	г. Бишкек, площадь Ала- Тоо	<p>&nbsp; &nbsp; &nbsp;В канун нового 2016 года нашими специалистами совместно с мэрией г. Бишкек был разработан и воплощен в реальность проект праздничного освещения площади Ала- Тоо.<br />\r\n&nbsp; &nbsp; &nbsp;Новогодняя елка, украшенная декорациями в форме национального орнамента, очень понравилась горожанам. А световое оформление фасадов зданий радует интересными цветными эффектами.</p>	\N			<p>&nbsp; &nbsp; &nbsp;В канун нового 2016 года нашими специалистами совместно с мэрией г. Бишкек был разработан и воплощен в реальность проект праздничного освещения площади Ала- Тоо.<br />\r\n&nbsp; &nbsp; &nbsp;Новогодняя елка, украшенная декорациями в форме национального орнамента, очень понравилась горожанам. А световое оформление фасадов зданий радует интересными цветными эффектами.</p>	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	Новогоднее украшение площади Ала- Тоо, 2015-2016г.	http://meria.kg/ru/structures/object/8	2015г.
9	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	МП "Бишкексвет"	t	prazdnichnoe-ukrashenie-ploshadi-ala-too-k-nooruzu-2018g	2018-03-15	г. Бишкек, площадь Ала- Тоо	<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; &nbsp;В 2017г. для праздничного оформления улиц и центральной площади Ала- Тоо в г. Бишкек был разработан и воплощен в жизнь уникальный дизайн декораций. Студия архитектуры и дизайна компании &laquo;Технология&raquo;, совместно со специалистами мэрии г. Бишкек, создали план по превращению площади в праздничную поляну, украшенную фигурами животных из Красной книги и светящимися композициями.</span></p>	\N			<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; &nbsp;В 2017г. для праздничного оформления улиц и центральной площади Ала- Тоо в г. Бишкек был разработан и воплощен в жизнь уникальный дизайн декораций. Студия архитектуры и дизайна компании &laquo;Технология&raquo;, совместно со специалистами мэрии г. Бишкек, создали план по превращению площади в праздничную поляну, украшенную фигурами животных из Красной книги и светящимися композициями.</span></p>	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	Праздничное украшение площади Ала- Тоо к Ноорузу, 2018г.	http://meria.kg/ru/structures/object/8	2017г.
12	Декоративная подсветка стелы Аска- Таш	декоративная подсветка стелы Аска- Таш, г. Бишкек	МП "Бишкексвет"	t	dekorativnaya-podsvetka-stely-aska-tash-g-bishkek	2016-08-20	г. Бишкек	<p>&nbsp; &nbsp; &nbsp;Компания &laquo;Технология&raquo; приняла участие в реставрации въездной стелы Аска-Таш, которая по праву называется &laquo;Воротами столицы&raquo;.&nbsp;Этот замечательный памятник архитектуры был капитально отремонтирован в соответствии с утвержденным проектом в 2016г.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; Ночная подсветка стелы выполнена с использованием контурной подсветки элементов (петроглифов), прожекторы с оптическими линзами, а также цветной подсветки основной конструкции.&nbsp;</p>	\N			<p>&nbsp; &nbsp; &nbsp;Компания &laquo;Технология&raquo; приняла участие в реставрации въездной стелы Аска-Таш, которая по праву называется &laquo;Воротами столицы&raquo;.&nbsp;Этот замечательный памятник архитектуры был капитально отремонтирован в соответствии с утвержденным проектом в 2016г.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; Ночная подсветка стелы выполнена с использованием контурной подсветки элементов (петроглифов), прожекторы с оптическими линзами, а также цветной подсветки основной конструкции.&nbsp;</p>	декоративная подсветка стелы Аска- Таш, г. Бишкек	Декоративная подсветка стелы Аска- Таш	http://meria.kg/ru/structures/object/8	2016г.
13	Праздничное оформление Площади Ала- Тоо, 2017г.	Праздничное оформление Площади Ала- Тоо, 2017г.	МП "Бишкексвет"	t	prazdnichnoe-oformlenie-ploshadi-ala-too-bishkek-2017g	2017-08-29	г. Бишкек, площадь Ала- Тоо	<p>&nbsp; &nbsp; &nbsp;Ко дню празднования 26-летия Независимости Республики Кыргызстан в Бишкеке было установлено декоративное украшение из металлических конструкций и светодиодов в виде государственного флага Кыргызстана.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Впервые в Кыргызстане применен опыт подобной установки декоративных украшений. Конструкция расположена на высоте более 15 метров над землей, закреплена на 14 металли-ческих тросах, натянутых над площадью Ала- тоо.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В изготовлении декорации использован металлический каркас, а также светодиодные гирлянды общей протяженностью более 50 км.&nbsp;</p>	\N			<p>&nbsp; &nbsp; &nbsp;Ко дню празднования 26-летия Независимости Республики Кыргызстан в Бишкеке было установлено декоративное украшение из металлических конструкций и светодиодов в виде государственного флага Кыргызстана.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Впервые в Кыргызстане применен опыт подобной установки декоративных украшений. Конструкция расположена на высоте более 15 метров над землей, закреплена на 14 металли-ческих тросах, натянутых над площадью Ала- тоо.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В изготовлении декорации использован металлический каркас, а также светодиодные гирлянды общей протяженностью более 50 км.&nbsp;</p>	Праздничное оформление Площади Ала- Тоо, 2017г.	Праздничное оформление Площади Ала- Тоо, 2017г.	http://meria.kg/ru/structures/object/8	2017г.
15	Реконструкция освещения в парке им. Т. Молдо	Реконструкция освещения в парке им. Т. Молдо	Мэрия г. Бишкек	t	rekonstrukciya-osvesheniya-v-parke-im-t-moldo-g-bishkek	2017-09-08	г. Бишкек	<p>&nbsp; &nbsp; &nbsp;Компания &laquo;Технология&raquo; приняла участие в проекте мэрии г. Бишкек по капитальной реконструкции инфраструктуры сквера им. Тоголок Молдо.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В процессе работ были заменены устаревшие линии электропередач и проложены новые силовые кабеля, а так же установлены новые опоры освещения собственного производства и светодиодные светильники.&nbsp;</p>	\N			<p>&nbsp; &nbsp; &nbsp;Компания &laquo;Технология&raquo; приняла участие в проекте мэрии г. Бишкек по капитальной реконструкции инфраструктуры сквера им. Тоголок Молдо.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;В процессе работ были заменены устаревшие линии электропередач и проложены новые силовые кабеля, а так же установлены новые опоры освещения собственного производства и светодиодные светильники.&nbsp;</p>	Реконструкция освещения в парке им. Т. Молдо	Реконструкция освещения в парке им. Т. Молдо	http://meria.kg/ru	2017г.
20	Внутреннее освещение магазинов	Внутреннее освещение магазинов	Частные лица и компании	t	vnutrennee-osveshenie-magazinov	2018-07-31	г. Бишкек	<p><span style="font-family:Arial,Helvetica,sans-serif">&nbsp; &nbsp; &nbsp;За перод рабты компании, реализовано множество проектов по освещению различных торговых помещений. В этот список входят магазины, кафе и бутики в больших торговых центрах г. Бишкек.</span></p>\r\n\r\n<p><span style="font-family:Arial,Helvetica,sans-serif">&nbsp; &nbsp; &nbsp;Правильно подобранное освещение витрин в торговых помещениях очень важно для выгодного представления товаров. Наши&nbsp;специалисты помогут Вам реализовать любую идею освещения торговой территории, от общего освещения до акцентированной подсветки нужных участков витрин. Как правило, используются светильники с цветовой температурой от теплого света до натурально белого света (3000К &ndash; 5000К). К тому же следует учесть общую освещенность помещения для создания условий для комфортного пребывания посетителей.</span></p>	\N			<p><span style="font-family:Arial,Helvetica,sans-serif">&nbsp; &nbsp; &nbsp;За перод рабты компании, реализовано множество проектов по освещению различных торговых помещений. В этот список входят магазины, кафе и бутики в больших торговых центрах г. Бишкек.</span></p>\r\n\r\n<p><span style="font-family:Arial,Helvetica,sans-serif">&nbsp; &nbsp; &nbsp;Правильно подобранное освещение витрин в торговых помещениях очень важно для выгодного представления товаров. Наши&nbsp;специалисты помогут Вам реализовать любую идею освещения торговой территории, от общего освещения до акцентированной подсветки нужных участков витрин. Как правило, используются светильники с цветовой температурой от теплого света до натурально белого света (3000К &ndash; 5000К). К тому же следует учесть общую освещенность помещения для создания условий для комфортного пребывания посетителей.</span></p>	Внутреннее освещение магазинов	Внутреннее освещение магазинов		2010-2018г.
23	Освещение пешеходных переходов	Освещение пешеходных переходов	Фонд развития Иссык-Кульской области	t	osveshenie-peshehodnyh-perehodov-g-balykchy	2018-03-22	г. Балыкчы	<p>&nbsp; &nbsp; &nbsp;Для предупреждения ДТП на пешеходных переходах в ночное время немаловажным условием является установка дополнительного освещения для подсветки пешеходной разметки. Это особенно актуально для нерегулируемых переходов без установленных светофоров или иных светящихся предупредительных знаков.<br />\r\n&nbsp; &nbsp; &nbsp;Компания &quot;Технология &quot; реализовала проект по подсветке нескольких пешеходных переходов&nbsp;рамках программы &quot;Жарык шаар&quot; (&quot;Светлый город&quot;) по инициативе Фонда развития Иссык-Кульской области.<br />\r\n&nbsp; &nbsp; &nbsp;&nbsp;Были использованы светодиодные светильники линейного типа, которые позволили создать модель освещения, обеспечивающую отсутствие&nbsp;ослепленности водителей, а пешеходы выделяются акцентированным светом на общем фоне автодороги.</p>	\N			<p>&nbsp; &nbsp; &nbsp;Для предупреждения ДТП на пешеходных переходах в ночное время немаловажным условием является установка дополнительного освещения для подсветки пешеходной разметки. Это особенно актуально для нерегулируемых переходов без установленных светофоров или иных светящихся предупредительных знаков.<br />\r\n&nbsp; &nbsp; &nbsp;Компания &quot;Технология &quot; реализовала проект по подсветке нескольких пешеходных переходов&nbsp;рамках программы &quot;Жарык шаар&quot; (&quot;Светлый город&quot;) по инициативе Фонда развития Иссык-Кульской области.<br />\r\n&nbsp; &nbsp; &nbsp;&nbsp;Были использованы светодиодные светильники линейного типа, которые позволили создать модель освещения, обеспечивающую отсутствие&nbsp;ослепленности водителей, а пешеходы выделяются акцентированным светом на общем фоне автодороги.</p>	Освещение пешеходных переходов	Освещение пешеходных переходов		2018г.
22	Освещение ипподрома для Игр кочевников, 2014г.	Освещение ипподрома для Игр кочевников, 2014г.	Министерство г. Бишкек	t	osveshenie-ippodroma-dlya-igr-kochevnikov-2014g	2014-09-01	г. Чолпон- Ата	<p>&nbsp; &nbsp; &nbsp;Для проведения церемоний &laquo;Всемирных игр кочевников&raquo; в сентябре 2014г была произведена серьезная реконструкция ипподрома в г. Чолпон- Ата. В рамках реконструкции было установлено новое экономичное и долговечное светодиодное освещение. Наши специалисты рассчитали уровень необходимого освещения и подобрали светильники, которые подошли по всем характеристикам для данного проекта. Общее энергопотребление освещения снизилось в 5 раз, а освещенность в то же время увеличилась в&nbsp;1,5 раза. К тому же, светильники не требуют техобслуживания и замены ламп, что&nbsp;в свою очередь&nbsp;дает дополнительную экономию средств.</p>	\N			<p>&nbsp; &nbsp; &nbsp;Для проведения церемоний &laquo;Всемирных игр кочевников&raquo; в сентябре 2014г была произведена серьезная реконструкция ипподрома в г. Чолпон- Ата. В рамках реконструкции было установлено новое экономичное и долговечное светодиодное освещение. Наши специалисты рассчитали уровень необходимого освещения и подобрали светильники, которые подошли по всем характеристикам для данного проекта. Общее энергопотребление освещения снизилось в 5 раз, а освещенность в то же время увеличилась в&nbsp;1,5 раза. К тому же, светильники не требуют техобслуживания и замены ламп, что&nbsp;в свою очередь&nbsp;дает дополнительную экономию средств.</p>	Освещение ипподрома для Игр кочевников, 2014г.	Освещение ипподрома для Игр кочевников, 2014г.		2014г.
7	Освещение супермаркета "Боорсок"	Освещение супермаркета "Боорсок"	компания "Тип-Топ"	t	osveshenie-supermarketa-boorsok-g-bishkek	2014-12-04	г.Бишкек,  Л.Толстого, 17а/3/Чапаева	<p>&nbsp; &nbsp; &nbsp;Инженерами компании &laquo;Технология&raquo;, совместно с группой дизайнеров, был реализован проект уличного и внутреннего освещения супермаркета &laquo;Боорсок&raquo; в г. Бишкек. Установленное светодиодное оборудование позволяет экономить электроэнергию в 7 раз по сравнению с традиционными лампами освещения. В то же время уровень освещенности на всех территориях проекта соответствует стандартам освещенности подобных объектов.</p>	\N			<p>&nbsp; &nbsp; &nbsp;Инженерами компании &laquo;Технология&raquo;, совместно с группой дизайнеров, был реализован проект уличного и внутреннего освещения супермаркета &laquo;Боорсок&raquo; в г. Бишкек. Установленное светодиодное оборудование позволяет экономить электроэнергию в 7 раз по сравнению с традиционными лампами освещения. В то же время уровень освещенности на всех территориях проекта соответствует стандартам освещенности подобных объектов.</p>	Освещение супермаркета "Боорсок"	Освещение супермаркета "Боорсок"		2014г.
24	Уличное освещение	Уличное освещение	Мэрия г. Балыкчы	t	ulichnoe-osveshenie-g-balykchy	2018-01-20	г. Балыкчы	<p>&nbsp; &nbsp; &nbsp;За период 2016-2018 годы наша&nbsp;компания произвела ряд поставок уличных светильников, предназначенных для освещения улиц в г. Балыкчы. Работы по монтажу проводились собственными силами администрации города,&nbsp;установлены светильники на всех главных улицах города:</p>\r\n\r\n<ul>\r\n\t<li>ул. Жусупа Абдрахманова</li>\r\n\t<li>ул. Аманбаева&nbsp;</li>\r\n\t<li>от Кругового кольца до&nbsp;улицы Калдыбаева</li>\r\n\t<li>&nbsp;въезд&nbsp;в&nbsp;город с&nbsp;западной части (экопост, памятник Семенову Тян-Шанскому)</li>\r\n</ul>	\N			<p>&nbsp; &nbsp; &nbsp;За период 2016-2018 годы наша&nbsp;компания произвела ряд поставок уличных светильников, предназначенных для освещения улиц в г. Балыкчы. Работы по монтажу проводились собственными силами администрации города,&nbsp;установлены светильники на всех главных улицах города:</p>\r\n\r\n<ul>\r\n\t<li>ул. Жусупа Абдрахманова</li>\r\n\t<li>ул. Аманбаева&nbsp;</li>\r\n\t<li>от Кругового кольца до&nbsp;улицы Калдыбаева</li>\r\n\t<li>&nbsp;въезд&nbsp;в&nbsp;город с&nbsp;западной части (экопост, памятник Семенову Тян-Шанскому)</li>\r\n</ul>	Уличное освещение	Уличное освещение	https://www.facebook.com/balykchymeria/	2016-2018г.
18	Реконструкция Аллеи Молодежи	Реконструкция Аллеи Молодежи	МП "Бишкексвет"	t	ulichnoe-osveshenie-allei-molodezhi-g-bishkek	2018-04-11	г. Бишкек	<p>&nbsp; &nbsp;В соответствии с&nbsp;проектом реконструкции&nbsp;освещения Аллеи Молодежи в г. Бишкек, сотрудники компании &laquo;Технология&raquo;&nbsp;полностью заменили&nbsp;существующие&nbsp;опоры&nbsp;освещения, кабеля электрокоммуникаций и установили светильники с функцией&nbsp;дистанционного контроля включения и регулирования уровня яркости освещения.</p>\r\n\r\n<p>&nbsp; &nbsp;Также были установлены централизованная&nbsp;система видеонаблюдения, оповещения и точки доступа WiFi.</p>\r\n\r\n<p>&nbsp; &nbsp;Все работы были проведены собственными силами компании &laquo;Технология&raquo; в установленные сроки.</p>	\N			<p>&nbsp; &nbsp;В соответствии с&nbsp;проектом реконструкции&nbsp;освещения Аллеи Молодежи в г. Бишкек, сотрудники компании &laquo;Технология&raquo;&nbsp;полностью заменили&nbsp;существующие&nbsp;опоры&nbsp;освещения, кабеля электрокоммуникаций и установили светильники с функцией&nbsp;дистанционного контроля включения и регулирования уровня яркости освещения.</p>\r\n\r\n<p>&nbsp; &nbsp;Также были установлены централизованная&nbsp;система видеонаблюдения, оповещения и точки доступа WiFi.</p>\r\n\r\n<p>&nbsp; &nbsp;Все работы были проведены собственными силами компании &laquo;Технология&raquo; в установленные сроки.</p>	Реконструкция Аллеи Молодежи	Реконструкция Аллеи Молодежи	http://meria.kg/ru/structures/object/8	2017г.
26	Реконструкция парка им. Ю. Фучика	Реконструкция парка им. Ю. Фучика	Администрация Ленинского района г. Бишкек	t	rekonstrukciya-parka-im-yu-fuchika-g-bishkek	2018-07-18	г. Бишкек	<p>&nbsp; &nbsp; &nbsp;В июле 2018г. состоялось&nbsp;открытие обновленного парка имени чехословацкого народного героя Ю. Фучика в г. Бишкек.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Компанией &quot;Технология&quot; была выполнена часть работ, предусмотренных по проекту реконструкции парка:</p>\r\n\r\n<ul>\r\n\t<li>спроектированы, изготовлены и установлены красивые опоры освещения</li>\r\n\t<li>произведен монтаж силовых линий и установка светодионых светильников</li>\r\n\t<li>установлены цифровые камеры видеонаблюдения, сервер управления расположен в здании администрации</li>\r\n\t<li>установлена сеть уличных музыкальных аудиоколонок</li>\r\n\t<li>смотрирована бесплатная сеть WiFi, покрывающая всю территорию парка.</li>\r\n</ul>	\N			<p>&nbsp; &nbsp; &nbsp;В июле 2018г. состоялось&nbsp;открытие обновленного парка имени чехословацкого народного героя Ю. Фучика в г. Бишкек.&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Компанией &quot;Технология&quot; была выполнена часть работ, предусмотренных по проекту реконструкции парка:</p>\r\n\r\n<ul>\r\n\t<li>спроектированы, изготовлены и установлены красивые опоры освещения</li>\r\n\t<li>произведен монтаж силовых линий и установка светодионых светильников</li>\r\n\t<li>установлены цифровые камеры видеонаблюдения, сервер управления расположен в здании администрации</li>\r\n\t<li>установлена сеть уличных музыкальных аудиоколонок</li>\r\n\t<li>смотрирована бесплатная сеть WiFi, покрывающая всю территорию парка.</li>\r\n</ul>	Реконструкция парка им. Ю. Фучика	Реконструкция парка им. Ю. Фучика		2018г.
2	Уличное освещение, г. Бишкек	Уличное освещение, г. Бишкек	МП "Бишкексвет"	t	ulichnoe-osveshenie-g-bishkek	2018-01-02	г. Бишкек	<p>&nbsp; &nbsp; &nbsp;За период 2016-2018г. в г. Бишкек было проведено масштабное переоснащение светильников на многих автодорогах&nbsp;столицы. Компаней&nbsp;&quot;Технология&quot; поставлены более 10 тыс. светильников для нужд МП &quot;Бишкексвет&quot;.&nbsp;Светодиодные светильники установлены на всех главных улицах, а также в жилых районах. Полный список улиц, где были заменены светильники состоит из более, чем 50-ти улиц.</p>	\N			<p>&nbsp; &nbsp; &nbsp;За период 2016-2018г. в г. Бишкек было проведено масштабное переоснащение светильников на многих автодорогах&nbsp;столицы. Компаней&nbsp;&quot;Технология&quot; поставлены более 10 тыс. светильников для нужд МП &quot;Бишкексвет&quot;.&nbsp;Светодиодные светильники установлены на всех главных улицах, а также в жилых районах. Полный список улиц, где были заменены светильники состоит из более, чем 50-ти улиц.</p>	Уличное освещение, г. Бишкек	Уличное освещение, г. Бишкек		2016-2018г.
21	Освещение здания Октябрьской администрации	Освещение здания Октябрьской администрации	Муниципальная Администрация по Октябрьскому району г. Бишкек	t	osveshenie-zdaniya-oktyabrskoj-administracii-g-bishkek	2017-12-04	г. Бишкек	<p>&nbsp; &nbsp; &nbsp;Фасадное освещение здания Муниципальной Администрации по Октябрьскому административному району г. Бишкек было выполнено компанией &laquo;Технология&raquo; в преддверии наступающего 2017 года. Использованы уличные светильники с узким углом раскрытия светового потока для достижения необходимого эффекта.</p>	\N			<p>&nbsp; &nbsp; &nbsp;Фасадное освещение здания Муниципальной Администрации по Октябрьскому административному району г. Бишкек было выполнено компанией &laquo;Технология&raquo; в преддверии наступающего 2017 года. Использованы уличные светильники с узким углом раскрытия светового потока для достижения необходимого эффекта.</p>	Освещение здания Октябрьской администрации	Освещение здания Октябрьской администрации		2016г.
10	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	МП "Бишкексвет"	t	novogodnee-ukrashenie-ploshadi-ala-too-2016-2017g	2017-12-20	г. Бишкек, площадь Ала- Тоо	<p>&nbsp; &nbsp; &nbsp;В преддверии наступающего 2017 года компания &laquo;Технология&raquo;, по поручению мэрии г. Бишкек, выполнила Новогоднее оформление главной Новогодней Ёлки и площади Ала- Тоо. Проект разрабатывался в собственной студии архитектуры и дизайна.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Были использованы новые технологии, позволяющие установить объемные фигуры в соответствии с праздничной тематикой. Новогодняя Ёлка, украшенная узорами&nbsp;в национальном стиле, а также декоративные светящиеся персонажи, образовали настоящий сказочный городок прямо в центре Бишкека.</p>	\N			<p>&nbsp; &nbsp; &nbsp;В преддверии наступающего 2017 года компания &laquo;Технология&raquo;, по поручению мэрии г. Бишкек, выполнила Новогоднее оформление главной Новогодней Ёлки и площади Ала- Тоо. Проект разрабатывался в собственной студии архитектуры и дизайна.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;Были использованы новые технологии, позволяющие установить объемные фигуры в соответствии с праздничной тематикой. Новогодняя Ёлка, украшенная узорами&nbsp;в национальном стиле, а также декоративные светящиеся персонажи, образовали настоящий сказочный городок прямо в центре Бишкека.</p>	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	Новогоднее украшение площади Ала- Тоо, 2016-2017г.	http://meria.kg/ru/structures/object/8	2016г.
3	Освещение Бизнес- Центра, г. Бишкек	Освещение Бизнес- Центра, г. Бишкек	СК "Авангард- Стиль"	t	osveshenie-biznes-centra-g-bishkek	2016-08-10	г. Бишкек, ул. Токтогула 125/1, между улицами Тоголок Молдо и Логвиненко.	<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; &nbsp;Специалистами нашей компании был разработан и реализован проект фасадной подсветки&nbsp;Бизнес Центра по ул. Токтогула (Бишкек). Дизайнеры и инженеры подобрали наилучший&nbsp;вариант эффектного оформления подсветки, подчеркивающий архитектуру и концепцию задуманного проекта. </span></p>\r\n\r\n<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; &nbsp;В данном проекте были использованы влагозащищенные линейные светильники.&nbsp;Зданиие занято под фешенебельную гостиницу и офисы&nbsp;различных компаний.</span></p>	\N			<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; &nbsp;Специалистами нашей компании был разработан и реализован проект фасадной подсветки&nbsp;Бизнес Центра по ул. Токтогула (Бишкек). Дизайнеры и инженеры подобрали наилучший&nbsp;вариант эффектного оформления подсветки, подчеркивающий архитектуру и концепцию задуманного проекта. </span></p>\r\n\r\n<p><span style="font-family:Georgia,serif">&nbsp; &nbsp; &nbsp;В данном проекте были использованы влагозащищенные линейные светильники.&nbsp;Зданиие занято под фешенебельную гостиницу и офисы&nbsp;различных компаний.</span></p>	Освещение Бизнес- Центра, г. Бишкек	Освещение Бизнес- Центра, г. Бишкек	https://avangardstyle.kg/	2015г.
6	Наружнее освещение супермаркетов "Глобус"	Наружнее освещение супермаркетов "Глобус"	СК "Авангард- Стиль"	t	naruzhnee-osveshenie-supermarketov-globus	2018-04-20	г. Бишкек: пр-т Чуй 41Б, ул. Аалы Токомбаева 53/1	<p>&nbsp; &nbsp; &nbsp;Инженерами и монтажниками компании Технология, совместно с группой дизайнеров, было реализовано энергосберегающее освещение сети гипермаркетов &laquo;Глобус&raquo;,<br />\r\nг. Бишкек. Были использованы уличные светильники для фасадной подсветки, а также различные решения по освещению внутренних помещений.&nbsp;</p>	\N			<p>&nbsp; &nbsp; &nbsp;Инженерами и монтажниками компании Технология, совместно с группой дизайнеров, было реализовано энергосберегающее освещение сети гипермаркетов &laquo;Глобус&raquo;,<br />\r\nг. Бишкек. Были использованы уличные светильники для фасадной подсветки, а также различные решения по освещению внутренних помещений.&nbsp;</p>	Наружнее освещение супермаркетов "Глобус"	Наружнее освещение супермаркетов "Глобус"	https://avangardstyle.kg/	2015г.
\.


--
-- Data for Name: works_donework_assigned_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.works_donework_assigned_products (id, donework_id, product_id) FROM stdin;
1	2	3
2	3	2
3	4	2
4	4	4
5	5	2
6	5	4
7	6	2
8	6	3
9	7	2
10	7	3
11	7	4
12	7	5
13	6	5
14	8	6
15	9	6
16	10	6
17	11	2
18	11	6
19	12	2
20	12	7
21	11	7
22	5	7
23	7	8
24	13	6
25	14	3
26	14	7
27	2	9
28	14	9
29	15	9
30	15	3
34	18	3
35	19	2
36	19	4
37	20	8
38	20	2
39	20	4
40	20	5
41	20	10
42	22	7
43	23	2
44	24	3
45	25	11
46	21	2
47	22	3
48	26	3
49	26	9
50	26	11
51	27	9
52	27	3
\.


--
-- Name: works_donework_assigned_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.works_donework_assigned_products_id_seq', 52, true);


--
-- Name: works_donework_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.works_donework_id_seq', 27, true);


--
-- Data for Name: works_donework_service; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.works_donework_service (id, donework_id, category_id) FROM stdin;
1	2	5
2	2	6
3	2	7
4	3	2
5	4	2
7	5	2
8	6	2
9	6	4
10	7	2
11	7	4
12	8	2
13	8	3
14	8	5
15	9	2
16	9	3
17	9	5
18	10	2
19	10	3
20	10	5
21	11	2
22	11	3
23	11	5
24	12	2
25	12	3
26	12	5
27	13	2
28	13	3
29	13	5
30	14	2
31	14	5
32	14	6
33	15	5
34	15	6
35	18	5
36	18	6
37	18	7
38	19	2
39	19	5
40	20	4
41	21	2
42	21	5
43	22	5
44	23	5
45	23	6
46	24	5
47	25	5
48	25	7
49	26	5
50	26	6
51	26	7
52	27	5
53	27	6
\.


--
-- Name: works_donework_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.works_donework_service_id_seq', 53, true);


--
-- Data for Name: works_workgallery; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.works_workgallery (id, main_image, done_project_id) FROM stdin;
10	done_works/covers/Proekt_Ala-Too_Novyi_god_2017_-_07.jpg	10
11	done_works/covers/Proekt_Ala-Too_Novyi_god_2016_-_03.JPG	11
12	done_works/covers/Proekt_Aska-_Tash_01_8FVtgli.jpg	12
13	done_works/covers/Proekt_Flaf_KG_-_05.jpg	13
2	done_works/covers/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_oblozhka_RO0EbrZ.jpg	2
15	done_works/covers/Proekt_Park_T.Moldo_-_01_vUEKXUk.jpg	15
17	done_works/covers/Proekt_Alleya_molodezhi_01_NF1tkNn.jpg	18
18	done_works/covers/Proekt_Gazprom_02_BLpXhHN.jpg	19
19	done_works/covers/Proekt_vnutrennee_osveschenie_02_cQ9cLiE.jpg	20
20	done_works/covers/Proekt_Okt_administraziya_01_OXxWWqq.jpg	21
21	done_works/covers/Proekt_igry_Kochevnikov_02.jpg	22
22	done_works/covers/Proekt_Balykchy_perehody_00.jpg	23
23	done_works/covers/Proekt_Balykchy_osveshenie_02.jpg	24
25	done_works/covers/Proekt_Park_Fuchika_00_sUm8xfw.jpg	26
26	done_works/covers/Proekt_Park_pobedy_01.jpg	27
24	done_works/covers/Proekt_Balykchy_video_06.jpg	25
5	done_works/covers/Proekt_Decorativnoe_osveschenie_Montreal_-_00_P0dVQvF.jpg	5
14	done_works/covers/Proekt_U.Salieva_-_11.jpg	14
3	done_works/covers/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_00_eZOicaS.jpg	3
4	done_works/covers/Proekt_Decorativnoe_osveschenie_Jannat_-_02_8SUyv5l.jpg	4
6	done_works/covers/Proekt_Decorativnoe_osveschenie_Globus_-_01_5yrHdUk.jpg	6
7	done_works/covers/Proekt_Decorativnoe_osveschenie_Boorsok_-_01_4HPrAyN.jpg	7
8	done_works/covers/Proekt_Ala-Too_Novyi_god_2018_-_01_QGO1Kns.jpg	8
9	done_works/covers/Proekt_Ala-Too_Noryz_2018_-_01_bc8Fh7A.jpg	9
\.


--
-- Name: works_workgallery_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.works_workgallery_id_seq', 26, true);


--
-- Data for Name: works_workgallery_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.works_workgallery_images (id, workgallery_id, workimage_id) FROM stdin;
241	9	241
242	9	242
243	9	243
244	9	244
245	9	245
246	9	246
247	9	247
323	25	323
324	25	324
325	25	325
326	25	326
248	9	248
249	10	249
327	25	327
328	25	328
329	26	329
330	26	330
331	26	331
332	24	332
333	14	333
334	14	336
335	14	337
336	14	334
337	14	335
250	10	250
251	10	251
252	10	252
338	14	338
253	10	253
254	10	254
255	10	255
256	11	256
257	11	257
258	11	258
259	11	259
260	11	260
339	14	339
261	11	261
262	11	262
263	12	263
264	12	264
265	12	265
266	12	266
267	12	267
268	13	268
269	13	272
271	13	270
272	13	271
284	15	284
285	15	285
286	15	286
287	15	287
288	17	288
289	17	289
290	17	290
291	17	291
292	17	292
293	18	293
294	18	294
295	18	295
296	19	296
297	19	297
298	19	298
299	19	299
300	19	300
301	19	301
302	19	302
195	2	196
196	2	197
197	2	198
198	2	199
199	2	200
200	2	201
303	20	304
202	2	203
203	2	204
304	20	305
305	20	306
306	20	303
207	3	208
307	21	307
308	21	308
309	21	309
310	21	310
311	22	312
312	22	313
313	22	311
215	4	216
314	23	314
217	5	218
218	5	219
219	5	220
220	5	221
221	3	210
222	3	222
315	23	315
224	4	224
225	6	225
226	6	226
227	6	227
228	6	228
229	6	229
230	7	232
231	7	233
232	7	230
233	7	231
234	8	234
235	8	235
236	8	236
237	8	237
238	8	238
239	8	239
240	9	240
316	23	316
317	23	317
318	24	320
319	24	321
320	24	322
322	24	319
\.


--
-- Name: works_workgallery_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.works_workgallery_images_id_seq', 339, true);


--
-- Data for Name: works_workimage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.works_workimage (id, image, created_at) FROM stdin;
195	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_01_lbiqlvx.jpg	2018-09-06 11:01:51.310242-04
197	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_07_y4jiukd.jpg	2018-09-06 11:04:07.308913-04
199	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_oblozhka_ZaP4Jou.jpg	2018-09-06 11:04:37.259541-04
201	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_05_TIrW7X8.jpg	2018-09-06 11:04:56.120901-04
203	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_03_jrJpshy.jpg	2018-09-06 11:05:14.366226-04
257	done_works/Proekt_Ala-Too_Novyi_god_2016_-_03.JPG	2018-09-12 05:31:14.617548-04
259	done_works/Proekt_Ala-Too_Novyi_god_2016_-_05.JPG	2018-09-12 05:32:13.932914-04
261	done_works/Proekt_Ala-Too_Novyi_god_2016_-_02.JPG	2018-09-12 05:32:37.541864-04
263	done_works/Proekt_Aska-_Tash_01_B29afV6.jpg	2018-09-12 07:34:29.055522-04
265	done_works/Proekt_Aska-_Tash_03_TpPUTFd.jpg	2018-09-12 08:58:05.387555-04
267	done_works/Proekt_Aska-_Tash_05_N9zyc1s.jpg	2018-09-12 08:58:17.325388-04
271	done_works/Proekt_Flaf_KG_-_01_R5jjjPH.jpg	2018-09-13 02:03:38.895474-04
219	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_02_4tUXiZB.jpg	2018-09-07 08:29:08.493143-04
221	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_04.jpg	2018-09-07 08:29:19.476477-04
225	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_01_fixGFFu.jpg	2018-09-10 08:01:46.34519-04
227	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_03_vznDW9i.jpg	2018-09-10 09:03:55.712505-04
229	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_06_yAzdcoo.jpg	2018-09-10 09:13:56.291975-04
231	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_04_9cmzhcj.jpg	2018-09-10 09:46:35.219208-04
233	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_02_DIpwPJn.jpg	2018-09-10 09:46:45.567708-04
235	done_works/Proekt_Ala-Too_Novyi_god_2018_-_02_pVxnZog.jpg	2018-09-10 10:25:23.355829-04
237	done_works/Proekt_Ala-Too_Novyi_god_2018_-_04_JoxylfM.jpg	2018-09-10 10:37:30.170195-04
239	done_works/Proekt_Ala-Too_Novyi_god_2018_-_06_ylYyVBQ.jpg	2018-09-10 10:37:40.235236-04
285	done_works/Proekt_Park_T.Moldo_-_02_LE9g6J5.jpg	2018-09-15 10:35:10.680564-04
287	done_works/Proekt_Park_T.Moldo_-_04_3HNSMg7.jpg	2018-09-15 10:35:21.959261-04
289	done_works/Proekt_Alleya_molodezhi_02_EcGe0FB.jpg	2018-09-17 01:58:35.107448-04
291	done_works/Proekt_Alleya_molodezhi_04.jpg	2018-09-17 01:58:46.873506-04
293	done_works/Proekt_Gazprom_02_MHZ00Ms.jpg	2018-09-17 07:53:18.884999-04
295	done_works/Proekt_Gazprom_03_Mw1Q13F.jpg	2018-09-17 07:53:28.389574-04
297	done_works/Proekt_vnutrennee_osveschenie_02_CJ2jt06.jpg	2018-09-18 02:53:22.253023-04
299	done_works/Proekt_vnutrennee_osveschenie_04_PZH5Fxt.jpg	2018-09-18 02:53:36.682966-04
301	done_works/Proekt_vnutrennee_osveschenie_06_tLOSw5G.jpg	2018-09-18 02:53:48.751117-04
303	done_works/Proekt_Okt_administraziya_01_V7vSgyT.jpg	2018-09-18 03:53:06.233261-04
305	done_works/Proekt_Okt_administraziya_03_fUvJ7Lw.jpg	2018-09-18 03:53:18.977375-04
307	done_works/Proekt_igry_Kochevnikov_02_o81H38G.jpg	2018-09-18 07:04:25.593735-04
309	done_works/Proekt_igry_Kochevnikov_00.jpg	2018-09-18 07:04:37.134958-04
241	done_works/Proekt_Ala-Too_Noryz_2018_-_03_OScXLH9.jpg	2018-09-11 05:43:17.269042-04
311	done_works/Proekt_Balykchy_perehody_00.jpg	2018-09-18 07:47:25.018369-04
313	done_works/Proekt_Balykchy_perehody_02_PxO1a2L.jpg	2018-09-18 07:47:35.379723-04
315	done_works/Proekt_Balykchy_osveshenie_03_4v3flwh.jpg	2018-09-18 08:09:13.172768-04
317	done_works/Proekt_Balykchy_osveshenie_05_yrdaeo5.jpg	2018-09-18 08:09:24.353165-04
319	done_works/Proekt_Balykchy_video_02_g7Sr3a7.jpg	2018-09-18 09:21:15.660453-04
321	done_works/Proekt_Balykchy_video_04.jpg	2018-09-18 09:21:25.815117-04
243	done_works/Proekt_Ala-Too_Noryz_2018_-_02-1.jpg	2018-09-11 05:43:38.193822-04
245	done_works/Proekt_Ala-Too_Noryz_2018_-_08_vhSET7I.jpg	2018-09-11 05:44:01.154484-04
247	done_works/Proekt_Ala-Too_Noryz_2018_-_04_jKmrZ0w.jpg	2018-09-11 05:44:18.587795-04
249	done_works/Proekt_Ala-Too_Novyi_god_2017_-_00.jpg	2018-09-11 08:12:08.016345-04
251	done_works/Proekt_Ala-Too_Novyi_god_2017_-_03_m8V4wvr.jpg	2018-09-11 08:47:43.913638-04
323	done_works/Proekt_Park_Fuchika_00_eUrm4gc.jpg	2018-09-18 10:03:07.601946-04
325	done_works/Proekt_Park_Fuchika_02_lvUOL0R.jpg	2018-09-18 10:03:17.364323-04
327	done_works/Proekt_Park_Fuchika_05_H2n0JaC.jpg	2018-09-18 10:03:39.834978-04
329	done_works/Proekt_Park_pobedy_01.jpg	2018-09-18 11:08:21.275744-04
331	done_works/Proekt_Park_pobedy_05.jpg	2018-09-18 11:08:32.155669-04
253	done_works/Proekt_Ala-Too_Novyi_god_2017_-_04_NUvFRY9.jpg	2018-09-11 08:48:06.748628-04
333	done_works/Proekt_U.Salieva_-_11.jpg	2018-11-05 03:41:14.987568-05
335	done_works/Proekt_U.Salieva_-_13.jpg	2018-11-05 04:16:10.471453-05
337	done_works/Proekt_U.Salieva_-_15.jpg	2018-11-05 04:16:22.124234-05
339	done_works/Proekt_U.Salieva_-_17.jpg	2018-11-05 04:22:49.781617-05
255	done_works/Proekt_Ala-Too_Novyi_god_2017_-_05_ngT3hFL.jpg	2018-09-11 08:48:34.888978-04
196	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_08_gCtHlVE.jpg	2018-09-06 11:03:55.427788-04
198	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_02_G6vJ4Ke.jpg	2018-09-06 11:04:20.852306-04
200	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_06_wQoa2V1.jpg	2018-09-06 11:04:48.66795-04
258	done_works/Proekt_Ala-Too_Novyi_god_2016_-_06.JPG	2018-09-12 05:32:05.142499-04
204	done_works/Proekt_Ulichnoe_osveschenie_g.Bishkek_-_01_kstWWsp.jpg	2018-09-06 11:08:37.463789-04
260	done_works/Proekt_Ala-Too_Novyi_god_2016_-_04.JPG	2018-09-12 05:32:26.589146-04
208	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_00_BAXbQmJ.jpg	2018-09-07 01:54:05.873754-04
210	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_02_aW8YEta.jpg	2018-09-07 01:54:42.596723-04
262	done_works/Proekt_Ala-Too_Novyi_god_2016_-_01_pDxOZdZ.JPG	2018-09-12 05:32:44.66739-04
264	done_works/Proekt_Aska-_Tash_02_Fm7Er7N.jpg	2018-09-12 08:57:58.859957-04
216	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_02_j5TDvD6.jpg	2018-09-07 05:38:19.672987-04
218	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_00_eNXL5xc.jpg	2018-09-07 08:29:02.100621-04
220	done_works/Proekt_Decorativnoe_osveschenie_Montreal_-_03_ysPPiVE.jpg	2018-09-07 08:29:14.392308-04
222	done_works/Proekt_Decorativnoe_osveschenie_Biznes_centra_-_03.jpg	2018-09-07 08:43:11.994294-04
224	done_works/Proekt_Decorativnoe_osveschenie_Jannat_-_03_BmYyNnH.jpg	2018-09-07 08:46:54.19759-04
226	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_02_n59sjVE.jpg	2018-09-10 08:56:54.803456-04
228	done_works/Proekt_Decorativnoe_osveschenie_Globus_-_05_hWVuelh.jpg	2018-09-10 09:13:50.060257-04
230	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_01_O7T4Eqc.jpg	2018-09-10 09:46:25.875342-04
232	done_works/Proekt_Decorativnoe_osveschenie_Boorsok_-_05_jjH0zcp.jpg	2018-09-10 09:46:41.018617-04
234	done_works/Proekt_Ala-Too_Novyi_god_2018_-_01_LoZ66Mu.jpg	2018-09-10 09:59:19.986524-04
236	done_works/Proekt_Ala-Too_Novyi_god_2018_-_03_P8ehgLj.jpg	2018-09-10 10:37:23.794942-04
238	done_works/Proekt_Ala-Too_Novyi_god_2018_-_05_34xWRZ6.jpg	2018-09-10 10:37:35.56103-04
240	done_works/Proekt_Ala-Too_Noryz_2018_-_01_KDHyGGy.jpg	2018-09-11 01:58:30.238158-04
242	done_works/Proekt_Ala-Too_Noryz_2018_-_02_ovo83Io.jpg	2018-09-11 05:43:26.568486-04
244	done_works/Proekt_Ala-Too_Noryz_2018_-_07_60aMNnl.jpg	2018-09-11 05:43:52.723802-04
246	done_works/Proekt_Ala-Too_Noryz_2018_-_06_S2InamT.jpg	2018-09-11 05:44:08.451646-04
248	done_works/Proekt_Ala-Too_Noryz_2018_-_05_cUDqD0x.jpg	2018-09-11 05:44:25.808516-04
250	done_works/Proekt_Ala-Too_Novyi_god_2017_-_02_RgB1DKB.jpg	2018-09-11 08:47:34.08489-04
252	done_works/Proekt_Ala-Too_Novyi_god_2017_-_01_UzUYjUt.jpg	2018-09-11 08:47:49.486167-04
254	done_works/Proekt_Ala-Too_Novyi_god_2017_-_07_4lae5FR.jpg	2018-09-11 08:48:29.55662-04
256	done_works/Proekt_Ala-Too_Novyi_god_2016_-_00.JPG	2018-09-12 05:30:23.80824-04
266	done_works/Proekt_Aska-_Tash_04_YpFdHjM.jpg	2018-09-12 08:58:11.31073-04
268	done_works/Proekt_Flaf_KG_-_05.jpg	2018-09-13 01:50:37.309447-04
270	done_works/Proekt_Flaf_KG_-_04_M7k3GLF.jpg	2018-09-13 01:59:48.353715-04
272	done_works/Proekt_Flaf_KG_-_03.jpg	2018-09-13 02:03:44.714734-04
284	done_works/Proekt_Park_T.Moldo_-_01_oIrH0jc.jpg	2018-09-15 10:34:17.502475-04
286	done_works/Proekt_Park_T.Moldo_-_03_IqvgnZU.jpg	2018-09-15 10:35:16.048826-04
288	done_works/Proekt_Alleya_molodezhi_01_QHSriZh.jpg	2018-09-17 01:58:29.820837-04
290	done_works/Proekt_Alleya_molodezhi_03_hCsoWA0.jpg	2018-09-17 01:58:40.191242-04
292	done_works/Proekt_Alleya_molodezhi_05.JPG	2018-09-17 01:58:52.179489-04
294	done_works/Proekt_Gazprom_01_bCA3kIt.jpg	2018-09-17 07:53:23.930785-04
296	done_works/Proekt_vnutrennee_osveschenie_01_dfOTmQx.jpg	2018-09-18 02:53:16.256476-04
298	done_works/Proekt_vnutrennee_osveschenie_03_86xyW6Q.jpg	2018-09-18 02:53:27.346386-04
300	done_works/Proekt_vnutrennee_osveschenie_05_4Rdb2YO.jpg	2018-09-18 02:53:43.109486-04
302	done_works/Proekt_vnutrennee_osveschenie_07_Zebqwog.jpg	2018-09-18 02:53:53.936591-04
304	done_works/Proekt_Okt_administraziya_02_LEFFHk4.jpg	2018-09-18 03:53:11.749538-04
306	done_works/Proekt_Okt_administraziya_04_s8qCPwX.jpg	2018-09-18 03:53:24.2218-04
308	done_works/Proekt_igry_Kochevnikov_03.jpg	2018-09-18 07:04:32.245862-04
310	done_works/Proekt_igry_Kochevnikov_01_3pRvt8q.jpg	2018-09-18 07:04:43.454272-04
312	done_works/Proekt_Balykchy_perehody_01_BD29eE5.jpg	2018-09-18 07:47:30.171993-04
314	done_works/Proekt_Balykchy_osveshenie_02_X0ffpF2.jpg	2018-09-18 08:09:06.918266-04
316	done_works/Proekt_Balykchy_osveshenie_01_Bs3iXY7.jpg	2018-09-18 08:09:18.367719-04
320	done_works/Proekt_Balykchy_video_03.jpg	2018-09-18 09:21:20.748764-04
322	done_works/Proekt_Balykchy_video_05.jpg	2018-09-18 09:21:31.826723-04
324	done_works/Proekt_Park_Fuchika_01_u20f4sY.jpg	2018-09-18 10:03:12.439663-04
326	done_works/Proekt_Park_Fuchika_03_4To59Dc.jpg	2018-09-18 10:03:23.965301-04
328	done_works/Proekt_Park_Fuchika_04_n63uHtW.jpg	2018-09-18 10:07:49.61326-04
330	done_works/Proekt_Park_pobedy_02.jpg	2018-09-18 11:08:27.055168-04
332	done_works/Proekt_Balykchy_video_06.jpg	2018-10-10 01:52:30.325804-04
334	done_works/Proekt_U.Salieva_-_12.jpg	2018-11-05 04:16:03.429246-05
336	done_works/Proekt_U.Salieva_-_14.jpg	2018-11-05 04:16:16.310125-05
338	done_works/Proekt_U.Salieva_-_16.jpg	2018-11-05 04:22:43.854768-05
\.


--
-- Name: works_workimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.works_workimage_id_seq', 339, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: categories_brand_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_brand
    ADD CONSTRAINT categories_brand_pkey PRIMARY KEY (id);


--
-- Name: categories_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_category
    ADD CONSTRAINT categories_category_pkey PRIMARY KEY (id);


--
-- Name: categories_category_slug_6fddebb1_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_category
    ADD CONSTRAINT categories_category_slug_6fddebb1_uniq UNIQUE (slug);


--
-- Name: categories_subcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_subcategory
    ADD CONSTRAINT categories_subcategory_pkey PRIMARY KEY (id);


--
-- Name: client_app_about_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_about
    ADD CONSTRAINT client_app_about_pkey PRIMARY KEY (id);


--
-- Name: client_app_aboutinnumbers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_aboutinnumbers
    ADD CONSTRAINT client_app_aboutinnumbers_pkey PRIMARY KEY (id);


--
-- Name: client_app_adminemails_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_adminemails
    ADD CONSTRAINT client_app_adminemails_pkey PRIMARY KEY (id);


--
-- Name: client_app_affiliatefeedback_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_affiliatefeedback
    ADD CONSTRAINT client_app_affiliatefeedback_pkey PRIMARY KEY (id);


--
-- Name: client_app_contact_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_contact
    ADD CONSTRAINT client_app_contact_pkey PRIMARY KEY (id);


--
-- Name: client_app_custompages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_custompages
    ADD CONSTRAINT client_app_custompages_pkey PRIMARY KEY (id);


--
-- Name: client_app_custompages_slug_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_custompages
    ADD CONSTRAINT client_app_custompages_slug_key UNIQUE (slug);


--
-- Name: client_app_datenschutz_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_datenschutz
    ADD CONSTRAINT client_app_datenschutz_pkey PRIMARY KEY (id);


--
-- Name: client_app_delivery_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_delivery
    ADD CONSTRAINT client_app_delivery_pkey PRIMARY KEY (id);


--
-- Name: client_app_feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_feedback
    ADD CONSTRAINT client_app_feedback_pkey PRIMARY KEY (id);


--
-- Name: client_app_manufacturers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_manufacturers
    ADD CONSTRAINT client_app_manufacturers_pkey PRIMARY KEY (id);


--
-- Name: client_app_ourteam_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_ourteam
    ADD CONSTRAINT client_app_ourteam_pkey PRIMARY KEY (id);


--
-- Name: client_app_repairtips_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_repairtips
    ADD CONSTRAINT client_app_repairtips_pkey PRIMARY KEY (id);


--
-- Name: client_app_returnpolicy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_returnpolicy
    ADD CONSTRAINT client_app_returnpolicy_pkey PRIMARY KEY (id);


--
-- Name: client_app_reviews_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_reviews
    ADD CONSTRAINT client_app_reviews_pkey PRIMARY KEY (id);


--
-- Name: client_app_seotext_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_seotext
    ADD CONSTRAINT client_app_seotext_pkey PRIMARY KEY (id);


--
-- Name: client_app_sitesettings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_sitesettings
    ADD CONSTRAINT client_app_sitesettings_pkey PRIMARY KEY (id);


--
-- Name: client_app_sitestrings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_sitestrings
    ADD CONSTRAINT client_app_sitestrings_pkey PRIMARY KEY (id);


--
-- Name: client_app_slider_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_slider
    ADD CONSTRAINT client_app_slider_pkey PRIMARY KEY (id);


--
-- Name: client_app_subscribers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_subscribers
    ADD CONSTRAINT client_app_subscribers_pkey PRIMARY KEY (id);


--
-- Name: client_app_termsofsaleofgoods_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_termsofsaleofgoods
    ADD CONSTRAINT client_app_termsofsaleofgoods_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: jet_bookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jet_bookmark
    ADD CONSTRAINT jet_bookmark_pkey PRIMARY KEY (id);


--
-- Name: jet_pinnedapplication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jet_pinnedapplication
    ADD CONSTRAINT jet_pinnedapplication_pkey PRIMARY KEY (id);


--
-- Name: news_post_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_post
    ADD CONSTRAINT news_post_pkey PRIMARY KEY (id);


--
-- Name: news_post_slug_a1a7d7b8_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_post
    ADD CONSTRAINT news_post_slug_a1a7d7b8_uniq UNIQUE (slug);


--
-- Name: news_postgalleryimage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_postgalleryimage
    ADD CONSTRAINT news_postgalleryimage_pkey PRIMARY KEY (id);


--
-- Name: order_order_order_items_order_id_82fb817c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_order_order_items
    ADD CONSTRAINT order_order_order_items_order_id_82fb817c_uniq UNIQUE (order_id, orderrow_id);


--
-- Name: order_order_order_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_order_order_items
    ADD CONSTRAINT order_order_order_items_pkey PRIMARY KEY (id);


--
-- Name: order_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_order
    ADD CONSTRAINT order_order_pkey PRIMARY KEY (id);


--
-- Name: order_orderrow_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_orderrow
    ADD CONSTRAINT order_orderrow_pkey PRIMARY KEY (id);


--
-- Name: product_allproductproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_allproductproperties
    ADD CONSTRAINT product_allproductproperties_pkey PRIMARY KEY (id);


--
-- Name: product_datadownloaddata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_downloaddata
    ADD CONSTRAINT product_datadownloaddata_pkey PRIMARY KEY (id);


--
-- Name: product_product_article_5ae10d3c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_article_5ae10d3c_uniq UNIQUE (article);


--
-- Name: product_product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_pkey PRIMARY KEY (id);


--
-- Name: product_product_slug_76cde0ae_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_slug_76cde0ae_uniq UNIQUE (slug);


--
-- Name: product_productcolor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productcolor
    ADD CONSTRAINT product_productcolor_pkey PRIMARY KEY (id);


--
-- Name: product_productcolor_size_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productcolor_size
    ADD CONSTRAINT product_productcolor_size_pkey PRIMARY KEY (id);


--
-- Name: product_productcolor_size_productcolor_id_74e576f1_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productcolor_size
    ADD CONSTRAINT product_productcolor_size_productcolor_id_74e576f1_uniq UNIQUE (productcolor_id, productsizesettings_id);


--
-- Name: product_productimage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productimage
    ADD CONSTRAINT product_productimage_pkey PRIMARY KEY (id);


--
-- Name: product_productproperty_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productproperty
    ADD CONSTRAINT product_productproperty_pkey PRIMARY KEY (id);


--
-- Name: product_productsize_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productsize
    ADD CONSTRAINT product_productsize_pkey PRIMARY KEY (id);


--
-- Name: product_productsizesettings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productsizesettings
    ADD CONSTRAINT product_productsizesettings_pkey PRIMARY KEY (id);


--
-- Name: promotions_promotion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promotions_promotion
    ADD CONSTRAINT promotions_promotion_pkey PRIMARY KEY (id);


--
-- Name: promotions_promotion_slug_7d42103c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promotions_promotion
    ADD CONSTRAINT promotions_promotion_slug_7d42103c_uniq UNIQUE (slug);


--
-- Name: users_user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user
    ADD CONSTRAINT users_user_email_key UNIQUE (email);


--
-- Name: users_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_pkey PRIMARY KEY (id);


--
-- Name: users_user_groups_user_id_b88eab82_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_user_id_b88eab82_uniq UNIQUE (user_id, group_id);


--
-- Name: users_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user
    ADD CONSTRAINT users_user_pkey PRIMARY KEY (id);


--
-- Name: users_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: users_user_user_permissions_user_id_43338c45_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_permissions_user_id_43338c45_uniq UNIQUE (user_id, permission_id);


--
-- Name: users_userimage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_userimage
    ADD CONSTRAINT users_userimage_pkey PRIMARY KEY (id);


--
-- Name: works_donework_assigned_products_donework_id_e52ff77b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework_assigned_products
    ADD CONSTRAINT works_donework_assigned_products_donework_id_e52ff77b_uniq UNIQUE (donework_id, product_id);


--
-- Name: works_donework_assigned_products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework_assigned_products
    ADD CONSTRAINT works_donework_assigned_products_pkey PRIMARY KEY (id);


--
-- Name: works_donework_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework
    ADD CONSTRAINT works_donework_pkey PRIMARY KEY (id);


--
-- Name: works_donework_service_donework_id_b546091e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework_service
    ADD CONSTRAINT works_donework_service_donework_id_b546091e_uniq UNIQUE (donework_id, category_id);


--
-- Name: works_donework_service_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework_service
    ADD CONSTRAINT works_donework_service_pkey PRIMARY KEY (id);


--
-- Name: works_donework_slug_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework
    ADD CONSTRAINT works_donework_slug_key UNIQUE (slug);


--
-- Name: works_workgallery_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_workgallery_images
    ADD CONSTRAINT works_workgallery_images_pkey PRIMARY KEY (id);


--
-- Name: works_workgallery_images_workgallery_id_b8f26a79_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_workgallery_images
    ADD CONSTRAINT works_workgallery_images_workgallery_id_b8f26a79_uniq UNIQUE (workgallery_id, workimage_id);


--
-- Name: works_workgallery_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_workgallery
    ADD CONSTRAINT works_workgallery_pkey PRIMARY KEY (id);


--
-- Name: works_workimage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_workimage
    ADD CONSTRAINT works_workimage_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_0e939a4f ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_8373b171 ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_417f1b1c ON public.auth_permission USING btree (content_type_id);


--
-- Name: categories_category_2263e5df; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX categories_category_2263e5df ON public.categories_category USING btree (parent_category_id);


--
-- Name: categories_category_slug_6fddebb1_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX categories_category_slug_6fddebb1_like ON public.categories_category USING btree (slug varchar_pattern_ops);


--
-- Name: client_app_affiliatefeedback_afb2f3b6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX client_app_affiliatefeedback_afb2f3b6 ON public.client_app_affiliatefeedback USING btree (affiliate_id);


--
-- Name: client_app_custompages_slug_d7474a18_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX client_app_custompages_slug_d7474a18_like ON public.client_app_custompages USING btree (slug varchar_pattern_ops);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_417f1b1c ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_e8701ad4 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_de54fa62 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: news_post_slug_a1a7d7b8_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX news_post_slug_a1a7d7b8_like ON public.news_post USING btree (slug varchar_pattern_ops);


--
-- Name: news_postgalleryimage_f3aa1999; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX news_postgalleryimage_f3aa1999 ON public.news_postgalleryimage USING btree (post_id);


--
-- Name: order_order_e8701ad4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX order_order_e8701ad4 ON public.order_order USING btree (user_id);


--
-- Name: order_order_order_items_190d8054; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX order_order_order_items_190d8054 ON public.order_order_order_items USING btree (orderrow_id);


--
-- Name: order_order_order_items_69dfcb07; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX order_order_order_items_69dfcb07 ON public.order_order_order_items USING btree (order_id);


--
-- Name: order_orderrow_9bea82de; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX order_orderrow_9bea82de ON public.order_orderrow USING btree (product_id);


--
-- Name: product_datadownloaddata_e8701ad4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_datadownloaddata_e8701ad4 ON public.product_downloaddata USING btree (user_id);


--
-- Name: product_product_521b20f5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_product_521b20f5 ON public.product_product USING btree (brand_id);


--
-- Name: product_product_article_5ae10d3c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_product_article_5ae10d3c_like ON public.product_product USING btree (article varchar_pattern_ops);


--
-- Name: product_product_b583a629; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_product_b583a629 ON public.product_product USING btree (category_id);


--
-- Name: product_product_slug_76cde0ae_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_product_slug_76cde0ae_like ON public.product_product USING btree (slug varchar_pattern_ops);


--
-- Name: product_productcolor_size_78ce6ffb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_productcolor_size_78ce6ffb ON public.product_productcolor_size USING btree (productcolor_id);


--
-- Name: product_productcolor_size_808870d9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_productcolor_size_808870d9 ON public.product_productcolor_size USING btree (productsizesettings_id);


--
-- Name: product_productimage_9bea82de; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_productimage_9bea82de ON public.product_productimage USING btree (product_id);


--
-- Name: product_productproperty_9bea82de; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_productproperty_9bea82de ON public.product_productproperty USING btree (product_id);


--
-- Name: product_productproperty_key_id_7be929b2_uniq; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_productproperty_key_id_7be929b2_uniq ON public.product_productproperty USING btree (key_id);


--
-- Name: product_productsize_b583a629; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_productsize_b583a629 ON public.product_productsize USING btree (category_id);


--
-- Name: product_productsize_fa50d5d8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_productsize_fa50d5d8 ON public.product_productsize USING btree (sub_category_id);


--
-- Name: product_productsizesettings_8222f9c0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX product_productsizesettings_8222f9c0 ON public.product_productsizesettings USING btree (size_id);


--
-- Name: promotions_promotion_9bea82de; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX promotions_promotion_9bea82de ON public.promotions_promotion USING btree (product_id);


--
-- Name: promotions_promotion_slug_7d42103c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX promotions_promotion_slug_7d42103c_like ON public.promotions_promotion USING btree (slug varchar_pattern_ops);


--
-- Name: users_user_email_243f6e77_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_email_243f6e77_like ON public.users_user USING btree (email varchar_pattern_ops);


--
-- Name: users_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_groups_0e939a4f ON public.users_user_groups USING btree (group_id);


--
-- Name: users_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_groups_e8701ad4 ON public.users_user_groups USING btree (user_id);


--
-- Name: users_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_user_permissions_8373b171 ON public.users_user_user_permissions USING btree (permission_id);


--
-- Name: users_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_user_permissions_e8701ad4 ON public.users_user_user_permissions USING btree (user_id);


--
-- Name: users_userimage_e8701ad4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_userimage_e8701ad4 ON public.users_userimage USING btree (user_id);


--
-- Name: works_donework_assigned_products_8842f37e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX works_donework_assigned_products_8842f37e ON public.works_donework_assigned_products USING btree (donework_id);


--
-- Name: works_donework_assigned_products_9bea82de; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX works_donework_assigned_products_9bea82de ON public.works_donework_assigned_products USING btree (product_id);


--
-- Name: works_donework_service_8842f37e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX works_donework_service_8842f37e ON public.works_donework_service USING btree (donework_id);


--
-- Name: works_donework_service_b583a629; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX works_donework_service_b583a629 ON public.works_donework_service USING btree (category_id);


--
-- Name: works_donework_slug_753dbb51_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX works_donework_slug_753dbb51_like ON public.works_donework USING btree (slug varchar_pattern_ops);


--
-- Name: works_workgallery_da9657b5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX works_workgallery_da9657b5 ON public.works_workgallery USING btree (done_project_id);


--
-- Name: works_workgallery_images_632b406d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX works_workgallery_images_632b406d ON public.works_workgallery_images USING btree (workgallery_id);


--
-- Name: works_workgallery_images_8348f9a8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX works_workgallery_images_8348f9a8 ON public.works_workgallery_images USING btree (workimage_id);


--
-- Name: D0697e228cd7fead8b4c6bd434e3ff8a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productcolor_size
    ADD CONSTRAINT "D0697e228cd7fead8b4c6bd434e3ff8a" FOREIGN KEY (productsizesettings_id) REFERENCES public.product_productsizesettings(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: categorie_parent_category_id_766ad864_fk_categories_category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_category
    ADD CONSTRAINT categorie_parent_category_id_766ad864_fk_categories_category_id FOREIGN KEY (parent_category_id) REFERENCES public.categories_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: client_app_affil_affiliate_id_a460b576_fk_client_app_contact_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_app_affiliatefeedback
    ADD CONSTRAINT client_app_affil_affiliate_id_a460b576_fk_client_app_contact_id FOREIGN KEY (affiliate_id) REFERENCES public.client_app_contact(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: news_postgalleryimage_post_id_3ddb3ca1_fk_news_post_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_postgalleryimage
    ADD CONSTRAINT news_postgalleryimage_post_id_3ddb3ca1_fk_news_post_id FOREIGN KEY (post_id) REFERENCES public.news_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_order_order_ite_orderrow_id_0ce2681f_fk_order_orderrow_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_order_order_items
    ADD CONSTRAINT order_order_order_ite_orderrow_id_0ce2681f_fk_order_orderrow_id FOREIGN KEY (orderrow_id) REFERENCES public.order_orderrow(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_order_order_items_order_id_1c1301d1_fk_order_order_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_order_order_items
    ADD CONSTRAINT order_order_order_items_order_id_1c1301d1_fk_order_order_id FOREIGN KEY (order_id) REFERENCES public.order_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_order_user_id_7cf9bc2b_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_order
    ADD CONSTRAINT order_order_user_id_7cf9bc2b_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_orderrow_product_id_566aa821_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_orderrow
    ADD CONSTRAINT order_orderrow_product_id_566aa821_fk_product_product_id FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_datadownloaddata_user_id_b52aa7fd_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_downloaddata
    ADD CONSTRAINT product_datadownloaddata_user_id_b52aa7fd_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_p_sub_category_id_48399d69_fk_categories_subcategory_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productsize
    ADD CONSTRAINT product_p_sub_category_id_48399d69_fk_categories_subcategory_id FOREIGN KEY (sub_category_id) REFERENCES public.categories_subcategory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_pro_productcolor_id_8f9380a2_fk_product_productcolor_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productcolor_size
    ADD CONSTRAINT product_pro_productcolor_id_8f9380a2_fk_product_productcolor_id FOREIGN KEY (productcolor_id) REFERENCES public.product_productcolor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_prod_key_id_7be929b2_fk_product_allproductproperties_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productproperty
    ADD CONSTRAINT product_prod_key_id_7be929b2_fk_product_allproductproperties_id FOREIGN KEY (key_id) REFERENCES public.product_allproductproperties(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_brand_id_fcf1b3f3_fk_categories_brand_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_brand_id_fcf1b3f3_fk_categories_brand_id FOREIGN KEY (brand_id) REFERENCES public.categories_brand(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_category_id_0c725779_fk_categories_category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_category_id_0c725779_fk_categories_category_id FOREIGN KEY (category_id) REFERENCES public.categories_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productimage_product_id_544084bb_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productimage
    ADD CONSTRAINT product_productimage_product_id_544084bb_fk_product_product_id FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productproper_product_id_9171fa14_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productproperty
    ADD CONSTRAINT product_productproper_product_id_9171fa14_fk_product_product_id FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_products_category_id_2d3effe6_fk_categories_category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productsize
    ADD CONSTRAINT product_products_category_id_2d3effe6_fk_categories_category_id FOREIGN KEY (category_id) REFERENCES public.categories_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productsizes_size_id_73390da3_fk_product_productsize_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_productsizesettings
    ADD CONSTRAINT product_productsizes_size_id_73390da3_fk_product_productsize_id FOREIGN KEY (size_id) REFERENCES public.product_productsize(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: promotions_promotion_product_id_23f17d69_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promotions_promotion
    ADD CONSTRAINT promotions_promotion_product_id_23f17d69_fk_product_product_id FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_groups_group_id_9afc8d0e_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_group_id_9afc8d0e_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_groups_user_id_5f6f5a90_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_user_id_5f6f5a90_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_user_pe_permission_id_0b93982e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_pe_permission_id_0b93982e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_user_permissions_user_id_20aca447_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_permissions_user_id_20aca447_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_userimage_user_id_348cee18_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_userimage
    ADD CONSTRAINT users_userimage_user_id_348cee18_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: works_donework_assign_donework_id_863fc038_fk_works_donework_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework_assigned_products
    ADD CONSTRAINT works_donework_assign_donework_id_863fc038_fk_works_donework_id FOREIGN KEY (donework_id) REFERENCES public.works_donework(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: works_donework_assign_product_id_9744f3ea_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework_assigned_products
    ADD CONSTRAINT works_donework_assign_product_id_9744f3ea_fk_product_product_id FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: works_donework_s_category_id_47271abc_fk_categories_category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework_service
    ADD CONSTRAINT works_donework_s_category_id_47271abc_fk_categories_category_id FOREIGN KEY (category_id) REFERENCES public.categories_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: works_donework_servic_donework_id_3133a655_fk_works_donework_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_donework_service
    ADD CONSTRAINT works_donework_servic_donework_id_3133a655_fk_works_donework_id FOREIGN KEY (donework_id) REFERENCES public.works_donework(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: works_workgalle_workgallery_id_f00aed53_fk_works_workgallery_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_workgallery_images
    ADD CONSTRAINT works_workgalle_workgallery_id_f00aed53_fk_works_workgallery_id FOREIGN KEY (workgallery_id) REFERENCES public.works_workgallery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: works_workgallery_done_project_id_db9df567_fk_works_donework_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_workgallery
    ADD CONSTRAINT works_workgallery_done_project_id_db9df567_fk_works_donework_id FOREIGN KEY (done_project_id) REFERENCES public.works_donework(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: works_workgallery_i_workimage_id_248515e3_fk_works_workimage_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.works_workgallery_images
    ADD CONSTRAINT works_workgallery_i_workimage_id_248515e3_fk_works_workimage_id FOREIGN KEY (workimage_id) REFERENCES public.works_workimage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

