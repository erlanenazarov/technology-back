# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin

from .models import *


# Register your models here.

class ProductImageAdmin(admin.TabularInline):
    model = ProductImage
    extra = 1
    max_num = 12


class ProductPropertyAdmin(admin.TabularInline):
    model = ProductProperty
    extra = 1


class ProductAdmin(admin.ModelAdmin):
    search_fields = ['title', 'id']
    list_display = 'title article category is_active'.split()
    list_filter = ['created_at']
    prepopulated_fields = {'slug': ('title',)}

    # readonly_fields = ('is_stock', 'is_promotion')

    inlines = (ProductImageAdmin, ProductPropertyAdmin)


class DownloadDataAdmin(admin.ModelAdmin):
    readonly_fields = 'mode created_at user'.split()

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False


admin.site.register(Product, ProductAdmin)
admin.site.register(AllProductProperties)
admin.site.register(DownloadData, DownloadDataAdmin)