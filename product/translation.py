from modeltranslation.translator import translator, TranslationOptions
from .models import Product, ProductProperty, AllProductProperties


class ProductTranslation(TranslationOptions):
    fields = ('title', 'description', )


class ProductPropertyTranslation(TranslationOptions):
    fields = ('value',)


class AllProductPropertyTranslation(TranslationOptions):
    fields = ('title',)


translator.register(Product, ProductTranslation)
translator.register(ProductProperty, ProductPropertyTranslation)
translator.register(AllProductProperties, AllProductPropertyTranslation)