# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-03-02 10:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0026_remove_product_consumption'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='slug',
            field=models.CharField(help_text='\u041d\u0443\u0436\u0435\u043d \u0434\u043b\u044f URL', max_length=255, unique=True, verbose_name='Slug'),
        ),
    ]
