# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-03-20 14:46
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0031_auto_20180317_1841'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='documentation',
        ),
    ]
