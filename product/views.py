# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import threading
from random import randint
import xlwt
import xlrd

import operator
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from django.db.models import Q
from django.forms import model_to_dict
from django.http import JsonResponse, HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template import loader
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from slugify import slugify
from django.views.decorators.http import require_POST

from MyBrands import settings
from categories.models import Category, Brand
from client_app.forms import SearchForm
from client_app.models import Slider, AdminEmails
from client_app.utils import generate_view_params, get_weight_property_object, send_email_notification
from order.models import Order, OrderRow
from order.utils import is_in_basket
from product.utils import set_cookie
from works.models import DoneWork
from .models import Product, ProductImage, ProductProperty, AllProductProperties, DownloadData
from .forms import OrderInOneClickForm


# Create your views here.


def product_single(request, product_slug):
    try:
        product = Product.objects.get(slug=product_slug)
    except:
        raise Http404

    suggested_products = Product.objects.filter(is_active=True, category=product.category).exclude(id=product.id)

    protocol = 'https://' if request.is_secure() else 'http://'

    projects_in_use = DoneWork.objects.filter(assigned_products__in=[product], is_active=True).distinct()

    params = {
        'product': product,
        'suggested_products': suggested_products,
        'form': OrderInOneClickForm(),
        'link': protocol + request.get_host() + request.get_full_path(),
        'projects': projects_in_use,
        'location': 'products'
    }
    params.update(generate_view_params(request))
    return render(request, 'app/product-single.html', params)


def search(request):
    form = SearchForm(request.GET)

    result = None
    result_count = 0

    if form.is_valid():
        query_list = form.cleaned_data['search_field'].split()
        holder = Product.objects.filter(
            reduce(operator.and_, (Q(title__icontains=q) | Q(slug__icontains=q) for q in query_list)))
        result_count = holder.count()
        pagination = Paginator(holder, 20)
        result = pagination.page(request.GET.get('page', 1))

    params = {
        'search_word': form.cleaned_data['search_field'],
        'products': result,
        'result_count': result_count
    }
    params.update(generate_view_params(request))
    return render(request, 'app/search-results.html', params)


def buy_on_one_click(request, slug):
    try:
        product = Product.objects.get(slug=slug)
    except ObjectDoesNotExist:
        raise Http404

    form = OrderInOneClickForm(request.POST)
    if form.is_valid():
        order = Order()
        order.email = form.cleaned_data['email']
        order.address = ""
        order.name = form.cleaned_data['name']
        order.comment = form.cleaned_data['comment']
        order.phone = form.cleaned_data['phone']
        order.save(force_insert=True)
        item = OrderRow.objects.create(product=product, count=form.cleaned_data['count'])
        order.order_items.add(item)

        template = loader.get_template('app/email/order.html')
        protocol = 'https://' if request.is_secure() else 'http://'
        context = {
            'link': protocol + request.get_host() + reverse(
                'admin:order_order_change',
                args=(order.id,)
            )
        }
        mail_body = template.render(context, request)

        thread = threading.Thread(
            target=send_email_notification,
            args=(
                'Новый заказ | Vesta Stroy',
                mail_body,
                [x.email for x in AdminEmails.objects.all()]
            )
        )
        thread.start()

        return JsonResponse(dict(success=True, message='Заказ оформлен'))

    return JsonResponse(dict(success=False, message=str(form.errors)))


def calculate_ceiling(request):
    category_id = request.POST.get('category_id')
    product_id = request.POST.get('product_id')

    value = request.POST.get('value')

    if category_id:
        category = Category.objects.get(id=category_id)
    else:
        category = None

    if product_id:
        product = Product.objects.get(id=product_id)
    else:
        product = None

    filters = dict()

    _cat = Category.objects.get(slug='gipsokartonnye-sistemy')

    if category:
        filters['category'] = category
    else:
        filters['category_id__in'] = [x.id for x in Category.objects.filter(parent_category=_cat)]

    result = None

    if product:
        result = value / product.consumption


def all_discounts(request):
    pagination = Paginator(Product.objects.filter(is_stock=True), 20)
    products = pagination.page(request.GET.get('page', 1))

    params = {
        'products': products,
    }
    params.update(generate_view_params(request))
    return render(request, 'app/discounts.html', params)


def best_sellers(request):
    pagination = Paginator(Product.objects.filter(best_seller=True), 20)
    products = pagination.page(request.GET.get('page', 1))

    params = {
        'products': products
    }
    params.update(generate_view_params(request))
    return render(request, 'app/best_sellers.html', params)


def data_integry(request):
    if request.user.is_anonymous or not request.user.is_superuser:
        return HttpResponseRedirect(reverse('admin:login') + '?next=' + reverse('products:data_integry'))

    params = {
        'last_download_info': DownloadData.objects.filter(mode='download').last(),
        'last_upload_info': DownloadData.objects.filter(mode='upload').last()
    }
    params.update(generate_view_params(request))
    return render(request, 'app/data-integry.html', params)


def download_data(request):
    DownloadData.objects.create(
        mode='download',
        user=request.user
    )

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="mega_market_products.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Товары')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Артикул', 'Название', 'Slug', 'Категория', 'Цена', 'Скидка']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()
    rows = Product.objects.all()

    for row in rows:
        row_num += 1
        ws.write(row_num, 0, row.article, font_style)
        ws.write(row_num, 1, row.title, font_style)
        ws.write(row_num, 2, row.slug, font_style)
        ws.write(row_num, 3, row.category_id, font_style)
        ws.write(row_num, 4, row.price, font_style)
        ws.write(row_num, 5, row.discount, font_style)

    nws = wb.add_sheet('Категории')

    row_num = 0

    font_style.font.bold = True

    columns = ['id', 'Название', 'slug', 'Родительская категория']

    for col_num in range(len(columns)):
        nws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()
    rows = Category.objects.all().values_list('id', 'title', 'slug', 'parent_category')

    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            nws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)

    return response


def upload_data(request):
    file = request.FILES.get('file')
    book = xlrd.open_workbook(file_contents=file.read(), encoding_override='utf-8')
    wb = book.sheet_by_index(0)

    header_columns = ['Артикул', 'Название', 'Slug', 'Категория', 'Цена', 'Скидка']

    is_valid = True
    for item in range(0, len(header_columns)):
        if header_columns[item].lower() != wb.cell_value(0, item).lower():
            is_valid = False

    temp_items = list()
    if (is_valid):
        for i in range(1, wb.nrows):
            temp_items.append(dict(
                article=wb.cell_value(i, 0),
                title=wb.cell_value(i, 1),
                slug=wb.cell_value(i, 2),
                category=int(wb.cell_value(i, 3)),
                price=wb.cell_value(i, 4),
                discount=wb.cell_value(i, 5)
            ))
    else:
        return JsonResponse(dict(
            success=False,
            message='''
                    Таблица не валидна!
                    Колонки в таблице должны быть расположены так:
                    [Артикул Название Slug Категория Цена Скидка]
                    '''
        ))

    messages = list()

    for item in temp_items:
        try:
            temp_db_product = Product.objects.get(article=item['article'])
        except ObjectDoesNotExist:
            temp_db_product = None

        if not temp_db_product:
            try:
                temp_db_product = Product.objects.get(slug=item['slug'])
            except ObjectDoesNotExist:
                temp_db_product = None

        if not temp_db_product:
            temp_db_product = Product()
            temp_db_product.article = int(item['article'])
            temp_db_product.title = item['title']
            temp_db_product.slug = item['slug']
            temp_db_product.category_id = item['category']
            temp_db_product.price = float(item['price'])
            temp_db_product.discount = int(item['discount'])
            temp_db_product.save()
            messages.append('Создан новый товар #%s - %s' % (temp_db_product.id, temp_db_product.title))
            continue

        if temp_db_product.article != item['article']:
            temp_db_product.article = int(item['article'])

        if temp_db_product.title != item['title']:
            temp_db_product.title = item['title']

        if temp_db_product.slug != item['slug']:
            temp_db_product.slug = item['slug']

        if temp_db_product.category_id != item['category']:
            temp_db_product.category_id = item['category']

        if float(temp_db_product.price) != float(item['price']):
            temp_db_product.price = float(item['price'])

        if int(temp_db_product.discount) != int(item['discount']):
            temp_db_product.discount = int(item['discount'])

        temp_db_product.save()
        messages.append('Товар #%s - %s отредактирован' % (temp_db_product.id, temp_db_product.title))

    DownloadData.objects.create(
        mode='upload',
        user=request.user
    )

    return JsonResponse(dict(success=True, message='База интегрирована', messages=messages))


def list(request, category_slug = None):
    filters = dict(is_active=True)

    category = None
    if category_slug:
        try:
            category = Category.objects.get(slug=category_slug)
        except ObjectDoesNotExist:
            raise Http404

    if category:
        if category.parent_category:
            filters['category__slug'] = category.slug
        else:
            child_categories = Category.objects.filter(is_active=True, parent_category_id=category.id)
            prepared_cats = [cat.id for cat in child_categories.all()]
            prepared_cats.append(category.id)
            filters['category_id__in'] = prepared_cats

    pagination = Paginator(Product.objects.filter(**filters), 20)
    products = pagination.page(request.GET.get('page', 1))

    # if not category:
    #     category = Category.objects.filter(is_active=True).order_by('id').first()

    params = {
        'products': products,
        'location': 'products',
        'current_category': (category.parent_category.slug if category.parent_category is not None else category.slug) if category else None,
        'sub_category': (category.slug if category.parent_category is not None else '') if category else ''
    }
    params.update(generate_view_params(request))
    return render(request, 'app/catalog.html', params)

