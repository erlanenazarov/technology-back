from django.conf.urls import url

from .views import *

urlpatterns = [
    url(
        r'^$',
        list,
        name='list'
    ),
    url(
        r'^category/(?P<category_slug>[\w-]+)/$',
        list,
        name='list_category'
    ),
    url(
        r'^search/$',
        search,
        name='search'
    ),
    url(
        r'^all_discounts/$',
        all_discounts,
        name='all_discounts'
    ),
    url(
        r'^best-sellers/$',
        best_sellers,
        name='best_sellers'
    ),
    url(
        r'^integrity/download/$',
        download_data,
        name='download_data'
    ),
    url(
        r'^integrity/upload/$',
        upload_data,
        name='upload_data'
    ),
    url(
        r'^integrity/$',
        data_integry,
        name='data_integry'
    ),
    url(
        r'^(?P<product_slug>[\w-]+)/$',
        product_single,
        name='product_single'
    ),
    url(
        r'^(?P<slug>[\w-]+)/checkout/in/one/click/$',
        buy_on_one_click,
        name='buy_on_one_click'
    )
]