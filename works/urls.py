from django.conf.urls import url
from .views import *

urlpatterns = [
    url(
        r'^list/$',
        list,
        name='list'
    ),
    url(
        r'^list/(?P<category_slug>[\w-]+)/$',
        list,
        name='category'
    ),
    url(
        r'^(?P<slug>[\w-]+)/$',
        single,
        name='single'
    )
]