from modeltranslation.translator import translator, TranslationOptions
from .models import DoneWork


class DoneWorkTranslation(TranslationOptions):
    fields = ('title', 'short_description', 'description',)


translator.register(DoneWork, DoneWorkTranslation)