# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

# Register your models here.


class DoneWorkInline(admin.TabularInline):
    model = WorkGallery
    extra = 1
    max_num = 1


class DoneWorkAdmin(admin.ModelAdmin):
    inlines = (DoneWorkInline,)
    prepopulated_fields = {'slug': ('title',)}



admin.site.register(DoneWork, DoneWorkAdmin)
admin.site.register(WorkGallery)
admin.site.register(WorkImage)
