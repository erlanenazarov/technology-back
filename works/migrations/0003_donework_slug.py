# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-08-11 08:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('works', '0002_donework_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='donework',
            name='slug',
            field=models.CharField(default='', max_length=255, unique=True, verbose_name='SLUG'),
        ),
    ]
