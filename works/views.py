# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt

from categories.models import Category
from client_app.utils import generate_view_params
from .models import *
from django.shortcuts import render


# Create your views here.

@csrf_exempt
def list(request, category_slug = None):
    # if not category_slug:
    #     category_slug = Category.objects.filter(parent_category=None, is_active=True).order_by('id').first()
    #     category_slug = category_slug.slug

    filters = dict(
        is_active=True,
        service__slug__in=[category_slug]
    ) if category_slug else dict(is_active=True)

    paginator = Paginator(DoneWork.objects.filter(**filters), 6)
    projects = paginator.page(request.GET.get('page', 1))

    if request.is_ajax() or request.POST:
        try:
            next_projects = paginator.page(request.POST.get('page', 1))
        except:
            next_projects = []

        return render(request, 'app/partial/projects-list.html', {'projects': next_projects})

    params = {
        'projects': projects,
        'service_slug': category_slug,
        'location': 'works'
    }
    params.update(generate_view_params(request))
    return render(request, 'app/done_projects.html', params)


def single(request, slug):
    try:
        project = DoneWork.objects.get(slug=slug, is_active=True)
    except ObjectDoesNotExist:
        raise Http404

    service_ids = [Category.objects.get(id=s.id) for s in project.service.all()]

    same_projects = DoneWork.objects.filter(is_active=True, service__in=service_ids).exclude(id=project.id).distinct()

    params = {
        'project': project,
        'same_projects': same_projects,
        'location': 'works'
    }
    params.update(generate_view_params(request))
    return render(request, 'app/single-done-project.html', params)
