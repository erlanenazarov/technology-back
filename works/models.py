# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.


class DoneWork(models.Model):
    class Meta:
        verbose_name = 'Готовый проект'
        verbose_name_plural = 'Готовые проекты'

    title = models.CharField(max_length=255, verbose_name='Наименование')
    slug = models.CharField(max_length=255, verbose_name='SLUG', unique=True, default="")
    short_description = models.CharField(max_length=255, verbose_name='Короткое описание')
    description = RichTextUploadingField(verbose_name='Описание', null=True, blank=True)
    # deadline = models.CharField(max_length=255, verbose_name='Срок')
    customer = models.CharField(max_length=255, verbose_name='Заказчик', null=True, blank=True)
    customer_link = models.CharField(
        max_length=255,
        verbose_name='Ссылка',
        help_text='Ссылка на сайт заказчика',
        null=True,
        blank=True
    )
    address = models.CharField(max_length=255, verbose_name='Адрес', null=True, blank=True)

    service = models.ManyToManyField('categories.Category', verbose_name='Тип услуги')

    assigned_products = models.ManyToManyField('product.Product', verbose_name='Продукты',
                                               help_text='''Товары, который задействованы при разработке''', blank=True)

    is_active = models.BooleanField(default=True, verbose_name='Активна?')

    created_at = models.DateField(verbose_name='Дата создания записи в базе', null=True, blank=True, auto_now_add=True)

    year_of_exec = models.CharField(max_length=255, verbose_name='Год исполнения', null=True, blank=True)

    def get_cover_image(self):
        galleries = self.galleries.all()
        actual = None
        for g in galleries:
            if g.main_image:
                actual = g
                break

        if not actual:
            actual = galleries.first()
            return actual.images.first.image
        else:
            return actual.main_image

    def __unicode__(self):
        return self.title


class WorkGallery(models.Model):
    class Meta:
        verbose_name = 'Галлерея выполненных работ'
        verbose_name_plural = 'Галлереи выполненых работ'

    main_image = models.FileField(upload_to='done_works/covers/', verbose_name='Обложка')
    images = models.ManyToManyField('WorkImage', verbose_name='Картинки')
    done_project = models.ForeignKey('DoneWork', verbose_name='Проект', related_name='galleries')

    def __unicode__(self):
        return 'Галлерея #[%s]' % str(self.id)


class WorkImage(models.Model):
    class Meta:
        verbose_name = 'Картинка работы'
        verbose_name_plural = 'Картинки работ'

    image = models.FileField(upload_to='done_works/', verbose_name='Фотография')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    def __unicode__(self):
        return str(self.image)
