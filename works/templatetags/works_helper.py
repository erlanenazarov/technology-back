from django import template

register = template.Library()


@register.filter
def divide_cat_name(name):
    if ' ' in name:
        names = name.split()
        result = '%s<span>%s</span>' % (names[0], names[1])
        return result

    return name


# def normalize_font_size(text):
#     if len(text) < 43:
#         return 30
#     elif len(text) < :
#         return 20