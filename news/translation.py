from modeltranslation.translator import translator, TranslationOptions
from .models import Post


class PostTranslation(TranslationOptions):
    fields = ('title', 'content', 'short_description', )


translator.register(Post, PostTranslation)