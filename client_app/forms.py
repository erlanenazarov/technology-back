# coding=utf-8
from django import forms
from .models import Feedback, Reviews, AffiliateFeedback
from django.utils.translation import ugettext_lazy as _


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        exclude = ('is_read',)

    def __init__(self, *args, **kwargs):
        super(FeedbackForm, self).__init__(*args, **kwargs)

        self.fields['first_name'].widget = forms.TextInput(attrs=dict(placeholder=_(u'Ваше имя')))
        self.fields['email'].widget = forms.EmailInput(attrs=dict(placeholder=_(u'Ваша почта')))
        self.fields['phone_number'].widget = forms.TextInput(attrs=dict(placeholder=_(u'Ваш телефон')))
        self.fields['message'].widget = forms.Textarea(attrs=dict(placeholder=_(u'Описание проекта'), rows=3))


class ReviewsForm(forms.ModelForm):
    class Meta:
        model = Reviews
        exclude = ('is_active', 'created_at')


class AffiliateFeedbackForm(forms.ModelForm):
    class Meta:
        model = AffiliateFeedback
        exclude = ('is_read', 'created_at')


class SearchForm(forms.Form):
    search_field = forms.CharField(max_length=255)
