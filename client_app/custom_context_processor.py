from client_app.models import DownloadPrice

def context_variables(request):

    return {
        "download_items": DownloadPrice.objects.all()
    }