import json
import os

from django.core.mail import EmailMessage
from django.forms import model_to_dict
from django.utils.translation import get_language

from categories.models import Category, Brand
from MyBrands import settings
from client_app.forms import FeedbackForm, SearchForm
from client_app.models import About, Contact, SiteSettings, Slider, AdminEmails, CustomPages, SiteStrings
from order.utils import get_basket_count, get_basket, get_comparable
from users.forms import LoginForm, RegisterForm


def generate_view_params(request):
    categories = []
    for category in Category.objects.filter(is_active=True, parent_category=None).order_by('id'):
        _item = model_to_dict(category)
        _item['sub_categories'] = []
        for sub_category in Category.objects.filter(parent_category=category, is_active=True).order_by('id'):
            _sub_item = model_to_dict(sub_category)
            _sub_item['sub_categories'] = []
            for _sub_category in Category.objects.filter(parent_category=sub_category, is_active=True).order_by('id'):
                _sub_item['sub_categories'].append(_sub_category)

            _item['sub_categories'].append(_sub_item)
        categories.append(_item)

    slides = Slider.objects.filter(is_active=True).all()

    site_settings = SiteSettings.objects.first()


    # if not site_settings.meta_description:
    meta_description = open(os.path.join(settings.BASE_DIR, 'Meta', 'description.txt'), 'r')
    d = ' '.join(line for line in meta_description)
    meta_description.close()
    # else:
    #     d = site_settings.meta_description

    # if not site_settings.meta_key_words:
    meta_keywords = open(os.path.join(settings.BASE_DIR, 'Meta', 'keywords.txt'), 'r')
    kw = ' '.join(line for line in meta_keywords)
    meta_keywords.close()
    # else:
    #     kw = site_settings.meta_key_words

    email = AdminEmails.objects.filter(is_active=True).first()

    custom_pages = CustomPages.objects.all()

    site_strings = SiteStrings.objects.first()

    params = {
        'meta_description': d,
        'meta_keywords': kw,
        'categories': categories,
        'abstract_feedback_form': FeedbackForm(request.POST),
        # 'abstract_register_form': RegisterForm(request.POST),
        # 'abstract_login_form': LoginForm(request.POST),
        # 'slides': slides,
        'settings': site_settings,
        'email': email,
        'brand_logos': Brand.objects.filter(is_active=True),
        # 'custom_pages': custom_pages,
        'search_form': SearchForm(request.GET),
        'language': get_language(),
        'strings': site_strings
    }
    return params


def send_email_notification(title, body, to, attachment=None):
    email = EmailMessage(title, body=body, to=to)
    email.content_subtype = 'html'
    if attachment:
        email.attach(attachment['file_name'], attachment['content'], attachment['type'])
    email.send()


def get_weight_property_object():
    _settings = SiteSettings.objects.first()
    if _settings:
        return _settings.weight.id
    return None
