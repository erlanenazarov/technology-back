# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django_2gis_maps.admin import DoubleGisAdmin
from django_2gis_maps.fields import AddressField
from django_2gis_maps.widgets import DoubleGisMapsMultipleMarkersWidget

from .models import *


from .models import *

# Register your models here.


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'is_read']

    readonly_fields = ['first_name', 'last_name', 'email', 'phone_number']

    def has_add_permission(self, request):
        return False


admin.site.register(Feedback, FeedbackAdmin)


class EmailAdmin(admin.ModelAdmin):
    list_display = ['email', 'is_active']


class SliderAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'is_active']
    list_display =  ['__unicode__', 'is_active']


class ReviewAdmin(admin.ModelAdmin):
    list_display = 'name email created_at is_active'.split()
    readonly_fields = 'name email message created_at'.split()


class SiteSettingsAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        else:
            return True

    def has_delete_permission(self, request, obj=None):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return True
        else:
            return False


class CustomPagesAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


class SiteStringsAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Верхнее меню', {'fields': (
            'nav_services',
            'nav_works',
            'nav_products',
            'nav_about',
            'nav_contacts',
            'nav_prices'
        )}),
        ('Главная страница', {'fields': (
            'our_services',
            'our_works',
            'our_production',
            'posts'
        )})
    )

    def has_add_permission(self, request):
        if self.model.objects.count() < 1:
            return True

        return False

class PriceListAdmin(admin.ModelAdmin):
    search_fields = ("title",)


class ContactAdmin(DoubleGisAdmin):
    multiple_markers = False

    def __init__(self, *args, **kwargs):
        super(DoubleGisAdmin, self).__init__(*args, **kwargs)
        try:
            if self.multiple_markers:
                self.formfield_overrides[AddressField]['widget'] = DoubleGisMapsMultipleMarkersWidget
        except:
            raise



admin.site.register(AdminEmails, EmailAdmin)
admin.site.register(About)
admin.site.register(Slider, SliderAdmin)
admin.site.register(SeoText)
# admin.site.register(Reviews, ReviewAdmin)
# admin.site.register(CustomPages, CustomPagesAdmin)
admin.site.register(SiteSettings, SiteSettingsAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(SiteStrings, SiteStringsAdmin)
admin.site.register(OrderConditions)
admin.site.register(DownloadPrice, PriceListAdmin)