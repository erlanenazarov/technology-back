# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import threading
from datetime import datetime

from django import http
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, JsonResponse
from django.shortcuts import render
from django.template import loader
from django.urls import reverse
from django.utils.http import is_safe_url
from django.utils.translation import check_for_language, activate
# from django.views.decorators.csrf import csrf_exempt
# from django.utils import translation
from client_app.forms import FeedbackForm, ReviewsForm, AffiliateFeedbackForm
from client_app.models import *
from news.models import Post
from product.models import Product
# from promotions.models import Promotion
from works.models import DoneWork
from .utils import generate_view_params, send_email_notification
# from categories.models import Brand
from django.conf import settings


from django.views.generic import TemplateView
# Create your views here.


def index(request):
    slides = Slider.objects.filter(is_active=True).order_by('id')
    seo_text = SeoText.objects.first()

    done_project = DoneWork.objects.filter(is_active=True).order_by('-id').all()[0:10]
    products = Product.objects.filter(is_active=True).order_by('-id').all()[0:16]

    params = {
        'location': 'home',
        'slides': slides,
        'seo_text': seo_text,
        'product_count': Product.objects.count(),
        'done_projects': done_project,
        'products': products,
        'posts': Post.objects.filter(is_active=True).all()
    }
    params.update(generate_view_params(request))
    return render(request, 'app/index.html', params)


def abstract_feedback(request):
    if request.POST:
        form = FeedbackForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            template = loader.get_template('app/email/feedback.html')
            context = {
                'first_name': form.cleaned_data['first_name'],
                'last_name': form.cleaned_data['last_name'],
                'email': email,
                'phone': form.cleaned_data['phone_number'],
                'today': datetime.today(),
                'message': form.cleaned_data['message']
            }
            emails = [x.email for x in AdminEmails.objects.filter(is_active=True)]
            emails.append(email)
            response = template.render(context, request)
            thread = threading.Thread(target=send_email_notification, args=(
                'Обратная связь | Technology',
                response,
                emails
            ))
            thread.start()

            form.save(commit=True)

            return JsonResponse(dict(
                success=True,
                message='Сообщение отправлено'
            ))
        return JsonResponse(dict(
            success=False,
            message=str(form.errors)
        ))
    return JsonResponse(dict(
        success=False,
        message='Чтобы эта функция сайта заработала - нужно отправить форму POST запросом'
    ))


def contacts(request):
    affiliates = Contact.objects.all()
    params = {
        'feedback_form': FeedbackForm(),
        'affiliates': affiliates,
        'location': 'contacts'
    }

    if request.is_ajax():
        form = FeedbackForm(request.POST)
        if form.is_valid():
            template = loader.get_template('app/email/feedback.html')
            context = {
                'name': form.cleaned_data['name'],
                'phone': form.cleaned_data['phone_number'],
                'email': form.cleaned_data['email'],
                'message': form.cleaned_data['message'],
                'today': datetime.today(),
                'id': form.auto_id
            }
            emails = [x.email for x in AdminEmails.objects.filter(is_active=True)]
            emails.append(form.cleaned_data['email'])
            response = template.render(context, request)
            thread = threading.Thread(target=send_email_notification, args=(
                'Обратная связь | %s' % settings.SITE_TITLE,
                response,
                emails
            ))
            thread.start()

            form.save(commit=True)

            return JsonResponse(dict(
                success=True,
                message='Successfully sent the message'
            ))
        return JsonResponse(dict(
            success=False,
            message=str(form.errors)
        ))

    params.update(generate_view_params(request))
    return render(request, 'app/contacts.html', params)


def about(request):
    about_text = About.objects.first()
    params = {
        'content': about_text,
        'location': 'about'
    }
    params.update(generate_view_params(request))
    return render(request, 'app/about.html', params)


def subscribe(request):
    if request.is_ajax():
        email = request.POST.get('email')
        try:
            subscriber = Subscribers.objects.get(email=email)
            return JsonResponse(dict(
                success=True,
                message='Вы уже подписаны'
            ))
        except ObjectDoesNotExist:
            subscriber = Subscribers.objects.create(email=email)
            return JsonResponse(dict(
                success=True,
                message='Подписка оформлена'
            ))

    return JsonResponse(dict(
        success=False,
        message='Запрос должен быть отправлен AJAX\'ом'
    ))


def help(request):
    params = {
        'info': CustomPages.objects.all()
    }
    params.update(generate_view_params(request))
    return render(request, 'app/help.html', params)


def payment_information(request):
    params = {
        'location': 'payments'
    }
    params.update(generate_view_params(request))
    return render(request, 'app/custom_pages/payment-information.html', params)


def delivery_information(request):
    params = {
        'location': 'delivery',
        'info': Delivery.objects.all().order_by('id')
    }
    params.update(generate_view_params(request))
    return render(request, 'app/custom_pages/delivery-information.html', params)


def price_list(request):
    params = {
        'location': 'price-list'
    }
    params.update(generate_view_params(request))
    return render(request, 'app/custom_pages/price-list.html', params)


def service(request):
    params = {
        'location': 'service',
    }
    params.update(generate_view_params(request))
    return render(request, 'app/custom_pages/service-information.html', params)


def to_partners(request):
    form = FeedbackForm(request.POST)

    if request.POST:
        if form.is_valid():
            email = form.cleaned_data['email']
            template = loader.get_template('app/email/feedback.html')
            context = {
                'first_name': form.cleaned_data['first_name'],
                'last_name': form.cleaned_data['last_name'],
                'email': email,
                'phone': form.cleaned_data['phone_number'],
                'today': datetime.today()
            }
            emails = [x.email for x in AdminEmails.objects.filter(is_active=True)]
            emails.append(email)
            response = template.render(context, request)
            thread = threading.Thread(target=send_email_notification, args=(
                'Обратная связь | Vesta Stroy',
                response,
                emails
            ))
            thread.start()

            form.save(commit=True)

            return JsonResponse(dict(
                success=True,
                message='Successfully sent the message'
            ))
        return JsonResponse(dict(
            success=False,
            message=str(form.errors)
        ))

    params = {
        'location': 'to-partners',
        'form': form
    }
    params.update(generate_view_params(request))
    return render(request, 'app/custom_pages/to-partners-information.html', params)


def reviews(request):
    all_visible_reviews = Reviews.objects.filter(is_active=True).order_by('id')
    form = ReviewsForm(request.POST)

    if request.POST:
        if form.is_valid():
            review = form.save(commit=True)

            template = loader.get_template('app/email/review-notification.html')
            protocol = 'https://' if request.is_secure() else 'http://'
            link = protocol + request.get_host() + reverse('admin:client_app_reviews_change', args=(review.id,))
            context = {
                'link': link
            }
            mail_body = template.render(context, request)
            thread = threading.Thread(target=send_email_notification, args=(
                'Оставили отзыв на сайте | Vesta Stroy',
                mail_body,
                [x.email for x in AdminEmails.objects.filter(is_active=True)]
            ))
            thread.start()

            return JsonResponse(dict(
                success=True,
                message='Отзыв отправлен'
            ))
        return JsonResponse(dict(
            success=False,
            message='Запрос должын быть POST!'
        ))

    params = {
        'location': 'reviews',
        'form': form,
        'reviews': all_visible_reviews
    }
    params.update(generate_view_params(request))
    return render(request, 'app/custom_pages/reviews.html', params)


def simple_pages(request, slug):
    try:
        page = CustomPages.objects.get(slug=slug)
    except ObjectDoesNotExist:
        raise Http404

    params = {
        'page': page
    }
    params.update(generate_view_params(request))
    return render(request, 'app/simple-page.html', params)


def set_language(request):
    lang_code = request.GET.get('language', 'ru')

    if not lang_code:
        lang_code = request.GET.get('language', settings.LANGUAGE_CODE)
    next_url = request.META.get('HTTP_REFERER', '')
    if not is_safe_url(url=next_url, host=request.get_host()):
        next_url = reverse('client_app:index')
    response = http.HttpResponseRedirect(next_url)
    if lang_code and check_for_language(lang_code):
        if hasattr(request, 'session'):
            request.session['django_language'] = lang_code
        response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code)
        activate(lang_code)

    return response


class OrderConditionsView(TemplateView):
    template_name = "app/order.html"
    def get_context_data(self, **kwargs):
        context = super(OrderConditionsView, self).get_context_data(**kwargs)
        context["orderconditions"] = OrderConditions.objects.first()
        return context
