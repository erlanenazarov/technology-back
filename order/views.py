# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import threading

import os
import xlwt
import datetime

from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, Http404
from django.shortcuts import render
from django.template import loader
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from MyBrands import settings
from client_app.models import AdminEmails
from client_app.utils import generate_view_params, get_weight_property_object, send_email_notification
from order.forms import CheckoutForm
from order.models import Order, OrderRow
from order.utils import splice_row, splice, contains
from product.models import Product
from product.utils import set_cookie
from users.models import User
from users.utils import get_first_and_last_name

# Create your views here.


def add_to_basket(request, p_slug):
    if request.is_ajax():
        try:
            product = Product.objects.get(slug=p_slug)
        except ObjectDoesNotExist:
            return JsonResponse(dict(success=False, messagae='Товар не найден'))

        if settings.BASKET_COOKIE_NAME in request.COOKIES:
            cookie_data = json.loads(request.COOKIES[settings.BASKET_COOKIE_NAME])
        else:
            cookie_data = []

        last_item_id = cookie_data[len(cookie_data) - 1]['id'] if len(cookie_data) > 0 else 0

        count = request.POST.get('count', 1)

        already_have = False
        for _item in cookie_data:
            if _item['product_id'] == product.id:
                already_have = True
                _item['count'] = int(_item['count']) + int(count)
                break

        if not already_have:
            item = {
                'product_id': product.id,
                'count': count,
                'id': last_item_id + 1
            }
            cookie_data.append(item)

        response = JsonResponse(dict(
            success=True,
            message='Товар добавлен в корзину' if not already_have else 'Количество товара обновлено',
            is_new=not already_have
        ))
        return set_cookie(response, settings.BASKET_COOKIE_NAME, json.dumps(cookie_data))
    return JsonResponse(dict(success=False, message='Запрос должен быть POST'))


def clean_basket(request):
    response = JsonResponse(dict(
        success=True,
        message='Корзина отчищена'
    ))
    return set_cookie(response, settings.BASKET_COOKIE_NAME, json.dumps([]))


def update_quantity_of_basket_item(request, b_id):

    if settings.BASKET_COOKIE_NAME in request.COOKIES:
        cookie_data = json.loads(request.COOKIES[settings.BASKET_COOKIE_NAME])
    else:
        cookie_data = []

    response = JsonResponse(dict(
        success=False,
        message='Товар не найден'
    ))

    for i in range(0, len(cookie_data)):
        c_data = cookie_data[i]

        if int(c_data['id']) == int(b_id):
            count = request.POST.get('count')
            c_data['count'] = int(count)
            cookie_data[i] = c_data
            response = JsonResponse(dict(
                success=True,
                message='Количество товара обновлено'
            ))

    return set_cookie(response, settings.BASKET_COOKIE_NAME, json.dumps(cookie_data))


def remove_from_basket(request, b_id):
    if settings.BASKET_COOKIE_NAME in request.COOKIES:
        cookie_data = json.loads(request.COOKIES[settings.BASKET_COOKIE_NAME])
    else:
        cookie_data = []

    new_cookie_data = []
    for r in cookie_data:
        if int(r['id']) != int(b_id):
            new_cookie_data.append(r)

    response = JsonResponse(dict(success=True, message='Товар убран с корзины'))
    return set_cookie(response, settings.BASKET_COOKIE_NAME, json.dumps(new_cookie_data))


def list_basket(request):
    if settings.BASKET_COOKIE_NAME in request.COOKIES:
        cookie_data = json.loads(request.COOKIES[settings.BASKET_COOKIE_NAME])
    else:
        cookie_data = []

    prepared_objects = []
    total_price = 0
    for row in cookie_data:
        try:
            product = Product.objects.get(id=int(row['product_id']))
        except ObjectDoesNotExist:
            cookie_data = splice_row(cookie_data, row['id'])
            continue
        item = {
            'product': product,
            'count': row['count'],
            'row_price': product.get_price() * int(row['count']),
            'id': row['id']
        }
        total_price += item['row_price']
        prepared_objects.append(item)

    params = {
        'basket': prepared_objects,
        'total_price': total_price,
        'product_count': len(prepared_objects),
        'form': CheckoutForm(request.POST)
    }
    params.update(generate_view_params(request))
    return render(request, 'app/basket.html', params)


def get_total_basket_price(request):
    if settings.BASKET_COOKIE_NAME in request.COOKIES:
        cookie_data = json.loads(request.COOKIES[settings.BASKET_COOKIE_NAME])
    else:
        cookie_data = []

    total_price = 0
    total_weight = 0
    for row in cookie_data:
        try:
            product = Product.objects.get(id=int(row['product_id']))
        except ObjectDoesNotExist:
            cookie_data = splice_row(cookie_data, row['id'])
            continue
        total_price += product.get_price() * int(row['count'])

    return JsonResponse(dict(
        total_price=total_price,
        product_count=len(cookie_data)
    ))


def add_to_comparison(request, p_slug):
    try:
        product = Product.objects.get(slug=p_slug)
    except:
        raise Http404

    if settings.COMPARISON_COOKIE_NAME in request.COOKIES:
        cookie_data = json.loads(request.COOKIES[settings.COMPARISON_COOKIE_NAME])
    else:
        cookie_data = []

    if int(product.id) not in cookie_data:
        cookie_data.append(product.id)
        response = JsonResponse(dict(
            success=True,
            message='Товар добавлен в избранное',
            mode='add',
            count=len(cookie_data)
        ))
    else:
        cookie_data = [int(x) for x in cookie_data if int(x) != int(product.id)]
        response = JsonResponse(dict(
            success=True,
            message='Товар убран из избранного',
            mode='remove',
            count=len(cookie_data)
        ))

    return set_cookie(response, settings.COMPARISON_COOKIE_NAME, json.dumps(cookie_data))


def list_of_comparison(request):
    if settings.COMPARISON_COOKIE_NAME in request.COOKIES:
        cookie_data = json.loads(request.COOKIES[settings.COMPARISON_COOKIE_NAME])
    else:
        cookie_data = []

    prepared_products = list()
    comparable_properties = list()
    for item in cookie_data:
        try:
            product = Product.objects.get(id=int(item))
        except:
            cookie_data = splice(cookie_data, item)
            continue

        prepared_products.append(product)
        for property in product.property.all():
            if not contains(comparable_properties, property.key.id, field='id'):
                comparable_properties.append(property)

    params = {
        'products': prepared_products,
        'properties': comparable_properties
    }
    params.update(generate_view_params(request))
    return render(request, 'app/comparison.html', params)


def ajax_checkout(request):
    if request.POST:
        form = CheckoutForm(request.POST)
        if form.is_valid():
            if settings.BASKET_COOKIE_NAME in request.COOKIES:
                cookie_data = json.loads(request.COOKIES[settings.BASKET_COOKIE_NAME])

                if request.user.is_anonymous:
                    first_name, last_name = get_first_and_last_name(form.cleaned_data['name'])

                    try:
                        user = User.objects.get(email=form.cleaned_data['email'])
                    except ObjectDoesNotExist:
                        user = User.objects.create(
                            email=form.cleaned_data['email'],
                            phone=form.cleaned_data['phone'],
                            address=form.cleaned_data['address'],
                            first_name=first_name,
                            last_name=last_name,
                            username='%s_%s_%s' % (first_name, last_name, str(datetime.datetime.microsecond)),
                            is_active=False
                        )

                    token = default_token_generator.make_token(user)
                    uid = urlsafe_base64_encode(force_bytes(user.pk))

                    template = loader.get_template('account/partial/confirm_email.html')

                    protocol = 'https://' if request.is_secure() else 'http://'

                    contexts = {
                        'link': protocol + request.get_host() + reverse('users:users_confirm',
                                                                        kwargs={'uidb64': uid, 'token': token})
                    }

                    response = template.render(contexts, request)

                    thread = threading.Thread(
                        target=send_email_notification,
                        args=(
                            'Подтверждение почты | %s' % settings.SITE_TITLE,
                            response,
                            list(user.email)
                        )
                    )
                    thread.start()
                else:
                    user = User.objects.get(id=request.user.id)


                try:
                    order_object = Order.objects.get(email=form.cleaned_data['email'])
                except ObjectDoesNotExist:
                    order_object = Order.objects.create(
                        name=form.cleaned_data['name'],
                        email=form.cleaned_data['email'],
                        phone=form.cleaned_data['phone'],
                        address=form.cleaned_data['address'],
                        user=user
                    )

                xl_products = list()

                for item in cookie_data:
                    try:
                        product = Product.objects.get(id=int(item['product_id']))
                    except ObjectDoesNotExist:
                        cookie_data = splice_row(cookie_data, item)
                        continue

                    try:
                        order_item = OrderRow.objects.get(product=product, count=int(item['count']), order=order_object)
                    except ObjectDoesNotExist:
                        order_item = OrderRow.objects.create(
                            product=product,
                            count=int(item['count'])
                        )
                        order_object.order_items.add(order_item)

                    xl_products.append(dict(
                        product=product,
                        count=int(item['count']),
                        total=order_item.get_price()
                    ))

                order_object.save()

                wb = xlwt.Workbook(encoding='utf-8')
                ws = wb.add_sheet('Заказ')

                font_style = xlwt.XFStyle()
                font_style.font.bold = True

                ws.write(0, 0, 'Имя', font_style)
                ws.write(1, 0, 'E-Mail', font_style)
                ws.write(2, 0, 'Телефон', font_style)
                ws.write(3, 0, 'Адрес', font_style)

                row_num = 5

                columns = ['Артикул', 'Название', 'Цена', 'Количество', 'Итого']

                for col_num in range(len(columns)):
                    ws.write(row_num, col_num, columns[col_num], font_style)

                font_style = xlwt.XFStyle()

                ws.write(0, 1, order_object.name, font_style)
                ws.write(1, 1, order_object.email, font_style)
                ws.write(2, 1, order_object.phone, font_style)
                ws.write(3, 1, order_object.address, font_style)

                for i in range(len(xl_products)):
                    row_num += 1
                    ws.write(row_num, 0, xl_products[i]['product'].article, font_style)
                    ws.write(row_num, 1, xl_products[i]['product'].title, font_style)
                    ws.write(row_num, 2, xl_products[i]['product'].get_price(), font_style)
                    ws.write(row_num, 3, xl_products[i]['count'], font_style)
                    ws.write(row_num, 4, xl_products[i]['total'], font_style)

                row_num += 2

                font_style = xlwt.XFStyle()
                font_style.font.bold = True

                ws.write(row_num, 0, 'Итоговая цена', font_style)

                font_style = xlwt.XFStyle()

                ws.write(row_num, 1, order_object.get_order_full_price(), font_style)

                file_name = 'order_%s.xls' % order_object.id
                file_path = os.path.join(settings.MEDIA_ROOT, 'orders', 'excel')

                if not os.path.exists(file_path):
                    os.makedirs(file_path)

                wb.save(os.path.join(file_path, file_name))

                file = open(os.path.join(file_path, file_name), 'r')

                file_to_send = dict(
                    file_name=file_name,
                    content=file.read(),
                    type='application/ms-excel'
                )

                file.close()

                os.remove(os.path.join(file_path, file_name))

                template = loader.get_template('app/email/order.html')
                protocol = 'https://' if request.is_secure() else 'http://'
                context = {
                    'link': protocol + request.get_host() + reverse(
                        'admin:order_order_change',
                        args=(order_object.id,)
                    )
                }
                mail_body = template.render(context, request)

                thread = threading.Thread(
                    target=send_email_notification,
                    args=(
                        'Новый заказ | %s' % settings.SITE_TITLE,
                        mail_body,
                        [x.email for x in AdminEmails.objects.all()],
                        file_to_send
                    )
                )
                thread.start()

                return JsonResponse(dict(
                    success=True,
                    message='Заказ отправлен на обработку'
                ))
            return JsonResponse(dict(
                success=False,
                message='Корзина пуста!'
            ))
        return JsonResponse(dict(
            success=False,
            message='Неверно заполнены поля!',
            errors=form.errors
        ))
    return Http404


def checkout(request):
    form = CheckoutForm(request.POST)
    params = {
        'form': form
    }
    if request.POST:
        if form.is_valid():
            if settings.BASKET_COOKIE_NAME in request.COOKIES:
                cookie_data = json.loads(request.COOKIES[settings.BASKET_COOKIE_NAME])

                order_object = Order(
                    name=form.cleaned_data['name'],
                    email=form.cleaned_data['email'],
                    phone=form.cleaned_data['phone'],
                    address=form.cleaned_data['address']
                )
                order_object.save()

                total_weight = 0
                for item in cookie_data:
                    try:
                        product = Product.objects.get(id=int(item['product_id']))
                    except ObjectDoesNotExist:
                        cookie_data = splice_row(cookie_data, item['id'])
                        continue
                    total_weight += (float(product.property.filter(key_id=get_weight_property_object()).first().value) * int(item['count']))
                    order_item = OrderRow.objects.create(
                        product=product,
                        count=int(item['count'])
                    )
                    order_object.order_items.add(order_item)

                order_object.total_weight = total_weight
                order_object.save()

                template = loader.get_template('app/email/order.html')
                protocol = 'https://' if request.is_secure() else 'http://'
                context = {
                    'link': protocol + request.get_host() + reverse(
                        'admin:order_order_change',
                        args=(order_object.id,)
                    )
                }
                mail_body = template.render(context, request)

                thread = threading.Thread(
                    target=send_email_notification,
                    args=(
                        'Новый заказ | %s' % settings.SITE_TITLE,
                        mail_body,
                        [x.email for x in AdminEmails.objects.all()]
                    )
                )
                thread.start()

                params['success'] = True

            else:
                params['success'] = False
                params['error'] = 'Вы не добавили товаров в карзину'
        else:
            params['success'] = False
            params['error'] = str(form.errors)



    params.update(generate_view_params(request))
    return render(request, 'app/paypal.html', params)
