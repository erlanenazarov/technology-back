# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from .models import Order, OrderRow


# Register your models here.

class OrderAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'phone', 'address', 'is_delivered']
    readonly_fields = ['order_items', 'created_at', 'name', 'email', 'phone', 'address', 'get_product_list']
    list_filter = ['is_ordered', 'is_delivered', 'created_at']
    search_fields = ['name', 'email', 'phone', 'address']

    fieldsets = (
        (None, {
            'fields': (
                'is_ordered',
                'user',
                'created_at',
                'name',
                'email',
                'phone',
                'address',
                'get_product_list',
            )
        }),
    )

    def get_product_list(self, obj):
        result = """
        <table>
        <thead>
        <tr>
            <th>Артикул</th>
            <th>Товар</th>
            <th></th>
            <th>Цена</th>
            <th>Кол-во</th>
            <th>Итого</th>
        </tr>
        </thead>
        <tbody>
        """
        for item in obj.order_items.all():
            link = reverse('products:product_single', kwargs={'product_slug': item.product.slug})
            result += """
            <tr>
                <td>%s</td>
                <td><a href="%s" target="_blank">%s</a><td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
            </tr>
            """ % (item.product.article, link, item.product.title, item.product.get_price(), item.count, item.get_price())

        result += """
        </tbody>
        <tfoot>
        <tr>
            <td>Итого</td>
            <td colspan="5">%s сом</td>
        </tr>
        </tfoot>
        </table>
        """ % obj.get_order_full_price()

        return mark_safe(result)

    get_product_list.short_description = 'Товары'

    def has_add_permission(self, request):
        return False


admin.site.register(Order, OrderAdmin)
admin.site.register(OrderRow)